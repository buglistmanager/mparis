<?php /* Smarty version Smarty-3.1.19, created on 2017-10-02 18:25:38
         compiled from "/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/forgot-password.tpl" */ ?>
<?php /*%%SmartyHeaderCode:104955695659d268823c3108-29900813%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0251556c72f1ffe7b8ed91ccd06ba687920dea3a' => 
    array (
      0 => '/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/forgot-password.tpl',
      1 => 1506961536,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '104955695659d268823c3108-29900813',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'email' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59d268823d3801_05795766',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59d268823d3801_05795766')) {function content_59d268823d3801_05795766($_smarty_tpl) {?>

<div id="resetPassword" class="container-fluid" style="display: none;">
    <div class="row">
        <div class="text-center">
            <img src="/modules/brainify/views/img/logo.svg">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h2 class="ba-h2"><?php echo smartyTranslate(array('s'=>'Reset password','mod'=>'brainify'),$_smarty_tpl);?>
</h2>
            <div>
            <span>
                <?php echo smartyTranslate(array('s'=>'Already have an account ?','mod'=>'brainify'),$_smarty_tpl);?>

                <span class="clickableTest" id="wantToLoginBis"><?php echo smartyTranslate(array('s'=>'Log in','mod'=>'brainify'),$_smarty_tpl);?>
</span>
                <br>
            </span>
            </div>
            <br>
            <form action="" name="resetPassword" id="resetPasswordForm">
                <div class="form-group ba-form-group">
                    <label for="email-reset"><?php echo smartyTranslate(array('s'=>'Email','mod'=>'brainify'),$_smarty_tpl);?>
</label>
                    <input type="text" id="email-reset" class="form-control ba-form-control" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" required/>
                </div>

                <div>
                    <span class="ba-form-fail reset-fail"><?php echo smartyTranslate(array('s'=>'Incorrect email.','mod'=>'brainify'),$_smarty_tpl);?>
</span>
                    <button class="btn-primary ba-btn pull-right" type="submit" id="resetPasswordFormButton"><?php echo smartyTranslate(array('s'=>'OK','mod'=>'brainify'),$_smarty_tpl);?>
</button>
                </div>
            </form>
        </div>
    </div>

</div>
<?php }} ?>
