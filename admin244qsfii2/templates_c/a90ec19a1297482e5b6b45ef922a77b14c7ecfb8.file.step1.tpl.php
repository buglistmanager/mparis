<?php /* Smarty version Smarty-3.1.19, created on 2017-10-02 18:25:38
         compiled from "/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/step1.tpl" */ ?>
<?php /*%%SmartyHeaderCode:49908262359d26882363292-61207040%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a90ec19a1297482e5b6b45ef922a77b14c7ecfb8' => 
    array (
      0 => '/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/step1.tpl',
      1 => 1506961536,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '49908262359d26882363292-61207040',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59d26882377ac3_70183538',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59d26882377ac3_70183538')) {function content_59d26882377ac3_70183538($_smarty_tpl) {?>

<div class="container-fluid" id="ba-step1">
    <div class="row">
        <div class="col-md-6">
            <img style="width: 100%;" src="/modules/brainify/views/img/computer.png">
        </div>
        <div class="col-md-6">
            <div>
                <img src="/modules/brainify/views/img/logo.svg">
            </div>
            <h1><?php echo smartyTranslate(array('s'=>'Thank you for installing Brainify Analytics.','mod'=>'brainify'),$_smarty_tpl);?>
</h1>
            <p class="uppercase"><?php echo smartyTranslate(array('s'=>'Create your account to access brainify features','mod'=>'brainify'),$_smarty_tpl);?>
</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <img src="/modules/brainify/views/img/icon-dashboard.svg">
                <h2><?php echo smartyTranslate(array('s'=>'Dashboard','mod'=>'brainify'),$_smarty_tpl);?>
</h2>
                <p><?php echo smartyTranslate(array('s'=>'A simplified ecommerce performance control center, to take the pulse of your business within few seconds, and take actions.','mod'=>'brainify'),$_smarty_tpl);?>
</p>
            </div>
            <div class="row">
                <img src="/modules/brainify/views/img/icon-overview.svg">
                <h2><?php echo smartyTranslate(array('s'=>'Overviews','mod'=>'brainify'),$_smarty_tpl);?>
</h2>
                <p><?php echo smartyTranslate(array('s'=>'5 overviews to proceed a strategic audit of your ecommerce. Make instant decisions without taking care of data extracts and formatting.','mod'=>'brainify'),$_smarty_tpl);?>
</p>
            </div>
            <div class="row">
                <img src="/modules/brainify/views/img/icon-report.svg">
                <h2><?php echo smartyTranslate(array('s'=>'Reports','mod'=>'brainify'),$_smarty_tpl);?>
</h2>
                <p><?php echo smartyTranslate(array('s'=>'13 ecommerce reports for an in-depth and careful study of key themes of your ecommerce. A few clicks is enough for detailed analysis.','mod'=>'brainify'),$_smarty_tpl);?>
</p>
            </div>
        </div>
        <div class="col-md-6">

            <?php echo $_smarty_tpl->getSubTemplate ("./sign-up.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


        </div>
    </div>

</div>
<?php }} ?>
