<?php /* Smarty version Smarty-3.1.19, created on 2017-10-02 18:25:38
         compiled from "/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:135663719659d268823aa804-41375112%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b60624922ff8fc3cd3d4b833aa61a4c8a833cc1' => 
    array (
      0 => '/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/login.tpl',
      1 => 1506961536,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '135663719659d268823aa804-41375112',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'brainifyEmail' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59d268823c0951_54142902',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59d268823c0951_54142902')) {function content_59d268823c0951_54142902($_smarty_tpl) {?>

<div id="login" class="container-fluid" style="display: none;">
    <div class="row">
        <div class="text-center">
            <img src="/modules/brainify/views/img/logo.svg">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
            <h2 class="ba-h2"><?php echo smartyTranslate(array('s'=>'Login','mod'=>'brainify'),$_smarty_tpl);?>
</h2>

            <div>
            <span>
                <?php echo smartyTranslate(array('s'=>'Don\'t have an account yet ?','mod'=>'brainify'),$_smarty_tpl);?>

                <span class="clickableTest" id="wantToRegister"><?php echo smartyTranslate(array('s'=>'Register','mod'=>'brainify'),$_smarty_tpl);?>
</span>
                <br>
            </span>
            <span>
                <?php echo smartyTranslate(array('s'=>'Forgot password ?','mod'=>'brainify'),$_smarty_tpl);?>

                <span id="wantToReset" class="clickableTest"><?php echo smartyTranslate(array('s'=>'Reset','mod'=>'brainify'),$_smarty_tpl);?>
</span>
                <br>
            </span>
            </div>
            <br>

            <form action="" id="loginForm" name="loginFormName">
                <div class="form-group ba-form-group">
                    <label for="email"><?php echo smartyTranslate(array('s'=>'Email','mod'=>'brainify'),$_smarty_tpl);?>
</label>
                    <input class="form-control ba-form-control" type="text" id="email" required value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['brainifyEmail']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
                </div>

                <div class="form-group ba-form-group">
                    <label for="password"><?php echo smartyTranslate(array('s'=>'Password','mod'=>'brainify'),$_smarty_tpl);?>
</label>
                    <input class="form-control ba-form-control" required type="password" id="password"/>
                </div>

                <div>
                    <span class="ba-form-fail login-fail"><?php echo smartyTranslate(array('s'=>'Incorrect credentials.','mod'=>'brainify'),$_smarty_tpl);?>
</span>
                    <button class="ba-btn btn-primary pull-right" type="submit" id="loginFormButton">
                        <?php echo smartyTranslate(array('s'=>'Log in','mod'=>'brainify'),$_smarty_tpl);?>

                    </button>
                </div>
            </form>
        </div>
    </div>

</div>
<?php }} ?>
