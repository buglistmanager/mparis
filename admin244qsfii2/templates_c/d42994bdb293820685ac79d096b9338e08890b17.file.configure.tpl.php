<?php /* Smarty version Smarty-3.1.19, created on 2017-10-02 18:25:38
         compiled from "/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82969385859d2688228c908-01672332%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd42994bdb293820685ac79d096b9338e08890b17' => 
    array (
      0 => '/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure.tpl',
      1 => 1506961536,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82969385859d2688228c908-01672332',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'isOnboarded' => 0,
    'userStatus' => 0,
    'isLogged' => 0,
    'confirmUrl' => 0,
    'loginUrl' => 0,
    'getActivitiesUrl' => 0,
    'signupUrl' => 0,
    'resetPasswordUrl' => 0,
    'onboardSiteUrl' => 0,
    'getShopsUrl' => 0,
    'getUserSitesUrl' => 0,
    'keysUrl' => 0,
    'logoutUrl' => 0,
    'resetUrl' => 0,
    'resendConfirmationEmail' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59d2688235fd31_30001707',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59d2688235fd31_30001707')) {function content_59d2688235fd31_30001707($_smarty_tpl) {?>

<div>
    <?php if ($_smarty_tpl->tpl_vars['isOnboarded']->value&&($_smarty_tpl->tpl_vars['userStatus']->value=='confirmed'||$_smarty_tpl->tpl_vars['userStatus']->value=='initialized')) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("./configure/final-step.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php } elseif ($_smarty_tpl->tpl_vars['isLogged']->value&&$_smarty_tpl->tpl_vars['userStatus']->value=='confirmation') {?>
        <?php echo $_smarty_tpl->getSubTemplate ("./configure/confirmEmail.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php } elseif ($_smarty_tpl->tpl_vars['isLogged']->value&&($_smarty_tpl->tpl_vars['userStatus']->value=='confirmed'||$_smarty_tpl->tpl_vars['userStatus']->value=='initialized')) {?>
        <?php echo $_smarty_tpl->getSubTemplate ("./configure/step2.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php } else { ?>
        <?php echo $_smarty_tpl->getSubTemplate ("./configure/step1.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php }?>

    <?php echo $_smarty_tpl->getSubTemplate ("./configure/login.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php echo $_smarty_tpl->getSubTemplate ("./configure/forgot-password.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


    
        
    
</div>

<script type="text/javascript">
    var brainifyConfirmUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['confirmUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyLoginUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['loginUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyGetActivitiesUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['getActivitiesUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifySignupUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['signupUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyResetPasswordUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['resetPasswordUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyOnboardSiteUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['onboardSiteUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyGetShopsUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['getShopsUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyGetUserSitesUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['getUserSitesUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyKeysUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['keysUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyLogoutUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['logoutUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyResetUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['resetUrl']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
    var brainifyResendConfirmationEmailUrl = "<?php echo strtr($_smarty_tpl->tpl_vars['resendConfirmationEmail']->value, array("\\" => "\\\\", "'" => "\\'", "\"" => "\\\"", "\r" => "\\r", "\n" => "\\n", "</" => "<\/" ));?>
";
</script>
<?php }} ?>
