<?php /* Smarty version Smarty-3.1.19, created on 2017-10-02 18:25:38
         compiled from "/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/sign-up.tpl" */ ?>
<?php /*%%SmartyHeaderCode:98063370759d2688237a0a8-35568089%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5e1a8a307fc32bddec02fd2b8202f4d4c891c2e4' => 
    array (
      0 => '/home/mparis/sites/prod/www/modules/brainify/views/templates/admin/configure/sign-up.tpl',
      1 => 1506961536,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '98063370759d2688237a0a8-35568089',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'email' => 0,
    'userName' => 0,
    'lang' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59d268823a7894_21520958',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59d268823a7894_21520958')) {function content_59d268823a7894_21520958($_smarty_tpl) {?>

<div id="signup">
    <h3><?php echo smartyTranslate(array('s'=>'Register','mod'=>'brainify'),$_smarty_tpl);?>
</h3>

    <div>
        <span>
            <?php echo smartyTranslate(array('s'=>'Already have an account ?','mod'=>'brainify'),$_smarty_tpl);?>

            <span class="clickableTest" id="wantToLogin"><?php echo smartyTranslate(array('s'=>'Log in','mod'=>'brainify'),$_smarty_tpl);?>
</span>
            <br>
        </span>
    </div>
    <br>
    <div style="display: none;" class="ba-form-fail" id="signupCommonError"><?php echo smartyTranslate(array('s'=>'An error has occured please try in few minutes','mod'=>'brainify'),$_smarty_tpl);?>
</div>
    <div style="display: none;" class="ba-form-fail" id="signupUserExists"><?php echo smartyTranslate(array('s'=>'User already exists','mod'=>'brainify'),$_smarty_tpl);?>
</div>
    <form action="" id="signupForm">
        <div class="form-group ba-form-group">
            <label for="email"><?php echo smartyTranslate(array('s'=>'Email','mod'=>'brainify'),$_smarty_tpl);?>
</label>
            <input class="form-control ba-form-control" required type="text" id="email" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>
        </div>
        <div class="form-group ba-form-group">
            <label for="name"><?php echo smartyTranslate(array('s'=>'Name','mod'=>'brainify'),$_smarty_tpl);?>
</label>
            <input class="form-control ba-form-control" required type="text" id="name" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['userName']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
"/>

        </div>
        <div class="form-group ba-form-group">
            <label for="password"><?php echo smartyTranslate(array('s'=>'Password','mod'=>'brainify'),$_smarty_tpl);?>
</label>
            <input class="form-control ba-form-control" required pattern=".{6,20}" type="password" id="password"/>

        </div>
        <div class="form-group ba-form-group">
            <label for="password2"><?php echo smartyTranslate(array('s'=>'Password confirmation','mod'=>'brainify'),$_smarty_tpl);?>
</label>
            <input class="form-control ba-form-control" required pattern=".{6,20}" type="password"
                   id="password2"/>
        </div>

        <div class="form-group ba-form-group">
            <label for="language"><?php echo smartyTranslate(array('s'=>'Language','mod'=>'brainify'),$_smarty_tpl);?>
</label>
            <div class="ba-select">
                <select name="language" id="language" required>
                    <option value="fr_FR" <?php if ($_smarty_tpl->tpl_vars['lang']->value=="fr") {?> selected <?php }?>>Français</option>
                    <option value="en_EN" <?php if ($_smarty_tpl->tpl_vars['lang']->value!="fr") {?> selected <?php }?>>English</option>
                </select>
                <img class="icon" src="/modules/brainify/views/img/arrow.svg">
            </div>
        </div>

        <div class="form-group ba-form-group">
            <label for="company"><?php echo smartyTranslate(array('s'=>'Company name','mod'=>'brainify'),$_smarty_tpl);?>
</label>
            <input class="form-control ba-form-control" required type="text" id="company" value=""/>
        </div>

        <div class="form-group ba-form-group">
            <label for="cgu">
                <input type="checkbox" id="cgu" name="cgu" required>
                <?php echo smartyTranslate(array('s'=>'I agree to the Terms of Service.','mod'=>'brainify'),$_smarty_tpl);?>

                <a href="http://www.brainify.it/cgu.html" target="_blank"><?php echo smartyTranslate(array('s'=>'Read the ToS','mod'=>'brainify'),$_smarty_tpl);?>
</a>
            </label>
        </div>

        <div>
            <button class="btn-primary ba-btn" type="submit" id="signupFormButton"><?php echo smartyTranslate(array('s'=>'Register','mod'=>'brainify'),$_smarty_tpl);?>
</button>
        </div>
    </form>

</div>
<?php }} ?>
