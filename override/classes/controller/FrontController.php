<?php 

	class FrontController extends FrontControllerCore
	{
		public function initContent()
		{
			$this->process();

			if (!isset($this->context->cart))
				$this->context->cart = new Cart();

			if (!$this->useMobileTheme())
			{
				// These hooks aren't used for the mobile theme.
				// Needed hooks are called in the tpl files.
				$this->context->smarty->assign(array(
					'HOOK_HEADER'       => Hook::exec('displayHeader'),
					'HOOK_TOP'          => Hook::exec('displayTop'),
					'HOOK_LEFT_COLUMN'  => ($this->display_column_left  ? Hook::exec('displayLeftColumn') : ''),
					'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),
				));
			}
			else
				$this->context->smarty->assign('HOOK_MOBILE_HEADER', Hook::exec('displayMobileHeader'));

			$id_shop = $this->context->shop->id;
			$id_lang = $this->context->language->id;

			switch($id_lang){
				case '2':
						$this->context->smarty->assign('VAR_TXT_INFORMATION', 'Information');
						$this->context->smarty->assign('VAR_TXT_FOLLOW_US', 'Follow us');
						$this->context->smarty->assign('VAR_TXT_NEWSLETTER', 'Newsletter');
                        $this->context->smarty->assign('VAR_TXT_CONTACT', 'Contact');
					break;
										
				default :
						$this->context->smarty->assign('VAR_TXT_INFORMATION', 'Informations');
						$this->context->smarty->assign('VAR_TXT_FOLLOW_US', 'Nous suivre');
						$this->context->smarty->assign('VAR_TXT_NEWSLETTER', 'Newsletter');
                        $this->context->smarty->assign('VAR_TXT_CONTACT', 'Contact');
			}
		}
	
	}