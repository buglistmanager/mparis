<?php
/**
 * GIFT CARD
 *
 *    @category pricing_promotion
 *    @author    Timactive - Romain DE VERA <support@timactive.com>
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         GIFT CARD                *
 **          V 1.0.0                 *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 */
class Product extends ProductCore {
	
	/*
	* module: giftcard
	* date: 2015-08-05 10:49:46
	* version: 1.0.12
	*/
	public static function getPriceStatic($id_product, $usetax = true, $id_product_attribute = null, $decimals = 6,
			$divisor = null, $only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false,
			$id_customer = null, $id_cart = null, $id_address = null, &$specific_price_output = null, $with_ecotax = true,
			$use_group_reduction = true, Context $context = null, $use_customer_price = true)
	{
		$giftcard = Module::getInstanceByName('giftcard');
		if ($giftcard && $giftcard->active && (int)$id_product > 0 && $giftcard->isGiftCard($id_product))
			return ((float)GiftCardProduct::getAmount($id_product));
		return (parent::getPriceStatic($id_product, $usetax, $id_product_attribute, $decimals, $divisor, $only_reduc, $usereduc, $quantity, $force_associated_tax, $id_customer, $id_cart, $id_address, $specific_price_output, $with_ecotax, $use_group_reduction, $context, $use_customer_price));
	}
}
