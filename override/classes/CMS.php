<?php 


class CMS extends CMSCore{
	
	public function do_shortcode(){
		$shop_url = Tools::getHttpHost(true).__PS_BASE_URI__;
		if(preg_match('/\[\[(?P<name>\w+) value=(?P<file>\w.+)\]\]/', $this -> content, $matches)){
			while(preg_match('/\[\[(?P<name>\w+) value=(?P<file>\w.+)\]\]/', $this -> content, $matches)){
				switch($matches['name']){
					case 'video':
							$blocvideo = '';
							$videos = explode(',',$matches['file']);
							foreach($videos AS $video){
								if(trim($video) != ''){
									$blocvideo .= '<video style="width:57.4025%; height: 321px" autoplay loop>
																	<source src="'.$shop_url.'videos/'.$video.'.mp4" type="video/mp4">
																	<source src="'.$shop_url.'videos/'.$video.'.ogg" type="video/ogg">
																	<source src="'.$shop_url.'videos/'.$video.'.webm" type="video/webm">
																	Your browser does not support the video tag.
																</video> ';
								}
							}
							$this -> content = preg_replace('/\[\[(?P<name>\w+) value=(?P<file>\w.+)\]\]/', $blocvideo, $this -> content);
						break;
					case 'BREAK':
							$this -> content = preg_replace('/\[\[(?P<name>\w+) value=(?P<file>\w.+)\]\]/', '<div class="break '.$matches['file'].'"></div>', $this -> content);
						break;
				}
			}
		}
	}
	
}
