<?php
/**
 * GIFT CARD
 *
 *    @category pricing_promotion
 *    @author    Timactive - Romain DE VERA <support@timactive.com>
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         GIFT CARD                *
 **          V 1.0.0                 *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 */
class Link extends LinkCore {
	
	/*
	* module: giftcard
	* date: 2015-08-05 10:49:45
	* version: 1.0.12
	*/
	public function getProductLink($product, $alias = null, $category = null, $ean13 = null, $id_lang = null, $id_shop = null, $ipa = 0, $force_routes = false, $relative_protocol = false)
	{
		$giftcard = Module::getInstanceByName ( 'giftcard' );
		if ($giftcard && $giftcard->active)
		{
			if (! is_object ( $product ))
			{
				if (is_array ( $product ) && isset ( $product['id_product'] ))
					$id_product = $product['id_product'];
				elseif ((int)$product)
					$id_product = (int)$product;
			}
			else
				$id_product = $product->id;
			if ((int)$id_product > 0 && $giftcard->isGiftCard ( $id_product ))
			{
				$params = array ();
				$params['id_product'] = $id_product;
				return ($this->getModuleLink ( 'giftcard', 'choicegiftcard', $params ));
			}
		}
		if (version_compare(_PS_VERSION_, '1.6.0.10', '<') === true)
			return (parent::getProductLink ( $product, $alias, $category, $ean13, $id_lang, $id_shop, $ipa, $force_routes ));
		return (parent::getProductLink ( $product, $alias, $category, $ean13, $id_lang, $id_shop, $ipa, $force_routes, $relative_protocol ));
			
	}
}
