<?php
/**
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*/

if (!defined('_PS_VERSION_'))
	exit;

/**
 * Extend logger class to implement logging disable and avoid to check before every log operation
 */
class SystempayFileLogger extends FileLogger
{
	protected $logs_enabled = false;

	public function __construct($logs_enabled, $level = self::INFO)
	{
		$this->logs_enabled = $logs_enabled;
		parent::__construct($level);
	}

	/**
	 * log message only if logs are enabled
	 *
	 * @param string message
	 * @param level
	 */
	public function log($message, $level = self::DEBUG)
	{
		if (!$this->logs_enabled)
			return;

		parent::log($message, $level);
	}
}