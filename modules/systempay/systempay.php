<?php
/**
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*/

if (!defined('_PS_VERSION_'))
	exit;

require_once dirname(__FILE__).'/classes/SystempayApi.php';
require_once dirname(__FILE__).'/classes/SystempayFileLogger.php';
require_once dirname(__FILE__).'/classes/admin/SystempayAdminDisplay.php';

/**
 * Systempay payment module main class.
 */
class Systempay extends PaymentModule
{
	const ON_FAILURE_RETRY = 'retry';
	const ON_FAILURE_SAVE = 'save';

	const TNT_RELAY_POINT_PREFIX = 'JD';

	/* regular expressions */
	const ORDER_ID_REGEX = '#^[a-zA-Z0-9]{1,9}$#';
	const CUST_ID_REGEX = '#^[a-zA-Z0-9]{1,8}$#';
	const PRODUCT_REF_REGEX = '#^[a-zA-Z0-9]{1,64}$#';
	const PRODUCT_LABEL_REGEX = '#^[A-Z0-9ÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ ]{1,255}$#ui';
	const DELIVERY_COMPANY_REGEX = '#^[A-Z0-9ÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ /\'-]{1,127}$#ui';

	/* fields lists */
	private $multi_lang_fields = array('SYSTEMPAY_STD_TITLE', 'SYSTEMPAY_MULTI_TITLE', 'SYSTEMPAY_ONEY_TITLE', 'SYSTEMPAY_ANCV_TITLE', 'SYSTEMPAY_SEPA_TITLE', 'SYSTEMPAY_SOFORT_TITLE' );
	private $amount_fields = array(
			'SYSTEMPAY_3DS_MIN_AMOUNT',
			'SYSTEMPAY_STD_AMOUNT_MIN', 'SYSTEMPAY_STD_AMOUNT_MAX',
			'SYSTEMPAY_MULTI_AMOUNT_MIN', 'SYSTEMPAY_MULTI_AMOUNT_MAX',
			'SYSTEMPAY_ANCV_AMOUNT_MIN', 'SYSTEMPAY_ANCV_AMOUNT_MAX',
			'SYSTEMPAY_SEPA_AMOUNT_MIN', 'SYSTEMPAY_SEPA_AMOUNT_MAX',
			'SYSTEMPAY_SOFORT_AMOUNT_MIN', 'SYSTEMPAY_SOFORT_AMOUNT_MAX'
	);

	/* module logger */
	public $logger = null;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->name = 'systempay';
		$this->tab = 'payments_gateways';
		$this->version = '1.5.0';
		$this->author = 'Lyra Network';
		$this->controllers = array('redirect', 'submit');
		$this->module_key = '7ead36be0617127d7cd39e0a72fc811b';

		// check version compatibility
		$minor = Tools::substr(_PS_VERSION_, strrpos(_PS_VERSION_, '.') + 1);
		$replace = (int)$minor + 1;
		$version = substr_replace(_PS_VERSION_, (string)$replace, Tools::strlen(_PS_VERSION_) - Tools::strlen($minor));
		$this->ps_versions_compliancy = array('min' => '1.5.0.0', 'max' => $version);

		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		parent::__construct();

		$order_id = (int)Tools::getValue('id_order', 0);
		$order = new Order($order_id);
		if (($order->module == $this->name) && (Tools::getValue('controller', '') == 'orderconfirmation'))
			// patch to use different display name according to the used payment mode
			$this->displayName = $order->payment;
		else
			$this->displayName = 'Systempay';

		$this->description = $this->l('Accept payments by credit cards');
		$this->confirmUninstall = $this->l('Are you sure you want to delete your module details ?');

		$this->logger = new SystempayFileLogger(Configuration::get('SYSTEMPAY_ENABLE_LOGS') != 'False');
		$this->logger->setFilename(_PS_ROOT_DIR_.'/log/'.date('Y_m').'_systempay.log');
	}

	/**
	 * Return the list of configuration parameters with their systempay names and default values.
	 *
	 * @return array[array[key, name, default]]
	 */
	private function getAdminParameters()
	{
		// NB : keys are 32 chars max

		return array(
				array('key' => 'SYSTEMPAY_ENABLE_LOGS', 'default' => 'True', 'label' => 'Logs'),

				array('key' => 'SYSTEMPAY_SITE_ID', 'name' => 'site_id', 'default' => '12345678', 'label' => 'Site ID'),
				array('key' => 'SYSTEMPAY_KEY_TEST', 'name' => 'key_test', 'default' => '1111111111111111', 'label' => 'Certificate in test mode'),
				array('key' => 'SYSTEMPAY_KEY_PROD', 'name' => 'key_prod', 'default' => '2222222222222222', 'label' => 'Certificate in production mode'),
				array('key' => 'SYSTEMPAY_MODE', 'name' => 'ctx_mode', 'default' => 'TEST', 'label' => 'Mode'),
				array('key' => 'SYSTEMPAY_PLATFORM_URL', 'name' => 'platform_url', 'default' => 'https://paiement.systempay.fr/vads-payment/',
					'label' => 'Payment page URL'),

				array('key' => 'SYSTEMPAY_DEFAULT_LANGUAGE', 'default' => 'fr', 'label' => 'Default language'),
				array('key' => 'SYSTEMPAY_AVAILABLE_LANGUAGES', 'name' => 'available_languages', 'default' => '', 'label' => 'Available languages'),
				array('key' => 'SYSTEMPAY_DELAY', 'name' => 'capture_delay', 'default' => '', 'label' => 'Capture delay'),
				array('key' => 'SYSTEMPAY_VALIDATION_MODE', 'name' => 'validation_mode', 'default' => '', 'label' => 'Payment validation'),
				array('key' => 'SYSTEMPAY_PAYMENT_CARDS', 'name' => 'payment_cards', 'default' => '', 'label' => 'Card Types'),
				array('key' => 'SYSTEMPAY_COMMON_CATEGORY', 'default' => 'FOOD_AND_GROCERY', 'label' => 'Category mapping'),
				array('key' => 'SYSTEMPAY_CATEGORY_MAPPING', 'default' => array(), 'label' => 'Category mapping'),

				array('key' => 'SYSTEMPAY_THEME_CONFIG', 'name' => 'theme_config', 'default' => '', 'label' => 'Theme configuration'),
				array('key' => 'SYSTEMPAY_SHOP_NAME', 'name' => 'shop_name', 'default' => '', 'label' => 'Shop name'),
				array('key' => 'SYSTEMPAY_SHOP_URL', 'name' => 'shop_url', 'default' => '', 'label' => 'Shop URL'),

				array('key' => 'SYSTEMPAY_3DS_MIN_AMOUNT', 'default' => '', 'label' => 'Minimum amount to activate 3-DS'),

				array('key' => 'SYSTEMPAY_REDIRECT_ENABLED', 'name' => 'redirect_enabled', 'default' => 'False', 'label' => 'Automatic redirection'),
				array('key' => 'SYSTEMPAY_REDIRECT_SUCCESS_T', 'name' => 'redirect_success_timeout', 'default' => '5',
					'label' => 'Redirection timeout on success'),
				array('key' => 'SYSTEMPAY_REDIRECT_SUCCESS_M', 'name' => 'redirect_success_message',
					'default' => 'Redirection vers la boutique dans quelques instants...', 'label' => 'Redirection message on success'),
				array('key' => 'SYSTEMPAY_REDIRECT_ERROR_T', 'name' => 'redirect_error_timeout', 'default' => '5', 'label' => 'Redirection timeout on failure'),
				array('key' => 'SYSTEMPAY_REDIRECT_ERROR_M', 'name' => 'redirect_error_message',
					'default' => 'Redirection vers la boutique dans quelques instants...', 'label' => 'Redirection message on failure'),
				array('key' => 'SYSTEMPAY_RETURN_MODE', 'name' => 'return_mode', 'default' => 'GET', 'label' => 'Return mode'),
				array('key' => 'SYSTEMPAY_FAILURE_MANAGEMENT', 'default' => self::ON_FAILURE_RETRY, 'label' => 'Payment failed management'),

				array('key' => 'SYSTEMPAY_ONEY_CONTRACT', 'default' => '0', 'label' => 'FacilyPay Oney contract'),
				array('key' => 'SYSTEMPAY_ONEY_SHIP_OPTIONS', 'default' => array(), 'label' => 'Shipping options'),

				array('key' => 'SYSTEMPAY_STD_TITLE',
					'default' => array('en' => 'Payment by bank card', 'fr' => 'Paiement par carte bancaire'),
					'label' => 'Method title'),
				array('key' => 'SYSTEMPAY_STD_ENABLED', 'default' => 'True', 'label' => 'Activation'),
				array('key' => 'SYSTEMPAY_STD_AMOUNT_MIN', 'default' => '', 'label' => 'Minimum amount'),
				array('key' => 'SYSTEMPAY_STD_AMOUNT_MAX', 'default' => '', 'label' => 'Maximum amount'),
				array('key' => 'SYSTEMPAY_STD_CARD_DATA_MODE', 'default' => '1', 'label' => 'Card data entry mode'),

				array('key' => 'SYSTEMPAY_MULTI_TITLE',
					'default' => array('en' => 'Payment by bank card in several times', 'fr' => 'Paiement par carte bancaire en plusieurs fois'),
					'label' => 'Method title'),
				array('key' => 'SYSTEMPAY_MULTI_ENABLED', 'default' => 'False', 'label' => 'Activation'),
				array('key' => 'SYSTEMPAY_MULTI_AMOUNT_MIN', 'default' => '', 'label' => 'Minimum amount'),
				array('key' => 'SYSTEMPAY_MULTI_AMOUNT_MAX', 'default' => '', 'label' => 'Maximum amount'),
				array('key' => 'SYSTEMPAY_MULTI_OPTIONS', 'default' => array(), 'label' => 'Payment options'),

				array('key' => 'SYSTEMPAY_ONEY_TITLE',
					'default' => array('en' => 'Payment with FacilyPay Oney', 'fr' => 'Paiement avec FacilyPay Oney'),
					'label' => 'Method title'),
				array('key' => 'SYSTEMPAY_ONEY_ENABLED', 'default' => 'False', 'label' => 'Activation'),
				array('key' => 'SYSTEMPAY_ONEY_AMOUNT_MIN', 'default' => '', 'label' => 'Minimum amount'),
				array('key' => 'SYSTEMPAY_ONEY_AMOUNT_MAX', 'default' => '', 'label' => 'Maximum amount'),

				array('key' => 'SYSTEMPAY_ANCV_TITLE',
					'default' => array('en' => 'Payment with ANCV', 'fr' => 'Paiement avec ANCV'),
					'label' => 'Method title'),
				array('key' => 'SYSTEMPAY_ANCV_ENABLED', 'default' => 'False', 'label' => 'Activation'),
				array('key' => 'SYSTEMPAY_ANCV_AMOUNT_MIN', 'default' => '', 'label' => 'Minimum amount'),
				array('key' => 'SYSTEMPAY_ANCV_AMOUNT_MAX', 'default' => '', 'label' => 'Maximum amount'),

				array('key' => 'SYSTEMPAY_SEPA_TITLE',
					'default' => array('en' => 'Payment with SEPA', 'fr' => 'Paiement avec SEPA'),
					'label' => 'Method title'),
				array('key' => 'SYSTEMPAY_SEPA_ENABLED', 'default' => 'False', 'label' => 'Activation'),
				array('key' => 'SYSTEMPAY_SEPA_AMOUNT_MIN', 'default' => '', 'label' => 'Minimum amount'),
				array('key' => 'SYSTEMPAY_SEPA_AMOUNT_MAX', 'default' => '', 'label' => 'Maximum amount'),

				array('key' => 'SYSTEMPAY_SOFORT_TITLE',
					'default' => array('en' => 'Payment with SOFORT Banking', 'fr' => 'Paiement avec SOFORT Banking'),
					'label' => 'Method title'),
					array('key' => 'SYSTEMPAY_SOFORT_ENABLED', 'default' => 'False', 'label' => 'Activation'),
					array('key' => 'SYSTEMPAY_SOFORT_AMOUNT_MIN', 'default' => '', 'label' => 'Minimum amount'),
					array('key' => 'SYSTEMPAY_SOFORT_AMOUNT_MAX', 'default' => '', 'label' => 'Maximum amount')
		);
	}

	/**
	 * @see PaymentModuleCore::install()
	 */
	public function install()
	{
		if (version_compare(_PS_VERSION_, '1.5', '<'))
			// incompatible version of PrestaShop
			return false;

		if (!parent::install() || !$this->registerHook('header') || !$this->registerHook('payment')
			|| !$this->registerHook('paymentReturn') || !$this->registerHook('displayShoppingCart'))
			return false;

		foreach ($this->getAdminParameters() as $param)
		{
			if (in_array($param['key'], $this->multi_lang_fields))
			{
				// multilingual field, use prestashop IDs as keys
				$default = array();

				foreach (Language::getLanguages(false) as $language)
					$default[$language['id_lang']] = key_exists($language['iso_code'], $param['default']) ? $param['default'][$language['iso_code']] : '';
			}
			else
				$default = $param['default'];

			if (!Configuration::updateValue($param['key'], $default, false, false, false))
				return false;
		}

		###BEGIN_ONEY_CODE###
		if (Configuration::get('SYSTEMPAY_ONEY_PENDING'))
		{
			// rename oney status
			Configuration::updateValue('SYSTEMPAY_OS_ONEY_PENDING', Configuration::get('SYSTEMPAY_ONEY_PENDING'));
			Configuration::deleteByName('SYSTEMPAY_ONEY_PENDING');
		}

		// Oney payment pending confirmation order status
		if (!Configuration::get('SYSTEMPAY_OS_ONEY_PENDING'))
		{
			// create a pending order status
			$lang = array (
					'en' => 'Funding request in progress',
					'fr' => 'Demande de financement en cours',
			);

			$name = array();
			foreach (Language::getLanguages(true) as $language)
				$name[$language['id_lang']] = key_exists($language['iso_code'], $lang) ? $lang[$language['iso_code']] : '';

			$oney_state = new OrderState();
			$oney_state->name = $name;
			$oney_state->invoice = false;
			$oney_state->send_email = false;
			$oney_state->module_name = $this->name;
			$oney_state->color = '#FF8C00';
			$oney_state->unremovable = true;
			$oney_state->hidden = false;
			$oney_state->logable = false;
			$oney_state->delivery = false;
			$oney_state->shipped = false;
			$oney_state->paid = false;

			if (!$oney_state->save() || !Configuration::updateValue('SYSTEMPAY_OS_ONEY_PENDING', $oney_state->id))
				return false;

			// add small icon to status
			@copy(_PS_MODULE_DIR_.'systempay/views/img/os_oney.gif', _PS_IMG_DIR_.'os/'.Configuration::get('SYSTEMPAY_OS_ONEY_PENDING').'.gif');
		}

		if (!Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK'))
		{
			// create a pending order status
			$lang = array (
					'en' => 'Payment accepted on backorder',
					'fr' => 'Paiement accepté en attente de réapprovisionnement',
			);

			$name = array();
			foreach (Language::getLanguages(true) as $language)
				$name[$language['id_lang']] = key_exists($language['iso_code'], $lang) ? $lang[$language['iso_code']] : '';

			$oos_state = new OrderState();
			$oos_state->name = $name;
			$oos_state->invoice = true;
			$oos_state->send_email = true;
			$oos_state->module_name = $this->name;
			$oos_state->color = 'HotPink';
			$oos_state->unremovable = true;
			$oos_state->hidden = false;
			$oos_state->logable = false;
			$oos_state->delivery = false;
			$oos_state->shipped = false;
			$oos_state->paid = true;
			$oos_state->template = 'outofstock';

			if (!$oos_state->save() || !Configuration::updateValue('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK', $oos_state->id))
				return false;

			// add small icon to status
			@copy(_PS_MODULE_DIR_.'systempay/views/img/os_oos.gif', _PS_IMG_DIR_.'os/'.Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK').'.gif');
		}
		###END_ONEY_CODE###

		// clear module compiled templates
		$tpls = array(
				'payment_errors', 'payment_ancv', 'payment_multi', 'payment_oney',
				'payment_return', 'payment_sepa', 'payment_sofort', 'payment_std', 'redirect'
		);
		foreach ($tpls as $tpl)
			$this->context->smarty->clearCompiledTemplate($this->getTemplatePath($tpl.'.tpl'));

		return true;
	}

	/**
	 * @see PaymentModuleCore::uninstall()
	 */
	public function uninstall()
	{
		$result = true;
		foreach ($this->getAdminParameters() as $param)
			$result &= Configuration::deleteByName($param['key']);

		// delete all obsolete systempay params but custom order states
		$result &= Db::getInstance()->execute(
				'DELETE FROM `'._DB_PREFIX_."configuration` WHERE `name` LIKE 'SYSTEMPAY_%' AND `name` NOT LIKE 'SYSTEMPAY_OS_%'");

		return $result && parent::uninstall();
	}

	/**
	 * Admin form management
	 * @return string
	 */
	public function getContent()
	{
		$msg = '';

		if (Tools::isSubmit('systempay_submit_admin_form'))
		{
			$this->postProcess();

			if (!count($this->_errors))
				// no error, display update ok message
				$msg .= $this->displayConfirmation($this->l('Settings updated.'));
			else
				// display errors
				$msg .= $this->displayError(implode('<br />', $this->_errors));

			$msg .= '<br />';
		}

		return $msg.$this->renderForm();
	}

	/**
	 * Validate and save module admin parameters
	 */
	private function postProcess()
	{
		$request = new SystempayRequest(); // new instance of SystempayRequest for parameters validation

		// load and validate from request
		foreach ($this->getAdminParameters() as $param)
		{
			$key = $param['key'];
			$label = $this->l($param['label'], 'systempayadmindisplay', null);
			$value = Tools::getValue($key, null);
			if ($value === '') // consider empty strings as null
				$value = null;

			if (in_array($key, $this->multi_lang_fields))
			{
				$value = array();

				foreach (Language::getLanguages(false) as $language)
					$value[$language['id_lang']] = Tools::getValue($key.'_'.$language['id_lang'], '');
			}
			elseif ($key === 'SYSTEMPAY_MULTI_OPTIONS')
			{
				if (!is_array($value) || empty($value))
					$value = array();
				else
				{
					$error = false;
					foreach ($value as $id => $option)
					{
						if (!$option['label']
								|| !is_numeric($option['count'])
								|| !is_numeric($option['period'])
								|| ($option['first'] && (!is_numeric($option['first']) || $option['first'] < 0 || $option['first'] > 100)))
						{
							unset($value[$id]); // error, do not save this option
							$error = true;
						}
					}

					if ($error)
						$this->_errors[] = $this->l('One or more values are invalid for field «Payment options». Only valid lines are saved.');
				}
				$value = serialize($value);
			}
			elseif ($key === 'SYSTEMPAY_AVAILABLE_LANGUAGES')
				$value = (is_array($value) && count($value) > 0) ? implode(';', $value) : '';
			elseif ($key === 'SYSTEMPAY_PAYMENT_CARDS')
			{
				if (!is_array($value) || in_array('', $value))
					$value = array();

				if (Tools::getValue('SYSTEMPAY_ONEY_CONTRACT', null))
				{
					$oney_card = !count($value) /* ALL */ || in_array('ONEY', $value) || in_array('ONEY_SANDBOX', $value);
					if ($oney_card && Configuration::get('PS_ALLOW_MULTISHIPPING'))
					{
						$this->_errors[] = $this->l('Multishipping is activated. FacilyPay Oney payment cannot be used.');
						$value = array_diff($value, array('ONEY', 'ONEY_SANDBOX'));
					}
				}
				else
					// no Oney contract, let's unselect Oney card types
					$value = array_diff($value, array('ONEY', 'ONEY_SANDBOX'));

				$value = implode(';', $value);
			}
			elseif ($key === 'SYSTEMPAY_ONEY_SHIP_OPTIONS')
			{
				if (!Tools::getValue('SYSTEMPAY_ONEY_CONTRACT', null))
					continue;

				if (!is_array($value) || empty($value))
					$value = array();
				else
				{
					foreach ($value as $id => $option)
					{
						$carrier = $option['label'].($option['address'] ?  ' '.$option['address'] : '');

						if (!preg_match(self::DELIVERY_COMPANY_REGEX, $carrier))
						{
							unset($value[$id]); // error, not save this option
							$this->_errors[] = sprintf($this->l('Invalid value «%s» for field «%s».'), $carrier, $label);
						}
					}
				}
				$value = serialize($value);
			}
			elseif ($key === 'SYSTEMPAY_CATEGORY_MAPPING')
			{
				if (Tools::getValue('SYSTEMPAY_COMMON_CATEGORY', null) != 'CUSTOM_MAPPING')
					continue;

				if (!is_array($value) || empty($value))
					$value = array();
				$value = serialize($value);
			}
			elseif (($key === 'SYSTEMPAY_ONEY_ENABLED') && ($value == 'True'))
			{
				$error = $this->validateOney();
				if (is_string($error) && !empty($error))
				{
					$this->_errors[] = $error;
					$value = 'False'; // there is error, not allow Oney activation
				}
			}
			elseif (in_array($key, $this->amount_fields))
			{
				if (!empty($value) && (!is_numeric($value) || $value < 0))
				{
					$this->_errors[] = sprintf($this->l('Invalid value «%s» for field «%s».'), $value, $label);
					continue;
				}
			}
			elseif ($key === 'SYSTEMPAY_STD_CARD_DATA_MODE' && $value == '3' && !$this->checkSsl())
			{
				$value = '1'; // force default card data entry mode
				$this->_errors[] = $this->l('The card data entry on merchant site cannot be used without enabling SSL.');
			}

			// validate with SystempayRequest
			if (key_exists('name', $param) && isset($param['name']) && !$request->set($param['name'], $value))
			{
				if (empty($value))
					$this->_errors[] = sprintf($this->l('The field «%s» is mandatory.'), $label);
				else
					$this->_errors[] = sprintf($this->l('Invalid value «%s» for field «%s».'), $value, $label);

				continue;
			}

			// valid field : try to save into DB
			if (!Configuration::updateValue($key, $value))
				$this->_errors[] = sprintf($this->l('Problem occured while saving field «%s».'), $label);
			else
				// temporary variable set to update PrestaShop cache
				Configuration::set($key, $value);
		}
	}

	private function validateOney()
	{
		if (Configuration::get('PS_ALLOW_MULTISHIPPING'))
			return $this->l('Multishipping is activated. FacilyPay Oney payment cannot be used.');

		if (!Tools::getValue('SYSTEMPAY_ONEY_CONTRACT', null))
			return $this->l('Please configure «ADDITIONAL OPTIONS» section of «GENERAL CONFIGURATION» tab. FacilyPay Oney payment cannot be used.');

		$amount_min = Tools::getValue('SYSTEMPAY_ONEY_AMOUNT_MIN', null);
		$amount_max = Tools::getValue('SYSTEMPAY_ONEY_AMOUNT_MAX', null);
		if (empty($amount_min) || !is_numeric($amount_min) || $amount_min < 0 || empty($amount_max) || !is_numeric($amount_max) || $amount_max < 0)
			return $this->l('Please, enter minimum and maximum amounts in FacilyPay Oney payment tab as agreed with Banque Accord.');

		return true;
	}

	private function checkSsl()
	{
		return Configuration::get('PS_SSL_ENABLED');
	}

	private function renderForm()
	{
		$this->context->controller->addJS($this->_path.'views/js/systempay.js');
		$this->context->controller->addJqueryUI('ui.accordion');

		$html = '';

		if (version_compare(_PS_VERSION_, '1.6', '>='))
		{
			$html .= '<style type="text/css">
							#content {
								min-width: inherit !important;
							}
					 </style>';
			$html .= "\n";
		}

		$display = new SystempayAdminDisplay($this);

		$this->context->smarty->assign(
				array(
						'systempay_request_uri'	=> htmlentities($_SERVER['REQUEST_URI']),
						'systempay_common'			=> $display->commonHtml(),
						'systempay_general_tab'	=> $display->generalTabHtml(),
						'systempay_single_tab'		=> $display->singleTabHtml(),
						'systempay_multi_tab'		=> $display->multiTabHtml(),
						'systempay_oney_tab'		=> $display->oneyTabHtml(),
						'systempay_ancv_tab'		=> $display->ancvTabHtml(),
						'systempay_sepa_tab'		=> $display->sepaTabHtml(),
						'systempay_sofort_tab'		=> $display->sofortTabHtml()
				)
		);

		$prefered_post_size = $display->getPostSize() + 2600;
		$prefered_post_vars = $display->getPostVars() + 50;
		if (ini_get('post_max_size') && (Tools::convertBytes(ini_get('post_max_size')) < $prefered_post_size))
			$html .= $this->displayError(sprintf($this->l('Warning, please increase the value of the post_max_size directive in php.ini to save module configurations correctly. Recommended value is %s.'), $prefered_post_size));
		elseif ((ini_get('suhosin.post.max_vars') && ini_get('suhosin.post.max_vars') < $prefered_post_vars)
				|| (ini_get('suhosin.request.max_vars') && ini_get('suhosin.request.max_vars') < $prefered_post_vars))
			$html .= $this->displayError(sprintf($this->l('Warning, please increase the suhosin patch for PHP post and request limits to save module configurations correctly. Recommended value is %s.'), $prefered_post_vars));
		elseif (ini_get('max_input_vars') && ini_get('max_input_vars') < $prefered_post_vars)
			$html .= $this->displayError(sprintf($this->l('Warning, please increase the value of the max_input_vars directive in php.ini to to save module configurations correctly. Recommended value is %s.'), $prefered_post_vars));

		$html .= $this->context->smarty->fetch(_PS_MODULE_DIR_.'systempay/views/templates/admin/back_office.tpl');
		return $html;
	}

	/**
	 * Payment method selection page header.
	 * @param array $params
	 */
	public function hookHeader()
	{
		if (($this->context->controller instanceof OrderController && $this->context->controller->step == 3)
			|| $this->context->controller instanceof OrderOpcController)
		{
			$suffix = '';
			if (version_compare(_PS_VERSION_, '1.6', '<'))
				$suffix = '_1.5';

			$this->context->controller->addCSS($this->_path."views/css/systempay{$suffix}.css", 'all');

			// load payment module style to apply it to ours
			if ($this->useMobileTheme())
				$css_file = _PS_THEME_MOBILE_DIR_.'css/global.css';
			else
				$css_file = _PS_THEME_DIR_.'css/global.css';

			$css = Tools::file_get_contents(str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $css_file));

			$matches = array();
			$res = preg_match_all('#(p\.payment_module(?: | a | a\:hover )\{[^\}]+\})#', $css, $matches);
			if ($res && count($matches) && isset($matches[1]) && is_array($matches[1]) && count($matches[1]))
			{
				$html = '<style type="text/css">'."\n";
				$html .= str_replace('p.payment_module', 'div.payment_module', implode("\n", $matches[1]))."\n";
				$html .= '</style>'."\n";

				return $html;
			}
		}
	}

	protected function useMobileTheme()
	{
		if (method_exists(get_parent_class($this), 'useMobileTheme'))
			return parent::useMobileTheme();
		elseif (method_exists($this->context, 'getMobileDevice'))
			return ($this->context->getMobileDevice() && file_exists(_PS_THEME_MOBILE_DIR_.'layout.tpl'));
		else
			return false;
	}

	/**
	 * Payment function, redirects the client to payment page.
	 *
	 * @param array $params
	 * @return void|Ambigous <string, void, boolean, mixed, unknown>
	 */
	public function hookPayment()
	{
		$cart = $this->context->cart;
		$cust = $this->context->customer;

		$billing_address = new Address($cart->id_address_invoice);

		$delivery_address = new Address($cart->id_address_delivery);
		$colissimo_address = $this->getColissimoShippingAddress($delivery_address, $cust->id);
		if ($colissimo_address instanceof Address)
			$delivery_address = $colissimo_address;

		// currency support
		if (!$this->checkCurrency())
			return;

		$html = '';

		if ($this->checkStandardPayment())
		{
			$title = Configuration::get('SYSTEMPAY_STD_TITLE', $this->context->language->id);
			if (!$title)
				$title = $this->l('Payment by bank card');
			$this->context->smarty->assign('systempay_title', $title);

			// get selected card types
			$cards = Configuration::get('SYSTEMPAY_PAYMENT_CARDS');
			if (!empty($cards))
				$cards = explode(';', $cards);
			else
				// no card type selected, display all supported cards
				$cards = array_keys(SystempayApi::getSupportedCardTypes());

			$errors = array();
			if ($this->sendOneyDataInStdPayment($cards))
			{
				// check address validity according to Oney payment specifications
				$errors = $this->checkAddressValidity($billing_address, $this->l('billing address'));
				if (empty($errors))
					// billing address is valid, check delivery address
					$errors = $this->checkAddressValidity($delivery_address, $this->l('delivery address'));

				if (!empty($errors))
				{
					$this->context->smarty->assign('systempay_logo', 'BannerLogo1.png');
					$this->context->smarty->assign('systempay_errors', $errors);
					$html .= $this->display(__FILE__, 'payment_errors.tpl');
				}
			}
			else
				// not sending oney data, do not display oney card types
				$cards = array_diff($cards, array('ONEY', 'ONEY_SANDBOX'));

			$avail_cards = array();
			foreach (SystempayApi::getSupportedCardTypes() as $code => $label)
			{
				if (in_array($code, $cards))
					$avail_cards[$code] = $label;
			}
			$this->context->smarty->assign('systempay_avail_cards', $avail_cards);

			// get data entry mode
			$entry_mode = Configuration::get('SYSTEMPAY_STD_CARD_DATA_MODE');
			if ($entry_mode == '3' && !($this->checkSsl() && Tools::usingSecureMode()))
				$entry_mode = '1'; // no data entry on merchant site without SSL
			$this->context->smarty->assign('systempay_std_card_data_mode', $entry_mode);

			if (empty($errors))
				$html .= $this->display(__FILE__, 'payment_std.tpl');
		}

		if ($this->checkMultiPayment())
		{
			$title = Configuration::get('SYSTEMPAY_MULTI_TITLE', $this->context->language->id);
			if (!$title)
				$title = $this->l('Payment by bank card in several times');
			$this->context->smarty->assign('systempay_title', $title);
			$this->context->smarty->assign('systempay_title', Configuration::get('SYSTEMPAY_MULTI_TITLE', $this->context->language->id));

			// multi payment options
			$this->context->smarty->assign('systempay_multi_options', $this->getAvailableMultiOptions());
			$html .= $this->display(__FILE__, 'payment_multi.tpl');
		}

		if ($this->checkOneyPayment())
		{
			$title = Configuration::get('SYSTEMPAY_ONEY_TITLE', $this->context->language->id);
			if (!$title)
				$title = $this->l('Payment with FacilyPay Oney');
			$this->context->smarty->assign('systempay_title', $title);

			// check address validity according to Oney payment specifications
			$errors = $this->checkAddressValidity($billing_address, $this->l('billing address'));
			if (empty($errors))
				// billing address is valid, check delivery address
				$errors = $this->checkAddressValidity($delivery_address, $this->l('delivery address'));

			if (!empty($errors))
			{
				$this->context->smarty->assign('systempay_logo', 'oney.png');
				$this->context->smarty->assign('systempay_errors', $errors);
				$html .= $this->display(__FILE__, 'payment_errors.tpl');
			}
			else
				$html .= $this->display(__FILE__, 'payment_oney.tpl');
		}

		$country = new Country($billing_address->id_country);

		if ($this->checkANCVPayment() && $country->iso_code == 'FR')
		{
			$title = Configuration::get('SYSTEMPAY_ANCV_TITLE', $this->context->language->id);
			if (!$title)
				$title = $this->l('Payment with ANCV');
			$this->context->smarty->assign('systempay_title', $title);

			$html .= $this->display(__FILE__, 'payment_ancv.tpl');
		}

		$sepa_area = array('FI','AT','PT','BE','BG','ES','HR','CY','CZ','DK','EE','FR','GF','DE','GI','GR',
				'GP','HU','IS','IE','LV','LI','LT','LU','PT','MT','MQ','YT','MC','NL','NO','PL',
				'RE','RO','BL','MF','PM','SM','SK','SE','CH','GB');
		if ($this->checkSEPAPayment() && in_array($country->iso_code, $sepa_area))
		{
			$title = Configuration::get('SYSTEMPAY_SEPA_TITLE', $this->context->language->id);
			if (!$title)
				$title = $this->l('Payment with SEPA');
			$this->context->smarty->assign('systempay_title', $title);
			$html .= $this->display(__FILE__, 'payment_sepa.tpl');
		}

		if ($this->checkSOFORTPayment())
		{
			$title = Configuration::get('SYSTEMPAY_SOFORT_TITLE', $this->context->language->id);
			if (!$title)
				$title = $this->l('Payment with SOFORT Banking');
			$this->context->smarty->assign('systempay_title', $title);

			$html .= $this->display(__FILE__, 'payment_sofort.tpl');
		}

		return $html;
	}

	private function checkCurrency()
	{
		$cart = $this->context->cart;

		$cart_currency = new Currency((int)$cart->id_currency);
		$currencies = $this->getCurrency((int)$cart->id_currency);

		if (!is_array($currencies) || !count($currencies))
			return false;

		foreach ($currencies as $currency)
		{
			if ($cart_currency->id == $currency['id_currency'])
				// cart currency is allowed for this module
				return SystempayApi::findCurrencyByAlphaCode($cart_currency->iso_code) != null;
		}

		return false;
	}

	private function sendOneyDataInStdPayment($cards = array())
	{
		if (!Configuration::get('SYSTEMPAY_ONEY_CONTRACT') || Configuration::get('SYSTEMPAY_STD_CARD_DATA_MODE') == '3')
			return false;

		if (empty($cards))
		{
			$cards = explode(';', Configuration::get('SYSTEMPAY_PAYMENT_CARDS'));
			if (in_array('', $cards))
				$cards = array_keys(SystempayApi::getSupportedCardTypes());
		}

		return in_array('ONEY', $cards) || in_array('ONEY_SANDBOX', $cards);
	}

	private function checkStandardPayment()
	{
		if (Configuration::get('SYSTEMPAY_STD_ENABLED') != 'True')
			return false;

		$cart = $this->context->cart;

		// check amount restrictions
		$min = Configuration::get('SYSTEMPAY_STD_AMOUNT_MIN');
		$max = Configuration::get('SYSTEMPAY_STD_AMOUNT_MAX');
		if (($min != '' && $cart->getOrderTotal() < $min) || ($max != '' && $cart->getOrderTotal() > $max))
			return false;

		if ($this->sendOneyDataInStdPayment())
			return $this->checkOneyPayment(true);

		return true;
	}

	private function checkMultiPayment()
	{
		if (Configuration::get('SYSTEMPAY_MULTI_ENABLED') != 'True')
			return false;

		$cart = $this->context->cart;

		// check amount restrictions
		$min = Configuration::get('SYSTEMPAY_MULTI_AMOUNT_MIN');
		$max = Configuration::get('SYSTEMPAY_MULTI_AMOUNT_MAX');
		if (($min != '' && $cart->getOrderTotal() < $min) || ($max != '' && $cart->getOrderTotal() > $max))
			return false;

		// check available options
		if (!count($this->getAvailableMultiOptions()))
			return false;

		return true;
	}

	private function getAvailableMultiOptions()
	{
		// multi payment options
		$options = @unserialize(Configuration::get('SYSTEMPAY_MULTI_OPTIONS'));
		if (!is_array($options) || !count($options))
			return array();

		$cart = $this->context->cart;

		$enabled_options = array();
		foreach ($options as $key => $option)
		{
			$min = $option['amount_min'];
			$max = $option['amount_max'];

			if (($min == '' || $cart->getOrderTotal() >= $min) && ($max == '' || $cart->getOrderTotal() <= $max))
				$enabled_options[$key] = $option;
		}

		return $enabled_options;
	}

	private function checkOneyPayment($in_standard = false)
	{
		$cart = $this->context->cart;

		if (!$in_standard)
		{
			// specific to Oney payment sub-module
			if (Configuration::get('SYSTEMPAY_ONEY_ENABLED') != 'True')
				return false;

			// check amount restrictions
			$min = Configuration::get('SYSTEMPAY_ONEY_AMOUNT_MIN');
			$max = Configuration::get('SYSTEMPAY_ONEY_AMOUNT_MAX');
			if (($min != '' && $cart->getOrderTotal() < $min) || ($max != '' && $cart->getOrderTotal() > $max))
				return false;
		}

		// check order_id param
		if (!preg_match(self::ORDER_ID_REGEX, $cart->id))
		{
			$msg = 'Order ID «%s» does not match FacilyPay Oney specifications. The regular expression for this field is «%s». Module is not displayed.';
			$this->logger->logWarning(sprintf($msg, $cart->id, self::ORDER_ID_REGEX));
			return false;
		}

		// check cust_id param
		$cust = $this->context->customer;
		if (!preg_match(self::CUST_ID_REGEX, $cust->id))
		{
			$msg = 'Customer ID «%s» does not match FacilyPay Oney specifications.';
			$msg .= ' The regular expression for this field is «%s». Module is not displayed.';
			$this->logger->logWarning(sprintf($msg, $cust->id, self::CUST_ID_REGEX));
			return false;
		}

		// check products
		foreach ($cart->getProducts(true) as $product)
		{
			if (!preg_match(self::PRODUCT_REF_REGEX, $product['id_product']))
			{
				// product id doesn't match FacilyPay Oney rules

				$msg = 'Product reference «%s» does not match FacilyPay Oney specifications.';
				$msg .= ' The regular expression for this field is «%s». Module is not displayed.';
				$this->logger->logWarning(sprintf($msg, $product['id_product'], self::PRODUCT_REF_REGEX));
				return false;
			}
			elseif (!preg_match(self::PRODUCT_LABEL_REGEX, $product['name']))
			{
				// product label doesn't match FacilyPay Oney rules

				$msg = 'Product label «%s» does not match FacilyPay Oney specifications.';
				$msg .= ' The regular expression for this field is «%s». Module is not displayed.';
				$this->logger->logWarning(sprintf($msg, $product['name'], self::PRODUCT_LABEL_REGEX));
				return false;
			}
		}

		return true;
	}

	private function checkANCVPayment()
	{
		if (Configuration::get('SYSTEMPAY_ANCV_ENABLED') != 'True')
			return false;

		$cart = $this->context->cart;

		// check amount restrictions
		$min = Configuration::get('SYSTEMPAY_ANCV_AMOUNT_MIN');
		$max = Configuration::get('SYSTEMPAY_ANCV_AMOUNT_MAX');
		if (($min != '' && $cart->getOrderTotal() < $min) || ($max != '' && $cart->getOrderTotal() > $max) || ($this->name =='innopay') || ($this->name =='osb'))
			return false;

		return true;
	}

	private function checkSEPAPayment()
	{
		if (Configuration::get('SYSTEMPAY_SEPA_ENABLED') != 'True')
			return false;

		$cart = $this->context->cart;

		// check amount restrictions
		$min = Configuration::get('SYSTEMPAY_SEPA_AMOUNT_MIN');
		$max = Configuration::get('SYSTEMPAY_SEPA_AMOUNT_MAX');
		if (($min != '' && $cart->getOrderTotal() < $min) || ($max != '' && $cart->getOrderTotal() > $max) || ($this->name !='payzen' ))
			return false;

		return true;
	}

	private function checkSOFORTPayment()
	{
		if (Configuration::get('SYSTEMPAY_SOFORT_ENABLED') != 'True')
			return false;

		$cart = $this->context->cart;

		// check amount restrictions
		$min = Configuration::get('SYSTEMPAY_SOFORT_AMOUNT_MIN');
		$max = Configuration::get('SYSTEMPAY_SOFORT_AMOUNT_MAX');
		if (($min != '' && $cart->getOrderTotal() < $min) || ($max != '' && $cart->getOrderTotal() > $max) || ($this->name !='payzen' && $this->name !='payzen_de'))
			return false;

		return true;
	}

	private function checkAddressValidity($address, $address_type)
	{
		$invalid_msg = $this->l('The field %s of your %s is invalid.');
		$empty_msg = $this->l('The field %s of your %s is mandatory.');

		$name_regex = "#^[A-ZÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ/ '-]{1,63}$#ui";
		$phone_regex = '#^[0-9]{10}$#';
		$city_regex = "#^[A-Z0-9ÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ/ '-]{1,127}$#ui";
		$street_regex = "#^[A-Z0-9ÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ/ '.,-]{1,127}$#ui";
		$country_regex = '#^FR$#i';
		$zip_regex = '#^[0-9]{5}$#';

		$errors = array();

		if (empty($address->lastname))
			$errors[] = sprintf($empty_msg, $this->l('Last name'), $address_type);
		elseif (!preg_match($name_regex, $address->lastname))
			$errors[] = sprintf($invalid_msg, $this->l('Last name'), $address_type);

		if (empty($address->firstname))
			$errors[] = sprintf($empty_msg, $this->l('First name'), $address_type);
		elseif (!preg_match($name_regex, $address->firstname))
			$errors[] = sprintf($invalid_msg, $this->l('First name'), $address_type);

		if (!empty($address->phone) && !preg_match($phone_regex, $address->phone))
			$errors[] = sprintf($invalid_msg, $this->l('Phone'), $address_type);

		if (!empty($address->phone_mobile) && !preg_match($phone_regex, $address->phone_mobile))
			$errors[] = sprintf($invalid_msg, $this->l('Phone mobile'), $address_type);

		if (empty($address->address1))
			$errors[] = sprintf($empty_msg, $this->l('Address'), $address_type);
		elseif (!preg_match($street_regex, $address->address1))
			$errors[] = sprintf($invalid_msg, $this->l('Address'), $address_type);

		if (!empty($address->address2) && !preg_match($street_regex, $address->address2))
			$errors[] = sprintf($invalid_msg, $this->l('Address2'), $address_type);

		if (empty($address->postcode))
			$errors[] = sprintf($empty_msg, $this->l('Zip code'), $address_type);
		elseif (!preg_match($zip_regex, $address->postcode))
			$errors[] = sprintf($invalid_msg, $this->l('Zip code'), $address_type);

		if (empty($address->city))
			$errors[] = sprintf($empty_msg, $this->l('City'), $address_type);
		elseif (!preg_match($city_regex, $address->city))
			$errors[] = sprintf($invalid_msg, $this->l('City'), $address_type);

		$country = new Country($address->id_country);
		if (empty($country->iso_code))
			$errors[] = sprintf($empty_msg, $this->l('Country'), $address_type);
		elseif (!preg_match($country_regex, $country->iso_code))
			$errors[] = sprintf($invalid_msg, $this->l('Country'), $address_type);

		return $errors;
	}


	/**
	 * Manage payement gateway response.
	 *
	 * @param array $params
	 */
	public function hookPaymentReturn($params)
	{
		if (!$this->active || $params['objOrder']->module != $this->name)
			return;

		$error_msg = (Tools::getValue('error') == 'yes');

		$array = array(
				'check_url_warn' => (Tools::getValue('check_url_warn') == 'yes'),
				'maintenance_mode' => (Configuration::get('PS_SHOP_ENABLE') == '0'),
				'prod_info' => (Tools::getValue('prod_info') == 'yes'),
				'error_msg' => $error_msg
		);

		if ($error_msg === false)
		{
			$array['total_to_pay'] = Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false);
			$array['id_order'] = $params['objOrder']->id;
		}

		$this->context->smarty->assign($array);

		return $this->display(__FILE__, 'payment_return.tpl');
	}

	/**
	 * Before shopping cart display.
	 *
	 * @param array $params
	 */
	public function hookDisplayShoppingCart()
	{
		if (Tools::getValue('systempay_pay_error') == 'yes')
		{
			$this->context->controller->errors[] = $this->l('Your payment was not accepted. Please, try to re-order.');

			// unset HTTP_REFERER from smarty server variable to avoid back button display
			unset($_SERVER['HTTP_REFERER']);
			$this->context->smarty->assign('server', $_SERVER);
		}
	}

	/**
	 * TODO to remove when So Colissimo fix cart delivery address id
	 */
	private function getColissimoShippingAddress($ps_address, $id_customer)
	{
		$cart = $this->context->cart;

		// So Colissimo not installed
		if (!Configuration::get('SOCOLISSIMO_CARRIER_ID'))
			return false;

		// So Colissimo is not selected as shipping method
		if ($cart->id_carrier != Configuration::get('SOCOLISSIMO_CARRIER_ID'))
			return false;

		// Get address saved by So Colissimo
		$return = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'socolissimo_delivery_info WHERE id_cart =\''.
				(int)$cart->id.'\' AND id_customer =\''.(int)$id_customer.'\'');
		$new_address = new Address();

		if (Tools::strtoupper($ps_address->lastname) != Tools::strtoupper($return['prname'])
			|| Tools::strtoupper($ps_address->firstname) != Tools::strtoupper($return['prfirstname'])
			|| Tools::strtoupper($ps_address->address1) != Tools::strtoupper($return['pradress3'])
			|| Tools::strtoupper($ps_address->address2) != Tools::strtoupper($return['pradress2'])
			|| Tools::strtoupper($ps_address->postcode) != Tools::strtoupper($return['przipcode'])
			|| Tools::strtoupper($ps_address->city) != Tools::strtoupper($return['prtown'])
			|| str_replace(array(' ', '.', '-', ',', ';', '+', '/', '\\', '+', '(', ')'), '', $ps_address->phone_mobile) != $return['cephonenumber'])
		{

			// Address is modified in So Colissimo page : use it as shipping address
			$new_address->lastname = Tools::substr($return['prname'], 0, 32);
			$new_address->firstname = Tools::substr($return['prfirstname'], 0, 32);
			$new_address->postcode = $return['przipcode'];
			$new_address->city = $return['prtown'];
			$new_address->id_country = Country::getIdByName(null, 'france');

			if (!in_array($return['delivery_mode'], array('DOM', 'RDV')))
			{
				$new_address->address1 = $return['pradress1'];
				$new_address->address1 .= isset($return['pradress2']) ?  ' '.$return['pradress2'] : '';
				$new_address->address1 .= isset($return['pradress3']) ?  ' '.$return['pradress3'] : '';
				$new_address->address1 .= isset($return['pradress4']) ?  ' '.$return['pradress4'] : '';
			}
			else
			{
				$new_address->address1 = $return['pradress3'];
				$new_address->address2 = isset($return['pradress4']) ? $return['pradress4'] : '';
				$new_address->other = isset($return['pradress1']) ?  $return['pradress1'] : '';
				$new_address->other .= isset($return['pradress2']) ?  ' '.$return['pradress2'] : '';
			}

			// Return the So Colissimo updated
			return $new_address;
		}
		else
			// Use initial address
			return null;
	}

	private function setOneyData(&$systempay_request, $delivery_address)
	{
		$cart = $this->context->cart;

		// Oney delivery options defined in admin panel
		$oney_options = @unserialize(Configuration::get('SYSTEMPAY_ONEY_SHIP_OPTIONS'));

		// retrieve carrier ID from cart
		if (isset($cart->id_carrier) && $cart->id_carrier > 0)
			$carrier_id = $cart->id_carrier;
		else
		{
			$delivery_option_list = $cart->getDeliveryOptionList();

			$delivery_option = $cart->getDeliveryOption();
			$carrier_key = $delivery_option[$delivery_address->id];

			foreach (array_keys($delivery_option_list[$delivery_address->id][$carrier_key]['carrier_list']) as $id)
			{
				$carrier_id = $id;
				break;
			}
		}

		// set shipping params
		$shop_name_regex_not_allowed = "#[^A-Z0-9ÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ /'-]#ui";
		if ($cart->isVirtualCart() || !isset($carrier_id) || !is_array($oney_options) || empty($oney_options))
		{
			// no shipping options or virtual cart
			$systempay_request->set('ship_to_type', 'ETICKET');
			$systempay_request->set('ship_to_speed', 'EXPRESS');

			$shop = Shop::getShop($cart->id_shop);
			$systempay_request->set('ship_to_delivery_company_name', preg_replace($shop_name_regex_not_allowed, ' ', $shop['name']));
		}
		elseif (Configuration::get('TNT_CARRIER_'.self::TNT_RELAY_POINT_PREFIX.'_ID') == $carrier_id)
		{
			// specific case : TNT relay point
			$systempay_request->set('ship_to_type', 'RELAY_POINT');
			$systempay_request->set('ship_to_speed', 'EXPRESS');

			$sql = 'SELECT * FROM `'._DB_PREFIX_."tnt_carrier_drop_off` WHERE `id_cart` = '".$cart->id."'";
			$row = Db::getInstance()->getRow($sql);

			$tnt_relay_address = $row['name'].' '.$row['address'].' '.$row['zipcode'].' '.$row['city'];
			$systempay_request->set('ship_to_delivery_company_name', preg_replace($shop_name_regex_not_allowed, ' ', $tnt_relay_address));

			// override shipping address
			$systempay_request->set('ship_to_first_name', null);
			$systempay_request->set('ship_to_last_name', preg_replace($shop_name_regex_not_allowed, ' ', $row['name']));
			$systempay_request->set('ship_to_street', preg_replace($shop_name_regex_not_allowed, ' ', $row['address']));
			$systempay_request->set('ship_to_street2', null);
			$systempay_request->set('ship_to_zip', $row['zipcode']);
			$systempay_request->set('ship_to_city', preg_replace($shop_name_regex_not_allowed, ' ', $row['city']));
			$systempay_request->set('ship_to_phone_num', null);
			$systempay_request->set('ship_to_state', null);
			$systempay_request->set('ship_to_country', 'FR');
		}
		else
		{
			// other cases
			$delivery_type = $oney_options[$carrier_id]['type'];
			$systempay_request->set('ship_to_type', $delivery_type);
			$systempay_request->set('ship_to_speed', $oney_options[$carrier_id]['speed']);

			$company_name = $oney_options[$carrier_id]['label'];
			if ($delivery_type === 'RECLAIM_IN_SHOP')
				$company_name .= ' '.$oney_options[$carrier_id]['address'];
			/*elseif ($delivery_type === 'RELAY_POINT' || $delivery_type === 'RECLAIM_IN_STATION')
			{
				// this block is intentionally let blank
				// merchant must implement its specific logic for these delivery types
			}*/

			$systempay_request->set('ship_to_delivery_company_name', $company_name);
		}

		// PrestaShop don't manage customer type
		$systempay_request->set('cust_status', 'PRIVATE');
		$systempay_request->set('ship_to_status', 'PRIVATE');
	}

	/**
	* Generate form fields to post to the payment gateway.
	*/
	public function getFormFields($type, $data = array())
	{
		/* @var $cust Customer */
		/* @var $cart Cart */
		$cust = $this->context->customer;
		$cart = $this->context->cart;

		/* @var $billing_country Address */
		$billing_address = new Address($cart->id_address_invoice);
		$billing_country = new Country($billing_address->id_country);

		/* @var $delivery_address Address */
		$delivery_address = new Address($cart->id_address_delivery);

		// get So Colissimo delivery address
		$colissimo_address = $this->getColissimoShippingAddress($delivery_address, $cust->id);
		if ($colissimo_address instanceof Address)
			$delivery_address = $colissimo_address;

		$delivery_country = new Country($delivery_address->id_country);

		$this->logger->logInfo("Form data generation for cart #{$cart->id} with $type payment.");

		/* @var $request SystempayRequest */
		$request = new SystempayRequest();
		$request->set('version', 'V2');

		$contrib = 'Prestashop1.5-1.6_1.5.0/'._PS_VERSION_;
		if (defined('_PS_HOST_MODE_'))
			$contrib = str_replace('Prestashop', 'Prestashop_Cloud', $contrib);
		$request->set('contrib', $contrib);

		foreach ($this->getAdminParameters() as $param)
		{
			if (key_exists('name', $param) && isset($param['name']))
				// only set Systempay payment params
				$request->set($param['name'], Configuration::get($param['key']));
		}

		// detect default language
		/* @var $language Language */
		$language = Language::getLanguage((int)$this->context->language->id);
		$language_iso_code = $language['language_code'] ? Tools::substr($language['language_code'], 0, 2) : $language['iso_code'];
		$language_iso_code = Tools::strtolower($language_iso_code);
		if (!SystempayApi::isSupportedLanguage($language_iso_code))
			$language_iso_code = Configuration::get('SYSTEMPAY_DEFAULT_LANGUAGE');

		// detect store currency
		$cart_currency = new Currency((int)$cart->id_currency);
		$currency = SystempayApi::findCurrencyByAlphaCode($cart_currency->iso_code);

		// amount rounded to currency decimals
		$amount = Tools::ps_round($cart->getOrderTotal(), $currency->getDecimals());

		$request->set('amount', $currency->convertAmountToInteger($amount));
		$request->set('currency', $currency->getNum());
		$request->set('language', $language_iso_code);
		$request->set('order_id', $cart->id);

		// customer data
		$request->set('cust_email', $cust->email);
		$request->set('cust_id', $cust->id);

		$cust_title = new Gender((int)$cust->id_gender);
		$request->set('cust_title', $cust_title->name[Context::getContext()->language->id]);

		$request->set('cust_first_name', $billing_address->firstname);
		$request->set('cust_last_name', $billing_address->lastname);
		$request->set('cust_legal_name', $billing_address->company);
		$request->set('cust_address', $billing_address->address1.' '.$billing_address->address2);
		$request->set('cust_zip', $billing_address->postcode);
		$request->set('cust_city', $billing_address->city);
		$request->set('cust_phone', $billing_address->phone);
		$request->set('cust_country', $billing_country->iso_code);
		if ($billing_address->id_state)
		{
			$state = new State((int)$billing_address->id_state);
			$request->set('cust_state', $state->iso_code);
		}

		$cards = isset($data['card_type']) ? array($data['card_type']) : array();
		$oney_payment = ($type === 'standard' && $this->sendOneyDataInStdPayment($cards)) || $type === 'oney';
		if ($oney_payment)
		{
			// prepare cart data to send to gateway
			if (Configuration::get('SYSTEMPAY_COMMON_CATEGORY') != 'CUSTOM_MAPPING')
				$category = Configuration::get('SYSTEMPAY_COMMON_CATEGORY');
			else
				$oney_categories = @unserialize(Configuration::get('SYSTEMPAY_CATEGORY_MAPPING'));

			// $product_label_regex_not_allowed = '#[^A-Z0-9ÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÇ ]#ui';
			foreach ($cart->getProducts(true) as $product)
			{
				if (!isset($category))
				{
					// build query to get product default category
					$sql = 'SELECT `id_category_default` FROM `'._DB_PREFIX_.'product` WHERE `id_product` = '.(int)$product['id_product'];
					$db_category = Db::getInstance()->getValue($sql);

					$category = $oney_categories[$db_category];
				}

				// $checked = $this->sendOneyDataInStdPayment() || $type === 'oney'; // if Oney, product labels are checked
				$request->addProduct(
						$product['name'], // $oney_payment ? $product['name'] : preg_replace($product_label_regex_not_allowed, ' ', $product['name']),
						$currency->convertAmountToInteger($product['price']),
						$product['cart_quantity'],
						$product['id_product'],
						$category
				);
			}
		}

		// set misc optional params as possible
		// $request->set('insurance_amount', 0); // by default, shipping insurance amount is not available in PrestaShop
		// $request->set('shipping_amount', $currency->convertAmountToInteger($cart->getOrderTotal(false, Cart::ONLY_SHIPPING)));

		// $subtotal = 0;
		// for ($index = 0; $index < $request->get('nb_products'); $index++)
		// 	$subtotal += $request->get('product_amount'.$index) * $request->get('product_qty'.$index);
		// $request->set('tax_amount', $request->get('amount') - $subtotal - $request->get('shipping_amount') - $request->get('insurance_amount'));

		$title = '';
		switch ($type)
		{
			case 'standard' :
				// single payment

				if (key_exists('card_type', $data) && $data['card_type'])
				{
					// override payemnt_cards parameter
					$request->set('payment_cards', $data['card_type']);

					if (key_exists('card_number', $data) && $data['card_number'])
					{
						$request->set('card_number', $data['card_number']);
						$request->set('cvv', $data['cvv']);
						$request->set('expiry_year', $data['expiry_year']);
						$request->set('expiry_month', $data['expiry_month']);

						// override action_mode to do a silent payment
						$request->set('action_mode', 'SILENT');
					}
				}

				if ($oney_payment)
					$this->setOneyData($request, $delivery_address);

				$title = Configuration::get('SYSTEMPAY_STD_TITLE', $this->context->language->id);
				if (!$title)
					$title = $this->l('Payment by bank card');

				break;

			case 'oney':
				// Oney payment

				// override with FacilyPay Oney payment cards
				$test_mode = $request->get('ctx_mode') == 'TEST';
				$request->set('payment_cards', $test_mode ? 'ONEY_SANDBOX' : 'ONEY');

				$this->setOneyData($request, $delivery_address);

				$title = Configuration::get('SYSTEMPAY_ONEY_TITLE', $this->context->language->id);
				if (!$title)
					$title = $this->l('Payment with FacilyPay Oney');

				break;

			case 'multi' :
				// multiple payment options

				$multi_options = $this->getAvailableMultiOptions();
				$option = $multi_options[$data['opt']];

				$config_first = $option['first'];
				$first = ($config_first != '') ? $currency->convertAmountToInteger(($config_first / 100) * $amount) : null;
				$request->setMultiPayment(null /* to use already set amount */, $first, $option['count'], $option['period']);

				// override cb contract
				$request->set('contracts', ($option['contract']) ? 'CB='.$option['contract'] : null);

				$title = Configuration::get('SYSTEMPAY_MULTI_TITLE', $this->context->language->id);
				if (!$title)
					$title = $this->l('Payment by bank card in several times');
				$title .= ' ('.$option['count'].' x)';

				break;

			case 'ancv':
				// ANCV payment;

				// override with ANCV card
				$request->set('payment_cards', 'E_CV');

				$title = Configuration::get('SYSTEMPAY_ANCV_TITLE', $this->context->language->id);
				if (!$title)
					$title = $this->l('Payment with ANCV');

				break;

			case 'sepa':
				// SEPA payment

				// override with SEPA card
				$request->set('payment_cards', 'SDD');

				$title = Configuration::get('SYSTEMPAY_SEPA_TITLE', $this->context->language->id);
				if (!$title)
					$title = $this->l('Payment with SEPA');

				break;

			case 'sofort':
				// SOFORT payment;

				// override with SOFORT payment card
				$request->set('payment_cards', 'SOFORT_BANKING');

				$title = Configuration::get('SYSTEMPAY_SOFORT_TITLE', $this->context->language->id);
				if (!$title)
					$title = $this->l('Payment with SOFORT Banking');

				break;
		}

		$request->set('order_info', $title);

		if ($request->get('ship_to_type') == null || $request->get('ship_to_type') == 'PACKAGE_DELIVERY_COMPANY')
		{
			$request->set('ship_to_first_name', $delivery_address->firstname);
			$request->set('ship_to_last_name', $delivery_address->lastname);
			$request->set('ship_to_legal_name', $delivery_address->company);
			$request->set('ship_to_street', $delivery_address->address1);
			$request->set('ship_to_street2', $delivery_address->address2);
			$request->set('ship_to_zip', $delivery_address->postcode);
			$request->set('ship_to_city', $delivery_address->city);
			$request->set('ship_to_phone_num', $delivery_address->phone);
			$request->set('ship_to_country', $delivery_country->iso_code);
			if ($delivery_address->id_state)
			{
				$state = new State((int)$delivery_address->id_state);
				$request->set('ship_to_state', $state->iso_code);
			}
		}

		// store id_shop to recover it in IPN script
		$request->set('order_info2', 'id_shop='.$cart->id_shop);

		// activate 3ds ?
		$threeds_mpi = null;
		if (Configuration::get('SYSTEMPAY_3DS_MIN_AMOUNT') != '' && $amount < Configuration::get('SYSTEMPAY_3DS_MIN_AMOUNT'))
			$threeds_mpi = '2';
		$request->set('threeds_mpi', $threeds_mpi);

		// return URL
		$request->set('url_return', $this->context->link->getModuleLink($this->name, 'submit', array(), true));

		// patch to avoid signature error with HTML minifier introduced since PrestaShop 1.6.0.3
		if (Configuration::get('PS_HTML_THEME_COMPRESSION') && version_compare(_PS_VERSION_, '1.6.0.3', '>='))
			foreach ($request->getRequestFields() as $field)
			{
				$value = preg_replace('/\s+/m', ' ', $field->getValue());
				$request->set($field->getName(), $value);
			}

		// prepare data for Systempay payment form
		return $request->getRequestFieldsArray();
	}

	/**
	* Save order and transaction info.
	*/
	public function saveOrder($cart, $order_status, $systempay_response)
	{
		$this->logger->logInfo("Create order for cart #{$cart->id}.");

		// retrieve customer from cart
		$customer = new Customer($cart->id_customer);

		// PrestaShop id_currency from currency iso num code
		$currency_id = Currency::getIdByIsoCodeNum((int)$systempay_response->get('currency'));

		// 3-DS extra message
		$msg3ds = "\n".$this->l('3DS authentication : ');
		if ($systempay_response->get('threeds_status') == 'Y')
		{
			$msg3ds .= $this->l('YES');
			$msg3ds .= "\n".$this->l('3DS certificate : ').$systempay_response->get('threeds_cavv');
		}
		else
			$msg3ds .= $this->l('NO');

		$currency = SystempayApi::findCurrencyByNumCode($systempay_response->get('currency'));

		// real paid total on platform
		$paid_total = $systempay_response->getFloatAmount();
		if (number_format($cart->getOrderTotal(), $currency->getDecimals()) == number_format($paid_total, $currency->getDecimals()))
			$paid_total = $cart->getOrderTotal(); // to avoid rounding issues and bypass PaymentModule::validateOrder() check

		// call payment module validateOrder
		$this->validateOrder(
			$cart->id,
			$order_status,
			$paid_total,
			$systempay_response->get('order_info'), // title defined in admin panel and sent to platform as order_info
			$systempay_response->getLogString().$msg3ds,
			array(), // $extraVars
			$currency_id, // $currency_special
			true, // $dont_touch_amount
			$customer->secure_key
		);

		// reload order
		$order = new Order((int)Order::getOrderByCartId($cart->id));
		$this->logger->logInfo("Order #{$order->id} created successfully for cart #{$cart->id}.");

		$this->savePayment($order, $systempay_response);

		return $order;
	}

	/**
	 * Update current order state.
	 */
	public function setOrderState($order, $order_state, $systempay_response)
	{
		$this->logger->logInfo("Payment status for cart #{$order->id_cart} has changed. New order status is $order_state.");

		$msg = new Message();
		$msg->message = $systempay_response->getLogString();
		$msg->id_order = (int)$order->id;
		$msg->private = 1;
		$msg->add();

		$order->setCurrentState($order_state);
		$this->logger->logInfo("Order status successfully changed, cart #{$order->id_cart}.");

		$this->savePayment($order, $systempay_response);
	}

	/**
	 * Save payment information.
	 */
	public function savePayment($order, $systempay_response)
	{
		$payments = $order->getOrderPayments();

		// delete payments created by default
		if (is_array($payments) && !empty($payments))
			foreach ($payments as $payment)
				if (!$payment->transaction_id)
				{
					$order->total_paid_real -= $payment->amount;
					$payment->delete();
				}

		if (!$this->isSuccessState($order) && !$systempay_response->isAcceptedPayment())
			// no payment creation
			return;

		// save transaction info
		$this->logger->logInfo("Save payment information for cart #{$order->id_cart}.");

		$invoices = $order->getInvoicesCollection();
		$invoice = count($invoices) > 0 ? $invoices[0] : null;

		$currency = SystempayApi::findCurrencyByNumCode($systempay_response->get('currency'));

		$payment_ids = array();
		if ($systempay_response->get('card_brand') == 'MULTI')
		{
			$sequences = Tools::jsonDecode($systempay_response->get('payment_seq'));
			$transactions = array_filter($sequences->transactions, 'Systempay::filterTransactions');

			$last_trs = end($transactions); // last transaction
			foreach ($transactions as $trs)
			{
				// real paid total on platform
				$amount = $currency->convertAmountToFloat($trs->{'amount'});

				if ($trs === $last_trs)
				{
					$remaining = $order->total_paid - $order->total_paid_real;
					if (number_format($remaining, $currency->getDecimals()) == number_format($amount, $currency->getDecimals()))
						$amount = $remaining; // to avoid rounding problems and pass PaymentModule::validateOrder() check
				}

				$transaction_id = $trs->{'sequence_number'}.'-'.$trs->{'trans_id'};
				$payment_method = sprintf($this->l('%s payment'), $trs->{'card_brand'});
				if (!$order->addOrderPayment($amount, $payment_method, $transaction_id, null, null, $invoice)
						|| !($pcc = $this->getOrderPayment($order->reference, $transaction_id)))
				{
					$this->logger->logWarn("Problem : payment information for cart #{$order->id_cart} cannot been saved.
							Error may be occured in another module hooked on order update event.");
					return;
				}

				$payment_ids[] = $pcc->id;

				// set card info
				$pcc->card_number = $trs->{'card_number'};
				$pcc->card_brand = $trs->{'card_brand'};
				if (isset($trs->{'expiry_month'}) && isset($trs->{'expiry_year'}))
					$pcc->card_expiration = str_pad($trs->{'expiry_month'}, 2, '0', STR_PAD_LEFT).'/'.$trs->{'expiry_year'};
				$pcc->card_holder = null;

				$pcc->update();
			}
		}
		else
		{
			// real paid total on platform
			$amount = $systempay_response->getFloatAmount();

			if (number_format($order->total_paid, $currency->getDecimals()) == number_format($amount, $currency->getDecimals()))
				$amount = $order->total_paid; // to avoid rounding problems and pass PaymentModule::validateOrder() check

			$transaction_id = $systempay_response->get('sequence_number').'-'.$systempay_response->get('trans_id');
			$payment_method = sprintf($this->l('%s payment'), $systempay_response->get('card_brand'));
			if (!$order->addOrderPayment($amount, $payment_method, $transaction_id, null, null, $invoice)
					|| !($pcc = $this->getOrderPayment($order->reference, $transaction_id)))
			{
				$this->logger->logWarn("Problem : payment information for cart #{$order->id_cart} cannot been saved.
						Error may be occured in another module hooked on order update event.");
				return;
			}

			$payment_ids[] = $pcc->id;

			// set card info
			$pcc->card_number = $systempay_response->get('card_number');
			$pcc->card_brand = $systempay_response->get('card_brand');
			if ($systempay_response->get('expiry_month') && $systempay_response->get('expiry_year'))
				$pcc->card_expiration = str_pad($systempay_response->get('expiry_month'), 2, '0', STR_PAD_LEFT).'/'.$systempay_response->get('expiry_year');
			$pcc->card_holder = null;

			$pcc->update();
		}

		$payment_ids = implode(', ', $payment_ids);
		$this->logger->logInfo("Payment information with ID(s) {$payment_ids} saved successfully for cart #{$order->id_cart}.");
	}

	private function isSuccessState($order)
	{
		$os = new OrderState($order->getCurrentState());

		// if state is one of supported states or custom state with paid flag
		return $os->id === (int)Configuration::get('PS_OS_PAYMENT')
				|| $os->id === (int)Configuration::get('PS_OS_OUTOFSTOCK')
				|| $os->id === (int)Configuration::get('SYSTEMPAY_OS_ONEY_PENDING')
				|| $os->id === (int)Configuration::get('SYSTEMPAY_OS_TRANS_PENDING')
				|| $os->id === (int)Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK')
				|| (bool)$os->paid;
	}

	private function getOrderPayment($order_ref, $trans_id)
	{
		$payment_id = Db::getInstance()->getValue(
				'SELECT `id_order_payment` FROM `'._DB_PREFIX_.'order_payment`
				WHERE `order_reference` = \''.$order_ref.'\' AND transaction_id = \''.$trans_id.'\''
		);

		if (!$payment_id)
			return false;

		return new OrderPayment((int)$payment_id);
	}

	public function isOney($systempay_response)
	{
		return $systempay_response->get('card_brand') == 'ONEY' || $systempay_response->get('card_brand') == 'ONEY_SANDBOX';
	}

	public function isOneyPendingPayment($systempay_response)
	{
		return $this->isOney($systempay_response) && $systempay_response->isPendingPayment();
	}

	public function isSofort($systempay_response)
	{
		return $systempay_response->get('card_brand') == 'SOFORT_BANKING';
	}

	public function isSepa($systempay_response)
	{
		return $systempay_response->get('card_brand') == 'SDD';
	}

	public static function filterTransactions($trs)
	{
		$successful_states = array(
				'INITIAL', 'WAITING_AUTHORISATION', 'WAITING_AUTHORISATION_TO_VALIDATE',
				'UNDER_VERIFICATION', 'AUTHORISED', 'AUTHORISED_TO_VALIDATE', 'CAPTURED',
				'CAPTURE_FAILED' /* capture will be redone */
		);

		return $trs->{'operation_type'} == 'DEBIT' && in_array($trs->{'trans_status'}, $successful_states);
	}
}