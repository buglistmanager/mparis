<?php
/**
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*/

/**
 * Instant payment notification file. Wait for Systempay payment confirmation, then validate order.
 */
 
// @mail('regis@bigdistrict.com', 'Appel validation Systempay', date('Y-m-d H:i:s'));
// @mail('aimery@bigdistrict.com', 'Appel validation Systempay', date('Y-m-d H:i:s'));


if ($id_shop = substr($_REQUEST['vads_order_info2'], strlen('id_shop=')))
	$_GET['id_shop'] = $id_shop;

require_once dirname(dirname(dirname(__FILE__))).'/config/config.inc.php';

if (($cart_id = Tools::getValue('vads_order_id')) && ($hash = Tools::getValue('vads_hash')))
{
	/* module main class */
	require_once(dirname(__FILE__).'/systempay.php');
	$systempay = new Systempay();

	$systempay->logger->logInfo("Server call process starts for cart #$cart_id.");

	$cart = new Cart($cart_id);

	/* cart errors */
	if (!Validate::isLoadedObject($cart))
	{
		$systempay->logger->logError("Cart #$cart_id not found in database.");
		die('<span style="display:none">KO-'.Tools::getValue('vads_trans_id')."=Impossible de retrouver la commande\n</span>");
	}
	elseif ($cart->nbProducts() <= 0)
	{
		$systempay->logger->logError("Cart #$cart_id was emptied before redirection.");
		die('<span style="display:none">KO-'.Tools::getValue('vads_trans_id')."=Le panier a été vidé avant la redirection\n</span>");
	}

	/* reload context */
	Context::getContext()->cart = $cart;

	if (isset($cart->id_shop))
		Context::getContext()->shop = new Shop((int)$cart->id_shop);

	$address = new Address((int)$cart->id_address_invoice);
	Context::getContext()->country = new Country((int)$address->id_country);
	Context::getContext()->customer = new Customer((int)$cart->id_customer);
	Context::getContext()->language = new Language((int)$cart->id_lang);
	Context::getContext()->currency = new Currency((int)$cart->id_currency);

	/** @var SystempayResponse $systempay_response */
	$systempay_response = new SystempayResponse(
			$_POST,
			Configuration::get('SYSTEMPAY_MODE'),
			Configuration::get('SYSTEMPAY_KEY_TEST'),
			Configuration::get('SYSTEMPAY_KEY_PROD')
	);

	/* check the authenticity of the request */
	if (!$systempay_response->isAuthentified())
	{
		$systempay->logger->logError("Cart #$cart_id : authentication error !");
		die($systempay_response->getOutputForGateway('auth_fail'));
	}

	/* search order in db */
	$order_id = Order::getOrderByCartId($cart->id);

	if ($order_id == false)
	{
		/* order has not been processed yet */

		if ($systempay_response->isAcceptedPayment())
		{
			if ($systempay->isOneyPendingPayment($systempay_response))
				$new_state = Configuration::get('SYSTEMPAY_OS_ONEY_PENDING');
			elseif ($systempay->isSofort($systempay_response) || $systempay->isSepa($systempay_response))
				$new_state = Configuration::get('SYSTEMPAY_OS_TRANS_PENDING');
			else
				$new_state = Configuration::get('PS_OS_PAYMENT');

			$systempay->logger->logInfo("Payment accepted for cart #$cart_id. New order status is $new_state.");

			$order = $systempay->saveOrder($cart, $new_state, $systempay_response);

			if (number_format($order->total_paid, 2) != number_format($order->total_paid_real, 2))
			{
				/* amount paid not equals initial amount. */
				$systempay->logger->logWarning("Error: amount paid not equals initial amount. Order is in a failed status, cart #$cart_id.");
				die($systempay_response->getOutputForGateway('ko', 'Le montant payé est différent du montant intial'));
			}
			else
				/* response to server */
				die ($systempay_response->getOutputForGateway('payment_ok'));
		}
		else
		{
			/* payment KO */
			$systempay->logger->logInfo("Payment failed for cart #$cart_id.");

			if (Configuration::get('SYSTEMPAY_FAILURE_MANAGEMENT') == Systempay::ON_FAILURE_SAVE || $systempay->isOney($systempay_response))
			{
				/* save on failure option is selected or oney payment */
				$new_state = $systempay_response->isCancelledPayment() ? Configuration::get('PS_OS_CANCELED') : Configuration::get('PS_OS_ERROR');

				$msg = $systempay->isOney($systempay_response) ? 'FacilyPay Oney payment' : 'Save on failure option is selected';
				$systempay->logger->logInfo("$msg : save failed order for cart #$cart_id. New order status is $new_state.");
				$order = $systempay->saveOrder($cart, $new_state, $systempay_response);
			}
			die($systempay_response->getOutputForGateway('payment_ko'));
		}
	}
	else
	{
		/* order already registered */
		$systempay->logger->logInfo("Order already registered for cart #$cart_id.");

		$order = new Order((int)$order_id);
		$old_state = $order->getCurrentState();
		$systempay->logger->logInfo("Old status #$old_state.");
		switch ($old_state)
		{
			case Configuration::get('PS_OS_ERROR'):
			case Configuration::get('PS_OS_CANCELED'):

				$msg = $systempay->isOney($systempay_response) ? 'FacilyPay Oney payment' : 'Save on failure option is selected';
				$systempay->logger->logInfo("$msg. Order for cart #$cart_id is in a failed status.");

				if ($systempay_response->isAcceptedPayment())
				{
					/* order saved with failed status while payment is successful */

					if (number_format($order->total_paid, 2) != number_format($order->total_paid_real, 2))
					{
						/* amount paid not equals initial amount. */
						$systempay->logger->logWarning("Error: amount paid not equals initial amount. Order is in a failed status, cart #$cart_id.");
						die($systempay_response->getOutputForGateway('ko', 'Le montant payé est différent du montant intial'));
					}
					else
						$systempay->logger->logWarning("Error: payment success received from platform while order is in a failed status, cart #$cart_id.");

					$msg = 'payment_ko_on_order_ok';
				}
				else
				{
					/* just display a failure confirmation message */
					$systempay->logger->logInfo("Payment failure confirmed for cart #$cart_id.");
					$msg = 'payment_ko_already_done';
				}

				die($systempay_response->getOutputForGateway($msg));

			case (Configuration::get('SYSTEMPAY_OS_ONEY_PENDING') && ($old_state == Configuration::get('SYSTEMPAY_OS_ONEY_PENDING'))):
			case (($old_state == Configuration::get('PS_OS_OUTOFSTOCK')) && $systempay->isOney($systempay_response)):

				$systempay->logger->logInfo("Order for cart #$cart_id is saved but waiting FacilyPay Oney confirmation.
										Update order status according to payment result.");

				if ($systempay_response->isPendingPayment())
				{
					$systempay->logger->logInfo("No changes for cart #$cart_id status, payment remains pending confirmation.");
					$msg = 'payment_ok_already_done';
				}
				elseif ($systempay_response->isAcceptedPayment())
				{
					$new_state = ($old_state == Configuration::get('SYSTEMPAY_OS_ONEY_PENDING')) ?
								Configuration::get('PS_OS_PAYMENT') :
								Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK');
					$systempay->setOrderState($order, $new_state, $systempay_response);
					$msg = 'payment_ok';
				}
				else
				{
					/* order is pending, payment failed : update order status */
					$new_state = $systempay_response->isCancelledPayment() ? Configuration::get('PS_OS_CANCELED') : Configuration::get('PS_OS_ERROR');
					$systempay->setOrderState($order, $new_state, $systempay_response);
					$msg = 'payment_ko';
				}

				die($systempay_response->getOutputForGateway($msg));

			case Configuration::get('SYSTEMPAY_OS_TRANS_PENDING'):

				if ($systempay_response->isAcceptedPayment())
					$msg = 'payment_ok_already_done';
				else
				{
					/* order is pending, payment failed : update order status */
					$new_state = $systempay_response->isCancelledPayment() ? Configuration::get('PS_OS_CANCELED') : Configuration::get('PS_OS_ERROR');
					$systempay->setOrderState($order, $new_state, $systempay_response);
					$msg = 'payment_ko';
				}

				die($systempay_response->getOutputForGateway($msg));

			case Configuration::get('PS_OS_PAYMENT'):
			case (Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK') && ($old_state == Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK'))):
			case (($old_state == Configuration::get('PS_OS_OUTOFSTOCK')) && !$systempay->isOney($systempay_response)):

				if ($systempay_response->isAcceptedPayment())
				{
					/* just display a confirmation message */
					$systempay->logger->logInfo("Payment success confirmed for cart #$cart_id.");
					$msg = 'payment_ok_already_done';
				}
				else
				{
					/* order saved with success status while payment failed */
					$systempay->logger->logWarning("Error: payment failure received from platform while order is in a success status, cart #$cart_id.");
					$msg = 'payment_ko_on_order_ok';
				}

				die($systempay_response->getOutputForGateway($msg));

			default:

				$systempay->logger->logInfo("Unknown order status for cart #$cart_id. Managed by merchant.");

				die($systempay_response->getOutputForGateway('ok', 'Statut de commande inconnu'));
		}
	}
}