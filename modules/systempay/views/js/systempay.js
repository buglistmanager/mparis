/**
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*/

/**
 * Misc JavaScript functions.
 */

function systempayAddOption(first, deleteText) {
	if(first) {
		$('#systempay_multi_options_btn').css('display', 'none');
		$('#systempay_multi_options_table').css('display', '');
	}

	var timestamp = new Date().getTime();
	
	var optionLine = '<tr id="systempay_multi_option_' + timestamp + '">' +
					 '	<td><input name="SYSTEMPAY_MULTI_OPTIONS[' + timestamp + '][label]" style="width: 150px;" type="text"/></td>' + 
					 '	<td><input name="SYSTEMPAY_MULTI_OPTIONS[' + timestamp + '][amount_min]" style="width: 80px;" type="text"/></td>' +
					 '	<td><input name="SYSTEMPAY_MULTI_OPTIONS[' + timestamp + '][amount_max]" style="width: 80px;" type="text"/></td>' +
					 '	<td><input name="SYSTEMPAY_MULTI_OPTIONS[' + timestamp + '][contract]" style="width: 70px;" type="text"/></td>' +
					 '	<td><input name="SYSTEMPAY_MULTI_OPTIONS[' + timestamp + '][count]" style="width: 70px;" type="text"/></td>' +
					 '	<td><input name="SYSTEMPAY_MULTI_OPTIONS[' + timestamp + '][period]" style="width: 70px;" type="text"/></td>' +
					 '	<td><input name="SYSTEMPAY_MULTI_OPTIONS[' + timestamp + '][first]" style="width: 70px;" type="text"/></td>' +
					 '	<td><input type="button" value="' + deleteText + '" onclick="javascript: systempayDeleteOption(' + timestamp + ');"/></td>' +
					 '</tr>';

	$(optionLine).insertBefore('#systempay_multi_option_add');
}

function systempayDeleteOption(key) {
	$('#systempay_multi_option_' + key).remove();

	if($('#systempay_multi_options_table tbody tr').length == 1) {
		$('#systempay_multi_options_btn').css('display', '');
		$('#systempay_multi_options_table').css('display', 'none');
	}	
}

function systempayCategoryTableVisibility() {
	var category = $('select#SYSTEMPAY_COMMON_CATEGORY option:selected').val();

	if(category == 'CUSTOM_MAPPING') {
		$('.systempay_category_mapping').css('display', '');
		$('.systempay_category_mapping select').removeAttr('disabled');
	} else {
		$('.systempay_category_mapping').css('display', 'none');
		$('.systempay_category_mapping select').attr('disabled', 'disabled');
	}
}

function systempayDeliveryTypeChanged(key) {
	var type = $('#SYSTEMPAY_ONEY_SHIP_OPTIONS[' + key + '][type]').val();

	if(type == 'RECLAIM_IN_SHOP') {
		$('#SYSTEMPAY_ONEY_SHIP_OPTIONS[' + key + '][address]').css('display', '');
	} else {
		$('#SYSTEMPAY_ONEY_SHIP_OPTIONS[' + key + '][address]').val('');
		$('#SYSTEMPAY_ONEY_SHIP_OPTIONS[' + key + '][address]').css('display', 'none');
	}
}

function systempayOneyContractChanged() {
	var hasContract = $('select#SYSTEMPAY_ONEY_CONTRACT option:selected').val() == '1';

	if(hasContract) {
		$('.systempay_oney_ship_options').css('display', '');
		$('.systempay_oney_ship_options select, .systempay_oney_ship_options input').removeAttr('disabled');
	} else {
		$('.systempay_oney_ship_options').css('display', 'none');
		$('.systempay_oney_ship_options select, .systempay_oney_ship_options input').attr('disabled', 'disabled');
	}
}