{*
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*}

{capture name=path}Systempay{/capture}
{if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
	{include file="$tpl_dir./breadcrumb.tpl"}
{/if}

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if isset($systempay_params) && $systempay_params.vads_action_mode == 'SILENT'}
	<h2 class="page-heading">{l s='Payment processing' mod='systempay'}</h2>
{else}
	<h2 class="page-heading">{l s='Redirection to payment gateway' mod='systempay'}</h2>
{/if}


{if isset($systempay_empty_cart) && $systempay_empty_cart}
	<p class="warning">{l s='Your shopping cart is empty.' mod='systempay'}</p>
{else}
	{* <!--<h3>{l s='Payment by bank card' mod='systempay'}</h3> --> *}

	<form action="{$systempay_url|strval}" method="post" id="systempay_form" name="systempay_form">
		{foreach from=$systempay_params key='key' item='value'}
			<input type="hidden" name="{$key|strval}" value="{$value|strval}" />
		{/foreach}

		<p class="cart_navigation payment_secure clearfix ta_c" style="padding-top: 30px; float: left; width: 100%; margin-bottom: 30px">
			{l s='secure payment'} <img class="paymentsecure" src="{$img_dir}paiement-securise.png" alt="{$shop_name|escape:'html':'UTF-8'}" />
		</p>
		<p class="ta_c" id="msgpmt">
		{* <!-- <img src="{$base_dir_ssl|strval}modules/systempay/views/img/{$systempay_logo|strval}" alt="Systempay" style="margin-bottom: 5px" /> --> *}
			<br />

			{if $systempay_params.vads_action_mode == 'SILENT'}
				{l s='Please wait a moment. Your order payment is now processing.' mod='systempay'}
			{else}
				{l s='Please wait, you will be redirected to the payment platform.' mod='systempay'}
			{/if}
			<br />
			{l s='If nothing happens in 10 seconds, please click the button below.' mod='systempay'}
			<br /><br />
		</p>

	{if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
		<p class="cart_navigation">
			<input type="submit" name="submitPayment" value="{l s='Pay' mod='systempay'}" class="exclusive" />
		</p>
	{else}
		<p class="cart_navigation clearfix">
			<button type="submit" name="submitPayment" class="button btn btn-default standard-checkout button-medium" >
				<span>{l s='Pay' mod='systempay'}</span>
			</button>
		</p>
	{/if}
	</form>

	<script type="text/javascript">
		{literal}
			$(function() {
				setTimeout(function() {
					$('#systempay_form').submit();
				}, 1000);
			});
		{/literal}
	</script>
{/if}