{*
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
<div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}
<div class="payment_module systempay">
	<a class="unclickable" href="javascript: void(0);">
		<img class="logo" src="{$base_dir_ssl|strval}modules/systempay/views/img/{$systempay_logo|strval}" alt="Systempay"/>{$systempay_title|strval}

		<br />
		<form>
			<span style="color: red;">
				{foreach from=$systempay_errors item="error"}
					{$error|strval} <br />
				{/foreach}
			</span>
		</form>
	</a>
</div>
{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
</div></div>
{/if}