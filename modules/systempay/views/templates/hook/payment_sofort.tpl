{*
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
<div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}
<div class="payment_module systempay">
	<a onclick="javascript: $('#systempay_sofort').submit();" title="{l s='Click here to pay with SOFORT Banking' mod='systempay'}" href="javascript: void(0);">
		<img class="logo" src="{$base_dir_ssl|strval}modules/systempay/views/img/sofort_banking.png" alt="Systempay"/>{$systempay_title|strval}

		<form action="{$link->getModuleLink('systempay', 'redirect', array(), true)|strval}" method="post" id="systempay_sofort">
			<input type="hidden" name="systempay_payment_type" value="sofort" />
		</form>
	</a>
</div>
{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
</div></div>
{/if}