{*
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
<div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}
<div class="payment_module systempay">
	{if $systempay_std_card_data_mode == 1}
		<a onclick="javascript: $('#systempay_standard').submit();" title="{l s='Click here to pay by bank card' mod='systempay'}" href="javascript: void(0);">
	{else} 
		<a class="unclickable" title="{l s='Enter payment information and click «Pay now» button' mod='systempay'}">
	{/if}
		<img class="logo" src="{$base_dir_ssl|strval}modules/systempay/views/img/BannerLogo1.png" alt="Systempay"/>{$systempay_title}

		<form action="{$link->getModuleLink('systempay', 'redirect', array(), true)|strval}" method="post" id="systempay_standard">
			<input type="hidden" name="systempay_payment_type" value="standard" />

			{if ($systempay_std_card_data_mode == 2) OR ($systempay_std_card_data_mode == 3)}
				<br />

				{assign var=first value=true}
				{foreach from=$systempay_avail_cards key="key" item="label"}
					<div style="display: inline-block;">
						{if $systempay_avail_cards|@count == 1}
							<input type="hidden" id="systempay_card_type_{$key|strval}" name="systempay_card_type" value="{$key|strval}" >
						{else}
							<input type="radio" id="systempay_card_type_{$key|strval}" name="systempay_card_type" value="{$key|strval}" style="vertical-align: middle;"{if $first == true} checked="checked"{/if} >
						{/if}

						<label for="systempay_card_type_{$key}">
							{assign var="card_logo" value="{$smarty.const._PS_MODULE_DIR_}systempay/views/img/{$key|lower}.png"}
							<img src="{if file_exists($card_logo)}{$base_dir_ssl|strval}modules/systempay/views/img/{$key|lower}.png{/if}" alt="{$label}" title="{$label}" class="card" >
						</label>

						{assign var=first value=false}
					</div>
				{/foreach}
				<br />
				<div style="margin-bottom: 12px;"></div>

				{if $systempay_std_card_data_mode == 3}
					<label for="systempay_card_number"> {l s='Card number' mod='systempay'}</label><br />
					<input type="text" name="systempay_card_number" value="" autocomplete="off" maxlength="19" id="systempay_card_number" style="max-width: 220px;" maxlength="16" class="data" >
					<br />

					<label for="systempay_cvv"> {l s='CVV' mod='systempay'}</label><br />
					<input type="text" name="systempay_cvv" value="" autocomplete="off" maxlength="4" id="systempay_cvv" style="max-width: 55px;" maxlength="4" class="data" >
					<br />

					<label for="systempay_expiry_month">{l s='Expiration date' mod='systempay'}</label><br />
					<select name="systempay_expiry_month" id="systempay_expiry_month" style="width: 90px;" class="data">
						<option value="">{l s='Month' mod='systempay'}</option>
						{section name=expiry start=1 loop=13 step=1}
						<option value="{$smarty.section.expiry.index|intval}">{$smarty.section.expiry.index|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}</option>
						{/section}
					</select>

					<select name="systempay_expiry_year" id="systempay_expiry_year" style="width: 90px;" class="data">
						<option value="">{l s='Year' mod='systempay'}</option>
						{assign var=year value=$smarty.now|date_format:"%Y"}
						{section name=expiry start=$year loop=$year+9 step=1}
						<option value="{$smarty.section.expiry.index|intval}">{$smarty.section.expiry.index|intval}</option>
						{/section}
					</select>
					<br />
				{/if}

				{if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
					<input type="submit" name="submit" value="{l s='Pay now' mod='systempay'}" class="button" />
				{else}
					<button type="submit" name="submit" class="button btn btn-default standard-checkout button-medium" >
						<span>{l s='Pay now' mod='systempay'}</span>
					</button>
				{/if}
			{/if}
		</form>
	</a>

	{if $systempay_std_card_data_mode == 3} {literal}
		<script type="text/javascript">
			$(document).ready(function() {
				$('#systempay_standard').bind('submit', function() {
					$('#systempay_standard input, #systempay_standard select').each(function() {
						$(this).removeClass('invalid');
					});

					var cardNumber = $('#systempay_card_number').val();
					if(cardNumber.length <= 0 || !(/^\d{13,19}$/.test(cardNumber))){
						$('#systempay_card_number').addClass('invalid');
					}

					var cvv = $('#systempay_cvv').val();
					if(cvv.length <= 0 || !(/^\d{3,4}$/.test(cvv))) {
						$('#systempay_cvv').addClass('invalid');
					}

					var currentTime  = new Date();
					var currentMonth = currentTime.getMonth() + 1;
					var currentYear  = currentTime.getFullYear();

					var expiryYear = $('select[name="systempay_expiry_year"] option:selected').val();
					if(expiryYear.length <= 0 || !(/^\d{4,4}$/.test(expiryYear)) || expiryYear < currentYear) {
						$('#systempay_expiry_year').addClass('invalid');
					}

					var expiryMonth = $('select[name="systempay_expiry_month"] option:selected').val();
					if(expiryMonth.length <= 0 || !(/^\d{1,2}$/.test(expiryMonth)) || (expiryYear == currentYear && expiryMonth < currentMonth)) {
						$('#systempay_expiry_month').addClass('invalid');
					}

					return ($('#systempay_standard').find('.invalid').length > 0) ? false : true;
				});
			});
		</script>
	{/literal} {/if}
</div>
{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
</div></div>
{/if}