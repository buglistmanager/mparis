{*
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*}

{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
<div class="row"><div class="col-xs-12{if version_compare($smarty.const._PS_VERSION_, '1.6.0.11', '<')} col-md-6{/if}">
{/if}
<div class="payment_module systempay multi">
	<a class="unclickable" title="{l s='Click on a payment option to pay in several times' mod='systempay'}" href="javascript: void(0);">
		<img class="logo" src="{$base_dir_ssl|strval}modules/systempay/views/img/BannerLogo2.png" alt="Systempay"/>{$systempay_title|strval}

		<form action="{$link->getModuleLink('systempay', 'redirect', array(), true)|strval}" method="post" id="systempay_multi">
			<input type="hidden" name="systempay_payment_type" value="multi" />
			<input type="hidden" name="systempay_opt" value="" id="systempay_opt" />
			<br />

			<p>{l s='Click below to choose a payment in several times :' mod='systempay'}</p>
			<ul>
				{foreach from=$systempay_multi_options key="key" item="option"}
					<li onclick="javascript: $('#systempay_opt').val('{$key|strval}'); $('#systempay_multi').submit();">
						{$option.label|strval}
					</li>
				{/foreach}
			</ul>
		</form>
	</a>
</div>
{if version_compare($smarty.const._PS_VERSION_, '1.6', '>=')}
</div></div>
{/if}