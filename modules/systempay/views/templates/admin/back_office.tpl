{*
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*}

<script>
	$(function() {
		$('#accordion').accordion({
			active: false,
			collapsible: true,
			autoHeight: false,
			heightStyle: 'content',
			header: 'h4',
			animated: false
		});
	});
 </script>

<form method="POST" action="{$systempay_request_uri|strval}" class="defaultForm form-horizontal">
	<div style="width: 100%;">
		{$systempay_common|strval}
	</div>

	<br /><br />

	<div id="accordion" style="width: 100%; float: none;">
		<h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
			<a href="#">{l s='GENERAL CONFIGURATION' mod='systempay'}</a>
		</h4>
		<div>
			{$systempay_general_tab|strval}
		</div>

		<h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
			<a href="#">{l s='ONE-TIME PAYMENT' mod='systempay'}</a>
		</h4>
		<div>
			{$systempay_single_tab|strval}
		</div>

		<h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
			<a href="#">{l s='PAYMENT IN SEVERAL TIMES' mod='systempay'}</a>
		</h4>
		<div>
			{$systempay_multi_tab|strval}
		</div>

		<!--BEGIN_ONEY_CODE-->
		<h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
			<a href="#">{l s='FACILYPAY ONEY PAYMENT' mod='systempay'}</a>
		</h4>
		<div>
			{$systempay_oney_tab|strval}
		</div>
		<!--END_ONEY_CODE-->

		<!--BEGIN_ANCV_CODE-->
		<h4 style="font-weight: bold; margin-bottom: 0; overflow: hidden; line-height: unset !important;">
			<a href="#">{l s='ANCV PAYMENT' mod='systempay'}</a>
		</h4>
		<div>
			{$systempay_ancv_tab|strval}
		</div>
		<!--END_ANCV_CODE-->
	</div>

	{if version_compare($smarty.const._PS_VERSION_, '1.6', '<')}
		<div class="clear" style="width: 100%;">
			<input type="submit" class="button" name="systempay_submit_admin_form" value="{l s='Save' mod='systempay'}" style="float: right;"/>
		</div>
	{else}
		<div class="panel-footer" style="width: 100%;">
			<button type="submit" value="1" name="systempay_submit_admin_form" class="btn btn-default pull-right" style="float: right !important;">
				<i class="process-icon-save"></i>
				{l s='Save' mod='systempay'}
			</button>
		</div>
	{/if}
</form>

<br />
<br />