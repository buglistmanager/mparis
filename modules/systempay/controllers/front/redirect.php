<?php
/**
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*/

/**
 * This controller prepares form and redirects to Systempay payment gateway.
 */
class SystempayRedirectModuleFrontController extends ModuleFrontController
{
	private $accepted_payment_types = array('standard', 'multi', 'oney', 'ancv' , 'sepa', 'sofort');

	public function __construct()
	{
		parent::__construct();

		$this->display_column_left = false;
		$this->display_column_right = false;
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();

		if ($this->context->cart->nbProducts() <= 0)
			$this->context->smarty->assign('systempay_empty_cart', true);
		else
		{
			$this->context->smarty->assign('systempay_empty_cart', false);

			$data = array();
			$logo = '';

			$module = new Systempay();

			$type = Tools::getValue('systempay_payment_type'); /* the selected Systempay payment type */
			if (!in_array($type, $this->accepted_payment_types))
			{
				$module->logger->logInfo('Error: payment type "'.$type.'" is not supported. Load standard payment as default.');
				$module->logger->logInfo('Request data : '.print_r($_REQUEST, true));

				$type = 'standard'; // by default, payment is standard
			}

			switch ($type)
			{
				case 'standard' :
					if (Configuration::get('SYSTEMPAY_STD_CARD_DATA_MODE') == 2 || Configuration::get('SYSTEMPAY_STD_CARD_DATA_MODE') == 3)
					{
						$data['card_type'] = Tools::getValue('systempay_card_type');

						if (Configuration::get('SYSTEMPAY_STD_CARD_DATA_MODE') == 3)
						{
							$data['card_number'] = Tools::getValue('systempay_card_number');
							$data['cvv'] = Tools::getValue('systempay_cvv');
							$data['expiry_month'] = Tools::getValue('systempay_expiry_month');
							$data['expiry_year'] = Tools::getValue('systempay_expiry_year');
						}
					}

					$logo = 'BannerLogo1.png';
					break;

				case 'multi' :
					$data['opt'] = Tools::getValue('systempay_opt');

					$logo = 'BannerLogo2.png';
					break;

				case 'oney' :
					$logo = 'oney.png';
					break;

				case 'ancv' :
					$logo = 'e_cv.png';
					break;

				case 'sepa' :
					$logo = 'sdd.png';
					break;

				case 'sofort' :
					$logo = 'sofort_banking.png';
					break;
			}

			$params = $module->getFormFields($type, $data);

			$module->logger->logInfo('Data to be sent to payment platform : '.print_r($params, true));

			$this->context->smarty->assign('systempay_params', $params);
			$this->context->smarty->assign('systempay_url', Configuration::get('SYSTEMPAY_PLATFORM_URL'));
			$this->context->smarty->assign('systempay_logo', $logo);
		}

		$this->setTemplate('redirect.tpl');
	}
}