<?php
/**
 * Systempay V2-Payment Module version 1.5.0 (revision 66901) for Prestashop 1.5-1.6.
 *
 * Copyright (C) 2014-2015 Lyra Network and contributors
 * Support contact : supportvad@lyra-network.com
 * Author link : http://www.lyra-network.com/
 *
 * NOTICE OF LICENSE
 *
 * This source file is licensed under the Open Software License version 3.0
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category  payment
 * @package   systempay
 * @author    Lyra Network <supportvad@lyra-network.com>
 * @copyright 2014-2015 Lyra Network and contributors
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version   1.5.0 (revision 66901)
*/

/**
 * This controller manages return from Systempay payment gateway.
 */
class SystempaySubmitModuleFrontController extends ModuleFrontController
{
	public $display_column_left = false;

	private $current_cart;

	public function postProcess()
	{
		$cart_id = Tools::getValue('vads_order_id');
		$this->current_cart = new Cart((int)$cart_id);

		$this->module->logger->logInfo("User return to shop process starts for cart #$cart_id.");

		// cart errors
		if (!Validate::isLoadedObject($this->current_cart) || $this->current_cart->nbProducts() <= 0)
		{
			$this->module->logger->logWarning("Cart is empty, redirect to home page. Cart ID: $cart_id.");
			Tools::redirectLink('index.php');
		}

		if ($this->current_cart->id_customer == 0 || $this->current_cart->id_address_delivery == 0
				|| $this->current_cart->id_address_invoice == 0 || !$this->module->active)
		{
			$this->module->logger->logWarning("No address selected for customer or module disabled, redirect to checkout first page. Cart ID: $cart_id.");
			Tools::redirect('index.php?controller=order&step=1');
		}

		$customer = new Customer($this->current_cart->id_customer);
		if (!Validate::isLoadedObject($customer))
		{
			$this->module->logger->logWarning("Customer not logged in, redirect to checkout first page. Cart ID: $cart_id.");
			Tools::redirect('index.php?controller=order&step=1');
		}

		$this->processPaymentReturn();
	}

	private function processPaymentReturn()
	{
		/** @var SystempayResponse $systempay_response */
		$systempay_response = new SystempayResponse(
				$_REQUEST,
				Configuration::get('SYSTEMPAY_MODE'),
				Configuration::get('SYSTEMPAY_KEY_TEST'),
				Configuration::get('SYSTEMPAY_KEY_PROD')
		);

		$cart_id = $this->current_cart->id;

		// check the authenticity of the request
		if (!$systempay_response->isAuthentified())
		{
			$this->module->logger->logError("Cart #$cart_id : authentication error ! Redirect to home page.");
			Tools::redirectLink('index.php');
		}

		// search order in db
		$order_id = Order::getOrderByCartId($cart_id);

		if ($order_id == false)
		{
			// order has not been processed yet

			if ($systempay_response->isAcceptedPayment())
			{
				$this->module->logger->logWarning("Payment for cart #$cart_id has been processed by client return ! This means the IPN URL did not work.");

				if ($this->module->isOneyPendingPayment($systempay_response))
					$new_state = Configuration::get('SYSTEMPAY_OS_ONEY_PENDING');
				elseif ($this->module->isSofort($systempay_response) || $this->module->isSepa($systempay_response))
					$new_state = Configuration::get('SYSTEMPAY_OS_TRANS_PENDING');
				else
					$new_state = Configuration::get('PS_OS_PAYMENT');

				$this->module->logger->logInfo("Payment accepted for cart #$cart_id. New order status is $new_state.");
				$order = $this->module->saveOrder($this->current_cart, $new_state, $systempay_response);

				// redirect to success page
				$this->redirectSuccess($order, $this->module->id, true);
			}
			else
			{
				// payment KO

				if (Configuration::get('SYSTEMPAY_FAILURE_MANAGEMENT') == Systempay::ON_FAILURE_SAVE || $this->module->isOney($systempay_response))
				{
					// save on failure option is selected or oney payment : save order and go to history page
					$new_state = $systempay_response->isCancelledPayment() ? Configuration::get('PS_OS_CANCELED') : Configuration::get('PS_OS_ERROR');

					$this->module->logger->logWarning("Payment for order #$cart_id has been processed by client return ! This means the IPN URL did not work.");

					$msg = $this->module->isOney($systempay_response) ? 'FacilyPay Oney payment' : 'Save on failure option is selected';
					$this->module->logger->logInfo("$msg : save failed order for cart #$cart_id. New order status is $new_state.");

					$order = $this->module->saveOrder($this->current_cart, $new_state, $systempay_response);

					$this->module->logger->logInfo("Redirect to history page, cart ID : #$cart_id.");
					Tools::redirect('index.php?controller=history');
				}
				else
				{
					// option 2 choosen : get back to checkout process and show message
					$this->module->logger->logInfo("Payment failed, redirect to order checkout page, cart ID : #$cart_id.");

					$controller = Configuration::get('PS_ORDER_PROCESS_TYPE') ? 'order-opc' : 'order'
							.(version_compare(_PS_VERSION_, '1.5.1', '>=') ? '&step=3' : '');
					Tools::redirect('index.php?controller='.$controller.'&systempay_pay_error=yes');
				}
			}
		}
		else
		{
			// order already registered
			$this->module->logger->logInfo("Order already registered for cart #$cart_id.");

			$order = new Order((int)$order_id);
			$old_state = $order->getCurrentState();

			switch ($old_state)
			{
				case Configuration::get('PS_OS_ERROR'):
				case Configuration::get('PS_OS_CANCELED'):

					$msg = $this->module->isOney($systempay_response) ? 'FacilyPay Oney payment' : 'Save on failure option is selected';
					$this->module->logger->logInfo("$msg. Order for cart #$cart_id is in a failed status.");

					if ($systempay_response->isAcceptedPayment())
					{
						// order saved with failed status while payment is successful
						if (number_format($order->total_paid, 2) != number_format($order->total_paid_real, 2))
							// amount paid not equals initial amount.
							$this->module->logger->logWarning("Error: amount paid not equals initial amount. Order is in a failed status, cart #$cart_id.");
						else
							// order status error
							$this->module->logger->logWarning("Error: payment success received from platform while order is in a failed status, cart #$cart_id.");

						Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart_id
								.'&id_module='.$this->module->id.'&id_order='.$order->id
								.'&key='.$order->secure_key.'&error=yes');
					}
					else
					{
						// just redirect to order history page
						$this->module->logger->logInfo("Payment failure confirmed for cart #$cart_id. Redirect to history page.");
						Tools::redirect('index.php?controller=history');
					}

					break;

				case (Configuration::get('SYSTEMPAY_OS_ONEY_PENDING') && ($old_state == Configuration::get('SYSTEMPAY_OS_ONEY_PENDING'))):
				case (($old_state == Configuration::get('PS_OS_OUTOFSTOCK')) && $this->module->isOney($systempay_response)):

					$this->module->logger->logInfo("Order for cart #$cart_id is saved but waiting FacilyPay Oney confirmation.");

					if ($systempay_response->isPendingPayment())
					{
						// redirect to success page
						$this->module->logger->logInfo("FacilyPay Oney pending status confirmed for cart #$cart_id. Just redirect to success page.");
						$this->redirectSuccess($order, $this->module->id);
					}
					else
					{
						// order is pending Oney confirmation, payment is not pending : error case
						$this->module->logger->logWarning("Error: order saved with FacilyPay Oney pending status while payment is not pending, cart ID : #$cart_id.");
						Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart_id
								.'&id_module='.$this->module->id.'&id_order='.$order->id
								.'&key='.$order->secure_key.'&error=yes');
					}

					break;

				case Configuration::get('SYSTEMPAY_OS_TRANS_PENDING'):
				case Configuration::get('PS_OS_PAYMENT'):
				case (Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK') && ($old_state == Configuration::get('SYSTEMPAY_OS_PAYMENT_OUTOFSTOCK'))):
				case (($old_state == Configuration::get('PS_OS_OUTOFSTOCK')) && !$this->module->isOney($systempay_response)):

					if ($systempay_response->isAcceptedPayment())
					{
						// redirect to success page
						$this->module->logger->logInfo("Payment success confirmed for cart #$cart_id. Just redirect to success page.");
						$this->redirectSuccess($order, $this->module->id);
					}
					else
					{
						// order saved with success status while payment failed
						$this->module->logger->logWarning("Error: payment failure received from platform while order is in a success status, cart ID : #$cart_id.");
						Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart_id
								.'&id_module='.$this->module->id.'&id_order='.$order->id
								.'&key='.$order->secure_key.'&error=yes');
					}

					break;

				default:

					// order saved with unmanaged status, redirect client according to payment result
					$this->module->logger->logInfo("Order saved with unmanaged status for cart #$cart_id, redirect client according to payment result.");

					if ($systempay_response->isAcceptedPayment())
					{
						// redirect to success page
						$this->module->logger->logInfo("Payment success for cart #$cart_id. Redirect to success page.");
						$this->redirectSuccess($order, $this->module->id);
					}
					else
					{
						$this->module->logger->logInfo("Payment failure for cart #$cart_id. Redirect to history page.");
						Tools::redirect('index.php?controller=history');
					}

					break;
			}
		}
	}

	private function redirectSuccess($order, $id_module, $check = false)
	{
		// display a confirmation message

		$link = 'index.php?controller=order-confirmation&id_cart='.$order->id_cart
				.'&id_module='.$id_module.'&id_order='.$order->id
				.'&key='.$order->secure_key;

		// amount paid not equals initial amount. Error !
		if (number_format($order->total_paid, 2) != number_format($order->total_paid_real, 2))
			$link .= '&error=yes';

		if (Configuration::get('SYSTEMPAY_MODE') == 'TEST')
		{
			if ($check)
			{
				// TEST mode (user is the webmaster) : order has not been paid, but we receive a successful
				// payment code => IPN didn't work, so we display a warning
				$link .= '&check_url_warn=yes';
			}

			$link .= '&prod_info=yes';
		}

		Tools::redirect($link);
	}
}