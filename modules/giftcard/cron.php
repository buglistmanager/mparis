<?php
/**
 * GIFT CARD
 *
 *    @category pricing_promotion
 *    @author    Timactive - Romain DE VERA <support@timactive.com>
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         GIFT CARD                *
 **          V 1.0.0                 *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 */

ini_set ( 'memory_limit', '600M' );
if (function_exists('set_time_limit'))
	@set_time_limit ( 1200 );
include (dirname ( __FILE__ ).'/../../config/config.inc.php');
include (dirname ( __FILE__ ).'/../../init.php');
include (dirname ( __FILE__ ).'/giftcard.php');
$giftcard = Module::getInstanceByName ( 'giftcard' );
if ($giftcard && $giftcard->active)
{
	$giftcard->accesslog = $giftcard->accessLogsDirectory ();
	if ($token = Tools::getValue ( 'token' ))
	{
		if (trim ( $token ) == trim ( Configuration::get ( 'GIFTCARD_TOKEN' ) ))
		{
			if (((int)date ( 'Ymd' ) > (int)Configuration::get ( 'GIFTCARD_BATCHLASTDATE' )))
			{
				Configuration::updateValue ( 'GIFTCARD_BATCHLASTDATE', (int)date ( 'Ymd' ) );
				$giftcard->sendPendingMailRecipient ();
				if ((int)Configuration::get ( 'GIFTCARD_TRIGGERWEB' ) == 0)
					die ( 'Gift card launched' );
			}
			else
			{
				$giftcard->loglongline ( 'The date '.(int)date ( 'Ymd' ).' is every worked' );
				if ((int)Configuration::get ( 'GIFTCARD_TRIGGERWEB' ) == 0)
					die ( 'The date '.(int)date ( 'Ymd' ).' is every worked' );
			}
		}
		else
		{
			$giftcard->loglongline ( 'Not a valid token' );
			if ((int)Configuration::get ( 'GIFTCARD_TRIGGERWEB' ) == 0)
				die ( 'Not a valid token' );
		}
	}
	else
	{
		$giftcard->loglongline ( 'Token is required' );
		if ((int)Configuration::get ( 'GIFTCARD_TRIGGERWEB' ) == 0)
			die ( 'Token is required' );
	}
	/* generate empty picture http://www.nexen.net/articles/dossier/16997-une_image_vide_sans_gd.php */
	if ((int)Configuration::get ( 'GIFTCARD_TRIGGERWEB' ) == 1)
	{
		$hex = '47494638396101000100800000ffffff00000021f90401000000002c00000000010001000002024401003b';
		$img = '';
		$t = Tools::strlen ( $hex ) / 2;
		for ($i = 0; $i < $t; $i ++)
			$img .= chr ( hexdec ( Tools::substr ( $hex, $i * 2, 2 ) ) );
		header ( 'Last-Modified: Fri, 01 Jan 1999 00:00 GMT', true, 200 );
		header ( 'Content-Length: '.Tools::strlen ( $img ) );
		header ( 'Content-Type: image/gif' );
		echo $img;
	}
	else
		die ( 'OK' );
}
else
{
	if ((int)Configuration::get ( 'GIFTCARD_TRIGGERWEB' ) == 0)
		die ( 'Gift card is desactived' );
}
