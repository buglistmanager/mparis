<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_c03d53b70feba4ea842510abecd6c45e'] = 'Photo';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_5e2ff470c4413ed3333b78b6c45e2c5f'] = 'Affichage devise';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_3601146c4e948c32b6424d2c0a7f0118'] = 'Prix';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_344d095f3685cd7909e2c2e89ea2bce0'] = 'Utilisation partielle';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_b00b85425e74ed2c85dc3119b78ff2c3'] = 'Frais de port offert';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_7a1920d61156abc05a60135aefe8bc67'] = 'Défaut';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_8cdf1c6345219fcfde50615a78ad3ce6'] = 'Boutique défaut';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_d3b206d196cd6be3a2764c1fb90b200f'] = 'Supprimer la sélection';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Supprimer la sélection?';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_ede4759c9afae620fd586628789fa304'] = 'Activer la sélection';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_ab7fd6e250b64a46027a996088fdff74'] = 'Désactiver la sélection';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_a098c6f7ed63782a8d5f917277f3a81d'] = 'Un modèle défaut est requis pour la création des images produits';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_193bbde39cb6cf9f53d2ace856a367e0'] = 'Une erreur s\'est produite pendant la création de l\'image à partir du modèle défaut.';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_6e2ef4251e0a17e9fdf092e9e071865b'] = 'Le bon a été créée avec succès';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_9c34fe9d05bccbb346eec96392857ef6'] = 'Le bon a été mis à jour avec succès';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_9556869542476a8e2288cc5f7d4092b6'] = 'Une erreur s\'est produite pendant la mise à jour';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_23d643a27605a124ebc91ad18a21b66e'] = 'Le champ \'nom\' est vide. Vous devez saisir un nom pour la langue défaut avant de pouvoir enregistrer.';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_b9a7535676a66ca537ed5d4c6a15f51a'] = 'Le champ \'prix\' est requis et doit être supérieur à 0';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_bf82064ed593c4b8f773403b14c9cf7f'] = 'Le champ ean13 n\'est pas valide';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_38b598df404e245a5514da56c1e71419'] = 'Le champs upc n\'est pas valide';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_dc24e60bf8e307a56e417944b521e0e2'] = 'Le champs période est requis et doit être supérieur à 0 et inférieur à 1000';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_0f5c3acf37a61219addff19aae347970'] = 'Le champs quantité est requis et doit être supérieur à 0';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_f7de1b71605a10ef04416effa4c6e09e'] = 'Sauvegarder et rester';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_e6bc658d1d1b80a5b522ad370116a2c9'] = 'Bon Cadeau';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_4e140ba723a03baa6948340bf90e2ef6'] = ' Nom:';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_6252c0f2c2ed83b7b06dfca86d4650bb'] = 'Invalide caractères:';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_386c339d37e737a436499d423a77df0c'] = 'Devise';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_1800367c230c28bdf3fd2e839345ea21'] = 'Bon cadeau seulement visible';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_14eaed34083f2053dfc768d756e0ec3c'] = 'Toutes devises';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_694e8d1f2ee056f98ee488bdc4982d73'] = 'Quantité';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_58a67a15610d2597611bb837b763702d'] = 'Période de validité';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_63d5049791d9d79d86e9a108b0a999ca'] = 'Référence';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_51b2e055027d9ef6255110810fb06207'] = 'EAN13';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_4fb223e52c7cfb3596cf0c4e345e07ed'] = 'UPC';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_b51a203cee37965537db75688feaef75'] = 'Ne s\'applique que si la valeur de la réduction est supérieure au montant total du panier.';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_45ec8a24a8f55af16fad097e8a4fb9fe'] = 'Si vous n\'autorisez pas les utilisations partielles, la valeur du bon de réduction sera abaissée à la valeur du panier. Si en revanche vous autorisez les utilisations partielles, un nouveau bon sera créé avec le solde.';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_9d55fc80bbb875322aa67fd22fc98469'] = 'Boutiques associées';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_5fb1f955b45e38e31789286a1790398d'] = 'Toutes';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_6f8497a354aa10bd46e65cdf8e39f6b6'] = 'Non défaut';
$_MODULE['<{giftcard}prestashop>admingiftcardcontroller_d9b5b2302d57f3d13d5387ba9c99daae'] = 'Une erreur s\'est produite, pendant la création';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_a240fa27925a635b08dc28c9e4f9216d'] = 'Commande';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_278c491bdd8a53618c149c4ac790da34'] = 'Modèle';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_3601146c4e948c32b6424d2c0a7f0118'] = 'Prix';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_5d6103b662f41b07e10687f03aca8fdc'] = 'Destinataire';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_ce26601dac0dea138b7295f02b7620a7'] = 'Client';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_ca0dbad92a874b2f69b549293387925e'] = 'Code';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_4059b0251f66a18cb56f544728796875'] = 'Info';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_4351cfebe4b61d8aa5efa1d020710005'] = 'Voir';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_1fc54c0ab4a0602ae1f8c5d22bd82446'] = 'Voir la personnalisation';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_b314b715132fb2f685cd7ac691706d84'] = 'Attente commande acceptée';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_cb65733b2ee55e1e4f0c489485801829'] = 'Envoi en erreur';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_0e22fe7d45f8e5632a4abf369b24e29c'] = 'Annulée';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_654a39eb70f05b3e91adbe05b050923e'] = 'Utilisé dans la commande #%s';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_f90bec5942e6aa8ef29c4a31aa68ff10'] = 'Non utilisée';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_1e895ae086994065ceec58cf7ceb1e5d'] = 'Email envoyé';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_fac61083c7bd8f697c4a05ef1c003fe7'] = 'L\'email est envoyé';
$_MODULE['<{giftcard}prestashop>admingiftcardordercontroller_342caf3cd7fb85056123866705766415'] = 'Email non envoyé, en attente de la date d\'envoi';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_be53a0541a6d36f6ecb879fa2c584b08'] = 'Image';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_57dea6f5039281b7fee517fc43bf3110'] = 'Vecteur';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_7a1920d61156abc05a60135aefe8bc67'] = 'Défaut';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_8cdf1c6345219fcfde50615a78ad3ce6'] = 'Boutique défaut';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_d3b206d196cd6be3a2764c1fb90b200f'] = 'Supprimer la sélection';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Supprimer les items sélectionnés?';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_ede4759c9afae620fd586628789fa304'] = 'Activer la sélection';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_ab7fd6e250b64a46027a996088fdff74'] = 'Désactiver la sélection';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_f7de1b71605a10ef04416effa4c6e09e'] = 'Sauvegarder et rester';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_f10b845272fcb0b2b1ee00eef8fb8869'] = 'Modèle bon cadeau';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_d9b5b2302d57f3d13d5387ba9c99daae'] = 'Un erreur c\'est produite pendant la création de l\'objet';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_5fb1f955b45e38e31789286a1790398d'] = 'Tous';
$_MODULE['<{giftcard}prestashop>admingiftcardtemplatecontroller_6f8497a354aa10bd46e65cdf8e39f6b6'] = 'Non défaut';
$_MODULE['<{giftcard}prestashop>choicegiftcard_607e1d854783c8229998ac2b5b6923d3'] = 'Token invalide';
$_MODULE['<{giftcard}prestashop>choicegiftcard_52bb80bdcba46b3da1bc035dc1cda14a'] = 'Le modèle est requis pour consulter l’aperçu PDF';
$_MODULE['<{giftcard}prestashop>choicegiftcard_f92b541cdcb956ab92d38a4a0f5e9d41'] = 'La carte est inactive, sélectionné une autre carte.';
$_MODULE['<{giftcard}prestashop>choicegiftcard_6fc8d8a3a966d91d8dc4ebba09ffafd8'] = 'Format de l\'adresse mail invalide';
$_MODULE['<{giftcard}prestashop>choicegiftcard_cb242fffcd3407fce1771b215547f488'] = 'Vous devez sélectionner un modèle';
$_MODULE['<{giftcard}prestashop>choicegiftcard_a3d10c3ffadac2467e4f7ef428eb0e5c'] = 'Le modèle n\'est pas valide, merci de sélectionner un autre modèle';
$_MODULE['<{giftcard}prestashop>choicegiftcard_d9b3c25946229b55a490acdcc478ce2f'] = 'De la part est requis';
$_MODULE['<{giftcard}prestashop>choicegiftcard_ec8214037ef63653aa5273f30fe68e1b'] = 'Le champ %s contient un ou plusieurs caractères invalides';
$_MODULE['<{giftcard}prestashop>choicegiftcard_d98a07f84921b24ee30f86fd8cd85c3c'] = 'De la part de';
$_MODULE['<{giftcard}prestashop>choicegiftcard_e90982d764ff26bdd2460134e10fb406'] = 'Le prénom du bénéficiaire est requis';
$_MODULE['<{giftcard}prestashop>choicegiftcard_209f55887902a865bb6c999fc16912bb'] = 'Le prénom du bénéficiaire';
$_MODULE['<{giftcard}prestashop>choicegiftcard_caf57bac80302530124a084701e9c019'] = 'Message est requis';
$_MODULE['<{giftcard}prestashop>choicegiftcard_4c2a8fe7eaf24721cc7a9f0175115bd4'] = 'Message';
$_MODULE['<{giftcard}prestashop>choicegiftcard_3c47bb035bdb43672d1500cb05f5d1ce'] = 'La date d\'envoi de la carte est invalide';
$_MODULE['<{giftcard}prestashop>choicegiftcard_c2d9a6be0adeaf729d20718396379822'] = 'La date de l\'envoi de l\'email doit être supérieur à la date du jour';
$_MODULE['<{giftcard}prestashop>choicegiftcard_569506ad28c5ff23cc23444be654f354'] = 'Une erreur s\'est produite pendant la création du modèle';
$_MODULE['<{giftcard}prestashop>choicegiftcard_ad013ca5f69d114d0d07b59d41adf674'] = 'Une erreur s\'est produite pendant la création du modèle.';
$_MODULE['<{giftcard}prestashop>choicegiftcard_041023e70b06d609a9dae191d867c46e'] = 'Une erreur s\'est produite pendant la copie';
$_MODULE['<{giftcard}prestashop>choicegiftcard_911b2cb176e338abb2b28a7d11ef9494'] = 'La carte cadeau a été ajoutée à votre panier';
$_MODULE['<{giftcard}prestashop>informations_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{giftcard}prestashop>informations_092ff92c5c5fb215f214c07d9221e949'] = 'Sera affiché dans le récapitulatif du panier du client ainsi que sur la facture.';
$_MODULE['<{giftcard}prestashop>informations_386c339d37e737a436499d423a77df0c'] = 'Devise';
$_MODULE['<{giftcard}prestashop>informations_14eaed34083f2053dfc768d756e0ec3c'] = 'Toutes les devises';
$_MODULE['<{giftcard}prestashop>informations_e913ab4c27bcd1adbdb7e9465122c632'] = 'Carte cadeau seulement visible';
$_MODULE['<{giftcard}prestashop>informations_3601146c4e948c32b6424d2c0a7f0118'] = 'Prix';
$_MODULE['<{giftcard}prestashop>informations_694e8d1f2ee056f98ee488bdc4982d73'] = 'Quantité';
$_MODULE['<{giftcard}prestashop>informations_58a67a15610d2597611bb837b763702d'] = 'Période de validité';
$_MODULE['<{giftcard}prestashop>informations_7cbb885aa1164b390a0bc050a64e1812'] = 'Mois';
$_MODULE['<{giftcard}prestashop>informations_63d5049791d9d79d86e9a108b0a999ca'] = 'Référence';
$_MODULE['<{giftcard}prestashop>informations_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{giftcard}prestashop>informations_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{giftcard}prestashop>informations_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{giftcard}prestashop>formno_e6bc658d1d1b80a5b522ad370116a2c9'] = 'Carte Cadeau';
$_MODULE['<{giftcard}prestashop>formno_a82be0f551b8708bc08eb33cd9ded0cf'] = 'Information';
$_MODULE['<{giftcard}prestashop>formno_e19cae6a8af7eca4fa921d5b66c87814'] = 'Information de la carte cadeau';
$_MODULE['<{giftcard}prestashop>formno_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{giftcard}prestashop>custom_field_65fba9ec06765f60ab9510fff2bd2b16'] = 'Acheteur';
$_MODULE['<{giftcard}prestashop>custom_field_5da618e8e4b89c66fe86e32cdafde142'] = 'De la part';
$_MODULE['<{giftcard}prestashop>custom_field_1d58b02872ba12914a06fe38bf6dd266'] = 'Réception';
$_MODULE['<{giftcard}prestashop>custom_field_fa460b2b27e0abdc94218912661ed599'] = 'E-mail à une date spécique';
$_MODULE['<{giftcard}prestashop>custom_field_53a10624b93334d999f75d26b45f1bb4'] = 'Imprimer chez soi';
$_MODULE['<{giftcard}prestashop>custom_field_0ea0a2da60925a9b5d7d80e1ee55e5b6'] = 'Personne qui reçoit la carte';
$_MODULE['<{giftcard}prestashop>custom_field_0c4f82ac8c388cb72b1ba684f10fa098'] = 'Date d\'envoi';
$_MODULE['<{giftcard}prestashop>custom_field_4dfbb099eafd3c82e033bf92946d3ce6'] = 'Mail';
$_MODULE['<{giftcard}prestashop>custom_field_dff4bf10409100d989495c6d5486035e'] = 'Nom';
$_MODULE['<{giftcard}prestashop>custom_field_78e731027d8fd50ed642340b7c9a63b3'] = 'message';
$_MODULE['<{giftcard}prestashop>list_header_95923cff2ad4637372adcdf25e4581f8'] = 'Signification des statuts';
$_MODULE['<{giftcard}prestashop>list_header_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{giftcard}prestashop>list_header_b314b715132fb2f685cd7ac691706d84'] = 'Attente commande acceptée';
$_MODULE['<{giftcard}prestashop>list_header_051e85808dc9d2dac5a78ad21da24f98'] = 'La commande est en attente de paiement, ainsi la carte n\'est pas envoyée et le code de réduction correspondant n\'est pas généré';
$_MODULE['<{giftcard}prestashop>list_header_aadb3ee341867ca2327dd458d9f1e42b'] = 'Non utilisée';
$_MODULE['<{giftcard}prestashop>list_header_9bdd710c6874e8456d6431771b616c55'] = 'La carte cadeau est créée, cependant elle n\'est pas utilisée';
$_MODULE['<{giftcard}prestashop>list_header_019d1ca7d50cc54b995f60d456435e87'] = 'Utilisée';
$_MODULE['<{giftcard}prestashop>list_header_7e380c26d3327617e89ec781db11a80e'] = 'La carte cadeau est utilisée, vous pouvez consulter la commande associée.';
$_MODULE['<{giftcard}prestashop>list_header_ea4788705e6873b424c65e91c2846b19'] = 'Annulée';
$_MODULE['<{giftcard}prestashop>list_header_218af2f29d281d912433ef6076177da4'] = 'La carte cadeau a été annulée, par exemple la commande est annulée ou remboursée';
$_MODULE['<{giftcard}prestashop>customize_999ee65fd5205dff5305ce99ae1a54a5'] = 'La fonctionnalité de personnalisation requière l\'extension PHP Imagick, cette extension n\'est pas présente sur votre serveur.';
$_MODULE['<{giftcard}prestashop>customize_73e4ff6ffa29353a5bacfc71619b4183'] = 'Données variables';
$_MODULE['<{giftcard}prestashop>customize_d6effb5796170adc1333091e3d2d1edb'] = '0 pour ne pas afficher le prix sur le modèle';
$_MODULE['<{giftcard}prestashop>customize_3601146c4e948c32b6424d2c0a7f0118'] = 'Prix';
$_MODULE['<{giftcard}prestashop>customize_aa74397584da0fd719c7f349ba31e399'] = 'Code de réduction';
$_MODULE['<{giftcard}prestashop>customize_5da618e8e4b89c66fe86e32cdafde142'] = 'De la part';
$_MODULE['<{giftcard}prestashop>customize_dff4bf10409100d989495c6d5486035e'] = 'Prénom';
$_MODULE['<{giftcard}prestashop>customize_78e731027d8fd50ed642340b7c9a63b3'] = 'Message';
$_MODULE['<{giftcard}prestashop>customize_6ac11f1d1865be9280dde66a755b5b81'] = 'Expire';
$_MODULE['<{giftcard}prestashop>customize_c49529a6df6f4ad97b66871c4df32ba6'] = 'Texte personnalisable';
$_MODULE['<{giftcard}prestashop>customize_c67e704415318b8ac85c0e29ccf52f58'] = 'Couleur personnalisable';
$_MODULE['<{giftcard}prestashop>customize_bcd1b68617759b1dfcff0403a6b5a8d1'] = 'PDF';
$_MODULE['<{giftcard}prestashop>customize_d98a041006805772d5fcf402c466375c'] = 'Si activé, seul l\'image sera présente dans le PDF, le contenu paramétré dans la configuration du module sera ignoré.';
$_MODULE['<{giftcard}prestashop>customize_5a0130bad1222ff4ab8a88bd52e64255'] = 'PDF Image seul';
$_MODULE['<{giftcard}prestashop>customize_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{giftcard}prestashop>customize_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{giftcard}prestashop>customize_31fde7b05ac8952dacf4af8a704074ec'] = 'Prévisualiser';
$_MODULE['<{giftcard}prestashop>customize_4c2a8fe7eaf24721cc7a9f0175115bd4'] = 'Message';
$_MODULE['<{giftcard}prestashop>customize_4afd5767d4c6193da75ce3488c9076d3'] = 'Seulement l\'image';
$_MODULE['<{giftcard}prestashop>customize_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{giftcard}prestashop>customize_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{giftcard}prestashop>customize_e20a3ef58162098507fe519205ccfbcb'] = 'Langue utilisée pour prévisualiser';
$_MODULE['<{giftcard}prestashop>informations_d075eb73e62ee5941fdb32a02edd8644'] = 'Affiché dans la liste des modèles';
$_MODULE['<{giftcard}prestashop>informations_afa0e89407bf95c692f0de3516d41556'] = 'Si vous sélectionner une langue, le modèle sera affiché seulement dans cette langue';
$_MODULE['<{giftcard}prestashop>informations_4994a8ffeba4ac3140beb89e8d41f174'] = 'Language';
$_MODULE['<{giftcard}prestashop>informations_53ded23a712b29ac06cb364ea8be689b'] = 'Toutes les langues';
$_MODULE['<{giftcard}prestashop>informations_d1cad276c41899ad987ccf40441c1be2'] = 'Télécharger une image depuis votre ordinateur';
$_MODULE['<{giftcard}prestashop>informations_c2d0bf5ad42279c519cdcb4a94eb46b6'] = 'Boutiques associées:';
$_MODULE['<{giftcard}prestashop>informations_3e053943605d9e4bf7dd7588ea19e9d2'] = 'Caractères interdit :';
$_MODULE['<{giftcard}prestashop>informations_bb168dfc60807bb1ba1fbe643845238b'] = 'Tags séparé par une virgule (anniversaire, saint valentin, coeur,...)';
$_MODULE['<{giftcard}prestashop>informations_1908624a0bca678cd26b99bfd405324e'] = 'Taille fichier';
$_MODULE['<{giftcard}prestashop>form_f10b845272fcb0b2b1ee00eef8fb8869'] = 'Modèle carte cadeau';
$_MODULE['<{giftcard}prestashop>form_63a78ed4647f7c63c2929e35ec1c95e3'] = 'Personnaliser';
$_MODULE['<{giftcard}prestashop>form_3923bfba54b6d8294df02a7a264f0ff9'] = 'Information du modèle';
$_MODULE['<{giftcard}prestashop>form_47d44ed4727a9bed10ad17773b1b922e'] = 'Personnaliser le modèle';
$_MODULE['<{giftcard}prestashop>form_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{giftcard}prestashop>choicegiftcard_1306bade25c9c93111838baadc58300e'] = 'Carte cadeau';
$_MODULE['<{giftcard}prestashop>choicegiftcard_b2f40690858b404ed10e62bdf422c704'] = 'Montant';
$_MODULE['<{giftcard}prestashop>choicegiftcard_ae1470ad29c7462a4ab4e3885253e4be'] = 'Sélectionner un mode de réception';
$_MODULE['<{giftcard}prestashop>choicegiftcard_dfebb148d1368ade513f1732c8839e0c'] = 'Envoyer par e-mail';
$_MODULE['<{giftcard}prestashop>choicegiftcard_720ccd4a0625fb6f4d2983e5a52b1d6c'] = 'Bénéficiaire : email';
$_MODULE['<{giftcard}prestashop>choicegiftcard_bb82b5bf11254e1fead2aa45fa23851c'] = 'Date d\'envoi de la carte';
$_MODULE['<{giftcard}prestashop>choicegiftcard_86f5978d9b80124f509bdb71786e929e'] = 'Janvier';
$_MODULE['<{giftcard}prestashop>choicegiftcard_659e59f062c75f81259d22786d6c44aa'] = 'Février';
$_MODULE['<{giftcard}prestashop>choicegiftcard_fa3e5edac607a88d8fd7ecb9d6d67424'] = 'Mars';
$_MODULE['<{giftcard}prestashop>choicegiftcard_3fcf026bbfffb63fb24b8de9d0446949'] = 'Avril';
$_MODULE['<{giftcard}prestashop>choicegiftcard_195fbb57ffe7449796d23466085ce6d8'] = 'Mai';
$_MODULE['<{giftcard}prestashop>choicegiftcard_688937ccaf2a2b0c45a1c9bbba09698d'] = 'Juin';
$_MODULE['<{giftcard}prestashop>choicegiftcard_1b539f6f34e8503c97f6d3421346b63c'] = 'Juillet';
$_MODULE['<{giftcard}prestashop>choicegiftcard_41ba70891fb6f39327d8ccb9b1dafb84'] = 'Août';
$_MODULE['<{giftcard}prestashop>choicegiftcard_cc5d90569e1c8313c2b1c2aab1401174'] = 'Septembre';
$_MODULE['<{giftcard}prestashop>choicegiftcard_eca60ae8611369fe28a02e2ab8c5d12e'] = 'Octobre';
$_MODULE['<{giftcard}prestashop>choicegiftcard_7e823b37564da492ca1629b4732289a8'] = 'Novembre';
$_MODULE['<{giftcard}prestashop>choicegiftcard_82331503174acbae012b2004f6431fa5'] = 'Décembre';
$_MODULE['<{giftcard}prestashop>choicegiftcard_e925f4e4eef28c40d3ab8698e1914a05'] = 'Indiquer votre message';
$_MODULE['<{giftcard}prestashop>choicegiftcard_a5257a0b90b7f7f19bca83d0336269fb'] = 'caractères restants';
$_MODULE['<{giftcard}prestashop>choicegiftcard_61b728377e59eb7ac900896c4f9bd0fa'] = 'Dés que le paiement sera confirmé, la carte cadeau sera envoyée au bénéficiaire par e-mail. Vous pouvez choisir d\'envoyer la carte ultérieurement en sélectionnant une date ci-dessous.';
$_MODULE['<{giftcard}prestashop>choicegiftcard_31fde7b05ac8952dacf4af8a704074ec'] = 'Aperçu';
$_MODULE['<{giftcard}prestashop>choicegiftcard_2d0f6b8300be19cf35e89e66f0677f95'] = 'Ajouter au panier';
$_MODULE['<{giftcard}prestashop>admin_order_047b13a64bff8bbccce2108e3b83a9ce'] = 'Annuler la carte cadeau?';
$_MODULE['<{giftcard}prestashop>admin_order_e7dea18a6a3ce2d1be570a95a484c299'] = 'Activer la carte cadeau?';
$_MODULE['<{giftcard}prestashop>admin_order_a6ca74f6a9eab979ddbde680cd7db038'] = 'Carte cadeau achetée';
$_MODULE['<{giftcard}prestashop>admin_order_3f688b59bb8d216732ba68a0bd264757'] = 'La carte cadeau %1$s avec le code %2$s est utilisée cependant la commande est annulée';
$_MODULE['<{giftcard}prestashop>admin_order_627eaedf5c1477f753a6d7bbaa6d6405'] = 'La carte cadeau %1$s avec le code %2$s doit être annulée en effet la commande est annulée ou remboursée';
$_MODULE['<{giftcard}prestashop>admin_order_1135e4470c306e38db58d991837cd658'] = 'Montant réduction';
$_MODULE['<{giftcard}prestashop>admin_order_ca0dbad92a874b2f69b549293387925e'] = 'Code';
$_MODULE['<{giftcard}prestashop>admin_order_03ab340b3f99e03cff9e84314ead38c0'] = 'Qté';
$_MODULE['<{giftcard}prestashop>admin_order_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Activé';
$_MODULE['<{giftcard}prestashop>admin_order_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{giftcard}prestashop>admin_order_a10311459433adf322f2590a4987c423'] = 'activé';
$_MODULE['<{giftcard}prestashop>admin_order_075ae3d2fc31640504f814f60e5ef713'] = 'désactivé';
$_MODULE['<{giftcard}prestashop>admin_order_654a39eb70f05b3e91adbe05b050923e'] = 'Utilisée commande #%s';
$_MODULE['<{giftcard}prestashop>admin_order_1bda80f2be4d3658e0baa43fbe7ae8c1'] = 'voir';
$_MODULE['<{giftcard}prestashop>admin_order_92e592d90b9548016776a6fb68dccded'] = 'Non utilisée';
$_MODULE['<{giftcard}prestashop>admin_order_cfd24a56c5e99cf518e59d53a5833231'] = 'Attente commande valide';
$_MODULE['<{giftcard}prestashop>admin_order_0e22fe7d45f8e5632a4abf369b24e29c'] = 'Annulée';
$_MODULE['<{giftcard}prestashop>admin_order_902b0d55fddef6f8d651fe1035b7d4bd'] = 'Erreur';
$_MODULE['<{giftcard}prestashop>admin_order_74563371db148820f89d790136a4ed59'] = 'Voir PDF';
$_MODULE['<{giftcard}prestashop>admin_order_ad37680764427cd6e1602b27575caced'] = 'Carte utilisée aucun action disponible';
$_MODULE['<{giftcard}prestashop>admin_order_134f50f3e7118c42ed5d6ddfff4e3e31'] = 'Annuler la carte cadeau';
$_MODULE['<{giftcard}prestashop>admin_order_8104510b577f551afcabca9c48c2e935'] = 'Activer la carte cadeau';
$_MODULE['<{giftcard}prestashop>admin_order_2320f8f4b1253ec77ebf584c9560d27e'] = 'Carte cadeau utilisée';
$_MODULE['<{giftcard}prestashop>admin_order_fc0404b5b34824c62b2ad58ba2b5d3fd'] = 'Le client a utilisé la carte %1$s avec le code %2$s cependant la commande d\'achat #%3$s est annulée';
$_MODULE['<{giftcard}prestashop>admin_order_0e75d9736a52522519588a00416c7154'] = 'Le client a utilisé une carte cadeau dans cette commande, vous pouvez consulter dans cette liste, la commande d\'achat associée à cette carte cadeau';
$_MODULE['<{giftcard}prestashop>admin_order_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{giftcard}prestashop>admin_order_e38e5ef12954a20e30a68eceb9d87ade'] = 'Date commande';
$_MODULE['<{giftcard}prestashop>admin_order_49414cda71621b3ee718ae5ff40804c5'] = 'ID commande';
$_MODULE['<{giftcard}prestashop>admin_order_bbd3a18e6d45b8331cf8a715f3f0008d'] = 'Commande Statut';
$_MODULE['<{giftcard}prestashop>blockgiftcard_08638fbbe8a6bfe95f300351d5393ad6'] = 'Offrir une carte cadeau';
$_MODULE['<{giftcard}prestashop>blockgiftcard_58c25c4b8b4382e37d89e0ef6bd625cf'] = 'Cartes cadeaux';
