{*
 *
 * GIFT CARD
 *
 * @category pricing_promotion
 * @author Timactive - Romain DE VERA <support@timactive.com>
 * @copyright TIMACTIVE 2013
 * @version 1.0.0
 *
 *************************************
 **            GIFT CARD			 *              
 **             V 1.0.0              *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5
 *
 *}
<div id="giftcard_block" class="block">
  <h4>{l s='Offer a gift card' mod='giftcard'}</h4>
    <p class="block_content link_gift_cards">
		<a  href="{$link_choicegiftcard}"  title="{l s='Offer a gift card' mod='giftcard'} {$shop_name}">{l s='Gift cards' mod='giftcard'} {$shop_name}</a>
    </p>
</div>