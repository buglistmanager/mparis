{*
 *
 * GIFT CARD
 *
 * @category pricing_promotion
 * @author Timactive - Romain DE VERA <support@timactive.com>
 * @copyright TIMACTIVE 2013
 * @version 1.0.0
 *
 *************************************
 **            GIFT CARD			 *              
 **             V 1.0.0              *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5
 *
 *}
{* Variable used in js*}
<script type="text/javascript">
	var linkcgc_controller = '{$linkcgc|escape:'quotes'}';
	var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
	var currencyRate = '{$currencyRate|floatval}';
	var currencyFormat = '{$currencyFormat|intval}';
	var currencyBlank = '{$currencyBlank|intval}';
	var mailpreview_maxwidth = 500;
	var itemscarousel = [];
	{* fix nb item if width superior *}
	{literal}
	itemscarousel[0] = {width:980,nb:6};
	itemscarousel[1] = {width:800,nb:5};
	itemscarousel[2] = {width:768,nb:4};
	itemscarousel[2] = {width:450,nb:3};
	itemscarousel[4] = {width:320,nb:2};
	itemscarousel[5] = {width:0,nb:1};
	itemscarousel.sort(function(a, b){
 		return b.width-a.width;
	})
	{/literal}
</script>


<div id="choicegiftcard">
	{* visible when ajax call*}
	<div class="ui-loader-background"> </div>
	
		<!-- left infos-->
		<div class="pb-left-column col-xs-12 col-sm-4 col-md-5">
			<!-- product img-->
			<div id="image-block" class="clearfix">
				<span id="view_full_size">
						{if isset($templates) && count($templates) > 0}
														
							{foreach from=$templates item=template name=thumbnails}
																				
								<img src="{$link->getMediaLink("`$giftcard_templates_dir``$template.id_gift_card_template`/`$template.id_gift_card_template`-front{if $template.issvg}-{$id_lang}{/if}.jpg")}" alt="
								{$template.name|escape}" title="{$template.name|escape}">
																				
																				
							{/foreach}
						{/if}
				</span>
			</div> <!-- end image-block -->
			{*
			{if $HOOK_SHARE}
				<!-- usefull links-->
				<ul id="usefull_link_block" class="clearfix no-print">
					<li>{l s="Partager"} : </li>
					{if isset($HOOK_SHARE) && $HOOK_SHARE}{$HOOK_SHARE}{/if}
				</ul>
			{/if}
			*}
			{if isset($images) && count($images) > 0}
				<!-- thumbnails -->
				<div id="views_block" class="clearfix {if isset($images) && count($images) < 2}hidden{/if}">
					{if isset($images) && count($images) > 2}
						<span class="view_scroll_spacer">
							<a id="view_scroll_left" class="" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
								{l s='Previous'}
							</a>
						</span>
					{/if}
					<div id="thumbs_list">
						<ul id="thumbs_list_frame">
						{if isset($images)}
							{foreach from=$images item=image name=thumbnails}
								{assign var=imageIds value="`$product->id`-`$image.id_image`"}
								{if !empty($image.legend)}
									{assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
								{else}
									{assign var=imageTitle value=$product->name|escape:'html':'UTF-8'}
								{/if}
								<li id="thumbnail_{$image.id_image}"{if $smarty.foreach.thumbnails.last} class="last"{/if}>
									<a{if $jqZoomEnabled && $have_image && !$content_only} href="javascript:void(0);" rel="{literal}{{/literal}gallery: 'gal1', smallimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}',largeimage: '{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}'{literal}}{/literal}"{else} href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}"	data-fancybox-group="other-views" class="fancybox{if $image.id_image == $cover.id_image} shown{/if}"{/if} title="{$imageTitle}">
										<img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'cart_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($cartSize)} height="{$cartSize.height}" width="{$cartSize.width}"{/if} itemprop="image" />
									</a>
								</li>
							{/foreach}
						{/if}
						</ul>
					</div> <!-- end thumbs_list -->
					{if isset($images) && count($images) > 2}
						<a id="view_scroll_right" title="{l s='Other views'}" href="javascript:{ldelim}{rdelim}">
							{l s='Next'}
						</a>
					{/if}
				</div> <!-- end views-block -->
				<!-- end thumbnails -->
			{/if}
			{if isset($images) && count($images) > 1}
				<p class="resetimg clear no-print">
					<span id="wrapResetImages" style="display: none;">
						<a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" data-id="resetImages">
							<i class="icon-repeat"></i>
							{l s='Display all pictures'}
						</a>
					</span>
				</p>
			{/if}
		</div> <!-- end pb-left-column -->
	
		<div class="pb-center-column col-xs-12 col-sm-4 col-md-6">
	
			{capture name=path}{l s='Gift card' mod='giftcard'}{/capture}
			{*** CONTENT HTML / SETTING MODULE ***}
			{$front_content|escape:'UTF-8'}
			{*** END CONTENT HTML / SETTING MODULE ***}
		</div>
	
	{*** STEP 1 BLOCK SELECT TEMPLATE ***}
	{*** END STEP 1 BLOCK SELECT TEMPLATE ***}
	<br/>
	<form class="form" id="formgiftcard" action="{$linkcgc|escape:'quotes'}" method="POST">
		<div class="pb-right-column col-xs-12 col-sm-4 col-md-3">
		{*** STEP 2 GIFT CARD INFORMATION ***}
		{include file="$tpl_dir./errors.tpl"}
				<input type="hidden" name="action" value="" />
				<input type="hidden" name="id_lang" value="{$id_lang|intval}" />
				<input type="hidden" name="token" value="{$token|escape:'html':'UTF-8'}" />
				<input type="hidden" name="id_gift_card_template" id="id_gift_card_template" value="{$template_default->id|intval}"/>
				<div>
					{*
					<!--
					<select name="id_product" class="form-control attribute_select no-print mySelectBoxClass">
						<option value="">{l s='Amount' mod='giftcard'}</option>
						{foreach from=$cards item=carditem name=foo}
							<option value="{$carditem.id_product}" {if ((!isset($card)&&$carditem.isdefaultgiftcard) || (isset($card) && $card->id==$carditem.id_product))}selected{/if}>{convertPrice price=$carditem.price}
							</option>
						{/foreach}
					</select>
					-->
					*}
					<div class="montantbc">{l s='Amount' mod='giftcard'}</div>
					<div>
						{foreach from=$cards item=carditem name=foo}
						<input name="id_product" type="radio" value="{$carditem.id_product}" id="bc{$carditem.id_product}"><label for="bc{$carditem.id_product}">{$carditem.price}</label>
						{/foreach}
					</div>
				</div>
			</div>
			<section class="page-product-box">
				<p class="name">
					 <input name="lastname" type="text" class="input input_user_to" placeholder="{l s='A l\'attention de' mod='giftcard'}"  />
				</p>
				<p class="from">
					<input name="from" type="text" class="input input_user_from" placeholder="{l s='De la part de' mod='giftcard'}"  />
				</p>
				
				<div class="text">
					<h2>{l s='Select the reception mode' mod='giftcard'}</h2>
					<input type="radio" name="receptmode" id="receptmode_mail" value="1" checked>
					<label for="receptmode_mail">{l s='Send by e-mail' mod='giftcard'}</label>
					<input type="radio" name="receptmode" id="receptmode_postal" value="0">
					<label for="receptmode_postal">{l s='La Poste' mod='giftcard'}</label> &nbsp; 
				</div>
				<p class="postale addresspostalsendcard" style="display:none">
					<textarea name="adresse_postale" class="input textarea_comment"  placeholder="{l s='To : postal address' mod='giftcard'}"  style="resize: none;"></textarea>
				</p>
				<p class="email datesendcard">
					<input name="mailto" type="text" class="input email" placeholder="{l s='To : Email' mod='giftcard'}" />
				</p>
				
				{*** STEP 3 RECEPTION MODE ***}
				<div class="text">
					<h3 class="">{l s='Date send card' mod='giftcard'}</h3>
					<p class="select">
					{* Not delete is use in translat tools
						{l s='January' mod='giftcard'}
						{l s='February' mod='giftcard'}
						{l s='March' mod='giftcard'}
						{l s='April' mod='giftcard'}
						{l s='May' mod='giftcard'}
						{l s='June' mod='giftcard'}
						{l s='July' mod='giftcard'}
						{l s='August' mod='giftcard'}
						{l s='September' mod='giftcard'}
						{l s='October' mod='giftcard'}
						{l s='November' mod='giftcard'}
						{l s='December' mod='giftcard'}
					*}
					<select id="years" name="years" class="attribute_select no-print">
						<option value="">-</option>
						{foreach from=$years item=v}
							<option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
						{/foreach}
					</select>
					<select id="months" name="months" class="attribute_select no-print">
						<option value="">-</option>
						{foreach from=$months key=k item=v}
							<option value="{$k}" {if ($sl_month == $k)}selected="selected"{/if}>{l s=$v mod='giftcard'}&nbsp;</option>
						{/foreach}
					</select>
					<select name="days" id="days" class="attribute_select no-print">
						<option value="">-</option>
						{foreach from=$days item=v}
							<option value="{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
						{/foreach}
					</select>
					</p>
					{*** END STEP 3 RECEPTION MODE ***}
				</div>	
			<p class="text">
				<textarea name="message" class="input textarea_comment"  style="resize: none;" placeholder="{l s='Indicate your message' mod='giftcard'}"></textarea> {* <!-- onkeyup="countChar(this)" --> *}
				<div id="remaining characters" style="display: none">(<span id="charNum">200</span>&nbsp;{l s='remaining characters' mod='giftcard'})</div>
			</p>

			<br/>
			<p class="description addresspostalsendcard" style="display:none">
				{l s='The gift card will be sent by postal route to the recipient as soon as your payment is confirmed. You can also choose to send at a later date by selecting that date below.' mod='giftcard'}	
			</p>
			<p class="description datesendcard">
				{l s='The gift card will be sent by email to the recipient as soon as your payment is confirmed. You can also choose to send at a later date by selecting that date below.' mod='giftcard'}	
			</p>
			<br/>
			{*** END STEP 2 GIFT CARD INFORMATION ***}

			<br/>
			{* is use for display ajax messages errors, sucess*}
			<div class="messages"></div>
			{*** BUTTON SUBMIT ***}
			 <div class="submit">
				{*}<input type="button" value="{l s='Preview' mod='giftcard'}" class="preview_form submit_button"/>*}
					<input type="button" value="{l s='Add to cart' mod='giftcard'}"  class="submit_form submit_button"/>
					<div class="ease"></div>
			</div>
			{*** END BUTTON SUBMIT ***}
		</section>
		<br class="clear" />
	</form>
</div>