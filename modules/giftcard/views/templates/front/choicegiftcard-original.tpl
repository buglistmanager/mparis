{*
 *
 * GIFT CARD
 *
 * @category pricing_promotion
 * @author Timactive - Romain DE VERA <support@timactive.com>
 * @copyright TIMACTIVE 2013
 * @version 1.0.0
 *
 *************************************
 **            GIFT CARD			 *              
 **             V 1.0.0              *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5
 *
 *}
{* Variable used in js*}
<script type="text/javascript">
	var linkcgc_controller = '{$linkcgc|escape:'quotes'}';
	var currencySign = '{$currencySign|html_entity_decode:2:"UTF-8"}';
	var currencyRate = '{$currencyRate|floatval}';
	var currencyFormat = '{$currencyFormat|intval}';
	var currencyBlank = '{$currencyBlank|intval}';
	var mailpreview_maxwidth = 500;
	var itemscarousel = [];
	{* fix nb item if width superior *}
	{literal}
	itemscarousel[0] = {width:980,nb:6};
	itemscarousel[1] = {width:800,nb:5};
	itemscarousel[2] = {width:768,nb:4};
	itemscarousel[2] = {width:450,nb:3};
	itemscarousel[4] = {width:320,nb:2};
	itemscarousel[5] = {width:0,nb:1};
	itemscarousel.sort(function(a, b){
 		return b.width-a.width;
	})
	{/literal}
</script>


<div id="choicegiftcard">
	{* visible when ajax call*}
	<div class="ui-loader-background"> </div>
	{capture name=path}{l s='Gift card' mod='giftcard'}{/capture}
	{*** CONTENT HTML / SETTING MODULE ***}
	{$front_content|escape:'UTF-8'}
	{*** END CONTENT HTML / SETTING MODULE ***}
	
	{if !isset($templates) || count($templates) == 0}
		<p class="warning">
					{l s='No model available' mod='giftcard'}
		</p>
	{elseif !isset($cards) || $cards|@count == 0}
		<p class="warning">
					{l s='No card available' mod='giftcard'}
		</p>
	{else}
	{*** STEP 1 BLOCK SELECT TEMPLATE ***}
	<h2>1.&nbsp;{l s='Select a template' mod='giftcard'}</h2>
	<div id="templates_block" class="clear">
		<ul  class="gctabs clearfix">
			<li><a id="tab_template_all"  href="javascript:;" class="selected tab_template" data-tab="block_templates_all">{l s='All' mod='giftcard'}&nbsp;({$templates|@count})</a></li>										
			{if isset($tags) && $tags && $tags|@count > 0}
				{foreach from=$tags item=tag} 
					{if (isset($templatesGroupTag[$tag.id_gift_card_tag]) && $templatesGroupTag[$tag.id_gift_card_tag]|@count > 0)}
						<li>
							<a id="tab_template_tag{$tag.id_gift_card_tag}" href="javascript:;"  data-tab="block_templates_in_tags{$tag.id_gift_card_tag}"  class="tab_template">{$tag.name}&nbsp;({$templatesGroupTag[$tag.id_gift_card_tag]|@count})</a>
						</li>
					{/if}
				{/foreach}
			{/if}
		</ul>
		<div>
			{******* TAB CONTENT ALL TEMPLATES ******}
			<div id="block_templates_all" class="gctab_content selected">
				{if isset($templates) && count($templates) > 0}
				<div class="jcarousel-wrapper" id="jcarouselcardtemplates-all">
	                <div class="jcarousel" >
	                    <ul>
	                    	
								{foreach from=$templates item=template name=thumbnails}
			                        <li class="template_item template_item{$template.id_gift_card_template} {if isset($template_default) && $template_default->id==$template.id_gift_card_template}selected{/if}">
			                        		<a href="javascript:;"  class="link_template" rel="link_template{$template.id_gift_card_template|intval}">
			                        			<!--span class="title_giftcard">{l s='Template'  mod='giftcard'}:&nbsp;{$template.name}</span-->
			                        			
			                        			<img src="{$link->getMediaLink("`$giftcard_templates_dir``$template.id_gift_card_template`/`$template.id_gift_card_template`-front{if $template.issvg}-{$id_lang}{/if}.jpg")}" alt="
			                        			{$template.name|escape}" title="{$template.name|escape}">
			                        			
			                        			
			                        		</a>
			                        		
			                        		<a href="{$link->getMediaLink("`$giftcard_templates_dir``$template.id_gift_card_template`/`$template.id_gift_card_template`-thickbox{if $template.issvg}-{$id_lang}{/if}")}.jpg")}" data-fancybox-group="other-views" class="thickbox-giftcard shown" title="{$template.name}">	
			                        			<span class="zoom_link">{l s='View larger' mod='giftcard'}</span>
			                        		</a>
			                        		<span class="check"></span>
			                        </li>
			                	{/foreach}
		            	</ul>
	        		</div>
	       			<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	        		<a href="#" class="jcarousel-control-next">&rsaquo;</a>
	        		<p class="jcarousel-pagination"></p>
	            </div>
            	{/if}
			</div>
			{******* END TAB CONTENT ALL TEMPLATES ******}
			{******* TAB CONTENT BY FILTER TAG ******}
			{if isset($tags) && $tags && $tags|@count > 0}
				{foreach from=$tags item=tag} 
					<div id="block_templates_in_tags{$tag.id_gift_card_tag|intval}" class="rte gctab_content">
					{if isset($templatesGroupTag[$tag.id_gift_card_tag]) && $templatesGroupTag[$tag.id_gift_card_tag]|@count > 0}
					<div class="jcarousel-wrapper" id="jcarouselcardtemplates-tag{$tag.id_gift_card_tag|intval}">
		                <div class="jcarousel" >
		                <ul>
		                			{foreach from=$templatesGroupTag[$tag.id_gift_card_tag] item=template}
			                        <li class="template_item template_item{$template.id_gift_card_template} {if isset($template_default) && $template_default->id==$template.id_gift_card_template}selected{/if}">
			                        		<a href="javascript:;"  class="link_template" rel="link_template{$template.id_gift_card_template|intval}">
			                        			<!--span class="title_giftcard">{l s='Template'  mod='giftcard'}:&nbsp;{$template.name}</span-->
			                        		
			                        		<img src="{$link->getMediaLink("`$giftcard_templates_dir``$template.id_gift_card_template`/`$template.id_gift_card_template`-front{if $template.issvg}-{$id_lang}{/if}.jpg")}" alt="
			                        			{$template.name|escape}" title="{$template.name|escape}">
			                        		</a>
			                        		
			                        		<a href="{$link->getMediaLink("`$giftcard_templates_dir``$template.id_gift_card_template`/`$template.id_gift_card_template`-thickbox{if $template.issvg}-{$id_lang}{/if}")}.jpg")}" data-fancybox-group="other-views" class="thickbox-giftcard shown" title="{$template.name}">	
			                        			<span class="zoom_link">{l s='View larger' mod='giftcard'}</span>
			                        		</a>
			                        		<span class="check"></span>
			                        	
			                        </li>

			                		{/foreach}
				        </ul>
		        		</div>
		        		<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
	        			<a href="#" class="jcarousel-control-next">&rsaquo;</a>
	        			<p class="jcarousel-pagination"></p>
		        	</div>
					{/if}
					</div>
				{/foreach}
			{/if}
			{******* END TAB CONTENT BY FILTER TAG ******}
		</div>
	</div>
	{*** END STEP 1 BLOCK SELECT TEMPLATE ***}
	<br/>
	{*** STEP 2 GIFT CARD INFORMATION ***}
	<h2>2.&nbsp;{l s='Gift card information' mod='giftcard'}</h2>
	{include file="$tpl_dir./errors.tpl"}
	<form class="form" id="formgiftcard" action="{$linkcgc|escape:'quotes'}" method="POST">
		<input type="hidden" name="action" value="" />
		<input type="hidden" name="id_lang" value="{$id_lang|intval}" />
		<input type="hidden" name="token" value="{$token|escape:'html':'UTF-8'}" />
		<input type="hidden" name="id_gift_card_template" id="id_gift_card_template" value="{$template_default->id|intval}"/>
		<p> {l s='Amount' mod='giftcard'}&nbsp;
			<select name="id_product">
			{foreach from=$cards item=carditem name=foo}
            	<option value="{$carditem.id_product}" {if ((!isset($card)&&$carditem.isdefaultgiftcard) || (isset($card) && $card->id==$carditem.id_product))}selected{/if}>{convertPrice price=$carditem.price}
            	</option>
            {/foreach}
        	</select>
		</p>
		<p class="from">
       		 <input name="from" type="text" class="input input_user_from" placeholder="{l s='From : your lastname' mod='giftcard'}"  />
     	 </p>
      	<p class="name">
       		 <input name="lastname" type="text" class="input input_user_to" placeholder="{l s='To : Lastname' mod='giftcard'}"  />
     	</p>

     
      <p class="text">
        <textarea name="message" class="input textarea_comment"  placeholder="{l s='Indicate your message' mod='giftcard'}" onkeyup="countChar(this)"></textarea>
        <div id="remaining characters">(<span id="charNum">200</span>&nbsp;{l s='remaining characters' mod='giftcard'})</div>
      </p>

	<br/>
	<h2>3.&nbsp;{l s='Select the reception mode' mod='giftcard'}</h2>
	<p class="text">
		     <input type="radio" name="receptmode" id="receptmode_printathome" value="0" checked>
		     <label for="receptmode_printathome">{l s='Print at home' mod='giftcard'}</label>
		     <input type="radio" name="receptmode" id="receptmode_mail" value="1">
		     <label for="receptmode_mail">{l s='Send by e-mail' mod='giftcard'}</label>
	</p>
	

	<p class="email datesendcard" style="display:none">
		        <input name="mailto" type="text" class="input email" placeholder="{l s='To : Email' mod='giftcard'}" />
	</p>
	<p class="description datesendcard" style="display:none">
		{l s='The gift card will be sent by email to the recipient as soon as your payment is confirmed. You can also choose to send at a later date by selecting that date below.' mod='giftcard'}	
	</p>
	<br/>
	{*** END STEP 2 GIFT CARD INFORMATION ***}

	{*** STEP 3 RECEPTION MODE ***}
	<h3 class="datesendcard" style="display:none">{l s='Date send card' mod='giftcard'}</h3>
	<p class="select datesendcard"  style="display:none">
				{* Not delete is use in translat tools
					{l s='January' mod='giftcard'}
					{l s='February' mod='giftcard'}
					{l s='March' mod='giftcard'}
					{l s='April' mod='giftcard'}
					{l s='May' mod='giftcard'}
					{l s='June' mod='giftcard'}
					{l s='July' mod='giftcard'}
					{l s='August' mod='giftcard'}
					{l s='September' mod='giftcard'}
					{l s='October' mod='giftcard'}
					{l s='November' mod='giftcard'}
					{l s='December' mod='giftcard'}
				*}
				<select name="days" id="days">
					<option value="">-</option>
					{foreach from=$days item=v}
						<option value="{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
					{/foreach}
				</select>
				<select id="months" name="months">
					<option value="">-</option>
					{foreach from=$months key=k item=v}
						<option value="{$k}" {if ($sl_month == $k)}selected="selected"{/if}>{l s=$v mod='giftcard'}&nbsp;</option>
					{/foreach}
				</select>
				<select id="years" name="years">
					<option value="">-</option>
					{foreach from=$years item=v}
						<option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
					{/foreach}
				</select>
	</p>
	{*** END STEP 3 RECEPTION MODE ***}
	<br/>
	{* is use for display ajax messages errors, sucess*}
	<div class="messages"></div>
	{*** BUTTON SUBMIT ***}
	 <div class="submit">
	 	<input type="button" value="{l s='Preview' mod='giftcard'}" class="preview_form submit_button"/>
	    <input type="button" value="{l s='Add to cart' mod='giftcard'}"  class="submit_form submit_button"/>
	    <div class="ease"></div>
	</div>
	{*** END BUTTON SUBMIT ***}
	</form>
	{/if}
</div>