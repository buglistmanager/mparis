<?php
/**
 * GIFT CARD
 *
 *    @category pricing_promotion
 *    @author    Timactive - Romain DE VERA <support@timactive.com>
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         GIFT CARD                *
 **          V 1.0.0                 *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 */

$sql = array();
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'giftcardproduct`;';  
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'giftcardorder`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'giftcardtemplate`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'giftcardtemplate_shop`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'giftcardtemplate_lang`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'giftcardtag`;';
$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'giftcardtemplate_tag`;';


