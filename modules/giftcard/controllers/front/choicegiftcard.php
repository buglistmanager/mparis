<?php
/**
 * GIFT CARD
 *
 *    @category pricing_promotion
 *    @author    Timactive - Romain DE VERA <support@timactive.com>
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         GIFT CARD                *
 **          V 1.0.0                 *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 */

class GiftCardChoiceGiftCardModuleFrontController extends ModuleFrontController {
	public $ssl = false;
	public function __construct()
	{
		parent::__construct ();
		$this->context = Context::getContext ();
	}
	/**
	 *
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent ();
		$this->assign ();
	}
	public function setMedia()
	{
		parent::setMedia ();
		if (count ( $this->errors ))
			return;
		$this->addCSS ( array (
			_PS_CSS_DIR_.'jquery.fancybox-1.3.4.css' => 'screen',
			_MODULE_DIR_.'giftcard/css/choicegiftcard.css' => 'screen'
		) );
		$this->addJqueryPlugin ( array (
			'fancybox',
			'scrollTo',
			'serialScroll'
		) );
		$this->addJS ( array (
			_THEME_JS_DIR_.'tools.js',
			_MODULE_DIR_.'giftcard/js/jquery.jcarousel.min.js',
			_MODULE_DIR_.'giftcard/js/choicegiftcard.js'
		) );
		if (Configuration::get ( 'PS_DISPLAY_JQZOOM' ) == 1)
			$this->addJqueryPlugin ( 'jqzoom' );
	}
	/* Use to preview image in email */
	private function dataUri($file, $mime)
	{
		$contents = Tools::file_get_contents ( $file );
		$base64 = base64_encode ( $contents );
		return ('data:'.$mime.';base64,'.$base64);
	}
	public function postProcess()
	{
		/* PREVIEW EMAIL */
		if (trim ( Tools::getValue ( 'action' ) ) == 'preview')
		{
			if (Tools::getToken () != Tools::getValue ( 'token' ))
				die ( $this->module->l('Invalid token', 'choicegiftcard') );
			/*$id_lang_mail = (int)GiftCardTools::getLangMail ( $this->context->language->id );
			if (! $id_lang_mail)
				die ( $this->module->l ( 'No possible to preview' ) );*/
				// $languageMail = new Language($id_lang_mail);
			$template_id = (int)Tools::getValue ( 'id_gift_card_template', 0 );
			$id_shop = $this->context->shop->id;
			$gift_card_template = null;
			if (! $template_id)
				$gift_card_template = GiftCardTemplate::getDefault ();
			else
				$gift_card_template = new GiftCardTemplate ( $template_id );
			$output = '';
			if (! $gift_card_template)
				$output .= $this->displayError ( $this->l('Default gift card template is required to preview PDF') );
			else
			{
				if (Tools::getValue ( 'id_product' ))
					$card = new GiftCardProduct ( (int)Tools::getValue ( 'id_product' ) );
				else
					$card = GiftCardProduct::getDefault ();
				$params = array ();
				$params['id_shop'] = (int)$id_shop;
				$params['id_currency'] = $this->context->currency->id;
				$params['discountcode'] = '##################';
				$params['price'] = round ( $card->price );
				$params['date_to'] = Tools::displayDate ( date ( 'Y-m-d', strtotime ( date ( 'Y-m-d', time () ).' + '.(int)$card->period_val.' month' ) ) );
				$params['id_lang'] = $this->context->language->id;
				$params['message'] = Tools::getValue ( 'message', '' );
				$params['lastname'] = Tools::getValue ( 'lastname', '' );
				$params['from'] = Tools::getValue ( 'from', '' );
				$prefix_pdf = Configuration::get ( 'GIFTCARD_PDF_PREFIX', (int)$params['id_lang'] );
				if (! $prefix_pdf || empty ( $prefix_pdf ))
					$prefix_pdf = 'GIFTCARD';
				$filename = $prefix_pdf.sprintf ( '%06d', 0 );
				GiftCardTools::processGeneratePdfV2 ( $gift_card_template, $params, true, $filename );
			}
		}
		/* LOAD CONTENT */
		if (! Tools::getValue ( 'action' ))
		{
			$card = null;
			if (Tools::getValue ( 'id_product' ))
				$card = new GiftCardProduct ( (int)Tools::getValue ( 'id_product' ) );
				/* CONTROL PRICE SPECIFIC : explain in rule : RG-PRICE-01 */
			$cards = GiftCardProduct::getGiftCards ( $this->context->language->id, true, $this->context->currency->id, (int)$this->context->shop->id );
			$cardschecked = array ();
			foreach ($cards as $carditem)
			{
				// It is not possible to applicate other specific price
				if (((float)$carditem['price']) == ((float)$carditem['amount']))
					$cardschecked[] = $carditem;
				else
				{
					$cardtodisable = new GiftCardProduct ( (int)$carditem['id_product'] );
					$cardtodisable->active = 0;
					$cardtodisable->update ();
				}
			}
			$tags = GiftCardTag::getTags ( $this->context );
			$templates = GiftCardTemplate::getTemplates ( $this->context->language->id, true, (int)$this->context->shop->id );
			$template_default = GiftCardTemplate::getDefault ();
			$templates_group_tag = GiftCardTemplate::getTemplatesGroupByTag ( $this->context->language->id, true, (int)$this->context->shop->id );
			$front_content = Configuration::get ( 'GIFTCARD_FRONT_CONTENT', (int)$this->context->language->id );
			$this->context->smarty->assign ( array (
				'tags' => $tags,
				'template_default' => $template_default,
				'front_content' => $front_content,
				'templates' => $templates,
				'giftcard_templates_dir' => _MODULE_DIR_.'giftcard/img/templates/',
				'templatesGroupTag' => $templates_group_tag,
				'currencySign' => $this->context->currency->sign,
				'currencyRate' => $this->context->currency->conversion_rate,
				'currencyFormat' => $this->context->currency->format,
				'currencyBlank' => $this->context->currency->blank,
				'sl_year' => date ( 'Y' ),
				'sl_month' => date ( 'n' ),
				'sl_day' => date ( 'j' ),
				'years' => array(date ( 'Y' ),(int)date ( 'Y' ) + 1, (int)date ( 'Y' ) + 2),
				'months' => Tools::dateMonths (),
				'days' => Tools::dateDays (),
				'card' => $card,
				'cards' => $cardschecked,
				'id_lang' => $this->context->language->id
			) );
			
		// $this->context->smarty->assign(
			// array(
						// 'HOOK_SHARE' => Hook::exec('displayGiftCard')
					// )
			// );
		}
		elseif (trim ( Tools::getValue ( 'action' ) ) == 'addgiftcard')
		{
			$errors = array ();
			$card = new GiftCardProduct ( (int)Tools::getValue ( 'id_product' ) );
			if (Tools::getToken () != Tools::getValue ( 'token' ))
				$errors[] = $this->module->l('Invalid token', 'choicegiftcard');
			if (count ( $errors ) == 0)
			{
				$currency_default = (int)Configuration::get ( 'PS_CURRENCY_DEFAULT' );
				if ($this->context->currency->id != $currency_default && ! $card->validityPrice ( (int)$this->context->currency->id ))
				{
					// message display to customer
					$errors[] = $this->module->l('This card is disabled, please select other card thank.', 'choicegiftcard');
					$card->active = 0;
					$card->update ();
				}
				else
				{
					$mailto = Tools::getValue ( 'mailto', '' );
					$postaladdressto = Tools::getValue ( 'adresse_postale', '' );
					$receptmode = (int)Tools::getValue ( 'receptmode', 0 );
					$lastname = Tools::getValue ( 'lastname', '' );
					$from = Tools::getValue ( 'from', '' );
					$message = trim ( Tools::getValue ( 'message', '' ) );
					$template_id = (int)Tools::getValue ( 'id_gift_card_template', 0 );
					$delivery_date = (Tools::getValue ( 'years', '' ) == '' ? '' :
							(int)Tools::getValue ( 'years' ).'-'.(int)Tools::getValue ( 'months' ).'-'.(int)Tools::getValue ( 'days' ));
					$gift_card_template = null;
					if ($receptmode && (! Validate::isEmail ( $mailto ) || empty ( $mailto )))
						$errors[] = $this->module->l('Badly formatted email address', 'choicegiftcard');
					if (! $template_id > 0)
						$errors[] = $this->module->l('You must select a model', 'choicegiftcard').$template_id;
					else
					{
						$gift_card_template = new GiftCardTemplate ( $template_id );
						if ((int)$gift_card_template->id > 0)
							$this->module->l('Template is not valide, please choice other', 'choicegiftcard');
					}
					if (empty ( $from ))
						$errors[] = $this->module->l('From is required', 'choicegiftcard');
					else if (! Validate::isMessage ( $from ))
						$errors[] = sprintf ( $this->module->l('The field %s contain invalid character <>{}', 'choicegiftcard'),
								$this->module->l('from', 'choicegiftcard') );
					if (empty ( $lastname ))
						$errors[] = $this->module->l('Recipient firstname is required', 'choicegiftcard');
					else if (! Validate::isMessage ( $lastname ))
						$errors[] = sprintf ( $this->module->l('The field %s contain invalid character <>{}', 'choicegiftcard'),
								$this->module->l('Recipient first name', 'choicegiftcard') );
					if (empty ( $message ))
						$errors[] = $this->module->l('Message is required', 'choicegiftcard');
					if (empty ( $postaladdressto ) && $receptmode == 0 )
						$errors[] = $this->module->l('Postal address is required', 'choicegiftcard');
					else if (! Validate::isMessage ( $message ))
						$errors[] = sprintf ( $this->module->l('The field %s contain invalid character <>{}', 'choicegiftcard'),
								$this->module->l('Message', 'choicegiftcard') );
					if (! @checkdate ( Tools::getValue ( 'months' ),
							Tools::getValue ( 'days' ), Tools::getValue ( 'years' ) ))
						$errors[] = $this->module->l('Date send card is invalid', 'choicegiftcard');
					// if ((int)Tools::getValue ( 'receptmode' ))
					// {
						$date_now = strtotime(date('Y').'-'.date('m').'-'.date('d'));
						$date_send = strtotime(Tools::getValue ( 'years' ).'-'.Tools::getValue ( 'months' ).'-'.Tools::getValue ( 'days' ));
						$diff_date = (($date_now - $date_send) / 3600 / 24);
						if ($diff_date >= 1)
							$errors[] = $this->module->l('The mailing date must be greater than or equal to the date of days', 'choicegiftcard');
					// }
				}
			}
			if (count ( $errors ) == 0)
			{
				/* add new card if not exist */
				if (! $this->context->cart->id)
				{
					if (Context::getContext ()->cookie->id_guest)
					{
						$guest = new Guest ( Context::getContext ()->cookie->id_guest );
						$this->context->cart->mobile_theme = $guest->mobile_theme;
					}
					$this->context->cart->add ();
					if ($this->context->cart->id)
						$this->context->cookie->id_cart = (int)$this->context->cart->id;
				}
				$id_cfield_mailto = (int)$card->id_customization_field_mailto;
				$id_cfield_postal_address_to = (int)$card->id_customization_field_to_postal_address;
				$id_cfield_message = (int)$card->id_customization_field_message;
				$id_cfield_from = (int)$card->id_customization_field_from;
				$id_cfield_lastname = (int)$card->id_customization_field_lastname;
				$id_cfield_deliverydate = (int)$card->id_customization_field_deliverydate;
				$id_cfield_template = (int)$card->id_customization_field_template;
				$id_cfield_image = (int)$card->id_customization_field_image;
				/* Upload de l'image */
				$file_name = md5 ( uniqid ( rand (), true ) );
				$product_picture_width = (int)Configuration::get ( 'PS_PRODUCT_PICTURE_WIDTH' );
				$product_picture_height = (int)Configuration::get ( 'PS_PRODUCT_PICTURE_HEIGHT' );
				if ($gift_card_template->issvg)
				{
					$svgparams = array ();
					$svgparams['price'] = round ( $card->amount );
					$svgparams['from'] = $from;
					$svgparams['lastname'] = $lastname;
					$svgparams['message'] = $message;
					$svgparams['mailto'] = $mailto;
					$svgparams['postaladdressto'] = $postaladdressto;
					$svg = GiftCardTools::buildTemplateSvgV2 ( $gift_card_template, $svgparams, $this->context->language->id );
					if (! GiftCardTools::resizeImageWithTemplate ( $svg, _PS_UPLOAD_DIR_.$file_name, 0, 1600, 920, 'jpg' ))
						$errors[] = $this->module->l('An error occurred while creating template');
					elseif (! GiftCardTools::resizeImageWithTemplate ( $svg,
							_PS_UPLOAD_DIR_.$file_name.'_small',
							0,
							$product_picture_width,
							$product_picture_height,
							'jpg' ))
						$errors[] = $this->module->l('An error occurred while creating template' );
					elseif (! chmod ( _PS_UPLOAD_DIR_.$file_name, 0777 ) || ! chmod ( _PS_UPLOAD_DIR_.$file_name.'_small', 0777 ))
						$errors[] = $this->module->l('An error occurred while creating template.');
					else
						$this->context->cart->addPictureToProduct ( $card->id, $id_cfield_image, Product::CUSTOMIZE_FILE, $file_name );
				}
				else
				{
					$template_path = $gift_card_template->img_dir.$gift_card_template->id.'/';
					if (! ImageManager::resize ( $template_path.$gift_card_template->id.'.jpg', _PS_UPLOAD_DIR_.$file_name ))
						$errors[] = $this->module->l('An error occurred while copying image');
					elseif (! ImageManager::resize ( $template_path.$gift_card_template->id.'.jpg',
							_PS_UPLOAD_DIR_.$file_name.'_small', $product_picture_width, $product_picture_height, 'jpg' ))
						$errors[] = $this->module->l('An error occurred while copying image');
					elseif (! chmod ( _PS_UPLOAD_DIR_.$file_name, 0777 ) || ! chmod ( _PS_UPLOAD_DIR_.$file_name.'_small', 0777 ))
						$errors[] = $this->module->l('An error occurred while creating template.');
					else
						$this->context->cart->addPictureToProduct ( $card->id, $id_cfield_image, Product::CUSTOMIZE_FILE, $file_name );
				}
				$this->context->cart->addTextFieldToProduct ( $card->id, $id_cfield_from, Product::CUSTOMIZE_TEXTFIELD, $from );
				$this->context->cart->addTextFieldToProduct ( $card->id, $id_cfield_lastname, Product::CUSTOMIZE_TEXTFIELD, $lastname );
				$this->context->cart->addTextFieldToProduct ( $card->id, $id_cfield_message, Product::CUSTOMIZE_TEXTFIELD, $message );
				$this->context->cart->addTextFieldToProduct ( $card->id, $id_cfield_template, Product::CUSTOMIZE_TEXTFIELD, $template_id );
				$this->context->cart->addTextFieldToProduct ( $card->id, $id_cfield_postal_address_to, Product::CUSTOMIZE_TEXTFIELD, $postaladdressto );
				$this->context->cart->addTextFieldToProduct ( $card->id, $id_cfield_mailto, Product::CUSTOMIZE_TEXTFIELD, $mailto );
				$this->context->cart->addTextFieldToProduct ( $card->id, $id_cfield_deliverydate, Product::CUSTOMIZE_TEXTFIELD, $delivery_date );
				$update_quantity = $this->context->cart->updateQty ( 1, $card->id );
				if (! $update_quantity)
					$errors[] = Tools::displayError('You already have the maximum quantity available for this product.', false );
				if (! count ( $errors ) > 0)
				{
					$results = array (
						'hasError' => false,
						'message' => $this->module->l('The gift card as added in your cart', 'choicegiftcard'),
						'errors' => $errors
					);
					die ( Tools::jsonEncode ( $results ) );
				}
			}
			if (count ( $errors ) > 0)
			{
				$results = array (
					'hasError' => true,
					'errors' => $errors
				);
				die ( Tools::jsonEncode ( $results ) );
			}
		}
	}
	public function assign()
	{
		$this->context->smarty->assign ( array (
			'linkcgc' => $this->context->link->getModuleLink ( 'giftcard', 'choicegiftcard' )
		) );
		$this->setTemplate ( 'choicegiftcard.tpl' );
	}
}
