<?php
/**
 * GIFT CARD
 *
 *    @category pricing_promotion
 *    @author    Timactive - Romain DE VERA <support@timactive.com>
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         GIFT CARD                *
 **          V 1.0.0                 *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 * 
 *  ******* 1.0.9 ******
 * FEATURE #3722: Control Date send card superior or equal date of days
 * BUG     #3721: Errors message in front office form gift card
 * FEATURE #3444: Documentation English
 * 
 *  ******* 1.0.8 ******
 * BUG #3634: PS VERSION <= 1.5.2 / getMimeTypeByExtension not exist
 * BUG #3640: Bug / mail lang select error
 * 
 *  ******* 1.0.7 ******
 * BUG #3599: when order template select is ko
 *
 *  ******* 1.0.6 ******
 * http://support.timactive.com/versions/119
 * Evolution #3559: Fixed price
 * BUG  #3551: Admin order / blank page for ps_version < 1.5.5.0
 * BUG  #3548: Display date warning PS < 1.5.5
 * Evolution #3542: Multiboutique / intégration
 *  
 *  ******* 1.0.5 ******
 * http://support.timactive.com/versions/113
 * BUG  #3472: Cannot add to giftcard in the cart
 * BUG  #3499: Traduction not work after code formatter
 * BUG  #3500: installation / on time to use / image is to all
 * Evolution #3462: carte cadeaux / option free delivery
 * Evolution #3495: Free Shipping 
 * 
 *  ******* 1.0.4 ******
 * BUG #3441: Initialisation des variables / erreur clé incorrecte
 * BUG #3443: Page commande / erreur d'alignement
 * 
 *  ******* 1.0.3 ******
 * BUG #3426 Giftcard / ready to use / lang
 * BUG #3427 Date d'envoi inférieur au lieu supérieur
 * BUG #3428 Front / apercu Pdf / langue autre que fr et en
 * 
 *  ******* 1.0.2 ******
 * BUG 3300 TPL 1.5, no image, breadcrumb ...
 *  ******* 1.0.1 ******
 * BUG 3364 Prestashop 1.5 create default template error update
 */

if (!defined('_PS_VERSION_'))
	exit;

function upgrade_module_1_0_7($object, $install = false)
{
	if (($object->active || $install))
	{
		$result = true;
		Db::getInstance()->Execute('
		ALTER TABLE `'._DB_PREFIX_.'giftcardorder` 
				CHANGE COLUMN `id_gift_card_template` `id_gift_card_template` INT(10) NULL DEFAULT NULL');
	}
	return $result;
}
