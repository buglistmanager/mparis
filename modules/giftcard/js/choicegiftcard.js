/**
 * GIFTCARD
*
*    @category pricing_promotion
*    @author    Timactive - Romain DE VERA <support@timactive.com>
*    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
*    @version 1.0.0
*    @license   Commercial license
*
*************************************
**         GIFTCARD                 *
**          V 1.0.0                 *
*************************************
* +
* + Languages: EN, FR
* + PS version: 1.5,1.6
*/

$( document ).ready(function() {
	$('.mySelectBoxClass').selectbox({
		effect: "fade"
	});
	$('.thickbox-giftcard').fancybox({
				'hideOnContentClick': true,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
	});
    initJCarousel('jcarouselcardtemplates-all'); 
    $('.tab_template').click(function(){
    		$('.tab_template').removeClass('selected');
    		var datatab = $(this).attr("data-tab");
    		$(this).addClass('selected');
        	$('.gctab_content').removeClass('selected');
        	$('#' + datatab).addClass('selected');
        	var jcarouselid = 'jcarouselcardtemplates-all';
        	if(datatab != 'block_templates_all')
        	{
        		var strid = $(this).attr('id');
        		var tag_id = strid.replace("tab_template_tag","");
        		jcarouselid = 'jcarouselcardtemplates-tag'+tag_id;
        	}
        	initJCarousel(jcarouselid);
	
    });
    $('.link_template').click(function(){
        	var link_rel = $(this).attr('rel');
        	var template_id = link_rel.replace("link_template","");
        	$('#id_gift_card_template').val(template_id);
        	$('.template_item').removeClass('selected');
        	$('.template_item'+template_id).addClass('selected');
    });
	
	$("input[name='receptmode']").change(function() {
			if ($("input[name='receptmode']:checked").val() == '1'){
				$(".addresspostalsendcard").hide();
				$(".datesendcard").show();
			}
			else{
				$(".addresspostalsendcard").show();
				$(".datesendcard").hide();
			}
    });

	$('#choicegiftcard .preview_form').click(function(){
		//$('#choicegiftcard .ui-loader-background').show();
		$("#formgiftcard input[name='action']").val('preview');
		$("#formgiftcard").submit();
		/* view mail preview ajax
		$.ajax({
			type: 'POST',
			url:	linkcgc_controller,
			async: true,
			cache: false,
			dataType: 'html',
			data: "ajax=1"+
				  "&"+$('#formgiftcard').serialize() + 
				  "&rand=" + new Date().getTime(),
			success: function(data)
			{
					$('#choicegiftcard .ui-loader-background').hide(200);
					$.fancybox({
							 autoDimensions: false,
	               			 'maxWidth' : mailpreview_maxwidth,
							  content: data,
	            	});
			},
			error:function()
			{
				$('#choicegiftcard .ui-loader-background').hide(200);
			}});
		}*/
	}
	);

	$('#choicegiftcard .submit_form').click(function()
	{
		var choicedone = 0;
		$('.montantbc + div .radio').each(function(){
			if($(this).hasClass('checked'))
				choicedone = 1;
		})
		if(choicedone == 0){
			var error = (id_lang == 1) ? 'Vous n\'avez pas choisi de montant' : 'Please select an amount';
			$('#popin-msg #inside-popin div').prepend("<h4>Erreur</h4><p class='error' style='text-transform: lowercase;'>"+error+"</p>");
			$('#popin-msg').fadeIn();
			return false;
		}
		$("#formgiftcard input[name='action']").val('addgiftcard');
		// $('#choicegiftcard .ui-loader-background').show();
		$('body').append('<div class="store" style="display: block"></div>');
		$.ajax({
		type: 'POST',
		url:	linkcgc_controller,
		async: true,
		cache: false,
		dataType: 'json',
		data: "ajax=1"+
			  "&"+$('#formgiftcard').serialize() + 
			  "&rand=" + new Date().getTime(),
		success: function(data)
		{
			// $('#choicegiftcard .ui-loader-background').hide(200);
			if (!data.hasError)
			{
					$('#popin-msg #inside-popin div').html('');
					$('#choicegiftcard .messages').find('.error').fadeOut(function(){
						$(this).remove();
					});
					// display a confirmation message
					$('#choicegiftcard .messages').html('');
					// $('#choicegiftcard .messages').prepend("<p class='success'>"+data.message+"</p>");
					$('#formgiftcard .submit').html('<p id="giftcard_success">'+data.message+'</p>');
					$.ajax({
						type: 'POST',
						headers: { "cache-control": "no-cache" },
						url: baseUri + '?rand=' + new Date().getTime(),
						async: true,
						cache: false,
						dataType : "json",
						data: 'controller=cart&ajax=true&token=' + static_token,
						success: function(jsonData)
						{
							ajaxCart.updateCart(jsonData);
						},
						error: function(XMLHttpRequest, textStatus, errorThrown) {
							alert("TECHNICAL ERROR: \n\nDetails:\nError thrown: " + XMLHttpRequest + "\n" + 'Text status: ' + textStatus);
						}
					});
			}
			else
			{
				$('#choicegiftcard .messages').find('.success').fadeOut(function(){
						$(this).remove();
				});
				// display an error message
				$('#popin-msg #inside-popin div').html('');
				$('#popin-msg #inside-popin div').prepend("<h4>Erreur</h4><p class='error' style='text-transform: lowercase;'></p>");
				for (var i = 0; i < data.errors.length; i++)
					$('#popin-msg #inside-popin div .error').html($('#popin-msg #inside-popin div .error').html()+data.errors[i]+"<br />");
				$('#popin-msg').fadeIn();
				// $('#choicegiftcard .messages').html('');
				// $('#choicegiftcard .messages').prepend("<h4>Erreur</h4><p class='error'></p>");
				// for (var i = 0; i < data.errors.length; i++)
					// $('#choicegiftcard .error').html($('#choicegiftcard .error').html()+data.errors[i]+"<br />");
				
			}
			$('.store').remove();
		},
		error:function()
		{
			$('#choicegiftcard .ui-loader-background').hide(200);
		}
		});
	});
	}

);
function countChar(val) {
        var len = val.value.length;
        if (len >= 200) {
          val.value = val.value.substring(0, 200);
        } else {
          $('#charNum').text(200 - len);
        }
};
function initJCarousel(idcarousel)
{
	/*count total of template giftcard element*/
	var nbitems = $( '#'+idcarousel + ' .jcarousel ul > li' ).size();
	var showprevnext = false;
	var jcarousel = $('#'+idcarousel + ' .jcarousel');
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var width = jcarousel.innerWidth();
           		itemscarouselsort = itemscarousel.sort();
           		for (var key in itemscarouselsort){
  					 if(width >= itemscarouselsort[key].width)
  					 {
  					 	width = width / itemscarouselsort[key].nb;
  					 	break;
  					 }
				}
                
                /**/
                if(nbitems * width > jcarousel.innerWidth())
                {
                	showprevnext = true;
                }
                if(!showprevnext)
                {
	               	 $('#'+idcarousel + ' .jcarousel-control-prev').hide();
	               	 $('#'+idcarousel + ' .jcarousel-control-next').hide();
	               	 $('#'+idcarousel + ' .jcarousel-pagination').hide();
               	}
               	else
               	{
               		$('#'+idcarousel + ' .jcarousel-control-prev').show();
               	 	$('#'+idcarousel + ' .jcarousel-control-next').show();
               	 	$('#'+idcarousel + ' .jcarousel-pagination').show();	
               	}
                jcarousel.jcarousel('items').css('width', width + 'px');

            })
            .jcarousel({
                wrap: 'circular'
            });

        $('#'+idcarousel + ' .jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('#'+idcarousel + ' .jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('#'+idcarousel + ' .jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
}

