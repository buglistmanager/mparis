<?php
/**
 * GIFT CARD
 *
 *    @category pricing_promotion
 *    @author    Timactive - Romain DE VERA <support@timactive.com>
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra AI
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         GIFT CARD                *
 **          V 1.0.0                 *
 *************************************
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 *
 *  ******* 1.0.10 ******
 * FEATURE #3735: Keep install in reinit
 * BUG #3800 :  Id lang template tag
 * 
 *  ******* 1.0.9 ******
 * FEATURE #3722: Control Date send card superior or equal date of days
 * BUG     #3721: Errors message in front office form gift card
 * FEATURE #3444: Documentation English
 * 
 *  ******* 1.0.8 ******
 * BUG #3634: PS VERSION <= 1.5.2 / getMimeTypeByExtension not exist
 * BUG #3640: Bug / mail lang select error
 * 
 *  ******* 1.0.7 ******
 * BUG #3599: when order template select is ko
 *
 *  ******* 1.0.6 ******
 * http://support.timactive.com/versions/119
 * Evolution #3559: Fixed price
 * BUG  #3551: Admin order / blank page for ps_version < 1.5.5.0
 * BUG  #3548: Display date warning PS < 1.5.5
 * Evolution #3542: Multiboutique / intégration
 *  
 *  ******* 1.0.5 ******
 * http://support.timactive.com/versions/113
 * BUG  #3472: Cannot add to giftcard in the cart
 * BUG  #3499: Traduction not work after code formatter
 * BUG  #3500: installation / on time to use / image is to all
 * Evolution #3462: carte cadeaux / option free delivery
 * Evolution #3495: Free Shipping 
 * 
 *  ******* 1.0.4 ******
 * BUG #3441: Initialisation des variables / erreur clé incorrecte
 * BUG #3443: Page commande / erreur d'alignement
 * 
 *  ******* 1.0.3 ******
 * BUG #3426 Giftcard / ready to use / lang
 * BUG #3427 Date d'envoi inférieur au lieu supérieur
 * BUG #3428 Front / apercu Pdf / langue autre que fr et en
 * 
 *  ******* 1.0.2 ******
 * BUG 3300 TPL 1.5, no image, breadcrumb ...
 *  ******* 1.0.1 ******
 * BUG 3364 Prestashop 1.5 create default template error update
 */

if (! defined ( '_PS_VERSION_' ))
	exit ();

if (! defined ( '_MYSQL_ENGINE_' ))
	define ( '_MYSQL_ENGINE_', 'MyISAM' );

require_once (_PS_MODULE_DIR_.'giftcard/models/GiftCardProduct.php');
require_once (_PS_MODULE_DIR_.'giftcard/models/GiftCardOrder.php');
require_once (_PS_MODULE_DIR_.'giftcard/models/GiftCardTag.php');
require_once (_PS_MODULE_DIR_.'giftcard/models/GiftCardTemplate.php');
require_once (_PS_MODULE_DIR_.'giftcard/models/PDFGiftCard.php');
require_once (_PS_MODULE_DIR_.'giftcard/tools/GiftCardTools.php');
require_once (_PS_MODULE_DIR_.'giftcard/tools/MailGiftCard.php');
class Giftcard extends Module {
	public function __construct()
	{
		$this->name = 'giftcard';
		$this->version = '1.0.12';
		$this->module_key = '5cab7347ab5e4b12806ca91019ec40ba';
		$this->accesslog = false;
		parent::__construct ();
		$this->bootstrap = true;
		$this->page = basename ( __FILE__, '.php' );
		$this->tab = 'pricing_promotion';
		$this->author = 'Timactive';
		$this->displayName = $this->l ( 'Gift card' );
		$this->description = $this->l ( 'Add the functionality needed to the creation, management and use of gift card.' );
		$this->template_front_directory = dirname ( __FILE__ ).'/views/templates/front/';
		$this->template_admin_directory = dirname ( __FILE__ ).'/views/templates/admin/';
		$this->confirmUninstall = $this->l ( 'Are you sure uninstall module gift card , all information will be deleted?' );
		$this->log_directory = dirname ( __FILE__ ).'/logs/';
		$this->img_directory = _MODULE_DIR_.'giftcard/img/';
		$this->log_fic_name = '';
		$this->ps_versions_compliancy['min'] = '1.5';
		$this->imagick = false;
		if (extension_loaded ( 'imagick' ))
			$this->imagick = true;
		if ((Configuration::get('GIFTCARD_VERSION') != $this->version) && Configuration::get('GIFTCARD_VERSION'))
			$this->runUpgrades(true);
	}
	public function install($delete_params = true)
	{
		if (!parent::install() ||
			! $this->registerHook ( 'actionValidateOrder' ) ||
			! $this->registerHook ( 'shoppingCart' ) ||
			! $this->registerHook ( 'actionOrderStatusUpdate' ) ||
			! $this->registerHook ( 'displayHeader' ) ||
			! $this->registerHook ( 'displayAdminOrder' ) ||
			! $this->registerHook ( 'displayFooter' ) ||
			! $this->registerHook ( 'displayLeftColumn' ))
				return false;
		if ($delete_params)
		{
			$sql = '';
			include (dirname ( __FILE__ ).'/sql/sql-install.php');
			/* ISSUE 3241 : Catbibi */
			Configuration::updateGlobalValue ( 'PS_CUSTOMIZATION_FEATURE_ACTIVE', '1' );
			Configuration::updateGlobalValue ( 'PS_VIRTUAL_PROD_FEATURE_ACTIVE', '1' );
			foreach ($sql as $s)
			{
				if (! Db::getInstance ()->execute ( $s ))
					return false;
			}
			$categorygift = new Category ();
			$languages = Language::getLanguages ();
			foreach ($languages as $language)
			{
				if ($language['iso_code'] == 'fr')
					$categorygift->name[(int)$language['id_lang']] = 'Module Carte Cadeau ne pas supprimer';
				else
					$categorygift->name[(int)$language['id_lang']] = 'Gift Card Addon no delete';
			}
			foreach ($languages as $language)
			{
				if ($language['iso_code'] == 'fr')
					$categorygift->link_rewrite[(int)$language['id_lang']] = 'carte-cadeau-'.(int)$language['id_lang'];
				else
					$categorygift->link_rewrite[(int)$language['id_lang']] = 'gift-card-'.(int)$language['id_lang'];
			}
			$categorygift->active = 0;
			$catroot = Category::getRootCategory ();
			Configuration::updateValue ( 'GIFTCARD_TOKEN', $this->generateCode ( '', 20, false ) );
			Configuration::updateValue ( 'GIFTCARD_PREFIX', 'GC_' );
			/* from */
			$from_l = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$from_l[$language['id_lang']] = 'De la part de';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$from_l[$language['id_lang']] = 'Desde';
				else
					$from_l[$language['id_lang']] = 'From';
			}
			Configuration::updateValue ( 'GIFTCARD_CF_FROM', $from_l );
			Configuration::updateValue ( 'GIFTCARD_PDF_IMG_WIDTH', 300 );
			Configuration::updateValue ( 'GIFTCARD_PDF_IMG_HEIGHT', 191 );
			Configuration::updateValue ( 'GIFTCARD_MAIL_IMG_WIDTH', 300 );
			Configuration::updateValue ( 'GIFTCARD_MAIL_IMG_HEIGHT', 191 );
			Configuration::updateValue ( 'GIFTCARD_FRONT_IMG_WIDTH', 300 );
			Configuration::updateValue ( 'GIFTCARD_FRONT_IMG_HEIGHT', 191 );
			Configuration::updateValue ( 'GIFTCARD_FRONT_LIMG_WIDTH', 800 );
			Configuration::updateValue ( 'GIFTCARD_FRONT_LIMG_HEIGHT', 460 );
			$frontcontent_install_dir = _PS_ROOT_DIR_.'/modules/giftcard/datadefault/frontcontent/';
			$frontcontents = array ();
			foreach ($languages as $language)
			{
				$content = '';
				if (@file_exists ( $frontcontent_install_dir.$language['iso_code'].'/content.html' ))
					$content = @Tools::file_get_contents ( $frontcontent_install_dir.$language['iso_code'].'/content.html' );
				elseif (@file_exists ( $frontcontent_install_dir.'en/content.html' ))
					$content = @Tools::file_get_contents ( $frontcontent_install_dir.'en/content.html' );
				$frontcontents[$language['id_lang']] = $content;
			}
			Configuration::updateValue ( 'GIFTCARD_FRONT_CONTENT', $frontcontents, true );
			$to_l = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$to_l[$language['id_lang']] = 'Email destinataire';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$to_l[$language['id_lang']] = 'Email destinatario';
				else
					$to_l[$language['id_lang']] = 'Email recipient';
			}
			Configuration::updateValue ( 'GIFTCARD_CF_TO', $to_l );
			$to_postal_address_l = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$to_postal_address_l[$language['id_lang']] = 'Adresse postale';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$to_postal_address_l[$language['id_lang']] = 'Adresse postale';
				else
					$to_postal_address_l[$language['id_lang']] = 'Postal address';
			}
			Configuration::updateValue ( 'GIFTCARD_CF_TO_ADDRESS_POSTAL', $to_postal_address_l );
			$lastname_l = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$lastname_l[$language['id_lang']] = 'Nom';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$lastname_l[$language['id_lang']] = 'Appelido';
				else
					$lastname_l[$language['id_lang']] = 'Lastname';
			}
			Configuration::updateValue ( 'GIFTCARD_CF_LASTNAME', $lastname_l );
			$deliverydatel = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$deliverydatel[$language['id_lang']] = 'Date d\'envoi';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$deliverydatel[$language['id_lang']] = 'Fecha de envío';
				else
					$deliverydatel[$language['id_lang']] = 'Date send';
			}
			Configuration::updateValue ( 'GIFTCARD_CF_DATESEND', $deliverydatel );
			$message_l = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$message_l[$language['id_lang']] = 'Message';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$message_l[$language['id_lang']] = 'Mensaje';
				else
					$message_l[$language['id_lang']] = 'Message';
			}
			Configuration::updateValue ( 'GIFTCARD_CF_MESSAGE', $message_l );
			$template_l = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$template_l[$language['id_lang']] = 'Modèle';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$template_l[$language['id_lang']] = 'Modelo';
				else
					$template_l[$language['id_lang']] = 'Template';
			}
			Configuration::updateValue ( 'GIFTCARD_CF_TEMPLATE', $template_l );
			$objs = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$objs[$language['id_lang']] = 'Carte cadeau offerte par %s';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$objs[$language['id_lang']] = 'Tarjeta regalo ofrecido por %s';
				else
					$objs[$language['id_lang']] = 'Gift card offer from %s';
			}
			Configuration::updateValue ( 'GIFTCARD_MAIL_OBJ_REC', $objs );
			$objs = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$objs[$language['id_lang']] = 'Votre carte cadeau';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$objs[$language['id_lang']] = 'Tarjeta regalo';
				else
					$objs[$language['id_lang']] = 'Your gift card';
			}
			Configuration::updateValue ( 'GIFTCARD_MAIL_OBJ_CUST', $objs );
			$pdf_install_dir = _PS_ROOT_DIR_.'/modules/giftcard/datadefault/pdf/';
			$pdfcontents = array ();
			foreach ($languages as $language)
			{
				$content = '';
				if (@file_exists ( $pdf_install_dir.$language['iso_code'].'/pdfcontent.html' ))
					$content = @Tools::file_get_contents ( $pdf_install_dir.$language['iso_code'].'/pdfcontent.html' );
				elseif (@file_exists ( $pdf_install_dir.'en/pdfcontent.html' ))
					$content = @Tools::file_get_contents ( $pdf_install_dir.'en/pdfcontent.html' );
				$pdfcontents[$language['id_lang']] = $content;
			}
			Configuration::updateValue ( 'GIFTCARD_PDF_CONTENT', $pdfcontents, true );
			$pdf_prefixs = array ();
			foreach ($languages as $language)
			{
				if (Tools::strtolower ( $language['iso_code'] ) == 'fr')
					$pdf_prefixs[$language['id_lang']] = 'CARTECADEAU-';
				elseif (Tools::strtolower ( $language['iso_code'] ) == 'es')
					$pdf_prefixs[$language['id_lang']] = 'TARJETAREGALO-';
				else
					$pdf_prefixs[$language['id_lang']] = 'GIFTCARD-';
			}
			Configuration::updateValue ( 'GIFTCARD_PDF_PREFIX', $pdf_prefixs );
			Configuration::updateValue('GIFTCARD_VERSION', $this->version);
			$categorygift->id_parent = $catroot->id;
			if (! $categorygift->add ())
				return false;
			if (! Configuration::updateValue ( 'GIFTCARD_CATEGORY_ID', $categorygift->id ))
				return false;
			if (! $this->installModuleTab ())
				return false;
		}
		return true;
	}
	public function runUpgrades($install = false)
	{
		if (Configuration::get('GIFTCARD_VERSION') != $this->version)
			foreach (array('1.0.5','1.0.6','1.0.7','1.0.11','1.0.12') as $version)
			{
				$file = dirname(__FILE__).'/upgrade/install-'.$version.'.php';
				if (version_compare($version, Configuration::get('GIFTCARD_VERSION')) > 0 && file_exists($file))
				{
					include_once $file;
					call_user_func('upgrade_module_'.str_replace('.', '_', $version), $this, $install);
				}
			}
		Configuration::updateValue('GIFTCARD_VERSION', $this->version);
	}
	public function reset()
	{
		if (!$this->uninstall(false))
			return false;
		if (!$this->install(false))
			return false;
		return true;
	}
	public function uninstall($delete_params = true)
	{
		if (! parent::uninstall ())
			return false;
		if ($delete_params)
		{
			if (!Configuration::deleteByName ( 'GIFTCARD_CATEGORY_ID' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_TOKEN' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_PREFIX' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_CF_FROM' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_CF_TO' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_CF_TO_ADDRESS_POSTAL' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_CF_DATESEND' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_CF_MESSAGE' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_CF_TEMPLATE' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_MAIL_OBJ_REC' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_MAIL_OBJ_CUST' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_PDF_CONTENT' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_PDF_IMG_WIDTH' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_PDF_IMG_HEIGHT' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_MAIL_IMG_WIDTH' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_MAIL_IMG_HEIGHT' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_FRONT_IMG_WIDTH' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_FRONT_IMG_HEIGHT' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_FRONT_LIMG_WIDTH' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_FRONT_LIMG_HEIGHT' ) ||
				!Configuration::deleteByName ( 'GIFTCARD_PDF_PREFIX' ) ||
				!$this->uninstallModuleTab ())
					return false;
			$sql = '';
			include (dirname ( __FILE__ ).'/sql/sql-uninstall.php');
			$giftcards = GiftCardProduct::getGiftCards ( null );
			$templates = GiftCardTemplate::getTemplates ();
			/* delete properly giftcards */
			foreach ($giftcards as $giftcard)
			{
				$product = new Product ( (int)$giftcard['id_product'] );
				if ($product->id && !$product->delete ())
					return false;
			}
			/* delete properly template */
			foreach ($templates as $template)
			{
				$template = new GiftCardTemplate ( (int)$template['id_gift_card_template'] );
				if ($template->id && !$template->delete ())
					return false;
			}
			foreach ($sql as $s)
			{
				if (! Db::getInstance ()->execute ( $s ))
					return false;
			}
			if (($id_category = (int)Configuration::get ( 'GIFTCARD_CATEGORY_ID' )) && $id_category > 0)
			{
				$category = new Category ( $id_category );
				if (! $category->delete ())
					return false;
			}
		}
		return true;
	}
	/* Installation de l'onglet */
	private function installModuleTab()
	{
		$admin_tab_catalog_id = Tab::getIdFromClassName ( 'AdminCatalog' );
		$admin_tab_order_id = Tab::getIdFromClassName ( 'AdminOrders' );
		/* Gift Card Template */
		$tabgiftcardtemplate = new Tab ();
		$languages = Language::getLanguages ( true );
		foreach ($languages as $language)
		{
			if ($language['iso_code'] == 'fr')
				$tabgiftcardtemplate->name[(int)$language['id_lang']] = 'Cartes Cadeaux Modèles';
			else
				$tabgiftcardtemplate->name[(int)$language['id_lang']] = 'Templates Gift Cards';
		}
		$tabgiftcardtemplate->class_name = 'AdminGiftCardTemplate';
		$tabgiftcardtemplate->module = 'giftcard';
		$tabgiftcardtemplate->id_parent = $admin_tab_catalog_id;
		if (! $tabgiftcardtemplate->save ())
			return false;
		$tabgiftcardproduct = new Tab ();
		foreach ($languages as $language)
		{
			if ($language['iso_code'] == 'fr')
				$tabgiftcardproduct->name[(int)$language['id_lang']] = 'Cartes Cadeaux';
			else
				$tabgiftcardproduct->name[(int)$language['id_lang']] = 'Gift Cards';
		}
		$tabgiftcardproduct->class_name = 'AdminGiftCard';
		$tabgiftcardproduct->module = 'giftcard';
		$tabgiftcardproduct->id_parent = $admin_tab_catalog_id;
		if (! $tabgiftcardproduct->save ())
			return false;
		$tabgiftcardorder = new Tab ();
		foreach ($languages as $language)
		{
			if ($language['iso_code'] == 'fr')
				$tabgiftcardorder->name[(int)$language['id_lang']] = 'Cartes Cadeaux';
			else
				$tabgiftcardorder->name[(int)$language['id_lang']] = 'Gift Cards';
		}
		$tabgiftcardorder->class_name = 'AdminGiftCardOrder';
		$tabgiftcardorder->module = 'giftcard';
		$tabgiftcardorder->id_parent = $admin_tab_order_id;
		if (! $tabgiftcardorder->save ())
			return false;
		return true;
	}
	private function uninstallModuleTab()
	{
		$id_tab_product = Tab::getIdFromClassName ( 'AdminGiftCard' );
		$id_tab_order = Tab::getIdFromClassName ( 'AdminGiftCardOrder' );
		$id_tab_template = Tab::getIdFromClassName ( 'AdminGiftCardTemplate' );
		if ($id_tab_template != 0)
		{
			$tab_template = new Tab ( $id_tab_template );
			$tab_template->delete ();
		}
		if ($id_tab_product != 0)
		{
			$tab_product = new Tab ( $id_tab_product );
			$tab_product->delete ();
		}
		if ($id_tab_order != 0)
		{
			$tab_order = new Tab ( $id_tab_order );
			$tab_order->delete ();
			return true;
		}
		return false;
	}
	public function hookDisplayLeftColumn($params)
	{
		$this->context->controller->addCSS ( ($this->_path).'css/giftcard.css', 'all' );
		$this->context->smarty->assign ( array (
			'link_choicegiftcard' => $this->context->link->getModuleLink ( 'giftcard', 'choicegiftcard' )
		) );
		return $this->display ( __FILE__, 'blockgiftcard.tpl' );
	}
	public function hookDisplayRightColumn($params)
	{
		return $this->hookDisplayLeftColumn ( $params );
	}
	public function hookDisplayHeader()
	{
	}
	public function hookDisplayFooter()
	{
		if ((int)Configuration::get ( 'GIFTCARD_TRIGGERWEB' ) == 1 && (int)date ( 'Ymd' ) > (int)Configuration::get ( 'GIFTCARD_BATCHLASTDATE' ))
			return '<img src="'.$this->_path.'cron.php?token='.Configuration::get ( 'GIFTCARD_TOKEN' ).
		'&time='.time ().'" alt="" width="0" height="0" style="border:none;margin:0; padding:0"/>';
	}
	public function hookDisplayAdminOrder($params)
	{
		$errors = array ();
		$infos = array ();
		$warnings = array ();
		if (Tools::isSubmit ( 'cancelGiftCardOrder' ) && Validate::isUnsignedId ( Tools::getValue ( 'id_gift_card_order' ) ))
		{
			$giftcard_order = new GiftCardOrder ( (int)Tools::getValue ( 'id_gift_card_order' ) );
			if (isset ( $giftcard_order->id_cart_rule ) && (int)$giftcard_order->id_cart_rule > 0)
			{
				$cart_rule = new CartRule ( (int)$giftcard_order->id_cart_rule );
				$cart_rule->active = 0;
				if (! $cart_rule->update ())
					$errors[] = $this->l ( 'Error update cartrule' );
				else
				{
					$giftcard_order->status = 'CANCEL';
					$firstname = $this->context->employee->firstname;
					$lastname = $this->context->employee->lastname;
					$message = $this->l ( 'Cancel Gift card by employee ' ).Tools::ucfirst ( $firstname ).' '.Tools::ucfirst ( $lastname );
					$giftcard_order->addInfoLine ( $message );
					if (! $giftcard_order->update ())
						$errors[] = $this->l ( 'Error update gift card order' );
					else
						$infos[] = $this->l ( 'Gift card is canceled with successful' );
				}
			}
		}
		if (Tools::isSubmit ( 'activeGiftCardOrder' ) && Validate::isUnsignedId ( Tools::getValue ( 'id_gift_card_order' ) ))
		{
			$giftcard_order = new GiftCardOrder ( (int)Tools::getValue ( 'id_gift_card_order' ) );
			if (isset ( $giftcard_order->id_cart_rule ) && (int)$giftcard_order->id_cart_rule > 0)
			{
				$cart_rule = new CartRule ( (int)$giftcard_order->id_cart_rule );
				$cart_rule->active = 1;
				if (! $cart_rule->update ())
					$errors[] = $this->l ( 'Error update cartrule' );
				else
				{
					$giftcard_order->status = 'CREATED';
					$firstname = $this->context->employee->firstname;
					$lastname = $this->context->employee->lastname;
					$message = $this->l ( 'Active Gift card by employee ' ).Tools::ucfirst ( $firstname ).' '.Tools::ucfirst ( $lastname );
					$giftcard_order->addInfoLine ( $message );
					if (! $giftcard_order->update ())
						$errors[] = $this->l ( 'Error update gift card order' );
					else
						$infos[] = $this->l ( 'Gift card is actived with successful' );
				}
			}
		}
		$this->_html = '';
		$current_index = AdminController::$currentIndex;
		$id_order = (int)$params['id_order'];
		$giftcardsorder = GiftCardOrder::exists ( $id_order, null, true );
		$purchaseorders = GiftCardOrder::getPurchaseOrders ( (int)$params['id_order'], (int)$this->context->language->id );
		if (($giftcardsorder && isset ( $giftcardsorder )) || ($purchaseorders && isset ( $purchaseorders )))
		{
			$order = new Order ( (int)$id_order );
			$order_state = new OrderState ( $order->current_state );
			$this->context->smarty->assign ( array (
				'ps_version' => _PS_VERSION_,
				'order' => $order,
				'currentState' => $order_state,
				'infos' => $infos,
				'errors' => $errors,
				'warnings' => $warnings,
				'current_index' => $current_index,
				'purchaseorders' => $purchaseorders,
				'giftcardsorder' => $giftcardsorder,
				'base_url' => _PS_BASE_URL_.__PS_BASE_URI__,
				'params' => $params
			) );
			$this->_html .= $this->display ( __FILE__, 'admin_order.tpl' );
		}
		return ($this->_html);
	}
	public function isGiftCard($id_product)
	{
		return (GiftCardProduct::isGiftCard ( $id_product ));
	}
	public static function getOrderedCustomizationsInProduct($id_cart, $id_product)
	{
		if (! $result = Db::getInstance ( _PS_USE_SQL_SLAVE_ )->executeS ( 'SELECT `id_customization`, `quantity`,`id_product`
				FROM `'._DB_PREFIX_.'customization`
				WHERE `id_product`='.(int)$id_product.' AND `id_cart` = '.(int)$id_cart))
			return false;
		$customizations = array ();
		foreach ($result as $row)
			$customizations[(int)$row['id_customization']] = $row;
		return $customizations;
	}
	public static function getCustDataValue($id_customization, $index)
	{
		$sql = 'SELECT `value` FROM `'._DB_PREFIX_.'customized_data`
				 WHERE `id_customization` = '.(int)$id_customization.' AND `index`='.$index;
		if (! $result = Db::getInstance ( _PS_USE_SQL_SLAVE_ )->getRow ( $sql ))
			return false;
		return $result['value'];
	}
	public function hookActionValidateOrder($params)
	{
		// $currency = $params['currency'];
		$order = $params['order'];
		$order_status = $params['orderStatus'];
		if (!GiftCardOrder::exists ( $order->id) &&
				isset($params['cart']) &&
				(int)$params['cart']->id &&
				!GiftCardOrder::cartexists ( (int)$params['cart']->id))
		{
			$customer = $params['customer'];
			$products = $params['cart']->getProducts ();
			// $currency_default = (int)Configuration::get('PS_CURRENCY_DEFAULT');
			foreach ($products as $product)
			{
				$id_product = (int)$product['id_product'];
				// $productqtyincart = (int)$product['cart_quantity'];
				$productqtydo = 0;
				$card = new GiftCardProduct ( $id_product );
				if (GiftCardProduct::isGiftCard ( (int)$product['id_product'] ))
				{
					/* first build customizations */
					$customizations = self::getOrderedCustomizationsInProduct ( $params['cart']->id, $id_product );
					foreach ($customizations as $customization)
					{
						$customquantity = (int)$customization['quantity'];
						$from = self::getCustDataValue ( $customization['id_customization'], $card->id_customization_field_from );
						$template_id = self::getCustDataValue ( $customization['id_customization'], $card->id_customization_field_template );
						$mail_to = self::getCustDataValue ( $customization['id_customization'], $card->id_customization_field_mailto );
						$postal_address_to = self::getCustDataValue ( $customization['id_customization'], $card->id_customization_field_to_postal_address );
						$lastname = self::getCustDataValue ( $customization['id_customization'], $card->id_customization_field_lastname );
						$message = self::getCustDataValue ( $customization['id_customization'], $card->id_customization_field_message );
						$delivery_date = self::getCustDataValue ( $customization['id_customization'], $card->id_customization_field_deliverydate );
						$i = 0;
						for ($i = 1; $i <= $customquantity; $i ++)
						{
							$giftcard_order = new GiftCardOrder ();
							$giftcard_order->id_customization = (int)$customization['id_customization'];
							$giftcard_order->id_gift_card_template = (int)$template_id;
							$giftcard_order->receptmode = ((isset ( $mail_to ) && ! empty ( $mail_to ) && trim ( $mail_to ) != '') ? 1 : 0);
							$giftcard_order->to_mail = ((isset ( $mail_to ) && trim ( $mail_to ) != '') ? $mail_to : null);
							$giftcard_order->to_postal_address = ((isset ( $postal_address_to ) && trim ( $postal_address_to ) != '') ? $postal_address_to : null);
							$giftcard_order->message = ((isset ( $message ) && trim ( $message ) != '') ? $message : null);
							$giftcard_order->lastname = ((isset ( $lastname ) && trim ( $lastname ) != '') ? $lastname : null);
							$giftcard_order->from = ((isset ( $from ) && trim ( $from ) != '') ? $from : null);
							$giftcard_order->sended = false;
							$giftcard_order->period_val = $card->period_val;
							$giftcard_order->delivery_date = ((isset ( $delivery_date ) && trim ( $delivery_date ) != '') ? $delivery_date : null);
							$giftcard_order->customer_mail = $customer->email;
							$giftcard_order->id_currency = $order->id_currency;
							$giftcard_order->id_lang = $order->id_lang;
							$giftcard_order->id_cart = $params['cart']->id;
							$giftcard_order->id_order = $order->id;
							$giftcard_order->id_product = (int)$product['id_product'];
							$giftcard_order->price = $card->amount;
							$giftcard_order->status = 'WAIT';
							$giftcard_order->add ();
							$productqtydo ++;
						}
					}
				}
			}
			if ($order_status->logable)
				$this->buildAndSend ( $order->id );
		}
	}
	/*
	 * Hook call before update arg new_os is Hook::exec('actionOrderStatusUpdate', array( 'newOrderStatus' => $new_os, 'id_order' => (int)$order->id ));
	 */
	public function hookActionOrderStatusUpdate($params)
	{
		$new_os = $params['newOrderStatus'];
		$id_order = (int)$params['id_order'];
		if ($new_os->logable)
			$this->buildAndSend ( $id_order );
		/*
		 * if (!$new_os->logable) { }
		 */
	}
	/* Send mail to recipient of the gift card */
	public function sendPendingMailRecipient()
	{
		$giftcardorders = GiftCardOrder::getGiftCardOrdersToSend ();
		if ($giftcardorders)
		{
			foreach ($giftcardorders as $go)
			{
				$giftcard_order = new GiftCardOrder ( (int)$go['id_gift_card_order'] );
				$giftcard_order->sendingMail ();
				$giftcard_order->update ();
			}
		}
	}
	public static function generateCode($prefix = '', $length = 8, $cardrulecheck = false)
	{
		$code = '';
		$possible = '123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$maxlength = Tools::strlen ( $possible );
		if ($length > $maxlength)
			$length = $maxlength;
		$i = 0;
		while ($i < $length)
		{
			$char = Tools::substr ( $possible, mt_rand ( 0, $maxlength - 1 ), 1 );
			if (! strstr ( $code, $char ))
			{
				$code .= $char;
				$i ++;
			}
		}
		if ($cardrulecheck)
		{
			$id = CartRule::getIdByCode ( $prefix.$code );
			/* test si le code existe */
			if ($id && (int)$id > 0)
			{
				$length ++;
				return (self::generateCode ( $prefix, $length, $cardrulecheck ));
			}
		}
		return $prefix.$code;
	}
	public function buildAndSend($id_order)
	{
		if ($giftcardsorders = GiftCardOrder::exists ( $id_order, 'WAIT', true ))
		{
			if (isset ( $giftcardsorders ) && $giftcardsorders != false)
			{
				$order = new Order ( (int)$id_order );
				$customer = new Customer ( (int)$order->id_customer );
				foreach ($giftcardsorders as $key => $giftcardsorder)
				{
					/* for each */
					$giftcard_order = new GiftCardOrder ( (int)$giftcardsorder['id_gift_card_order'] );
					$giftcard = new GiftCardProduct ( (int)$giftcardsorder['id_product'] );
					$cart_rule = new CartRule ();
					/* Information */
					$cart_rule->highlight = 0;
					if (! isset ( $giftcard->id ) || ! Validate::isLoadedObject ( $giftcard ) || ! ((int)$giftcard->id > 0))
					{
						$languages = Language::getLanguages ();
						foreach ($languages as $language)
							if ($language['iso_code'] == 'fr')
								$cart_rule->name[(int)$language['id_lang']] = 'Carte cadeau '.(string)$giftcard_order->price;
							else
								$cart_rule->name[(int)$language['id_lang']] = 'Gift card '.(string)$giftcard_order->price;
					}
					else
						$cart_rule->name = $giftcard->name;
					$cart_rule->partial_use = (int)$giftcard->cr_partial_use;
					$cart_rule->free_shipping = (int)$giftcard->cr_free_shipping;
					/* Condition */
					$cart_rule->date_to = date ( 'Y-m-d H:i:s',
							mktime ( date ( 'H' ), date ( 'i' ), date ( 's' ),
									date ( 'm' ) + ((int)$giftcard_order->period_val), date ( 'd' ), date ( 'Y' ) ) );
					$cart_rule->date_from = date ( 'Y-m-d H:i:s' );
					$cart_rule->quantity = 1;
					$cart_rule->quantity_per_user = 1;
					/* minimum amount */
					$cart_rule->minimum_amount = 0;
					/* Compatibilité avec les autre règles paniers */
					$cart_rule->cart_rule_restriction = 0;
					/**
					 * **** Action *****
					 */
					$cart_rule->reduction_tax = 1;
					$cart_rule->reduction_currency = $giftcard_order->id_currency;
					$cart_rule->reduction_amount = $giftcard_order->price;
					$cart_rule->reduction_percent = 0;
					$cart_rule->reduction_product = 0;
					$prefix = Configuration::get ( 'GIFTCARD_PREFIX' );
					$code = $this->generateCode ( $prefix, 20, true );
					$cart_rule->code = $code;
					$default_lang = (int)Configuration::get ( 'PS_LANG_DEFAULT' );
					$language_default = new Language ( $default_lang );
					if (Tools::strtolower ( $language_default->iso_code ) == 'fr')
						$cart_rule->description = 'Module Carte cadeau';
					elseif (Tools::strtolower ( $language_default->iso_code ) == 'es')
						$cart_rule->description = 'Tarjeta de Regalo';
					else
						$cart_rule->description = 'Gift Card Addon';
					if ($cart_rule->add ())
						$giftcard_order->status = 'CREATED';
					else
						$giftcard_order->status = 'ERROR';
					$giftcard_order->discountcode = $cart_rule->code;
					$giftcard_order->id_cart_rule = $cart_rule->id;
					$giftcard_order->sended = 0;
					$giftcard_order->sendingMail ( $customer );
					$giftcard_order->update ();
				}
			}
		}
	}
	public function buildFieldLang($field_key = null)
	{
		$languages = Language::getLanguages ( true );
		$cffield_l = array ();
		foreach ($languages as $language)
			$cffield_l[$language['id_lang']] = Tools::getValue ( $field_key.'_'.$language['id_lang'] );
		return ($cffield_l);
	}
	public function validateCustomField($cffield_l, $field_name, $methodvalidate = 'isName')
	{
		$error = '';
		$default_lang = (int)Configuration::get ( 'PS_LANG_DEFAULT' );
		if (! isset ( $cffield_l[$default_lang] ) || empty ( $cffield_l[$default_lang] ))
		{
			$language_default = new Language ( $default_lang );
			$error .= $this->displayError ( sprintf ( $this->l ( 'Field \'%s\' is required for the default lang ' ).$language_default->iso_code,
					$field_name ) );
		}
		foreach ($cffield_l as $key => $cffield)
		{
			if (! empty ( $cffield ) && ! Validate::$methodvalidate ( $cffield ))
			{
				$language = new Language ( (int)$key );
				if ($language && isset ( $language->iso_code ) && Validate::isLanguageIsoCode ( $language->iso_code ))
					$error .= $this->displayError ( sprintf ( $this->l ( 'Field %1$s with value \'%2$s\' is not valid format, iso code ' ).$language->iso_code,
							$field_name,
							$cffield ) );
			}
		}
		return ($error);
	}
	public function getContent()
	{
		$output = null;
		if (Tools::isSubmit ( 'loaddefault' ))
		{
			$issvg = ((int)Tools::getValue ( 'withgenerictemplate' ) ? true : false);
			if (! $this->imagick && $issvg)
				$output .= $this->displayError ( $this->l ( 'To use generic templates, you must install imagick, imagick is a popular free extension php' ) );
			else
			{
				GiftCardTools::creatingDefaultTemplates ( $issvg );
				GiftCardTools::creatingDefaultGiftCards ();
			}
		}
		if (Tools::isSubmit ( 'submit'.$this->name ))
		{
			$prefix = (string)Tools::getValue ( 'prefix' );
			$front_content_l = $this->buildFieldLang ( 'front_content' );
			$errorreturn = self::validateCustomField ( $front_content_l, $this->l ( 'Text in front office' ), 'isCleanHtml' );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_FRONT_CONTENT', $front_content_l, true );
			else
				$output .= $errorreturn;
			$cf_template_l = $this->buildFieldLang ( 'cf_template' );
			$errorreturn = self::validateCustomField ( $cf_template_l, $this->l ( 'Template label' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_CF_TEMPLATE', $cf_template_l );
			else
				$output .= $errorreturn;
			$cf_from_l = $this->buildFieldLang ( 'cf_from' );
			$errorreturn = self::validateCustomField ( $cf_from_l, $this->l ( 'From label' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_CF_FROM', $cf_from_l );
			else
				$output .= $errorreturn;
			$cf_lastname_l = $this->buildFieldLang ( 'cf_lastname' );
			$errorreturn = self::validateCustomField ( $cf_lastname_l, $this->l ( 'Lastname label' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_CF_LASTNAME', $cf_lastname_l );
			else
				$output .= $errorreturn;
			
			$cf_to_l = $this->buildFieldLang ( 'cf_to' );
			$errorreturn = self::validateCustomField ( $cf_to_l, $this->l ( 'Mail recipient label' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_CF_TO', $cf_to_l );
			else
				$output .= $errorreturn;
			
			$cf_to_address_postal_l = $this->buildFieldLang ( 'cf_to_address_postal' );
			$errorreturn = self::validateCustomField ( $cf_to_address_postal_l, $this->l ( 'Recipient postal address label' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_CF_TO_ADDRESS_POSTAL', $cf_to_address_postal_l );
			else
				$output .= $errorreturn;
			
			$cf_date_send_l = $this->buildFieldLang ( 'cf_date_send' );
			$errorreturn = self::validateCustomField ( $cf_date_send_l, $this->l ( 'Date send label' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_CF_DATESEND', $cf_date_send_l );
			else
				$output .= $errorreturn;
			$cf_message_l = $this->buildFieldLang ( 'cf_message' );
			$errorreturn = self::validateCustomField ( $cf_message_l, $this->l ( 'Message label' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_CF_MESSAGE', $cf_message_l );
			else
				$output .= $errorreturn;
			$cf_mail_obj_rec_l = $this->buildFieldLang ( 'mail_obj_rec' );
			$errorreturn = self::validateCustomField ( $cf_mail_obj_rec_l, $this->l ( 'Subject of email recipient' ), 'isMailSubject' );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_MAIL_OBJ_REC', $cf_mail_obj_rec_l );
			else
				$output .= $errorreturn;
			$cf_mail_obj_cust_l = $this->buildFieldLang ( 'mail_obj_cust' );
			$errorreturn = self::validateCustomField ( $cf_mail_obj_cust_l, $this->l ( 'Subject of email customer' ), 'isMailSubject' );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_MAIL_OBJ_CUST', $cf_mail_obj_cust_l );
			else
				$output .= $errorreturn;
			$pdf_content_l = $this->buildFieldLang ( 'pdf_content' );
			$errorreturn = self::validateCustomField ( $pdf_content_l, $this->l ( 'PDF content' ), 'isCleanHtml' );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_PDF_CONTENT', $pdf_content_l, true );
			else
				$output .= $errorreturn;
			$pdf_prefix_l = $this->buildFieldLang ( 'pdf_prefix' );
			$errorreturn = self::validateCustomField ( $pdf_prefix_l, $this->l ( 'PDF prefix' ) );
			if (empty ( $errorreturn ))
				Configuration::updateValue ( 'GIFTCARD_PDF_PREFIX', $pdf_prefix_l );
			else
				$output .= $errorreturn;
			$pdf_img_width = (int)Tools::getValue ( 'pdf_img_width', 0 );
			if (! $pdf_img_width > 0)
				$output .= $this->displayError ( $this->l ( 'The field width must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_PDF_IMG_WIDTH', $pdf_img_width );
			$pdf_img_height = (int)Tools::getValue ( 'pdf_img_height', 0 );
			if (! $pdf_img_height > 0)
				$output .= $this->displayError ( $this->l ( 'The field height must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_PDF_IMG_HEIGHT', $pdf_img_height );
			$front_img_width = (int)Tools::getValue ( 'front_img_width', 0 );
			if (! $front_img_width > 0)
				$output .= $this->displayError ( $this->l ( 'The field width must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_FRONT_IMG_WIDTH', $front_img_width );
			$front_img_height = (int)Tools::getValue ( 'front_img_height', 0 );
			if (! $front_img_height > 0)
				$output .= $this->displayError ( $this->l ( 'The field height must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_FRONT_IMG_HEIGHT', $front_img_height );
			$front_limg_width = (int)Tools::getValue ( 'front_limg_width', 0 );
			if (! $front_limg_width > 0)
				$output .= $this->displayError ( $this->l ( 'The field width must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_FRONT_LIMG_WIDTH', $front_limg_width );
			$front_limg_height = (int)Tools::getValue ( 'front_limg_height', 0 );
			if (! $front_limg_height > 0)
				$output .= $this->displayError ( $this->l ( 'The field height must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_FRONT_LIMG_HEIGHT', $front_limg_height );
			$mail_img_width = (int)Tools::getValue ( 'mail_img_width', 0 );
			if (! $mail_img_width > 0)
				$output .= $this->displayError ( $this->l ( 'The field img width must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_MAIL_IMG_WIDTH', $mail_img_width );
			$mail_img_height = (int)Tools::getValue ( 'pdf_img_height', 0 );
			if (! $mail_img_height > 0)
				$output .= $this->displayError ( $this->l ( 'The field img height must superior to 0' ) );
			else
				Configuration::updateValue ( 'GIFTCARD_MAIL_IMG_HEIGHT', $mail_img_height );
			GiftCardProduct::updateAllCustomFields ();
			$triggerweb = (int)Tools::getValue ( 'triggerweb', 0 );
			Configuration::updateValue ( 'GIFTCARD_TRIGGERWEB', $triggerweb );
			if (! $prefix || empty ( $prefix ) || ! Validate::isGenericName ( $prefix ))
				$output .= $this->displayError ( $this->l ( 'Invalid Configuration value' ) );
			else
			{
				Configuration::updateValue ( 'GIFTCARD_PREFIX', $prefix );
				$output .= $this->displayConfirmation ( $this->l ( 'Settings updated' ) );
			}
		}
		if (Tools::isSubmit ( 'previewpdf' ))
		{
			$giftcard_template = GiftCardTemplate::getDefault ();
			if (! $giftcard_template)
				$output .= $this->displayError ( $this->l ( 'Default gift card template is required to preview PDF' ) );
			else
			{
				$params = array ();
				$params['id_shop'] = (int)Configuration::get ( 'PS_SHOP_DEFAULT' );
				$params['id_currency'] = (int)Configuration::get ( 'PS_CURRENCY_DEFAULT' );
				$params['id_gift_card_order'] = 1;
				$params['discountcode'] = 'GC_JN43F8Q2OMTG7K1CREDP';
				$params['price'] = 100;
				$params['date_to'] = Tools::displayDate ( date ( 'Y-m-d', strtotime ( date ( 'Y-m-d', time () ).' + 365 day' ) ) );
				$params['id_lang'] = (int)Tools::getValue ( 'pdfpreview_id_lang', Configuration::get ( 'PS_LANG_DEFAULT' ) );
				$params['message'] = 'Message';
				$params['lastname'] = 'John';
				$params['from'] = 'William';
				$prefix_pdf = Configuration::get ( 'GIFTCARD_PDF_PREFIX', (int)$params['id_lang'] );
				if (! $prefix_pdf || empty ( $prefix_pdf ))
					$prefix_pdf = 'GIFTCARD';
				$filename = $prefix_pdf.sprintf ( '%06d', 0 );
				GiftCardTools::processGeneratePdfV2 ( $giftcard_template, $params, true, $filename );
			}
		}
		return $output.$this->displayForm ();
	}
	public function displayForm()
	{
		// Get default Language
		$default_lang = (int)Configuration::get ( 'PS_LANG_DEFAULT' );
		$cronurl = Tools::getProtocol ().$_SERVER['HTTP_HOST'].__PS_BASE_URI__.'modules/giftcard/cron.php?token='.Configuration::get ( 'GIFTCARD_TOKEN' );
		// Init Fields form array
		// $giftcards = GiftCardProduct::getGiftCards();
		if ($this->imagick)
			$description_useimagick = $this->l ( 'Imagick is installed you can use generic template' );
		else
			$description_useimagick = $this->l ( 'To use generic templates, you must install imagick, imagick is a popular free extension php' );
		$fields_form = array ();
		$fields_form[0]['form'] = array (
			'legend' => array (
				'title' => $this->l ( 'Documentation' ),
				'image' => $this->img_directory.'icon/pdf.gif'
			),
			'description' => $this->l ( 'Read' ).'&nbsp;'.$this->l ( 'before use it' ).'<br/>
			<a href="http://support.timactive.com/attachments/download/2353/README_EN.pdf" target="blank"><img src="'.$this->img_directory.'icon/doc_en.png'.'"/></a>&nbsp;
			<a href="http://support.timactive.com/attachments/download/2348/README_FR.pdf" target="blank"><img src="'.$this->img_directory.'icon/doc_fr.png'.'"/></a>&nbsp;'
		);
		$fields_form[1]['form'] = array (
			'legend' => array (
				'title' => $this->l ( 'Front office and configuration' ),
				'image' => $this->img_directory.'icon/front_conf.png'
			),
			'input' => array (
				array (
					'type' => 'text',
					'label' => $this->l ( 'Prefix' ),
					'name' => 'prefix',
					'desc' => $this->l ( 'Prefix used for gift card code (e.g. GC-****************)' ),
					'size' => 15,
					'required' => true
				),
				array (
					'type' => 'textarea',
					'label' => $this->l ( 'Text in front office:' ),
					'name' => 'front_content',
					'autoload_rte' => true,
					'required' => true,
					'lang' => true,
					'rows' => 10,
					'cols' => 100,
					'hint' => $this->l ( 'Invalid characters:' ).' <>;=#{}'
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Height card image' ),
					'name' => 'front_img_height',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Height card image in front office' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Width card image' ),
					'name' => 'front_img_width',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Width card image in front office' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Height card image' ),
					'name' => 'front_limg_height',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Height large card image in front office' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Width card image' ),
					'name' => 'front_limg_width',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Width large card image in front office' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Template label' ),
					'name' => 'cf_template',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'hint' => $this->l ( 'Invalid characters: numbers and' ).' !<>,;?=+()@#"�{}_$%:',
					'desc' => $this->l ( 'This will be displayed in the cart summary, as well as on the invoice' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'From label' ),
					'name' => 'cf_from',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'hint' => $this->l ( 'Invalid characters: numbers and' ).' !<>,;?=+()@#"�{}_$%:',
					'desc' => $this->l ( 'This will be displayed in the cart summary, as well as on the invoice' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Lastname label' ),
					'name' => 'cf_lastname',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'hint' => $this->l ( 'Invalid characters: numbers and' ).' !<>,;?=+()@#"�{}_$%:',
					'desc' => $this->l ( 'This will be displayed in the cart summary, as well as on the invoice' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Mail recipient label' ),
					'name' => 'cf_to',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'hint' => $this->l ( 'Invalid characters: numbers and' ).' !<>,;?=+()@#"�{}_$%:',
					'desc' => $this->l ( 'This will be displayed in the cart summary, as well as on the invoice' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Message label' ),
					'name' => 'cf_message',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'hint' => $this->l ( 'Invalid characters: numbers and' ).' !<>,;?=+()@#"�{}_$%:',
					'desc' => $this->l ( 'This will be displayed in the cart summary, as well as on the invoice' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Date send label' ),
					'name' => 'cf_date_send',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'hint' => $this->l ( 'Invalid characters: numbers and' ).' !<>,;?=+()@#"�{}_$%:',
					'desc' => $this->l ( 'This will be displayed in the cart summary, as well as on the invoice' )
				)
			),
			'submit' => array (
				'title' => $this->l ( 'Save' ),
				'class' => 'button'
			)
		);
		$fields_form[2]['form'] = array (
			'legend' => array (
				'title' => $this->l ( 'Email' ),
				'image' => $this->img_directory.'icon/mail.png'
			),
			'input' => array (
				array (
					'type' => 'text',
					'label' => $this->l ( 'Mail subject' ),
					'name' => 'mail_obj_cust',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'desc' => $this->l ( 'Received by the customer who purchased the gift card' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Mail subject' ),
					'name' => 'mail_obj_rec',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'desc' => $this->l ( 'Received by the recipient of the gift card' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Width card image' ),
					'name' => 'mail_img_width',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Width card image in mail' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Height card image' ),
					'name' => 'mail_img_height',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Height card image in mail' )
				)
			),
			'submit' => array (
				'title' => $this->l ( 'Save' ),
				'class' => 'button'
			)
		);
		$fields_form[3]['form'] = array (
			'legend' => array (
				'title' => $this->l ( 'Pdf' ),
				'image' => $this->img_directory.'icon/pdf.gif'
			),
			'input' => array (
				array (
					'type' => 'text',
					'label' => $this->l ( 'Pdf prefix' ),
					'name' => 'pdf_prefix',
					'lang' => true,
					'size' => 40,
					'required' => true,
					'desc' => $this->l ( 'Prefix used for gift card name (e.g. GIFTCARD-00001.pdf)' )
				),
				array (
					'type' => 'textarea',
					'label' => $this->l ( 'Pdf content:' ),
					'name' => 'pdf_content',
					'autoload_rte' => true,
					'required' => true,
					'lang' => true,
					'rows' => 10,
					'cols' => 100,
					'hint' => $this->l ( 'Invalid characters:' ).' <>;=#{}'
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Width card image' ),
					'name' => 'pdf_img_width',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Width card image in pdf generated' )
				),
				array (
					'type' => 'text',
					'label' => $this->l ( 'Height card image' ),
					'name' => 'pdf_img_height',
					'size' => 5,
					'required' => true,
					'suffix' => 'px',
					'desc' => $this->l ( 'Height card image in pdf generated' )
				),
				array (
					'type' => 'select',
					'label' => $this->l ( 'Preview PDF Language' ),
					'name' => 'pdfpreview_id_lang',
					'desc' => $this->l ( 'Use just to see the result' ),
					'required' => true,
					'options' => array (
						'query' => Language::getLanguages ( false ),
						'id' => 'id_lang',
						'name' => 'name'
				)
				)
			),
			'submit' => array (
				'title' => $this->l ( 'Save and Preview' ),
				'name' => 'previewpdf',
				'class' => 'button'
			)
		);
		$fields_form[4]['form'] = array (
			'legend' => array (
				'title' => $this->l ( 'Planification' ),
				'image' => $this->img_directory.'icon/time.gif'
			),
			'description' => $this->l ( 'A process is launched every day to check the gifts cards to send. For customers who checked a date specific delivery of the gift card at the time of purchase.' ),
			'input' => array (
				array (
					'type' => 'radio',
					'label' => $this->l ( 'First visitor' ),
					'name' => 'triggerweb',
					'required' => true,
					'class' => 't',
					'is_bool' => true,
					'values' => array (
						array (
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l ( 'First visitor' )
						),
						array (
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l ( 'Crontab' )
						)
					),
					'desc' => '<b><u>'.$this->l ( 'First visitor' ).'</u></b><br/>'.$this->l ( 'When the first visitor of the day connects to your site,is launched in background' ).'<br/>'.$this->l ( 'This solution is use when you cannot access a crontab on your server' ).'</br>'.'<b><u>'.$this->l ( 'Crontab' ).'</u></b><br/>'.$this->l ( 'Contact your host or webmaster to configure crontab.' ).'</br>'.$this->l ( 'The following lines are to be added to crontab :' ).'</br>'.'30 4 *       *       *       c'.'url <b>'.$cronurl.'</b></br>'.$this->l ( 'Also, giftcard is launch every day with a specific hour, here with this cron 4hours 30min AM' ).'</br>' 
				)
			),
			'submit' => array (
				'title' => $this->l ( 'Save' ),
				'class' => 'button'
			)
		);
		$fields_form[5]['form'] = array (
			'legend' => array (
				'title' => $this->l ( 'Ready to use' ),
				'image' => $this->img_directory.'icon/info.png'
			),
			'description' => $description_useimagick.'<br/>'.$this->l ( 'This action automatically creates models and gift cards.' ).'<br/>'.$this->l ( 'The process may take a few minutes..' ),
			'input' => array (
				array (
					'type' => 'radio',
					'label' => $this->l ( 'Customizable template' ),
					'name' => 'withgenerictemplate',
					'required' => true,
					'class' => 't',
					'is_bool' => true,
					'values' => array (
						array (
							'id' => 'withgenerictemplate_on',
							'value' => 1,
							'label' => $this->l ( 'Yes' )
						),
						array (
							'id' => 'withgenerictemplate_off',
							'value' => 0,
							'label' => $this->l ( 'No' )
						)
					)
				)
			),
			'submit' => array (
				'title' => $this->l ( 'Launch' ),
				'class' => 'button',
				'name' => 'loaddefault'
			)
		);
		$helper = new HelperForm ();
		// Module, token and currentIndex
		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite ( 'AdminModules' );
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		// Language
		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		// Title and toolbar
		$helper->title = $this->displayName;
		$helper->show_toolbar = true; // false -> remove toolbar
		$helper->toolbar_scroll = true; // yes - > Toolbar is always visible on the top of the screen.
		$helper->submit_action = 'submit'.$this->name;
		$languages = Language::getLanguages ( true );
		$nblang = count ( $languages );
		for ($i = 0; $i < $nblang; $i ++)
		{
			if ($languages[$i]['id_lang'] == $default_lang)
				$languages[$i]['is_default'] = 1;
			else
				$languages[$i]['is_default'] = 0;
		}
		$helper->languages = $languages;
		$helper->toolbar_btn = array (
			'save' => array (
				'desc' => $this->l ( 'Save' ),
				'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.'&token='.Tools::getAdminTokenLite ( 'AdminModules' )
			),
			'back' => array (
				'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite ( 'AdminModules' ),
				'desc' => $this->l ( 'Back to list' )
			)
		);
		// Load current value
		$helper->fields_value['withgenerictemplate'] = ($this->imagick ? 1 : 0);
		$helper->fields_value['prefix'] = Configuration::get ( 'GIFTCARD_PREFIX' );
		$helper->fields_value['pdf_img_width'] = Configuration::get ( 'GIFTCARD_PDF_IMG_WIDTH' );
		$helper->fields_value['pdf_img_height'] = Configuration::get ( 'GIFTCARD_PDF_IMG_HEIGHT' );
		$helper->fields_value['mail_img_width'] = Configuration::get ( 'GIFTCARD_PDF_IMG_WIDTH' );
		$helper->fields_value['mail_img_height'] = Configuration::get ( 'GIFTCARD_PDF_IMG_HEIGHT' );
		$helper->fields_value['front_img_width'] = Configuration::get ( 'GIFTCARD_FRONT_IMG_WIDTH' );
		$helper->fields_value['front_img_height'] = Configuration::get ( 'GIFTCARD_FRONT_IMG_HEIGHT' );
		$helper->fields_value['front_limg_width'] = Configuration::get ( 'GIFTCARD_FRONT_LIMG_WIDTH' );
		$helper->fields_value['front_limg_height'] = Configuration::get ( 'GIFTCARD_FRONT_LIMG_HEIGHT' );
		$helper->fields_value['triggerweb'] = Configuration::get ( 'GIFTCARD_TRIGGERWEB' );
		$helper->fields_value['pdfpreview_id_lang'] = Tools::getValue ( 'pdfpreview_id_lang', (int)Configuration::get ( 'PS_LANG_DEFAULT' ) );
		$helper->fields_value['pdfpreview_withinformation'] = (int)Tools::getValue ( 'pdfpreview_withinformation', 0 );
		$helper->fields_value['pdfpreview_id_gift_card'] = (int)Tools::getValue ( 'pdfpreview_id_gift_card', 0 );
		foreach ($languages as $language)
		{
			$helper->fields_value['front_content'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_FRONT_CONTENT', (int)$language['id_lang'] );
			$helper->fields_value['cf_template'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_CF_TEMPLATE', (int)$language['id_lang'] );
			$helper->fields_value['cf_from'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_CF_FROM', (int)$language['id_lang'] );
			$helper->fields_value['cf_to'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_CF_TO', (int)$language['id_lang'] );
			$helper->fields_value['cf_to_address_postal'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_CF_TO_ADDRESS_POSTAL', (int)$language['id_lang'] );
			$helper->fields_value['cf_lastname'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_CF_LASTNAME', (int)$language['id_lang'] );
			$helper->fields_value['cf_date_send'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_CF_DATESEND', (int)$language['id_lang'] );
			$helper->fields_value['cf_message'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_CF_MESSAGE', (int)$language['id_lang'] );
			$helper->fields_value['mail_obj_rec'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_MAIL_OBJ_REC', (int)$language['id_lang'] );
			$helper->fields_value['mail_obj_cust'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_MAIL_OBJ_CUST', (int)$language['id_lang'] );
			$helper->fields_value['pdf_content'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_PDF_CONTENT', (int)$language['id_lang'] );
			$helper->fields_value['pdf_prefix'][(int)$language['id_lang']] = Configuration::get ( 'GIFTCARD_PDF_PREFIX', (int)$language['id_lang'] );
		}
		return $helper->generateForm ( $fields_form );
	}
	public function accessLogsDirectory()
	{
		if (! is_readable ( _PS_ROOT_DIR_.'/modules/giftcard/logs' ) || ! is_writable ( _PS_ROOT_DIR_.'/modules/giftcard/logs' ))
			return (false);
		return true;
	}
	public function log($log_message)
	{
		if ($this->accesslog)
		{
			if ($this->log_fic_name == '')
				$this->log_fic_name = 'giftcardlog-'.date ( 'Ymd' ).'.log';
			$fp = @fopen ( $this->log_directory.$this->log_fic_name, 'a' );
			@fwrite ( $fp, date ( 'H\Hi' ).' '.$log_message.'\n' );
			@fclose ( $fp );
		}
	}
	public function loglongline($log_message)
	{
		if ($this->accesslog)
		{
			if ($this->log_fic_name == '')
				$this->log_fic_name = 'giftcardlog-'.date ( 'Ymd' ).'.log';
			$fp = @fopen ( $this->log_directory.'/'.$this->log_fic_name, 'a' );
			if ($log_message)
				@fwrite ( $fp, $log_message.'\n' );
			@fwrite ( $fp, '============================================================================\n' );
			@fclose ( $fp );
		}
	}
}
?>