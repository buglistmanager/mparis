{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $recommender_key}
	<div id="{$div_id|escape:'javascript':'UTF-8'}"></div>
	<script type="text/javascript">
    {literal}var _$brt = _$brt || [];
    _$brt.push(['registerRecommender', '{/literal}{$recommender_key|escape:'javascript':'UTF-8'}{literal}', '{/literal}{$div_id|escape:'javascript':'UTF-8'}{literal}', {item_id: '{/literal}{$product_id|escape:'javascript':'UTF-8'}{literal}'}]);{/literal}
	</script>
{/if}

{if $product_in_cart_update|@count > 0}
    <script type="text/javascript">
        {literal}var _$brt = _$brt || [];{/literal}
        {foreach from=$product_in_cart_update key=action item=products}
            {foreach from=$products item=product}
                {literal}
                _$brt.push(['{/literal}{$action|escape:'javascript':'UTF-8'}{literal}', {
                    item_id: {/literal}{$product.id_product|escape:'javascript':'UTF-8'}{literal},
                    item_variation_id: {/literal}{$product.id_product_attribute|escape:'javascript':'UTF-8'}{literal},
                    qnt: {/literal}{$product.qty|escape:'javascript':'UTF-8'}{literal},
                    price: {/literal}{$product.price|escape:'javascript':'UTF-8'}{literal},
                    qnt_after_event: {/literal}{$product.qnt_after_event|escape:'javascript':'UTF-8'}{literal},
                    cart_total: {/literal}{$product.cart_total|escape:'javascript':'UTF-8'}{literal},
                    shipping_price: {/literal}{$product.shipping_price|escape:'javascript':'UTF-8'}{literal},
                    discount_code: {/literal}{$product.discount_code|escape:'javascript':'UTF-8'}{literal},
                    discount_amount: {/literal}{$product.discount_amount|escape:'javascript':'UTF-8'}{literal},
                }]);{/literal}

            {/foreach}
        {/foreach}
    </script>
{/if}
