{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($brainify_key)}<script type="text/javascript">

	{assign var='controllerName' value=$smarty.get.controller}

	{literal}var _$brt = _$brt || [];{/literal}
	{if isset($product_id)}
		{literal}_$brt.push([['page_type', 'item page'],
		['item_id', '{/literal}{$product_id|escape:'javascript':'UTF-8'}{literal}'],
		['category_id', '{/literal}{$product_category_id|escape:'javascript':'UTF-8'}{literal}'],
		['item_variations_id', '{/literal}{$product_variations_id|escape:'javascript':'UTF-8'}{literal}'],
		['category_path', [{/literal}{$brainify_categories_id|escape:'javascript':'UTF-8'}]{literal}],
		['itemViewed', '{/literal}{$product_id|escape:'javascript':'UTF-8'}{literal}']]);{/literal}
	{/if}

	{if isset($category_id)}
		{literal}_$brt.push([['page_type', 'category page'],
		['category_id', '{/literal}{$category_id|escape:'javascript':'UTF-8'}{literal}'],
		['category_path', [{/literal}{$brainify_categories_id|escape:'javascript':'UTF-8'}]{literal}],
		['categoryViewed', '{/literal}{$category_id|escape:'javascript':'UTF-8'}{literal}']]);{/literal}
	{/if}

	{if $controllerName == 'index'}
		{literal}_$brt.push([['page_type', 'home page']]);{/literal}
	{/if}

	{if $controllerName == 'order'|| $controllerName == 'order-confirmation' || $controllerName == 'order-opc'}
		{literal}_$brt.push([['page_type', 'checkout page']]);{/literal}
	{/if}

	_$brt.push([['account_key', '{$brainify_key|escape:'javascript':'UTF-8'}'], ['flavour_key', '{$flavour_key|escape:'javascript':'UTF-8'}']]);
	{literal}(function() {
  		var bt = document.createElement('script');
  		bt.type = 'text/javascript';
  		bt.async = true;
		bt.src = document.location.protocol+'//static.brainify.io/bt-md.js';
  		var s = document.getElementsByTagName('script')[0];
  		s.parentNode.insertBefore(bt, s);
	})();{/literal}
</script>{/if}
