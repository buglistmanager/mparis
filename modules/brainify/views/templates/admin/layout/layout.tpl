{*
* Copyright (C) 2015-2016 Brainify - All Rights Reserved
*
* @author Brainify <tech@brainify.it>
* @copyright 2015-2016 Brainify
*}

<div class="container-fluid brainify-page">
    <div class="ba-header">
        {include file="./header.tpl"}
    </div>
    <div class="ba-content">
        {$bodyContent} {* $bodyContent contains HTML, no escape necessary *}
    </div>
    <!--<div class="ba-footer">-->
        <!--{include file="./footer.tpl"}-->
    <!--</div>-->
</div>
