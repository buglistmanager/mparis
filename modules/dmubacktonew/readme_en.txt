Manual of the module  "dmubacktonew" DMU Back to new

This module was made by the company Dream me up - http://www.dream-me-up.fr 
For questions, please contact us at: prestashop-en@dream-me-up.fr 

------------------------------------------------------------------------------------------------

1 - Installation 

The module does not require special handling:

- Copy the files in the directory "modules" of PrestaShop.
- Install the module from your administration space
- This one is immediately available in the "Catalog" as the "New products setup"

2 - Use

No configuration required for this module.

The "New products setup" tab allows you to enable or disable the "New product" status for your products.
Use the integrated search engine to find your products faster.
You can also change multiple status at once using radio buttons and multiple actions available.
Remember to set the number of days while a product will be considered as "New product" (see form below).