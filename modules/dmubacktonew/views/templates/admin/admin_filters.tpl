{**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL DREAM ME UP
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL DREAM ME UP is strictly forbidden.
*
*   .--.
*   |   |.--..-. .--, .--.--.   .--.--. .-.   .  . .,-.
*   |   ;|  (.-'(   | |  |  |   |  |  |(.-'   |  | |   )
*   '--' '   `--'`-'`-'  '  `-  '  '  `-`--'  `--`-|`-'
*        w w w . d r e a m - m e - u p . f r       '
*
*  @author    Dream me up <prestashop@dream-me-up.fr>
*  @copyright 2007 - 2015 Dream me up
*  @license   All Rights Reserved
*}

{if $ps_version == 15}
    
    <br/>
    <form id="_form_filter" class="defaultForm" action="index.php?controller=admindmubacktonew&amp;submitFilterFiltre=1&amp;token={$token|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="submitFilterpdtsnews" value="1"/>
        <input type="hidden" name="pdtsnews_pagination" value="{$pdtsnews_pagination|escape:'htmlall':'UTF-8'}"/>
        <fieldset id="fieldset_1">
            <legend>{l s='Filters' mod='dmubacktonew'}</legend>
                                                
            <div class="dmubacktonew-filter-block">
                <label>{l s='Category:' mod='dmubacktonew'}</label>
                <div class="margin-form">
                    <select id="filter_id_category" name="filter_id_category">
                        <option value="">{l s='All categories' mod='dmubacktonew'}</option>
                        {foreach from=$options_categories item=category name=categories}
                            <option value="{$category.infos.id_category|intval|escape:'htmlall':'UTF-8'}" {if $id_category_selected == $category.infos.id_category}selected{/if}>{"&nbsp;"|str_repeat:($category.infos.level_depth*3)}{$category.infos.name|escape:'htmlall':'UTF-8'} ({$category.nb_products|escape:'htmlall':'UTF-8'} {if $category.nb_products > 1}{l s='products' mod='dmubacktonew'}{else}{l s='product' mod='dmubacktonew'}{/if})</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="dmubacktonew-filter-block">
                <label>{l s='Manufacturer:' mod='dmubacktonew'}</label>
                <div class="margin-form">
                    <select id="filter_id_manufacturer" name="filter_id_manufacturer">
                        <option value="">{l s='All manufacturers' mod='dmubacktonew'}</option>
                        {foreach from=$options_marques item=marque name=marques}
                            <option value="{$marque.id_manufacturer|intval|escape:'htmlall':'UTF-8'}" {if $id_manufacturer_selected == $marque.id_manufacturer}selected{/if}>{$marque.name|escape:'htmlall':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="dmubacktonew-filter-block">
                <label>{l s='Products:' mod='dmubacktonew'}</label>
                <div class="margin-form">
                    <select id="filter_products" name="filter_products">
                        <option value="0" {if $products_selected == 0}selected{/if}>{l s='All Products' mod='dmubacktonew'}</option>
                        <option value="1" {if $products_selected == 1}selected{/if}>{l s='Only old products' mod='dmubacktonew'}</option>
                        <option value="2" {if $products_selected == 2}selected{/if}>{l s='Only new products' mod='dmubacktonew'}</option>
                    </select>
                </div>
            </div>
            <div class="dmubacktonew-filter-block">
                <label>{l s='Keywords:' mod='dmubacktonew'}</label>
                <div class="margin-form">
                    <input type="text" id="filter_keywords" name="filter_keywords" value="{$keywords_selected|escape:'htmlall':'UTF-8'}" placeholder="{l s='Product name or reference' mod='dmubacktonew'}" />
                </div>
            </div>

            <br/><br/>

            <div class="margin-form dmubacktonew-filter-block float-right">
                <input type="submit" id="submitFilter" name="submitFilterFiltre" value="{l s='Filter' mod='dmubacktonew'}" class="button"/>
                <input type="reset" id="resetFilter" name="resetFilterFiltre" value="{l s='Reset' mod='dmubacktonew'}" class="button"/>
            </div>
                          
        </fieldset>
    </form>
    <br/>

{else}

    <form id="form_filter" class="defaultForm" action="index.php?controller=admindmubacktonew&amp;submitFilter=1&amp;token={$token|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="submitFilterpdtsnews" value="1"/>
        <input type="hidden" name="pdtsnews_pagination" value="{$pdtsnews_pagination|escape:'htmlall':'UTF-8'}"/>
        <div class="panel">
            <div class="panel-heading"><i class="icon-filter"></i>&nbsp;{l s='Filters' mod='dmubacktonew'}</div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="row">
                        <label class="control-label col-xs-12">{l s='Category:' mod='dmubacktonew'}</label>
                        <div class="col-xs-12">
                            <select id="filter_id_category" name="filter_id_category">
                                <option>{l s='All categories' mod='dmubacktonew'}</option>
                                {foreach from=$options_categories item=category name=categories}
                                    <option value="{$category.infos.id_category|intval|escape:'htmlall':'UTF-8'}" {if $id_category_selected == $category.infos.id_category}selected{/if}>{"&nbsp;"|str_repeat:($category.infos.level_depth*5)}{$category.infos.name|escape:'htmlall':'UTF-8'} ({$category.nb_products|escape:'htmlall':'UTF-8'} {if $category.nb_products > 1}{l s='products' mod='dmubacktonew'}{else}{l s='product' mod='dmubacktonew'}{/if})</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="row">
                        <label class="control-label col-xs-12">{l s='Manufacturer:' mod='dmubacktonew'}</label>
                        <div class="col-xs-12">
                            <select id="filter_id_manufacturer" name="filter_id_manufacturer">
                                <option>{l s='All manufacturers' mod='dmubacktonew'}</option>
                                {foreach from=$options_marques item=marque name=marques}
                                    <option value="{$marque.id_manufacturer|intval|escape:'htmlall':'UTF-8'}" {if $id_manufacturer_selected == $marque.id_manufacturer}selected{/if}>{$marque.name|escape:'htmlall':'UTF-8'}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="row">
                        <label class="control-label col-xs-12">{l s='Products:' mod='dmubacktonew'}</label>
                        <div class="col-xs-12">
                            <select id="filter_products" name="filter_products">
                                <option value="0" {if $products_selected == 0}selected{/if}>{l s='All Products' mod='dmubacktonew'}</option>
                                <option value="1" {if $products_selected == 1}selected{/if}>{l s='Only old products' mod='dmubacktonew'}</option>
                                <option value="2" {if $products_selected == 2}selected{/if}>{l s='Only new products' mod='dmubacktonew'}</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-lg-3">
                    <div class="row">
                        <label class="control-label col-xs-12">{l s='Keywords:' mod='dmubacktonew'}</label>
                        <div class="col-xs-12">
                            <input type="text" id="filter_keywords" name="filter_keywords" value="{$keywords_selected|escape:'htmlall':'UTF-8'}" placeholder="{l s='Product name or reference' mod='dmubacktonew'}" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel-footer">
                <button type="submit" value="1" id="configuration_form_submit_btn" name="submitFilterFiltre" class="button">
                    <i class="icon-filter"></i>&nbsp;{l s='Filter' mod='dmubacktonew'}
                </button>
                <button type="submit" value="1" id="resetFilter" name="resetFilter" class="button">
                    <i class="icon-undo"></i>&nbsp;{l s='Reset' mod='dmubacktonew'}
                </button>
            </div>
        </div>
    </form>

{/if}

<script type="text/javascript">
$(document).ready(function(){
    // reset button
    $('#resetFilter').click(function(){
        $("#filter_id_category option:first-child").attr("selected", "selected");
        $("#filter_id_manufacturer option:first-child").attr("selected", "selected");
        $("#filter_products option:first-child").attr("selected", "selected");
        $('#filter_keywords').val('');
        $('#_form_filter').submit();
    });
});
</script>