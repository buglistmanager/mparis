{**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL DREAM ME UP
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL DREAM ME UP is strictly forbidden.
*
*   .--.
*   |   |.--..-. .--, .--.--.   .--.--. .-.   .  . .,-.
*   |   ;|  (.-'(   | |  |  |   |  |  |(.-'   |  | |   )
*   '--' '   `--'`-'`-'  '  `-  '  '  `-`--'  `--`-|`-'
*        w w w . d r e a m - m e - u p . f r       '
*
*  @author    Dream me up <prestashop@dream-me-up.fr>
*  @copyright 2007 - 2015 Dream me up
*  @license   All Rights Reserved
*}

{if $ps_version == 15}

	<form class="defaultForm" action="index.php?controller=admindmubacktonew&amp;submitSelection=1&amp;token={$token|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data">
		<input type="hidden" name="submitFilterpdtsnews" value="{$page|escape:'htmlall':'UTF-8'}"/>
		<input type="hidden" name="pdtsnews_pagination" value="{$pdtsnews_pagination|escape:'htmlall':'UTF-8'}"/>
		<fieldset id="fieldset_2">
		<legend>{l s='Selection' mod='dmubacktonew'}</legend>
	        <div class="margin-form">
	        	<input type="hidden" id="selectionProduct" name="selectionProduct" value=""/>
	            <input type="submit" name="setNewMultiple" value="{l s='Define selection like "New product"' mod='dmubacktonew'}" class="button disabled" disabled/>
	            <input type="submit" name="setOldMultiple" value="{l s='Define selection like "Old product"' mod='dmubacktonew'}" class="button disabled" disabled/>
	        </div>
		</fieldset>
	</form>
	<br/>

{else}

    <form id="fieldset_2" class="defaultForm" action="index.php?controller=admindmubacktonew&amp;submitSelection=1&amp;token={$token|escape:'htmlall':'UTF-8'}" method="post" enctype="multipart/form-data">
        <input type="hidden" name="submitFilterpdtsnews" value="{$page|escape:'htmlall':'UTF-8'}"/>
        <input type="hidden" name="pdtsnews_pagination" value="{$pdtsnews_pagination|escape:'htmlall':'UTF-8'}"/>
        <div class="panel">
            <div class="panel-heading"><i class="icon-list"></i>&nbsp;{l s='Selection' mod='dmubacktonew'}</div>
            <input type="hidden" id="selectionProduct" name="selectionProduct" value=""/>
            <button type="submit" value="1" id="setNewMultiple" name="setNewMultiple" class="button disabled" disabled><i class="icon-asterisk"></i>&nbsp;{l s='Define selection like "New product"' mod='dmubacktonew'}</button>
            <button type="submit" value="1" id="setOldMultiple" name="setOldMultiple" class="button disabled" disabled><i class="icon-ban"></i>&nbsp;{l s='Define selection like "Old product"' mod='dmubacktonew'}</button>
        </div>
    </form>

{/if}

<script type="text/javascript">
$(document).ready(function(){

	// selection multiple (checkbox)
	$('.sel').change(function () {

	 	var values = $(".sel:checked").map(function(){ return $(this).val(); }).get();
	 	$('#selectionProduct').val(values.join(';'));

	 	// activation/desactivation boutons
	 	if(values.length > 0)
	 	{
	 		$('#fieldset_2').find('.button').removeClass('disabled');
	 		$('#fieldset_2').find('.button').prop("disabled", false);
	 	}else{
	 		$('#fieldset_2').find('.button').addClass('disabled');
	 		$('#fieldset_2').find('.button').prop("disabled", true);
	 	}
	 	
	});
	
});
</script>