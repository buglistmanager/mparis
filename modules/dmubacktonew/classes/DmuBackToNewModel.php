<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL DREAM ME UP
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL DREAM ME UP is strictly forbidden.
*
*   .--.
*   |   |.--..-. .--, .--.--.   .--.--. .-.   .  . .,-.
*   |   ;|  (.-'(   | |  |  |   |  |  |(.-'   |  | |   )
*   '--' '   `--'`-'`-'  '  `-  '  '  `-`--'  `--`-|`-'
*        w w w . d r e a m - m e - u p . f r       '
*
*  @author    Dream me up <prestashop@dream-me-up.fr>
*  @copyright 2007 - 2015 Dream me up
*  @license   All Rights Reserved
*/

class DmuBackToNewModel extends ObjectModel
{
    public static function getProducts($all = false)
    {
        $page = Tools::getValue('submitFilterpdtsnews', 1);
        $limit = Tools::getValue('pdtsnews_pagination', 50);

        $data = array();
        $context = Context::getContext();
        $id_lang = (int)$context->cookie->id_lang;

        $nbr_days_new = (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT');
        $sql = '
        SELECT DISTINCT
        p.`id_product` as `check`,
        p.`id_product` as id_product,
        p.`id_category_default` as id_category,
        p.`id_manufacturer` as id_manufacturer,
        p.`id_product` as id_image,
        IF(DATEDIFF(DATE_ADD(p.date_add, INTERVAL '.$nbr_days_new.' DAY), "'.date('Y-m-d H:i:s').'")>0, 1, 0) as new,
        DATEDIFF(DATE_ADD(p.date_add, INTERVAL '.$nbr_days_new.' DAY), "'.date('Y-m-d H:i:s').'") as days_left,
        pl.`name` as `name`,
        cl.`name` as category,
        tags.`product_tags` as `keywords`,
        p.`reference`,
        p.`id_product` as `action1`,
        p.`id_product` as `action2`
        FROM `'._DB_PREFIX_.'product` p
        LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.id_product = pl.id_product)
        LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.id_category_default = cl.id_category)
        LEFT JOIN (
            SELECT pt.`id_product`, GROUP_CONCAT(t.`name` SEPARATOR ", ") as product_tags
            FROM '._DB_PREFIX_.'tag t
            LEFT JOIN '._DB_PREFIX_.'product_tag pt ON (pt.id_tag = t.id_tag)
            WHERE t.id_lang = '.$id_lang.'
            GROUP BY pt.`id_product`
        ) as tags ON (tags.id_product = p.id_product)
        WHERE pl.id_lang = '.$id_lang.'
        AND  cl.id_lang = '.$id_lang.'';

        // Si filtre par categorie
        if ((int)$context->cookie->filter_id_category > 0) {
            $sql .= ' AND p.`id_product` IN (
                SELECT id_product
                FROM '._DB_PREFIX_.'category_product
                WHERE id_category = '.(int)$context->cookie->filter_id_category.'
            )';
        }

        // Si filtre par fabricant/marque
        if ((int)$context->cookie->filter_id_manufacturer > 0) {
            $sql .= ' AND p.`id_manufacturer` = '.(int)$context->cookie->filter_id_manufacturer;
        }

        // Si filtre par nouveaute
        if ((int)$context->cookie->filter_products > 0) {
            $sql .= ' 
            AND IF(
                DATEDIFF(
                    DATE_ADD(
                        p.date_add, 
                        INTERVAL '.$nbr_days_new.' DAY
                    ), 
                    "'.date('Y-m-d').'"
                ) > 0, 
                2, 
                1
            ) = '.(int)$context->cookie->filter_products;
        }

        // Si filtre par mot clef
        if ($context->cookie->filter_keywords != '') {
            $keywords_selected = Tools::strtolower(trim($context->cookie->filter_keywords));
            $sql .= ' AND (
                pl.`name` LIKE "%'.$keywords_selected.'%" OR
                p.`reference` LIKE "%'.$keywords_selected.'%" OR
                tags.`product_tags` LIKE "%'.$keywords_selected.'%"
            )';
        }

        // Uniquement les produits actifs (par defaut)
        $all_products = (int)Configuration::get('DMUBACKTONEW_INACTIVE_PRODUCTS');
        if ($all_products == 0) {
            $sql .= ' AND p.`active` = 1 ';
        }

        // Order by
        if (Tools::getValue('pdtsnewsOrderby') != '') {
            $sql .= ' ORDER BY '.Tools::getValue('pdtsnewsOrderby').' '.Tools::getValue('pdtsnewsOrderway');
        } else {
            $sql .= ' ORDER BY p.date_add DESC';
        }

        // Limit
        if (!$all) {
            $start = 0;
            if ($page > 1) {
                $start = ($page-1)*$limit;
            }
            $sql .= ' LIMIT '.$start.', '.$limit;
        }

        $data = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

        return $data;
    }

    public static function recurseCategoryNbProds(
        &$tree,
        $categories,
        $current,
        $id_category = 1,
        $id_selected = 1,
        $nbr_days_new = 180
    ) {
        // Récupération du nombre de nouveaux produits de cette catégorie
        $sql = '
        SELECT
            COUNT(*) as total_product,
            SUM(
                IF(
                    DATEDIFF(
                        DATE_ADD(
                            p.date_add,
                            INTERVAL '.$nbr_days_new.' DAY
                        ), 
                        "'.date('Y-m-d').'"
                    ) > 0, 
                    1, 
                    0
                )
            ) as new
        FROM '._DB_PREFIX_.'product p
        WHERE id_product in (
            SELECT id_product
            FROM '._DB_PREFIX_.'category_product
            WHERE id_category = '.(int)$id_category.'
        )';
        $req = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);
        $current['nb_products'] = $req[0]['new'];

        if (!($current['infos']['id_parent'] == 0 && $current['infos']['is_root_category'] == 0)) {
            $tree[] = $current;
        }

        if (isset($categories[$id_category])) {
            foreach (array_keys($categories[$id_category]) as $key) {
                self::recurseCategoryNbProds(
                    $tree,
                    $categories,
                    $categories[$id_category][$key],
                    $key,
                    $id_selected,
                    $nbr_days_new
                );
            }
        }
    }

    public static function getProductRealDate($id_product)
    {
        $sql = 'SELECT date_add
                FROM `'._DB_PREFIX_.'dmubacktonew`
                WHERE id_product = '.(int)$id_product;
        if ($saved_date = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql)) {
            return $saved_date;
        } else {
            $product = new Product($id_product);
            $sql = 'INSERT INTO `'._DB_PREFIX_.'dmubacktonew` (
                        id_product, date_add
                    ) VALUES (
                        '.(int)$id_product.', \''.pSQL($product->date_add).'\'
                    )';
            Db::getInstance()->execute($sql);
            return $product->date_add;
        }
    }

    public static function setProductNew($id_product)
    {
        self::getProductRealDate($id_product);
        $new_product_date = date('Y-m-d H:i:s');

        $product = new Product($id_product);
        $product->date_add = $new_product_date;
        $product->save();
    }

    public static function setProductOld($id_product)
    {
        $saved_date = self::getProductRealDate($id_product);
        $old_date = date('Y-m-d H:i:s', strtotime('-'.((int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT')+1).'days'));
        $new_product_date = strtotime($saved_date) <= strtotime($old_date) ? $saved_date : $old_date;

        $product = new Product($id_product);
        $product->date_add = $new_product_date;
        $product->save();
    }

    public static function getDaysNewProduct($id_product)
    {
        // Verifie si le produit est actuellement considéré comme nouveau
        $pdt = new Product($id_product);
        $nb_days_selected = (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT');
        $today = date('Y-m-d H:i:s');
        $date_end = date('Y-m-d H:i:s', strtotime('+'.$nb_days_selected.' days', strtotime($pdt->date_add)));
        return self::getDaysBetweenDates($date_end, $today);
    }

    public static function getDaysBetweenDates($date_start, $date_end)
    {
        $date_start = strtotime($date_start);
        $date_end = strtotime($date_end);
        $datediff = $date_start - $date_end;
        if ($datediff > 0) {
            return (int)floor($datediff/(60*60*24));
        } else {
            return 0;
        }
    }
}
