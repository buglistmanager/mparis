Manuel d'utilisation du module "dmubacktonew" DMU Gestion des nouveaux produits

Ce module a ete realise par la societe Dream me up - http://www.dream-me-up.fr
Pour toute question, vous pouvez nous contacter a l'adresse : prestashop@dream-me-up.fr

------------------------------------------------------------------------------------------------

1 - Installation

L'installation du module ne necessite pas de manipulation particulière :

- Copiez les fichiers dans le repertoire "modules" de Prestashop.
- Installez le module depuis votre espace d'administration
- Celui-ci est alors immediatement disponible dans l'onglet "Catalogue" sous le nom "Gestion des nouveaux produits"

2 - Utilisation

Aucune configuration nécessaire pour ce module.

L'onglet "Gestion des nouveaux produits" vous permet d'activer ou désactiver le statut "Nouveautés" de vos produits.
Utilisez le moteur de recherche intégré afin de retrouver vos produits plus rapidement.
Vous pouvez aussi modifier plusieurs statuts à la fois en utilisant les boutons radio et les actions multiples disponibles.
Pensez à définir le nombre de jours durant lequel un produit sera considéré comme "Nouveau" (voir formulaire en bas de page).