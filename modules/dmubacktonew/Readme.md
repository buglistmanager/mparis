# DMU Gestion des nouveautés (par [Dream me up](http://www.dream-me-up.fr))

```
   .--.
   |   |.--..-. .--, .--.--.   .--.--. .-.   .  . .,-.
   |   ;|  (.-'(   | |  |  |   |  |  |(.-'   |  | |   )
   '--' '   `--'`-'`-'  '  `-  '  '  `-`--'  `--`-|`-'
        w w w . d r e a m - m e - u p . f r       '

  @author    Dream me up <prestashop@dream-me-up.fr>
  @copyright 2007 - 2015 Dream me up
  @license   All Rights Reserved

```

## changelog 1.0.3

* Sauvegarde de l'ancienne date

## changelog 1.0.2

* Compatibilité PrestaShop 1.7

## changelog 1.0.1

* Ajout d'une option de configuration - prise en compte ou non des produits inactifs
* Correction date 'Définir comme "Ancien produit"'

## changelog 1.0.0

* Création du module
