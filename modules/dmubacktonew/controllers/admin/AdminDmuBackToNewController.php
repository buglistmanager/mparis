<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from SARL DREAM ME UP
* Use, copy, modification or distribution of this source file without written
* license agreement from the SARL DREAM ME UP is strictly forbidden.
*
*   .--.
*   |   |.--..-. .--, .--.--.   .--.--. .-.   .  . .,-.
*   |   ;|  (.-'(   | |  |  |   |  |  |(.-'   |  | |   )
*   '--' '   `--'`-'`-'  '  `-  '  '  `-`--'  `--`-|`-'
*        w w w . d r e a m - m e - u p . f r       '
*
*  @author    Dream me up <prestashop@dream-me-up.fr>
*  @copyright 2007 - 2015 Dream me up
*  @license   All Rights Reserved
*/

class AdminDmuBackToNewController extends ModuleAdminController
{
    public function __construct()
    {
        $this->module = 'dmubacktonew';
        $this->className = 'DmuBackToNewModel';
        $this->lang = false;
        $this->context = Context::getContext();
        $this->bootstrap = true;
        $this->need_instance = 0;

        $this->context = Context::getContext();

        parent::__construct();
    }

    public function postProcess()
    {
        $context = Context::getContext();

        // Submit parametre général
        if (Tools::isSubmit('submitNbDaysNewProduct')) {
            if (trim(Tools::getValue('PS_NB_DAYS_NEW_PRODUCT')) != '' &&
                (int)Tools::getValue('PS_NB_DAYS_NEW_PRODUCT') > 0) {
                Configuration::updateValue('PS_NB_DAYS_NEW_PRODUCT', (int)Tools::getValue('PS_NB_DAYS_NEW_PRODUCT'));
            }
        }

        // Submit filtre
        if (Tools::isSubmit('submitFilterFiltre')) {
            $context->cookie->filter_id_category        =  Tools::getValue('filter_id_category');
            $context->cookie->filter_id_manufacturer    =  Tools::getValue('filter_id_manufacturer');
            $context->cookie->filter_products           =  Tools::getValue('filter_products');
            $context->cookie->filter_keywords           =  Tools::getValue('filter_keywords');
        } else {
            $context->cookie->filter_id_category        =  '';
            $context->cookie->filter_id_manufacturer    =  '';
            $context->cookie->filter_products           =  0;
            $context->cookie->filter_keywords           =  '';
        }

        // Set new (only one)
        if (Tools::isSubmit('setNew')) {
            DmuBackToNewModel::setProductNew((int)Tools::getValue('id_product'));
        }

        // Set old (only one)
        if (Tools::isSubmit('setOld')) {
            DmuBackToNewModel::setProductold((int)Tools::getValue('id_product'));
        }

        // Set new/old (multiple)
        if (Tools::isSubmit('submitSelection')) {
            $products_selected = Tools::getValue('selectionProduct');
            $products_selected = explode(';', $products_selected);
            
            if (Tools::isSubmit('setNewMultiple')) {
                foreach ($products_selected as $id_product) {
                    DmuBackToNewModel::setProductNew((int)$id_product);
                }
            }

            if (Tools::isSubmit('setOldMultiple')) {
                foreach ($products_selected as $id_product) {
                    DmuBackToNewModel::setProductold((int)$id_product);
                }
            }
        }

        return '';
    }

    public function getCheckbox($id_product)
    {
        return '<input type="checkbox" name="sel" id="sel_'.$id_product.'" class="sel" value="'.$id_product.'"/>';
    }

    public function getPhoto($id_product)
    {
        if (file_exists('../img/tmp/product_mini_'.(int)$id_product.'_1.jpg')) {
            return '<img src="../img/tmp/product_mini_'.(int)$id_product.'_1.jpg?time='.time().'" alt=""/>';
        } else {
            return '';
        }
    }

    public function getStatus($days_gap)
    {
        if ((int)$days_gap > 0) {
            return '
            <span class="new">
                '.$this->l('New product').' - '.(int)$days_gap.' '.$this->l('day(s) left').'
            </span>';
        } else {
            return '
            <span class="nonew">
                '.$this->l('Old product').'
            </span>';
        }
    }

    public function setNewProductLink($id_product)
    {
        return '
        <a href="index.php?controller=admindmubacktonew&setNew&id_product='.$id_product.'&token='.$this->token.'" 
        class="button btn btn-default btn-block">
            &nbsp;<i class="icon-asterisk"></i>&nbsp;'.$this->l('Define like "New product"').'&nbsp;
        </a>';
    }

    public function setOldProductLink($id_product)
    {
        return '
        <a href="index.php?controller=admindmubacktonew&setOld&id_product='.$id_product.'&token='.$this->token.'" 
        class="button btn btn-default btn-block">
            &nbsp;<i class="icon-ban"></i>&nbsp;'.$this->l('Define like "Old product"').'&nbsp;
        </a>';
    }

    public function renderList()
    {
        // Formulaire PS_NB_DAYS_NEW_PRODUCT
        $data = array();
        $data['PS_NB_DAYS_NEW_PRODUCT']    = Configuration::get('PS_NB_DAYS_NEW_PRODUCT');
        $data['submitFilterpdtsnews']      = Tools::getValue('submitFilterpdtsnews', 1);
        $data['pdtsnews_pagination']       = Tools::getValue('pdtsnews_pagination', 50);

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Configuration'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Number of days that a product is considered like "new":'),
                        'name' => 'PS_NB_DAYS_NEW_PRODUCT',
                        'size' => 10,
                        'required' => true
                    ),
                    array(
                        'type' => 'hidden',
                        'label' => '',
                        'name' => 'submitFilterpdtsnews',
                        'size' => 10,
                        'required' => false
                    ),
                    array(
                        'type' => 'hidden',
                        'label' => '',
                        'name' => 'pdtsnews_pagination',
                        'size' => 10,
                        'required' => false
                    )
                ),
                'submit' => array(
                    'name' => 'submitNbDaysNewProduct',
                    'class' => 'button',
                    'title' => $this->l('Save')
                )
            ),
        );

        $helper_form = new HelperForm();
        $helper_form->show_toolbar = false;
        $helper_form->table = $this->table;
        $helper_form->identifier = $this->identifier;
        $helper_form->token = $this->token;
        $helper_form->currentIndex = 'index.php?controller=admindmubacktonew';
        $helper_form->tpl_vars = array(
            'fields_value' => $data,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        // Liste des produits du catalogue
        $fields_list = array(
            'check' => array(
                'title' => '&nbsp;',
                'width' => 10,
                'callback' => 'getCheckbox',
                'orderby' => false
            ),
            'id_product' => array(
                'title' => 'ID',
                'width' => 20,
                'type' => 'text',
                'sort' => false
            ),
            'id_image' => array(
                'title' => $this->l('Image'),
                'align' => 'center',
                'callback' => 'getPhoto',
                'orderby' => false
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'type' => 'text'
            ),
            'category' => array(
                'title' => $this->l('Category'),
                'type' => 'text'
            ),
            'days_left' => array(
                'title' => $this->l('Status'),
                'type' => 'text',
                'align' => 'center',
                'callback' => 'getStatus'
            ),
            'action1' => array(
                'title' => '&nbsp;',
                'width' => 150,
                'callback' => 'setNewProductLink',
                'orderby' => false
            ),
            'action2' => array(
                'title' => '&nbsp;',
                'width' => 175,
                'callback' => 'setOldProductLink',
                'orderby' => false
            ),
        );

        // Selecteur multiple
        $this->context->smarty->assign(array(
            'ps_version'                => Tools::substr(str_replace('.', '', _PS_VERSION_), 0, 2),
            'token'                     => $this->token,
            'page'                      => Tools::getValue('submitFilterpdtsnews', 1),
            'pdtsnews_pagination'       => Tools::getValue('pdtsnews_pagination', 50)
        ));

        $helper_list = new HelperList();
        $helper_list->shopLinkType = '';
        $helper_list->simple_header = false;
        $helper_list->identifier = 'id_product';
        $helper_list->show_toolbar = false;
        $helper_list->toolbar_scroll = false;
        $helper_list->no_link = true;
        $helper_list->list_id = "pdtsnews";
        $helper_list->title = '<i class="icon-archive"></i>&nbsp;'.$this->l('Products');
        $helper_list->table = '';
        $helper_list->token = $this->token;
        $helper_list->currentIndex = 'index.php?controller=admindmubacktonew';

        $products = DmuBackToNewModel::getProducts();
        $helper_list->listTotal = count(DmuBackToNewModel::getProducts(true));

        return $this->renderProductsFilters()
        .$helper_list->generateList($products, $fields_list)
        .parent::renderView()
        .$helper_form->generateForm(array($fields_form));
    }

    public function renderView()
    {
        return $this->renderList();
    }

    public function renderForm()
    {
        return $this->renderList();
    }

    public function renderProductsFilters()
    {
        $context = Context::getContext();
        
        // Récupération des catégories
        $categories = Category::getCategories($this->context->language->id, false);
        $current = current($categories);
        $current = $current[key($current)];
        $options_categories = array();
        DmuBackToNewModel::recurseCategoryNbProds(
            $options_categories,
            $categories,
            $current,
            $current['infos']['id_category'],
            Tools::getValue('id_selected'),
            (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT')
        );

        // Récupération des marques
        $options_marques = Manufacturer::getManufacturers(false, $this->context->language->id);
        $options_marques = array_unique($options_marques, SORT_REGULAR);

        $this->context->smarty->assign(array(
            'ps_version'                => Tools::substr(str_replace('.', '', _PS_VERSION_), 0, 2),
            'token'                     => $this->token,
            'options_categories'        => $options_categories,
            'options_marques'           => $options_marques,
            'id_category_selected'      => (int)$context->cookie->filter_id_category,
            'id_manufacturer_selected'  => (int)$context->cookie->filter_id_manufacturer,
            'products_selected'         => (int)$context->cookie->filter_products,
            'keywords_selected'         => $context->cookie->filter_keywords,
            'page'                      => Tools::getValue('submitFilterpdtsnews', 1),
            'pdtsnews_pagination'       => Tools::getValue('pdtsnews_pagination', 50)
        ));
        return $this->context->smarty->fetch(dirname(__FILE__).'/../../views/templates/admin/admin_filters.tpl');
    }
}
