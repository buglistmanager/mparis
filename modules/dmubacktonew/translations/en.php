<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{dmubacktonew}prestashop>dmubacktonew_0ab947a8261f59b20efb4b2d70812c78'] = 'Define your products that will be considered like New product.';
$_MODULE['<{dmubacktonew}prestashop>dmubacktonew_5ee2162baa948781e4d694158bcfdf5f'] = 'Dream me up specializes in the creation of addons to improve the merchant experience, mainly in back office area. We develop tools to help you save time or to have a better view of your business. Discover now our addons for quick product administration, easy associations, or even real-time statistics.';