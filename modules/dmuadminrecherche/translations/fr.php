<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_65e784645a1f8e7aadb111dece48e197'] = 'DMU Administration rapide des produits';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_1bc4e3c56c42c381c935093d39821854'] = 'Permet de chercher, filtrer et éditer rapidement vos produits depuis l\'administration';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_876f23178c29dc2552c0b48bf23cd9bd'] = 'Êtes-vous sûr de désinstaller ?';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_254f642527b45bc260048e30704edb39'] = 'Configuration';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_b04f2c8a90cfc485fc990dc8dc4ba293'] = 'Configuration du module';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_d863fc2df09d7b450d627800c4f4a73e'] = 'Affichage du prix des déclinaisons';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_468d840a1358fc8824ee58bdd6705c44'] = 'L\'édition en 1 clic du prix se fera en fonction de ce paramètre';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_94dfb2009644e42ef41f47aece4c0350'] = 'TTC';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_58ef6750a23ba432fc1377b7de085d9f'] = 'HT';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{dmuadminrecherche}prestashop>dmuadminrecherche_df02343336cdce5bdc4f7ca50a4438ea'] = 'La configuration a été mise à jour.';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_b718adec73e04ce3ec720dd11a06a308'] = 'ID';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_be53a0541a6d36f6ecb879fa2c584b08'] = 'Image';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_63d5049791d9d79d86e9a108b0a999ca'] = 'Référence';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_bc67a1507258a758c3a31e66d7ceff8f'] = 'Référence fournisseur';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Marque';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_3adbdb3ac060038aa0e6e6c138ef9873'] = 'Catégorie';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_ce0ec0f2b759fa68416e3bc96ee59276'] = 'Prix de vente TTC';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_1fd6db3332676692077360fec4df12e4'] = 'Prix de vente';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_4c32794a96aec43624f14839274174bf'] = 'Prix de vente HT';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_c265e7bd60b9bf0470d39a8ef87e0727'] = 'Éco-participation (TTC)';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_694e8d1f2ee056f98ee488bdc4982d73'] = 'Quantité';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_813a766f117d6336274b53df5a1d1059'] = 'Prix d\'achat';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_a2df9b2ac2e63b127c1f5913ed68e964'] = 'Frais de port supplémentaires';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_a7b4930bf2fc5d57eec6b03097200152'] = 'EAN-13';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_fbd99ad01b92dbafc686772a39e3d065'] = 'UPC';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_7b9b3efac51fd736c3ddc0135c5b038e'] = 'Emplacement (entrepôt)';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_6b02519c0d7bf679f24108724f3e6e89'] = 'Largeur du colis';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_3bd95f22849743c8a2d8777158bc39c5'] = 'Hauteur du colis';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_fb3fc11dedb4b8c12eb6af80c5812b89'] = 'Profondeur du colis';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_2e1b76ebadd98a5b81d21bd2859fafdc'] = 'Poids du colis';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_ec136b444eede3bc85639fac0dd06229'] = 'Fournisseur';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_9e2941b3c81256fac10392aaca4ccfde'] = 'État';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_4b78ac8eb158840e9638a3aeb26c4a9d'] = 'Taxe';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_6c0e3c5209a6e08683b0d2d541d783bb'] = 'Prix unitaire HT';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_afa2e150d7f90ba2b7a57ca63ee92c97'] = 'Prix final';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_1b72aed8e84b91d73896efd48cf1a4e0'] = 'Quantité minimale';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_7d1c743f94a6bd5406682a36804bb2a5'] = 'Message quand en stock';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_2fd58c46269967d6264efa6c1d5a0961'] = 'Message quand hors-stock';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_9e11e4b371570340ca07913bc4783a7a'] = 'Balise titre';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_3f64b2beede1082fd32ddb0bf11a641f'] = 'Meta description';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_8725214cdd9f9af24e914b5da135793d'] = 'URL simplifiée';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_98369609669478919c74c916440e9978'] = 'Marge';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_06df33001c1d7187fdd81ea1f5b277aa'] = 'Actions';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_0f752ff067a64b4eb412b20fff6f38cb'] = 'Déplacer la colonne sur la gauche';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_9e81c11e50306289cd812000b3414aa3'] = 'Déplacer la colonne sur la droite';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_03c2e7e41ffc181a4e84080b4710e81e'] = 'Nouveau';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_019d1ca7d50cc54b995f60d456435e87'] = 'Utilisé';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_6da03a74721a0554b7143254225cc08a'] = 'Reconditionné';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_fe3838c7c11aa406dd956566e17360d5'] = 'par';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_e5dba1c9996ec7f313c1abeecf6ff291'] = 'Résultat de recherche pour :';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_ece82fb62fb7ac1d306649bcef16c0c7'] = 'Effacer le statut';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_dac0f551d772efdde3160118b3ca9b48'] = 'Statut neutre';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_c1ae4032211c9936af50194e39a64b45'] = 'Statut vert';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_891b7ee5c7bc5d5856d718d1859881d3'] = 'Statut rouge';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_0a218c03599060157b9e4c431291a5d9'] = 'Augmenter le prix';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_487226eaf83db671435c720e7673e649'] = 'Réduire le prix';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_d3b206d196cd6be3a2764c1fb90b200f'] = 'Supprimer la sélection';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_e25f0ecd41211b01c83e5fec41df4fe7'] = 'Supprimer les éléments sélectionnés ?';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_b537a15d0369b1e519c32b391f0f9b46'] = 'La valeur du prix est incorrecte';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_6964a66fedeb918001093c8f67e0378a'] = 'Le champ Référence fournisseur est invalide.';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_fe9e64fa2d5fe6fe4fd7d4707bd8e2e8'] = 'Paire attribut - valeur';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_3c1a34587e45bd9e9ae2efd9a6ecbb92'] = 'Impact sur le poids';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_4fc9f49cb85af9ba5caab7747fe35846'] = 'Éco-participation (HT)';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_c804723ccdde3d7a46933b208c6f928d'] = 'Prix d\'achat';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_a4f85d316cb4978824f7fe0efdbd1108'] = 'Supprimer les déclinaisons sélectionnées';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_f4b3ea0924e4f842758d974ffb6b2953'] = 'Supprimer les déclinaisons sélectionnées ?';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_c736c5b0df973f5ada1a981b847bcf83'] = 'Vous devez passer par la gestion des stocks';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_6c0e4c36574fc83ecfdbc74acd3614b1'] = ' pour modifier la quantité si vous utilisez plusieurs entrepôts.';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_103989bc0098100b2b55b436acf6072d'] = 'Vous devez associer les déclinaisons à un entrepôt';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_ac6b752a3c54a22c330eea516bd41f02'] = 'Vous devez associer le produit à un entrepôt';
$_MODULE['<{dmuadminrecherche}prestashop>admindmuadminrecherchecontroller_fbbe3c3ed246f348e5175d79d07bbf8a'] = 'Aucun entrepôt sélectionné';
$_MODULE['<{dmuadminrecherche}prestashop>configure_b1c1d84a65180d5912b2dee38a48d6b5'] = 'Version du module';
$_MODULE['<{dmuadminrecherche}prestashop>configure_1bc4e3c56c42c381c935093d39821854'] = 'Permet de chercher, filtrer et éditer rapidement vos produits depuis l\'administration';
$_MODULE['<{dmuadminrecherche}prestashop>configure_0faacc86fdd2252612e8dd360ebae5b5'] = 'Comment utiliser ce module ?';
$_MODULE['<{dmuadminrecherche}prestashop>configure_3423613aea592cb6ac9f33fc47c88d06'] = 'Pour utiliser le module, vous devez utiliser le menu pour accéder à Catalogue > Administration rapide';
$_MODULE['<{dmuadminrecherche}prestashop>configure_0396574364d3f674143ce8c8432e872c'] = 'Qui sommes-nous ?';
$_MODULE['<{dmuadminrecherche}prestashop>configure_53bcdd278b29db2acf0b0379d79c6aa7'] = 'Dream me up se spécialise dans la création de module destinés à l\'amélioration de l\'expérience d\'utilisation du E-commerçant, principalement en Back-Office. Nous développons des outils vous permettant de gagner du temps ou d\'avoir une meilleure visibilité sur votre activité. Découvrez vite nos modules d\'administration rapide des produits, d\'associations faciles, ou encore de statistiques en temps réel.';
$_MODULE['<{dmuadminrecherche}prestashop>configure_f41378dc2401a066a07547accdc467d0'] = 'Notre';
$_MODULE['<{dmuadminrecherche}prestashop>configure_db0956b98aef20b7ce48e0dff827411e'] = 'page Partenaire Prestashop dédiée';
$_MODULE['<{dmuadminrecherche}prestashop>configure_bc974e7de47bc7dd77a8dd5ea67288e5'] = 'Découvrez tous nos modules sur notre';
$_MODULE['<{dmuadminrecherche}prestashop>configure_0d68cec1024a0c3e4f15f1879a5f1bab'] = 'http://addons.prestashop.com/fr/9_dream-me-up';
$_MODULE['<{dmuadminrecherche}prestashop>configure_7236223600f5f1d16a462d357e79ed5c'] = 'page Partenaire Prestashop dédiée';
$_MODULE['<{dmuadminrecherche}prestashop>configure_d918f99442796e88b6fe5ad32c217f76'] = 'Suivez-nous';
$_MODULE['<{dmuadminrecherche}prestashop>configure_ed2b5c0139cec8ad2873829dc1117d50'] = 'sur';
$_MODULE['<{dmuadminrecherche}prestashop>configure_be5d5d37542d75f93a87094459f76678'] = 'et';
$_MODULE['<{dmuadminrecherche}prestashop>configure_7ce119e7cde8d64cb5474e607f649418'] = 'pour connaître toute l\'actualité de nos modules';
$_MODULE['<{dmuadminrecherche}prestashop>configure_9360d0532187ba889efca0eb1d801bd3'] = 'pour avoir tous les détails sur les nouvelles versions, les nouveaux modules, etc';
$_MODULE['<{dmuadminrecherche}prestashop>configure_703d2d3f6aa5b2ce745f7ed65ca218a6'] = 'Support et Documentation';
$_MODULE['<{dmuadminrecherche}prestashop>configure_197eb2a2ea2e05f4338089a14d9a711a'] = 'Cliquez-ici pour ouvrir la documentation du module';
$_MODULE['<{dmuadminrecherche}prestashop>configure_eb391129d48e03b3fb7936a369e12d4c'] = 'par l\'intermédiaire de Prestashop Addons';
$_MODULE['<{dmuadminrecherche}prestashop>configure_d4624f1420f97c87df7bbbd63152e8ee'] = 'Rendez-vous sur la page du module concerné puis utilisez le lien \"Contacter le développeur\"';
$_MODULE['<{dmuadminrecherche}prestashop>configure_f9e86c2f50cc848b1cb772a9ebb4012d'] = 'Vous devez mentionner';
$_MODULE['<{dmuadminrecherche}prestashop>configure_70afaf78a1d5abc1f480ce98c748cbf3'] = 'Une description détaillée du problème rencontré';
$_MODULE['<{dmuadminrecherche}prestashop>configure_0fa958b7cd91375af44ff419360e91c1'] = 'Votre version de Prestashop';
$_MODULE['<{dmuadminrecherche}prestashop>configure_8f377664c37ae64794b687408b218592'] = 'Votre version du Module';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_729a51874fe901b092899e9e8b31c97a'] = 'Êtes-vous sûr ?';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_248a35a152339f0acd78b5b39d8e464d'] = 'Vous devez sélectionner au moins une image et une déclinaison';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_c9ac3a4ce63d92ad976296e5d9f4d2a1'] = 'Souhaitez-vous enlever les images associées aux déclinaisons sélectionnées ?';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_8a22e4ff51c28388d3673b897872146f'] = 'Vous devez sélectionner au moins une déclinaison';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_cf3905df049698bb01639e4394e82048'] = 'Il n\'est pas possible de supprimer une déclinaison qui a des quantités dans la Gestion des Stocks Avancée. Vous devez d\'abord supprimer le stock correspondant.';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_7752d6c7d20afb7989687e202b77e10e'] = 'Déclinaisons :';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_e0f0b0564d3d29a93fad7a4178b7b1ca'] = 'ID :';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_8d60695e9450e52ca8dbe69287e9be7b'] = 'Vous devez mettre une image en ligne avant de pouvoir en sélectionner une pour votre déclinaison.';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_587c64cd213079dca452b47b13f1c294'] = 'Associer les déclinaisons avec les images sélectionnées';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_07076b8281615772c3e3a7adb7b196cc'] = 'Supprimer les images des déclinaisons sélectionnées';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_f9c128cbc52bb845834c0d3e62b424ea'] = 'La ligne en bleu est la déclinaison par défaut.';
$_MODULE['<{dmuadminrecherche}prestashop>combinations_55110f4c6a2c90b143c6fcd295cb7bc7'] = 'Une déclinaison par défaut doit être désignée pour chaque produit.';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_345c6634cd77596443b428b2192de275'] = 'Descriptions :';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_e0f0b0564d3d29a93fad7a4178b7b1ca'] = 'ID :';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_765a5021c58a220cb2a0bec7d0e83b17'] = 'Apparaît dans les listes de produits et sur le haut de la page du produit.';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_c1069a480848e06782b81b8bea9c0c94'] = 'Résumé';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_9a2d3323b05a18f9723ea91b05dd9988'] = 'Apparaît dans le corps de la page produit.';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Description';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_d80144504c7f2b1aa0c2725629d2d509'] = 'S\'affichera dans le bloc mots-clés s\'il est activé. Les mots-clés aident les clients à facilement trouver vos produits.';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_189f63f277cd73395561651753563065'] = 'Mots-clés';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_2efd89b3ccc76b0b03a34196fc6d1c8b'] = 'Ajouter un mot-clé';
$_MODULE['<{dmuadminrecherche}prestashop>descriptions_ce950d5c0bc7817ea0c7543782abafb5'] = 'Chaque mot-clé doit être suivi d\'une virgule. Les caractères suivants sont interdits : %s';
$_MODULE['<{dmuadminrecherche}prestashop>details_2babe53026309ed1bc4d26626dbaf9b3'] = 'Détails :';
$_MODULE['<{dmuadminrecherche}prestashop>details_e0f0b0564d3d29a93fad7a4178b7b1ca'] = 'ID :';
$_MODULE['<{dmuadminrecherche}prestashop>details_85f0bf927be7014e2265c4bc96bd50d2'] = '(Europe, Japan)';
$_MODULE['<{dmuadminrecherche}prestashop>details_9441356559f394a164fb218c062062fb'] = 'EAN-13 ou JAN';
$_MODULE['<{dmuadminrecherche}prestashop>details_104f50d9d335bbe38ee0c8e44ebddf03'] = '(US, Canada)';
$_MODULE['<{dmuadminrecherche}prestashop>details_fbd99ad01b92dbafc686772a39e3d065'] = 'UPC';
$_MODULE['<{dmuadminrecherche}prestashop>details_7b9b3efac51fd736c3ddc0135c5b038e'] = 'Emplacement (entrepôt)';
$_MODULE['<{dmuadminrecherche}prestashop>details_c63711f76d46f27c6d4e99c18385a6fa'] = 'Ce produit contient des déclinaisons, vous devez passer par la fiche produit pour modifier son emplacement.';
$_MODULE['<{dmuadminrecherche}prestashop>details_62fb84ae7e3f9ebd20292c0b1b5afbaf'] = 'Vous utiliser plusieurs entrepôts, vous devez passer par la fiche produit pour modifier son emplacement.';
$_MODULE['<{dmuadminrecherche}prestashop>details_baf2f2322b105caac66d9b4df13c5968'] = 'Vous devez d\'abord créer un entrepôt avant de définir un emplacement';
$_MODULE['<{dmuadminrecherche}prestashop>details_32954654ac8fe66a1d09be19001de2d4'] = 'Largeur';
$_MODULE['<{dmuadminrecherche}prestashop>details_eec6c4bdbd339edf8cbea68becb85244'] = 'Hauteur';
$_MODULE['<{dmuadminrecherche}prestashop>details_675056ad1441b6375b2c5abd48c27ef1'] = 'Profondeur';
$_MODULE['<{dmuadminrecherche}prestashop>details_8c489d0946f66d17d73f26366a4bf620'] = 'Poids';
$_MODULE['<{dmuadminrecherche}prestashop>features_d6295c05503596b3ed3528aee83e3ef7'] = 'Caractéristiques :';
$_MODULE['<{dmuadminrecherche}prestashop>features_e0f0b0564d3d29a93fad7a4178b7b1ca'] = 'ID :';
$_MODULE['<{dmuadminrecherche}prestashop>features_21021ea0e52be8e9c599f4dff41e5be0'] = 'Caractéristique';
$_MODULE['<{dmuadminrecherche}prestashop>features_8bcdd31a1ab28f0db60efb2087c3f235'] = 'Valeur prédéfinie';
$_MODULE['<{dmuadminrecherche}prestashop>features_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'ou';
$_MODULE['<{dmuadminrecherche}prestashop>features_12085f07b6313b69c63a625d8e23f706'] = 'Valeur personnalisée';
$_MODULE['<{dmuadminrecherche}prestashop>features_382b0f5185773fa0f67a8ed8056c7759'] = 'N/D';
$_MODULE['<{dmuadminrecherche}prestashop>features_544baae42433c186ff222736db985d7d'] = 'Ajouter une valeur prédéfinie dans un premier temps';
$_MODULE['<{dmuadminrecherche}prestashop>features_5fb1f955b45e38e31789286a1790398d'] = 'TOUS';
$_MODULE['<{dmuadminrecherche}prestashop>features_d61248df0ca451d57196a4b4cdb35e1b'] = 'Aucune caractéristique définie';
$_MODULE['<{dmuadminrecherche}prestashop>prices_11d0695520ad49f6ea737310b5d28472'] = 'Prix :';
$_MODULE['<{dmuadminrecherche}prestashop>prices_e0f0b0564d3d29a93fad7a4178b7b1ca'] = 'ID :';
$_MODULE['<{dmuadminrecherche}prestashop>prices_c804723ccdde3d7a46933b208c6f928d'] = 'Prix d\'achat';
$_MODULE['<{dmuadminrecherche}prestashop>prices_cc790edb31f5984a5754e4bc6aadf7bb'] = 'Prix d\'achat HT';
$_MODULE['<{dmuadminrecherche}prestashop>prices_1fd6db3332676692077360fec4df12e4'] = 'Prix de vente';
$_MODULE['<{dmuadminrecherche}prestashop>prices_4c32794a96aec43624f14839274174bf'] = 'Prix de vente HT';
$_MODULE['<{dmuadminrecherche}prestashop>prices_d8409efcbf2c3444399daa83172dd35a'] = 'Règle de taxe';
$_MODULE['<{dmuadminrecherche}prestashop>prices_7475ec0d41372a307c497acb7eeea8c4'] = 'Aucune taxe';
$_MODULE['<{dmuadminrecherche}prestashop>prices_58a1c17337319d632cac635cd5982707'] = 'Les taxes sont actuellement désactivées';
$_MODULE['<{dmuadminrecherche}prestashop>prices_5b41c03410b1dea5e179ac600ab9fa9f'] = 'Cliquez ici pour ouvrir la page de configuration des taxes.';
$_MODULE['<{dmuadminrecherche}prestashop>prices_c265e7bd60b9bf0470d39a8ef87e0727'] = 'Éco-participation (TTC)';
$_MODULE['<{dmuadminrecherche}prestashop>prices_ce0ec0f2b759fa68416e3bc96ee59276'] = 'Prix de vente TTC';
$_MODULE['<{dmuadminrecherche}prestashop>prices_cddb91d856108b4431ebc2a9f79d4769'] = 'Prix unitaire HT';
$_MODULE['<{dmuadminrecherche}prestashop>prices_fe3838c7c11aa406dd956566e17360d5'] = 'par';
$_MODULE['<{dmuadminrecherche}prestashop>prices_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'ou';
$_MODULE['<{dmuadminrecherche}prestashop>prices_1f87346a16cf80c372065de3c54c86d9'] = 'TTC';
$_MODULE['<{dmuadminrecherche}prestashop>prices_47d4999fc742a8e3eb51c399c8093d94'] = 'Afficher un bandeau \"Promo !\" sur la page produit ainsi qu\'en texte sur les pages catégories.';
$_MODULE['<{dmuadminrecherche}prestashop>prices_7b988b81a5fe3114360063de23f49016'] = 'Prix de vente final :';
$_MODULE['<{dmuadminrecherche}prestashop>prices_e2e79605fc9450ec17957cf0e910f5c6'] = 'TTC';
$_MODULE['<{dmuadminrecherche}prestashop>prices_887ee91702c962a70b87cbef07bbcaec'] = 'HT';
$_MODULE['<{dmuadminrecherche}prestashop>seo_51973889bbefe79884b2f2a9c29f8950'] = 'Référencement - SEO :';
$_MODULE['<{dmuadminrecherche}prestashop>seo_e0f0b0564d3d29a93fad7a4178b7b1ca'] = 'ID :';
$_MODULE['<{dmuadminrecherche}prestashop>seo_9e11e4b371570340ca07913bc4783a7a'] = 'Balise titre';
$_MODULE['<{dmuadminrecherche}prestashop>seo_3f64b2beede1082fd32ddb0bf11a641f'] = 'Meta description';
$_MODULE['<{dmuadminrecherche}prestashop>seo_1dec4f55522b828fe5dacf8478021a9e'] = 'URL simplifiée';
$_MODULE['<{dmuadminrecherche}prestashop>status_a97cda94e19f7862bddf71161d273bef'] = 'Changer le statut :';
$_MODULE['<{dmuadminrecherche}prestashop>status_e0f0b0564d3d29a93fad7a4178b7b1ca'] = 'ID :';
$_MODULE['<{dmuadminrecherche}prestashop>status_24a23d787190f2c4812ff9ab11847a72'] = 'Statut :';
$_MODULE['<{dmuadminrecherche}prestashop>status_e9bb5320b3890b6747c91b5a71ae5a01'] = 'Neutre';
$_MODULE['<{dmuadminrecherche}prestashop>status_d382816a3cbeed082c9e216e7392eed1'] = 'vert';
$_MODULE['<{dmuadminrecherche}prestashop>status_ee38e4d5dd68c4e440825018d549cb47'] = 'rouge';
$_MODULE['<{dmuadminrecherche}prestashop>status_240f3031f25601fa128bd4e15f0a37de'] = 'Commentaire :';
$_MODULE['<{dmuadminrecherche}prestashop>bulk_price_0a218c03599060157b9e4c431291a5d9'] = 'Augmenter le prix';
$_MODULE['<{dmuadminrecherche}prestashop>bulk_price_487226eaf83db671435c720e7673e649'] = 'Réduire le prix';
$_MODULE['<{dmuadminrecherche}prestashop>bulk_price_bc091abffb39f55202d88c50a0ac4d33'] = 'Impact sur le prix :';
$_MODULE['<{dmuadminrecherche}prestashop>bulk_price_94dfb2009644e42ef41f47aece4c0350'] = 'TTC';
$_MODULE['<{dmuadminrecherche}prestashop>bulk_price_58ef6750a23ba432fc1377b7de085d9f'] = 'HT';
$_MODULE['<{dmuadminrecherche}prestashop>bulk_price_acdb802bfc2b99b15d1782570285c427'] = 'Valeur :';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_7dce122004969d56ae2e0245cb754d35'] = 'Modifier';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_31fde7b05ac8952dacf4af8a704074ec'] = 'Visualiser';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_3ec365dd533ddb7ef3d1c111186ce872'] = 'Détails';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_8bc84316c4078bf66723fd019616d920'] = 'Descriptions';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_b9208b03bcc9eb4a336258dcdcb66207'] = 'Déclinaisons';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_e16dd6e118732c5d1586d6aba0b62f3a'] = 'Prix';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_d88946b678e4c2f251d4e292e8142291'] = 'Référencement - SEO';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_98f770b0af18ca763421bac22b4b6805'] = 'Caractéristiques';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_ed75712b0eb1913c28a3872731ffd48d'] = 'Dupliquer';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_7a1920d61156abc05a60135aefe8bc67'] = 'Par défaut';
$_MODULE['<{dmuadminrecherche}prestashop>list_content_f2a6c498fb90ee345d997f888fce3b18'] = 'Supprimer';
$_MODULE['<{dmuadminrecherche}prestashop>list_footer_c8fdde6ec8904647463144c3f3d8de10'] = 'Vous devez sélectionner au moins un produit';
$_MODULE['<{dmuadminrecherche}prestashop>list_footer_9ffa4765d2da990741800bbe1ad4e7f8'] = 'Actions groupées';
$_MODULE['<{dmuadminrecherche}prestashop>list_footer_4c41e0bd957698b58100a5c687d757d9'] = 'Tout sélectionner';
$_MODULE['<{dmuadminrecherche}prestashop>list_footer_237c7b6874386141a095e321c9fdfd38'] = 'Tous désélectionner';
$_MODULE['<{dmuadminrecherche}prestashop>list_footer_b9987a246a537f4fe86f1f2e3d10dbdb'] = 'Affichage';
$_MODULE['<{dmuadminrecherche}prestashop>list_footer_dd8921b41e0279a02c6a26a509241700'] = 'résultat(s)';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_f1ae2cfc018d0081c26c110bd949f78f'] = 'Merci de sélectionner une boutique pour modifier vos produits';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_55ffa325538ec4bd188604c30b29a168'] = 'Cela copiera également les images. Si vous souhaitez continuer, cliquez sur \"Oui\" ; sinon, cliquez sur \"Non\".';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_7bc873cba11f035df692c3549366c722'] = '-- Choisissez --';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_b5024b57ac260d7281c3fc7772a8d73c'] = 'Filtre de recherche';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_526d688f37a86d3c3f27d0c5016eb71d'] = 'Réinitialiser';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_56a8a9eb05f9014da51a4f9b57322ac7'] = 'Catégorie :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_770627b15fec6f46ebafe7bdc500d779'] = '--Toutes--';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_0552b4392893635362e2d4b6964ad8b0'] = 'Fabricant :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_b6c4e7b9dd977ea571eb44249b5c387b'] = '--Tous--';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_90bf278fe4384c50cd3ba3eb9d3c5393'] = 'Fournisseur :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_24a23d787190f2c4812ff9ab11847a72'] = 'Statut :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_e9bb5320b3890b6747c91b5a71ae5a01'] = 'Neutre';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_d382816a3cbeed082c9e216e7392eed1'] = 'Vert';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_ee38e4d5dd68c4e440825018d549cb47'] = 'Rouge';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_d5682e6409bded35ee5c6ca6c6297729'] = 'Attribut :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_027c95289e662ecbc9f99ad93ec87886'] = 'Valeur d\'attribut :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_b07e7131a6007b2a1a765bdd0e1e4894'] = 'Caractéristique :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_a69c9e091c19a4fd5ef34a4f5033d624'] = 'Valeur de caractéristique :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_24c29702dc9c63dedb98e92f3028e068'] = 'Recherche :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_13348442cc6a27032d2b4aa28b75a5d3'] = 'Recherche';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_54c857675bf63580a04999d43ab6ed06'] = 'La recherche s\'effectue dans les champs suivants:';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_2f5703fe62d5107967e74be7d4ccdd59'] = 'Nom, description, résumé, tags, référence, référence fournisseur, EAN-13, ID produit';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_3ccc3075ff24baea8bc9064fa4aa8164'] = 'Activé :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_b1c94ca2fbc3e78fc30069c8d0f01680'] = 'Tous';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_ce7ab0c91bab35201049ecec9662e039'] = 'Pack :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_c17ad205ddc7f337680a59679c13c4a7'] = 'En solde / réduit :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_11bca5941cb439cd23c93140a0959114'] = 'Stock :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_a7f60085d42c06a898c88b817ef4ad18'] = 'A télécharger :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_a184ec0e18daffd14001f6223b0c0542'] = 'Sans image :';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_6b46ae48421828d9973deec5fa9aa0c3'] = 'Tri';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_631b4656a118cf85e5524d3967d375b9'] = 'Date de création croissante';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_94ea634bd18e79832b54ed6afe487ca0'] = 'Date de création décroissante';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_90b8fed5d3138b2adaa717207b00a36c'] = 'Nom croissant';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_932feefb4fbe493eecea87c9dd597319'] = 'Nom décroissant';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_d83668c5b123257884c08d8c3f3203a5'] = 'Référence croissante';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_8234b8c4dd558aa386bdd6a33df6ad4a'] = 'Référence décroissante';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_b2b9933f88394891bcb9fb25ac4715d8'] = 'Prix de vente croissant';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_f2c862e30405959cc2b0485ede89bde2'] = 'Prix de vente TTC croissant';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_dbf21d32ed1d86ab6c52e16b4ae74539'] = 'Prix de vente décroissant';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_9fd32ca4a38704a8ad66e7bb078b5b86'] = 'Prix de vente TTC décroissant';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_eb80977a0fce4f1284d341ca1331e700'] = 'éditable';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_a34efbaa38ac9203d09156c1fad25aa9'] = 'non éditable';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_a7f5665af3f014739d6b25176099e2e1'] = 'Afficher les critères';
$_MODULE['<{dmuadminrecherche}prestashop>list_header_d3d2e617335f08df83599665eef8a418'] = 'Fermer';
