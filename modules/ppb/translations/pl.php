<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ppb}prestashop>ppb_a523e4b61a727f46b2d92eb6ef79340a'] = 'Powiązane / podobne produkty';
$_MODULE['<{ppb}prestashop>ppb_b4ea1bfe31de474469395b232559a020'] = 'Moduł pozwala na utworzenie list podobnych produktów oraz wyświetlenie ich na stronach wskazanych produktów';
$_MODULE['<{ppb}prestashop>ppb_ec211f7c20af43e742bf2570c3cb84f9'] = 'Dodaj';
$_MODULE['<{ppb}prestashop>ppb_b9af8635591dc44009ccd8e5389722ec'] = 'Brak produktów';
$_MODULE['<{ppb}prestashop>ppb_248336101b461380a4b2391a7625493d'] = 'Zapisano';
$_MODULE['<{ppb}prestashop>ppb_43fa58e71d2ecd825b906099c635e3c9'] = 'Nazwa bloku / zakładki';
$_MODULE['<{ppb}prestashop>ppb_8d15124a1d70a5eeafb8f7f401f8d52e'] = 'Zdefiniuj nagłówek listy z produktami. Nagłówek ten będzie widoczny dla Twoich klientów.';
$_MODULE['<{ppb}prestashop>ppb_b55e8bbee8bb462a6ad92cb0f6a0ad30'] = 'Metoda wyświetlania';
$_MODULE['<{ppb}prestashop>ppb_8bcccf048fa5084a300b1b7a31907a93'] = 'Wybierz sposób wyświetlania się listy z produktami';
$_MODULE['<{ppb}prestashop>ppb_e1e4c8c9ccd9fc39c391da4bcd093fb2'] = 'Blok';
$_MODULE['<{ppb}prestashop>ppb_5c6ba25104401c9ee0650230fc6ba413'] = 'Zakładka';
$_MODULE['<{ppb}prestashop>ppb_b1e0cf346b2b35aba8d1086494d1e602'] = 'Jakie produkty wyświetlić?';
$_MODULE['<{ppb}prestashop>ppb_6d72d38ca2dc4823408b66bd9ccf7456'] = 'Wybierz jaki typ produktów moduł ma wyświetlać';
$_MODULE['<{ppb}prestashop>ppb_6dfa1bf382f0e385abdba346f55c857a'] = 'Produkty z wybranej kategorii';
$_MODULE['<{ppb}prestashop>ppb_9ff0635f5737513b1a6f559ac2bff745'] = 'Nowe produkty';
$_MODULE['<{ppb}prestashop>ppb_01f7ac959c1e6ebbb2e0ee706a7a5255'] = 'Najlepiej sprzedające się produkty';
$_MODULE['<{ppb}prestashop>ppb_d1aa22a3126f04664e0fe3f598994014'] = 'Produkty w promocji (z obniżoną ceną)';
$_MODULE['<{ppb}prestashop>ppb_3db9cd44dbbffa69b2ff71ff4d1844ca'] = 'Wybrane produkty';
$_MODULE['<{ppb}prestashop>ppb_63496d05de44bfa43027e88bfa705d2a'] = 'Kategoria do wyświetlenia';
$_MODULE['<{ppb}prestashop>ppb_917534488513af496db2077e119e03b9'] = 'Wprowadź tutaj numery ID kategorii. Przeczytaj';
$_MODULE['<{ppb}prestashop>ppb_a1491c0eeba67c4ad6770e94e93a777c'] = 'Skąd wziąć ID kategorii';
$_MODULE['<{ppb}prestashop>ppb_681b35b63483b18bf2b5fb329bbce63e'] = 'Losowe produkty';
$_MODULE['<{ppb}prestashop>ppb_62a5c1b7e03604d3bc6ca8a971e2718c'] = 'Włącz tę opcję jeżeli chcesz wyświetlać losowe produkty';
$_MODULE['<{ppb}prestashop>ppb_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nie';
$_MODULE['<{ppb}prestashop>ppb_93cba07454f06a4a960172bbd6e2a435'] = 'Tak';
$_MODULE['<{ppb}prestashop>ppb_068f80c7519d0528fb08e82137a72131'] = 'Produkty';
$_MODULE['<{ppb}prestashop>ppb_f904afa44c642aa41e5c5ed8097121a9'] = 'Wprowadź powyżej numery ID produktów które chcesz wyświetlić, lub skorzystaj z wyszukiwarki poniżej';
$_MODULE['<{ppb}prestashop>ppb_d50967011df8d1a6280b778164a29ef5'] = 'Skąd wziąć ID produktu?';
$_MODULE['<{ppb}prestashop>ppb_647cdcd90850616b1614d35d99cf7a71'] = 'Możesz również skorzystać z wyszukiwarki produktów:';
$_MODULE['<{ppb}prestashop>ppb_e293099f8259cb2656a9dc712eed29eb'] = 'Wyszukaj produktu';
$_MODULE['<{ppb}prestashop>ppb_192984b7e7d81587d3117b5ba8b314a9'] = 'Liczba produktów jaką chcesz wyświetlić';
$_MODULE['<{ppb}prestashop>ppb_105aad58a64abee9e9a2f09084c61767'] = 'Zdefiniuj liczbę produktów jaka zostanie wyświetlona';
$_MODULE['<{ppb}prestashop>ppb_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Aktywuj';
$_MODULE['<{ppb}prestashop>ppb_a5c170fc1464f6f814fe0113a1eb7a2a'] = 'Zaznacz tę opcję jeżeli chcesz aktywować blok (będzie on widoczny zaraz po zapisaniu)';
$_MODULE['<{ppb}prestashop>ppb_19a20987be220d886da3b96f97587db4'] = 'Pierwszy element jako kod';
$_MODULE['<{ppb}prestashop>ppb_e00b904bf351a4c54470548b5ced3a6d'] = 'Włącz tę opcję tylko jeżeli chcesz wyświetlić jakiś kod html / css / js zamiast produktu';
$_MODULE['<{ppb}prestashop>ppb_efef3cf4977aa8eebb2b8a9f66ed2be1'] = 'Ta opcja zmienia tylko pierwszy element na liśćie';
$_MODULE['<{ppb}prestashop>ppb_ac7caa63b621583915a442e02f02f102'] = 'Twój kod';
$_MODULE['<{ppb}prestashop>ppb_413ad666afea973e50802b2008bbd8ea'] = 'Wprowadź swój kod';
$_MODULE['<{ppb}prestashop>ppb_3f0143c1dfdd827c93170eb57d11dfe4'] = 'Przykładowy kod do wykorzystania w responsywnych szablonach opartych o bootstrap';
$_MODULE['<{ppb}prestashop>ppb_a14a701597da890443c88da91d71e3fb'] = 'Przykładowy kod';
$_MODULE['<{ppb}prestashop>ppb_3d1dd42c2c09e8a4a05db29bbbd708ab'] = 'Wyświetlanie bloku na stronach wielu produktów';
$_MODULE['<{ppb}prestashop>ppb_af1b98adf7f686b84cd0b443e022b7a0'] = 'Kategorie';
$_MODULE['<{ppb}prestashop>ppb_1c1f2a7b63be4607f89abf5396ef1872'] = 'Włącz tę opcję jeżeli chcesz aby blok który właśnie edytujesz wyświetlał się na stronach produktów, które powiązane są z wybranymi kategoriami';
$_MODULE['<{ppb}prestashop>ppb_89efed24e451448de852f61138df60f5'] = 'numery ID kategorii';
$_MODULE['<{ppb}prestashop>ppb_9985e724cc2608cf48ea27cf33ac7199'] = 'Wprowadź numery id kategorii, oddziel je przecinkami';
$_MODULE['<{ppb}prestashop>ppb_2377be3c2ad9b435ba277a73f0f1ca76'] = 'Producenci';
$_MODULE['<{ppb}prestashop>ppb_8ce37b734bdaf2dedabb025d9c954f28'] = 'Włącz tę opcję jeżeli chcesz aby blok który właśnie edytujesz wyświetlał się na stronach produktów, które powiązane są z wybranymi producentami';
$_MODULE['<{ppb}prestashop>ppb_d207dc828adef0621aac22e86fceec4b'] = 'numery ID producentów';
$_MODULE['<{ppb}prestashop>ppb_6fc5b40a1d32ffa96c3e85aad02be8c3'] = 'wprowadź numery ID producentów';
$_MODULE['<{ppb}prestashop>ppb_daa6b483e9f6ca081ec7e0b4a352f9e9'] = 'zapisz zmiany';
$_MODULE['<{ppb}prestashop>ppb_b3b6d19917be7fe1c74516158498ba0a'] = 'Dodaj';
$_MODULE['<{ppb}prestashop>ppb_9a51a007b33a46e553def6423aad8648'] = 'Ustawienia globalne';
$_MODULE['<{ppb}prestashop>ppb_08a0231f8228b1d259a537f1d5bf116e'] = 'Wybierz sposób wyświetlania się zakładek. Jeżeli działasz w PrestaShop 1.6 z standardowym szablonem i chcesz utworzyć \"prawdziwe\" zakładki, musisz wprowadzić następujące zmiany:';
$_MODULE['<{ppb}prestashop>ppb_ad42672cce2c44b28a9c502fe6f18fad'] = 'realne zakładki w PrestaShop 1.6';
$_MODULE['<{ppb}prestashop>ppb_b7fedbaa431dea2fd1fc27eadaaff941'] = 'wybierz typ zakładek';
$_MODULE['<{ppb}prestashop>ppb_5dd3a39a0c3e2131bb15e60857809313'] = 'zakładki jak w PrestaShop 1.6 (szerokie bloki)';
$_MODULE['<{ppb}prestashop>ppb_7d4aae2db078ebc8be4318a60ee195a3'] = 'zakładki jak w PrestaShop 1.5 (prawdziwe zakładki)';
$_MODULE['<{ppb}prestashop>ppb_c9ae5a4214e87ad6fbed44a267471eee'] = 'zapisz zmiany';
$_MODULE['<{ppb}prestashop>ppb_b25bf2242c7540b6702b151ea1ef235c'] = 'Wykryto problem w bazie danych, moduł wprowadza poprawki';
$_MODULE['<{ppb}prestashop>ppb_007d375fece8a0df8c08bac5f6f931f5'] = '... NAPRAWIONE!';
$_MODULE['<{ppb}prestashop>ppb_eb4fe1daeae43993506b38da0c721ce3'] = '... BŁĄD!';
$_MODULE['<{ppb}prestashop>ppb_36f919521848d86b55d4bf823d1f7232'] = 'OK!';
$_MODULE['<{ppb}prestashop>ppb_7f10b7b76869bf2e029b6ce4632e1323'] = 'Aktualizacja i poprawki';
$_MODULE['<{ppb}prestashop>tabs_713b08aaddedf17b298ed4dc38732b40'] = 'Powróć do listy';
$_MODULE['<{ppb}prestashop>tabs_04d81db08a02fe3f2bc5be86907d38fb'] = 'Utwórz nową listę produktów';
$_MODULE['<{ppb}prestashop>tabs_93f41357957be7ef2859f10ba8679f57'] = 'Edytuj listę produktów';
$_MODULE['<{ppb}prestashop>tabs_846b75ee07b94396c75ec27d5b58fd9e'] = 'Zdefiniowane listy';
$_MODULE['<{ppb}prestashop>tabs_d820b059714dd9a81aaf154144917970'] = 'Utwórz nową listę';
$_MODULE['<{ppb}prestashop>tabs_14511f2f5564650d129ca7cabc333278'] = 'blok';
$_MODULE['<{ppb}prestashop>tabs_e7f8cbd87d347be881cba92dad128518'] = 'zakładka';
$_MODULE['<{ppb}prestashop>products16_1a8c0228da8fb16885881e6c97b96f08'] = 'brak produktów w tym momencie';
$_MODULE['<{ppb}prestashop>tabcontainer16_1a8c0228da8fb16885881e6c97b96f08'] = 'brak produktów w tym momencie';
