{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{foreach from=$blocks item=block}
    <div id="ppbContainer{$block->id|escape:'int':'utf-8'}" class="block products_block clearfix">
        <h4 class="title_block">{$block->name|escape:'html':'utf-8'}</h4>
            {if isset($block->products) && $block->products}
            	{include file="$tpl_dir./product-list.tpl" class="ppb{$block->id|escape:'int':'utf-8'} tab-pane" id="ppb{$block->id|escape:'int':'utf-8'}" products=$block->products}
            {else}
                <ul class="ppbContainer{$block->id|escape:'int':'utf-8'}_noProducts tab-pane">
                	<li class="alert alert-info">{l s='No products at this time.' mod='ppb'}</li>
                </ul>
            {/if}
    </div>
    {if $block->before==1}
        <div style="display:none!important;" id="ppbcontents{$block->id|escape:'int':'utf-8'}">
            {$block->custom_before|escape:'':'utf-8'}
        </div>
        <script>
            $(document).ready(function(){literal}{{/literal}
                $("#ppb{$block->id|escape:'int':'utf-8'} li").removeClass('first-in-line').removeClass('last-line').removeClass('first-item-of-tablet-line').removeClass('first-item-of-mobile-line');
                $("#ppb{$block->id|escape:'int':'utf-8'}").prepend($("#ppbcontents{$block->id|escape:'int':'utf-8'}").html());
                $("#ppbcontents{$block->id|escape:'int':'utf-8'}").remove();
            {literal}}{/literal});
        </script>
    {/if}
{/foreach}