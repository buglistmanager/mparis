{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{foreach from=$blocks item=block}
    <li><a data-toggle="tab" class="ppbTab" href="#ppbTabContainerMain{$block->id|escape:'int':'utf-8'}">{$block->name|escape:'html':'utf-8'}</a></li>
{/foreach}