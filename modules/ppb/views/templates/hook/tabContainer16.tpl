{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

{foreach from=$blocks item=block}
    {if Configuration::get('ppb_internal_tabs')==0}
        <div id="ppbTabContainerMain{$block->id|escape:'int':'utf-8'}" style="margin-top:20px;">
    {/if}
    {if Configuration::get('ppb_internal_tabs')==1}
        <h3 class="page-product-heading">{$block->name|escape:'html':'utf-8'}</h3>
    {/if}
    {if isset($block->products) && $block->products}
    	{include file="$tpl_dir./product-list.tpl" class='tab-pane' id="ppbTabContainer{$block->id|escape:'int':'utf-8'}" products=$block->products}
    {else}
        <ul id="ppbTabContainer{$block->id|escape:'int':'utf-8'}" class="tab-pane">
        	<li class="alert alert-info">{l s='No products at this time.' mod='ppb'}</li>
        </ul>
    {/if}
    {if $block->before==1}
        <div style="display:none!important;" id="ppbcontents{$block->id|escape:'int':'utf-8'}">
            {$block->custom_before|escape:'':'utf-8'}
        </div>
        <script>
            $(document).ready(function(){literal}{{/literal}
                $("#ppbTabContainer{$block->id|escape:'int':'utf-8'} li").removeClass('first-in-line').removeClass('last-line').removeClass('first-item-of-tablet-line').removeClass('first-item-of-mobile-line');
                $("#ppbTabContainer{$block->id|escape:'int':'utf-8'}").prepend($("#ppbcontents{$block->id|escape:'int':'utf-8'}").html());
                $("#ppbcontents{$block->id|escape:'int':'utf-8'}").remove();
            {literal}}{/literal});
        </script>
    {/if}
    {if Configuration::get('ppb_internal_tabs')==0}
        </div>
    {/if}
{/foreach}