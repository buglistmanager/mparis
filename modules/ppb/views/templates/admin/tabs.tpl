{**
* PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
*
* @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
* @copyright 2010-2015 VEKIA
* @license   This program is not free software and you can't resell and redistribute it
*
* CONTACT WITH DEVELOPER http://mypresta.eu
* support@mypresta.eu
*}

<input type="hidden" name="submitted_tabs[]" value="ppb" />
</form>
    {if isset($smarty.get.addtab)}
        <a class="button bt-icon btn btn-default" style="cursor:pointer;" style="cursor:pointer;" href="index.php?controller=AdminProducts&id_product={Tools::getValue('id_product')|escape:'':'utf-8'}&key_tab=ModulePpb&updateproduct&token={Tools::getValue('token')|escape:'':'utf-8'}">
            <img src="../img/admin/arrow2.gif" alt="" title="" /><span>{l s='back to list' mod='ppb'}</span>
        </a>
        <h4>{l s='Create product list' mod='ppb'}</h4>
        <div class="separation"></div>
        <form name="globalsettings" id="globalsettings" method="post" action="index.php?controller=AdminProducts&id_product={Tools::getValue('id_product')|escape:'':'utf-8'}&key_tab=ModulePpb&updateproduct&token={Tools::getValue('token')|escape:'':'utf-8'}">
            {$addform|escape:'':'utf-8'}
        </form>
    {elseif isset($smarty.get.editblock)}
        <a class="button bt-icon btn btn-default" style="cursor:pointer;" style="cursor:pointer;" href="index.php?controller=AdminProducts&id_product={Tools::getValue('id_product')|escape:'':'utf-8'}&key_tab=ModulePpb&updateproduct&token={Tools::getValue('token')|escape:'':'utf-8'}">
            <img src="../img/admin/arrow2.gif" alt="" title="" /><span>{l s='back to list' mod='ppb'}</span>
        </a>
        <h4>{l s='Edit list of products' mod='ppb'}</h4>
        <div class="separation"></div>
        <form name="globalsettings" id="globalsettings" method="post" action="index.php?controller=AdminProducts&id_product={Tools::getValue('id_product')|escape:'':'utf-8'}&key_tab=ModulePpb&updateproduct&token={Tools::getValue('token')|escape:'':'utf-8'}&editblock={Tools::getValue('editblock')|escape:'':'utf-8'}">
            {$editform|escape:'':'utf-8'}
        </form>
    {else}
<div style="overflow:hidden;">
    <div style="display:block; margin-right:10px; margin-top:20px;">
        <h4>{l s='Related products lists' mod='ppb'}</h4>
        <div class="separation"></div>
        <a class="button bt-icon btn btn-default" style="cursor:pointer;" style="cursor:pointer;" href="index.php?controller=AdminProducts&id_product={Tools::getValue('id_product')|escape:'':'utf-8'}&key_tab=ModulePpb&updateproduct&token={Tools::getValue('token')|escape:'':'utf-8'}&addtab=1">
		    <img src="../img/admin/add.gif" alt="" title="" /><span>{l s='Create new product list' mod='ppb'}</span>
        </a>
        <ul class="slides" id="ppblist">
        {if isset($product_ppb)}
            {foreach $product_ppb AS $ppb}
                <li id="ppbp_{$ppb->id|escape:'':'utf-8'}" class="{if $ppb->global_man==1}global_manufacturers_block{/if}{if $ppb->global_cat==1}global_categories_block{/if}" >
                    <span class="name">{$ppb->name|escape:'':'utf-8'} <font style="color:#c0c0c0">{if $ppb->block_position == 1} [{l s='block' mod='ppb'}]{elseif $ppb->block_position==0} [{l s='tab' mod='ppb'}]{/if}</font></span>
                    <span class="remove" onclick="ppbp_remove({$ppb->id|escape:'':'utf-8'})"></span>
                    <span class="edit"><a class="edit" href="index.php?controller=AdminProducts&id_product={Tools::getValue('id_product')|escape:'':'utf-8'}&key_tab=ModulePpb&updateproduct&token={Tools::getValue('token')|escape:'':'utf-8'}&editblock={$ppb->id|escape:'':'utf-8'}"></a></span>
                    <span class="{if $ppb->active==1}on{else}off{/if}" onclick="ppbp_toggle({$ppb->id|escape:'':'utf-8'})"></span>
                </li>
            {/foreach}
        {/if}
    </ul>
    </div>
</div>
{/if}
<script type="text/javascript" src="../js/jquery/ui/jquery.ui.sortable.min.js"></script>

{literal}
    <script>
        function ppbp_remove(id_tab){
            var postoptions = "id="+id_tab+"&action=removeTab";
            $.post("../modules/ppb/ajax_ppb.php", postoptions, function(data) {
            $("#ppbp_"+id_tab).fadeOut('slow');
            });
        }

        function ppbp_toggle(id_tab){
            var postoptions = "id="+id_tab+"&action=toggleTab";
            $.post("../modules/ppb/ajax_ppb.php", postoptions, function(data) {
            eval(data);
            });
        }
        $(".ppb_search").keypress(function(){
            $.post("../modules/ppb/ajax_ppb.php", { search: $(".ppb_search").val()}, function(data){
                $("#ppb_search_result").html(data);
            })
        });
        $(function() {
            var $mySlides = $("#ppblist");
            $mySlides.sortable({
                opacity: 0.6,
                cursor: "move",
                update: function() {
                    var order = $(this).sortable("serialize") + "&action=updateSlidesPosition";
                    $.post("../modules/ppb/ajax_ppb.php", order);
                }
            });
            $mySlides.hover(function() {
                        $(this).css("cursor","move");
                    },
                    function() {
                        $(this).css("cursor","auto");
                    });
        });
    </script>

<style>
    #blockedit td.col-left {width: 250px; padding-right:20px;}
    #blockedit .product_description {color:#C0B4B4;}
.slides {margin:0px; padding:0px; margin-top:10px!important;}
.visible {display:block!important;}
.slides li {overflow:hidden; font-size:14px!important; list-style: none; margin: 0 0 4px 0; padding: 10px 5px; background-color: #F4E6C9; border: #CCCCCC solid 1px; color:#000;}
.hname a {font-size:16px!important;}
.slides li:hover {border:1px #000 dashed; opacity:0.9}
.slides li span.idtab {width:30px; font-weight:bold; display:inline-block; float:left; text-align:center;}
.slides li span.name {margin-left:15px; font-weight:bold; display:inline-block; float:left;}
.slides li span.remove {opacity:0.3; position:relative;  width:24px; height:24px; display:inline-block; float:right; background:url("../modules/ppb/img/trash.png") top no-repeat; cursor:pointer;}
.slides li span.remove:hover, .slides li span a.edit:hover {opacity:1;}
.slides li span a.edit {top:1px; margin-right:6px; opacity:0.3; position:relative;  width:24px; height:24px; display:inline-block; float:right; background:url("../modules/ppb/img/edit.png") top no-repeat; cursor:pointer;}
.slides li span.off {top:2px; margin-right:6px; opacity:1.0; position:relative;  width:50px; height:22px; display:inline-block; float:right; background:url("../modules/ppb/img/pro_off.png") top no-repeat; cursor:pointer;}
.slides li span.on {top:2px; margin-right:6px; opacity:1.0; position:relative;  width:50px; height:22px; display:inline-block; float:right; background:url("../modules/ppb/img/pro_on.png") top no-repeat; cursor:pointer;}
#extratabs_editor {width:100%; display:block; min-height:100px; position:relative; top:0px;}
table td {vertical-align:top;}
.language_flags {display:none;}
.displayed_flag {float:right; margin-left:10px; position: relative!important;}
.slides li.global_categories_block {background: #D7ECFF!important;}
.slides li.global_manufacturers_block {background:#AEECA9!important;}
.slides li.global_manufacturers_blockglobal_categories_block {background: #FBD6D4 !important;}
.ppb_input {display:block; clear:both; position:relative; margin-bottom:20px;}
.ppb_input label {width:150px; display:inline-block; float:left; margin-top:4px; margin-right:20px; }
.ppb_input .margin-form {display:block; position:absolute; left: 170px; width:200px;}
    .language_flags {
        display: none;
        position: absolute;
        right: 0px;
        top: 80px;
        left: 50px;
    }
    .language_flags {display: none;
        position: absolute;
        background: #FFF;
        padding: 10px;
        border: 1px solid black;
        width: 200px;
        left: 210px;
        top:0px;
    }
    .ppopupflagbody .language_flags {
        left:0px!important;
        width:100px!important;
    }
    .langpop {
        position:relative;
    }
    table tr {position:relative;}
</style>
{/literal}