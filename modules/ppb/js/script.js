/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2015 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

$(document).ready(function(){
    $(".hpp_search").keypress(function(){
       $.post("../modules/ppb/ajax_ppb.php", { search: $(".ppb_search").val()}, function(data){
            $("#ppb_search_result").html(data);
       }) 
    });
});