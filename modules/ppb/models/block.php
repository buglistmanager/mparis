<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2015 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

class PpbBlock extends ObjectModel
{

	public $id;
	public $type;
	public $active;
	public $block_position;
	public $position;
	public $value;
	public $shop;
	public $name;
	public $nb;
	public $list_products;
	public $custom_before;
	public $before;
	public $random;
	public $product;
	public $global_cat;
	public $categories;
	public $global_man;
	public $manufacturers;

	public static $definition = array(
		'table' => 'ppbp_block',
		'primary' => 'id',
		'multilang' => true,
		'fields' => array(
			'id' => array('type' => ObjectModel::TYPE_INT),
			'type' => array('type' => ObjectModel::TYPE_INT, 'required' => true),
			'block_position' => array('type' => ObjectModel::TYPE_INT),
			'active' => array('type' => ObjectModel::TYPE_BOOL),
			'random' => array('type' => ObjectModel::TYPE_INT),
			'position' => array('type' => ObjectModel::TYPE_INT),
			'shop' => array('type' => ObjectModel::TYPE_INT),
			'product' => array('type' => ObjectModel::TYPE_INT),
			'value' => array('type' => ObjectModel::TYPE_STRING, 'size' => 254),
			'name' => array(
				'type' => ObjectModel::TYPE_STRING,
				'lang' => true,
				'size' => 254),
			'nb' => array('type' => ObjectModel::TYPE_INT),
			'list_products' => array('type' => ObjectModel::TYPE_STRING),
			'before' => array('type' => ObjectModel::TYPE_BOOL),
			'custom_before' => array(
				'type' => ObjectModel::TYPE_HTML,
				'lang' => true,
			),
			'global_cat' => array('type' => ObjectModel::TYPE_INT),
			'categories' => array('type' => ObjectModel::TYPE_STRING),
			'global_man' => array('type' => ObjectModel::TYPE_INT),
			'manufacturers' => array('type' => ObjectModel::TYPE_STRING),
		),
	);

	public function __construct($id = null)
	{
		parent::__construct($id);
	}

	public static function psversion($part = 1)
	{
		$version = _PS_VERSION_;
		$exp = explode('.', $version);
		if ($part == 1)
			return $exp[1];
		if ($part == 2)
			return $exp[2];
		if ($part == 3)
			return $exp[3];
	}

	public static function getAllBlocks()
	{
		$context = new Context();
		$context = $context->getContext();
		if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1)
			$whereshop = 'WHERE shop="'.$context->shop->id.'"';
		else
			$whereshop = '';

		$id_lang = context::getContext()->cookie->id_lang;
		$query = Db::getInstance()->ExecuteS('SELECT id FROM `'._DB_PREFIX_.'ppbp_block` '.(string)$whereshop.' ORDER BY `position` ');
		$blocks = array();
		foreach ($query as $key)
		{
			$blocks[$key['id']] = new PpbBlock($key['id']);
			$blocks[$key['id']]->name = $blocks[$key['id']]->name["$id_lang"];
			$blocks[$key['id']]->custom_before = $blocks[$key['id']]->custom_before["$id_lang"];
		}
		return $blocks;
	}
	public static function getAllBlocksByProduct($id = null)
	{
		$context = new Context();
		$context = $context->getContext();
		if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1)
			$whereshop = 'AND shop="'.$context->shop->id.'"';
		else
			$whereshop = '';

		$id_lang = context::getContext()->cookie->id_lang;
		$query = Db::getInstance()->ExecuteS('SELECT id FROM `'._DB_PREFIX_.'ppbp_block`  WHERE (product ="'.(int)$id.'" OR global_cat=1 OR global_man=1) '.(string)$whereshop.' ORDER BY `position` ');
		$blocks = array();
		if ($query)
		{
			foreach ($query as $key)
			{
				$blocks[$key['id']] = new PpbBlock($key['id']);
				$blocks[$key['id']]->name = $blocks[$key['id']]->name["$id_lang"];
				$blocks[$key['id']]->custom_before = $blocks[$key['id']]->custom_before["$id_lang"];
			}
		}
		return $blocks;
	}
	public static function getAllBlocksByPosition($position)
	{
		$context = new Context;
		$context = $context->getContext();
		$whereshop = '';
		if (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 1)
			$whereshop = 'AND shop="'.$context->shop->id.'"';

		$id_lang = context::getContext()->cookie->id_lang;
		$query = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'ppbp_block` WHERE block_position='.(string)$position.' AND active=1 AND (product ="'.(int)Tools::getValue('id_product').'" OR global_cat=1 OR global_man=1) '.(string)$whereshop.' ORDER BY position');
		$blocks = array();
		foreach ($query as $key)
		{
			$blocks[$key['id']] = new PpbBlock($key['id']);
			$blocks[$key['id']]->name = $blocks[$key['id']]->name["$id_lang"];
			$blocks[$key['id']]->custom_before = $blocks[$key['id']]->custom_before["$id_lang"];
		}
		return $blocks;
	}
}