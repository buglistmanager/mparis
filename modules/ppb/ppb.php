<?php
/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2015 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */

if (file_exists(_PS_MODULE_DIR_.'ppb/models/block.php'))
	require_once _PS_MODULE_DIR_.'ppb/models/block.php';

class Ppb extends Module
{
	public function __construct()
	{
		$this->name = 'ppb';
		$this->tab = 'advertising_marketing';
		$this->author = 'MyPresta.eu';
		$this->version = '1.4.5';
		$this->module_key = '6169e3dab76c5c67fb99739410409ba9';
		parent::__construct();
		$this->displayName = $this->l('Related products pro');
		$this->description = $this->l('Module allows to create & display blocks or tabs with products product page');
		$this->addproduct = $this->l('Add');
		$this->noproductsfound = $this->l('No products found');
	}

	public static function psversion($part = 1)
	{
		$version = _PS_VERSION_;
		$exp = explode('.', $version);
		if ($part == 1)
			return $exp[1];
		if ($part == 2)
			return $exp[2];
		if ($part == 3)
			return $exp[3];
	}

	public function install()
	{
		if ($this->psversion() == 5 || $this->psversion() == 6)
			if (parent::install() == false or
				!$this->registerHook('displayHeader') or
				!$this->registerHook('productFooter') or
				!$this->registerHook('productTab') or
				!$this->registerHook('productTabContent') or
				!$this->registerHook('displayAdminProductsExtra') or
				!$this->registerHook('actionProductUpdate') or
				!$this->installdb())
				return false;
		return true;
	}

	private function installdb()
	{
		$prefix = _DB_PREFIX_;
		$statements = array();
		$statements[] = "CREATE TABLE IF NOT EXISTS `${prefix}ppbp_block` (".
			'`id` int(10) NOT NULL AUTO_INCREMENT,'.
			'`type` int(3),'.
			'`block_position` int(3),'.
			'`active` int(1),'.
			'`position` int(5),'.
			'`value` int(5),'.
			'`nb` int(4),'.
			'`shop` int(4) DEFAULT 4,'.
			'`list_products` TEXT,'.
			'`before` int(1) DEFAULT 0,'.
			'`after` int(1) DEFAULT 1, '.
			' PRIMARY KEY (`id`)'.
			')';
		$statements[] = "CREATE TABLE IF NOT EXISTS `${prefix}ppbp_block_lang` (".
			'`id` int(10) NOT NULL,'.
			'`id_lang` int(10) NOT NULL,'.
			'`name` VARCHAR(230),'.
			'`custom_before` TEXT,'.
			'`custom_after` TEXT'.
			') COLLATE="utf8_general_ci"';

		foreach ($statements as $statement)
		{
			if (!Db::getInstance()->Execute($statement))
				return false;
		}
		$this->inconsistency();
		return true;
	}

	public function hookDisplayAdminProductsExtra($params) {

		if (Tools::isSubmit('add_new_block'))
		{
			$block = new PpbBlock();
			$block->name = Tools::getValue('name');
			$block->type = Tools::getValue('ppb_type');
			$block->shop = $this->context->shop->id;
			$block->block_position = Tools::getValue('ppbp_block_position');
			$block->active = Tools::getValue('ppb_active');
			$block->value = Tools::getValue('ppb_value');
			$block->nb = Tools::getValue('ppb_nb');
			$block->list_products = Tools::getValue('ppb_products');
			$block->before = Tools::getValue('ppb_before');
			$block->custom_before = Tools::getValue('custombefore');
			$block->random = Tools::getValue('ppb_random');
			$block->product = Tools::getValue('id_product');
			$block->global_cat = Tools::getValue('ppb_global_categories');
			$block->global_man = Tools::getValue('ppb_global_manufacturers');
			$block->categories = trim(str_replace(' ', '', Tools::getValue('ppb_categories')));
			$block->manufacturers = trim(str_replace(' ', '', Tools::getValue('ppb_manufacturers')));
			$block->add();
		}

		if (Tools::isSubmit('edit_block'))
		{
			$block = new ppbBlock(Tools::getValue('editblock'));
			$block->name = Tools::getValue('name');
			$block->type = Tools::getValue('ppb_type');
			$block->shop = $this->context->shop->id;
			$block->block_position = Tools::getValue('ppbp_block_position');
			$block->active = Tools::getValue('ppb_active');
			$block->value = Tools::getValue('ppb_value');
			$block->nb = Tools::getValue('ppb_nb');
			$block->list_products = Tools::getValue('ppb_products');
			$block->before = Tools::getValue('ppb_before');
			$block->custom_before = Tools::getValue('custombefore');
			$block->random = Tools::getValue('ppb_random');
			$block->global_cat = Tools::getValue('ppb_global_categories');
			$block->global_man = Tools::getValue('ppb_global_manufacturers');
			$block->categories = trim(str_replace(' ', '', Tools::getValue('ppb_categories')));
			$block->manufacturers = trim(str_replace(' ', '', Tools::getValue('ppb_manufacturers')));
			$block->update();
		}

		if (Tools::getValue('editblock', 'false') != 'false')
			$this->context->smarty->assign('editform', $this->returnAddForm(Tools::getValue('editblock')));

		if (Tools::getValue('addtab', 'false') != 'false')
			$this->context->smarty->assign('addform', $this->returnAddForm());

		$blocks_prepare = ppbBlock::getAllBlocksByProduct(Tools::getValue('id_product'));


		if (count($blocks_prepare)>0)
			$this->context->smarty->assign(array('product_ppb' => $blocks_prepare, 'employee_idlang'=>$this->context->cookie->id_lang, 'thismodule'=>$this, 'languages' => $this->context->controller->getLanguages()));
		else
			$this->context->smarty->assign(array('employee_idlang'=>$this->context->cookie->id_lang, 'thismodule'=>$this, 'languages' => $this->context->controller->getLanguages()));

		return $this->display(__FILE__, 'views/templates/admin/tabs.tpl');
	}

	public function hookProductFooter($params)
	{
		$blockss = array();
		$blocks = ppbblock::getAllBlocksByPosition(1);
		foreach ($blocks as $key => $value)
		{
			if ($value->type == 1)
			{
				$category = new Category($value->value);
				if ($value->random == 1)
					$blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
				else
					$blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
			}
			if ($value->type == 2)
				$blocks[$key]->products = Product::getNewProducts($this->context->language->id, 0, $value->nb);

			if ($value->type == 3)
				$blocks[$key]->products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);

			if ($value->type == 4)
				$blocks[$key]->products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);

			if ($value->type == 5)
			{
				$explode = explode(',', $value->list_products);
				foreach ($explode as $tproduct)
				{
					if ($tproduct != '')
					{
						$x = (array)new Product($tproduct, true, $this->context->language->id);
						$blockss[$key]->products[$tproduct] = $x;
						$blockss[$key]->products[$tproduct]['id_product'] = $tproduct;
						$image = self::geImagesByID($tproduct, 1);
						$picture = explode('-', $image[0]);
						$blockss[$key]->products[$tproduct]['id_image'] = $picture[1];
					}
				}

				$blocks[$key]->products = Product::getProductsProperties($this->context->language->id, $blockss[$key]->products);

			}
		}

		if ($this->psversion() == 5)
		{
			$this->smarty->assign(array(
				'blocks' => $this->prepareblocks($blocks),
				'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
			));
		} elseif ($this->psversion() == 6)
		{
			$this->smarty->assign(array(
				'blocks' => $this->prepareblocks($blocks),
				'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
			));
		}

		if ($this->psversion() == 6)
			return $this->display(__file__, 'products16.tpl');
		//if ($this->psversion() == 5)
			//return $this->display(__file__, 'products.tpl');

	}

	public function hookProductTab($params)
	{
		if (Configuration::get('ppb_internal_tabs') == 0)
		{
			$blocks = ppbblock::getAllBlocksByPosition(0);
			$this->smarty->assign(array('blocks' => $this->prepareblocks($blocks)));
			return $this->display(__file__, 'tab16.tpl');
		}
	}

	public function hookProductTabContent($params)
	{
		$blockss = array();
		$blocks = ppbblock::getAllBlocksByPosition(0);
		foreach ($blocks as $key => $value)
		{
			if ($value->type == 1)
			{
				$category = new Category($value->value);
				if ($value->random == 1)
					$blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb, null, null, null, true, true, $value->nb);
				else
					$blocks[$key]->products = $category->getProducts($this->context->cookie->id_lang, 0, $value->nb);
			}
			if ($value->type == 2)
				$blocks[$key]->products = Product::getNewProducts($this->context->language->id, 0, $value->nb);

			if ($value->type == 3)
				$blocks[$key]->products = ProductSale::getBestSalesLight($this->context->language->id, 0, $value->nb);

			if ($value->type == 4)
				$blocks[$key]->products = Product::getPricesDrop($this->context->language->id, 0, $value->nb, false);

			if ($value->type == 5)
			{
				$explode = explode(',', $value->list_products);
				foreach ($explode as $tproduct)
				{
					if ($tproduct != '')
					{
						$x = (array)new Product($tproduct, true, $this->context->language->id);
						$blockss[$key]->products[$tproduct] = $x;
						$blockss[$key]->products[$tproduct]['id_product'] = $tproduct;
						$image = self::geImagesByID($tproduct, 1);
						$picture = explode('-', $image[0]);
						$blockss[$key]->products[$tproduct]['id_image'] = $picture[1];
					}
				}
				$blocks[$key]->products = Product::getProductsProperties($this->context->language->id, $blockss[$key]->products);

			}
		}

		$this->smarty->assign(array(
			'blocks' => $this->prepareblocks($blocks),
			'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
		));
		if ($this->psversion() == 6)
			return $this->display(__file__, 'tabContainer16.tpl');
	}

	public function prepareblocks($blocks)
	{
		$array = array();
		$product = new Product(Tools::getValue('id_product'), true);
		foreach ($blocks as $block)
		{
			$remove_block = 1;
			if ($block->global_man == 1)
			{
				$explode_manufacturers = explode(',', $block->manufacturers);
				if (in_array($product->id_manufacturer, $explode_manufacturers))
				{
					$remove_block_manufacturer = 0;
					$array[] = $block;
				}
				else
					$remove_block_manufacturer = 1;
			}
			elseif ($block->global_cat == 1)
			{
				$explode_categories = explode(',', $block->categories);
				if (count(array_intersect($product->getCategories(), $explode_categories)) > 0)
				{
					$remove_block_categories = 0;
					$array[] = $block;
				}
				else
					$remove_block_categories = 1;
			}
			elseif ($block->product == Tools::getValue('id_product'))
				$array[] = $block;
		}
		return $array;
	}

	public function hookHeader($params)
	{
		if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'product')
			$this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
	}

	public function msg_saved()
	{
		return '<div class="conf confirm">'.$this->l('Saved').'</div>';
	}

	public function getContent()
	{
		$output = '';
		if (Tools::isSubmit('ppb_internal_tabs'))
			Configuration::updateValue('ppb_internal_tabs', Tools::getValue('ppb_internal_tabs'));

		$output .= '';
		return $output.$this->displayForm();
	}

	public function searchproduct($search)
	{
		return Db::getInstance()->ExecuteS('SELECT `id_product`,`name` FROM `'._DB_PREFIX_.'product_lang` WHERE `name` like "%'.(string)$search.'%" AND id_lang="'.Configuration::get('PS_LANG_DEFAULT').'" AND id_shop="'.(int)$this->context->shop->id.'" LIMIT 10');
	}

	public function runStatement($statement)
	{
		if (@!Db::getInstance()->Execute($statement))
			return false;
		return true;
	}

	public static function geImagesByID($id_product, $limit = 0)
	{
		$id_image = Db::getInstance()->ExecuteS('SELECT `id_image` FROM `'._DB_PREFIX_.'image` WHERE cover=1 AND `id_product` = '.(int)$id_product.' ORDER BY position ASC LIMIT 0, '.(int)$limit);
		$toReturn = array();
		if (!$id_image)
			return;
		else
			foreach ($id_image as $image)
				$toReturn[] = $id_product.'-'.$image['id_image'];
		return $toReturn;
	}

	public function returnAddForm($block = null)
	{

		if ($block != null){
			$block = new ppbBlock($block);
		}

		$languages = Language::getLanguages(false);
		$form='';
		$title = '';
		$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
		foreach (Language::getLanguages(false) as $language)
		{
			$title .= '
                     <div id="ppbname_'.$language['id_lang'].'" style="width:100%; margin-bottom:10px; clear:both; display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').'; float: left;">
                      <input type="text" id="name_'.$language['id_lang'].'" name="name['.$language['id_lang'].']" value="'.($block != null ? $block->name[$language['id_lang']]:'').'">
                     </div>';
		}
		$title .= '<div class="langpop">'.$this->displayFlags($languages, $id_lang_default, 'ppbname', 'ppbname', true).'</div>';

		$custom_before = '';
		$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
		foreach (Language::getLanguages(false) as $language)
		{
			$custom_before .= '
                     <div id="ppbcustombefore_'.$language['id_lang'].'" style="width:100%; margin-bottom:10px; clear:both; display: '.($language['id_lang'] == $id_lang_default ? 'block' : 'none').'; float: left;">
                      <textarea id="custombefore_'.$language['id_lang'].'" name="custombefore['.$language['id_lang'].']">'.($block != null ? $block->custom_before[$language['id_lang']]:'').'</textarea>
                     </div>';
		}
		$custom_before .= '<div class="langpop">'.$this->displayFlags($languages, $id_lang_default, 'ppbcustombefore', 'ppbcustombefore', true)."</div>";


		$form .= '
				<fieldset id="blockedit">
                    <table>
	                    <tbody>
							<tr>
			                    <td class="col-left">
				                    <label>'.$this->l('Name of block / tab').'</label>
				                    <p class="product_description">'.$this->l('Define the heading of the list of products. It will be visible for the customer as a name of tab or block.').'</p>
			                    </td>
			                    <td>
									'.$title.'
								</td>
							</tr>
							<tr>
			                    <td class="col-left">
                                    <label>'.$this->l('Method of appearing').'</label>
                                    <p class="product_description">'.$this->l('Select the way of how products will appear.').'</p>
								</td>
	                            <td>
		                            <select type="text" name="ppbp_block_position" style="max-width:200px;">
		                                <option value="1" '.($block != null ? ($block->block_position==1 ? 'selected="yes"':''):'').'>'.$this->l('Block').'</option>
		                                <option value="0" '.($block != null ? ($block->block_position==0 ? 'selected="yes"':''):'').'>'.$this->l('Tab').'</option>
		                            </select>
	                            </td>
							</tr>

	                        <tr>
	                            <td class="col-left">
	                                <label>'.$this->l('What to display?').'</label>
	                                <p class="product_description">'.$this->l('Select what kind of products module will display in this block.').'</p>
								</td>
								<td>
		                            <select type="text" name="ppb_type" id="ppb_type" style="max-width:200px;" onchange="if($(\'#ppb_type\').val()==5){$(\'#ppb_type_nb\').hide(); $(\'#ppb_type_products\').show();} else {$(\'#ppb_type_products\').hide(); $(\'#ppb_type_nb\').show();} if($(\'#ppb_type\').val()==1){$(\'.ppb_type_category\').show();}else{$(\'.ppb_type_category\').hide();}">
		                                <option value="1" '.($block != null ? ($block->type==1 ? 'selected="yes"':''):'').'>'.$this->l('Products from category').'</option>
		                                <option value="2" '.($block != null ? ($block->type==2 ? 'selected="yes"':''):'').'>'.$this->l('New products').'</option>
		                                <option value="3" '.($block != null ? ($block->type==3 ? 'selected="yes"':''):'').'>'.$this->l('Best sellers').'</option>
		                                <option value="4" '.($block != null ? ($block->type==4 ? 'selected="yes"':''):'').'>'.$this->l('Specials').'</option>
		                                <option value="5" '.($block != null ? ($block->type==5 ? 'selected="yes"':''):'').'>'.$this->l('Selected products').'</option>
		                            </select>
	                            </td>
							</tr>

	                        <tr class="ppb_type_category" '.($block != null ? ($block->type==1 ? '':'style="display:none;"'):'').'>
	                            <td class="col-left">
									<label>'.$this->l('Category to display').'</label>
									<p class="product_description">
									'.$this->l('Type here ID of category you want to display. Read').' <a href="http://mypresta.eu/en/art/basic-tutorials/prestashop-how-to-get-category-id.html" target="_blank">'.$this->l('how to get Category ID').'</a>
									</p>
								</td>
								<td>
									<input type="text" name="ppb_value" value="'.($block != null ? $block->value:'').'">
								</td>
							</tr>

			                <tr class="ppb_type_category" '.($block != null ? ($block->type==1 ? '':'style="display:none;"'):'').'>
	                            <td class="col-left">
									<label>'.$this->l('Random order').'</label>
									<p class="product_description">'.$this->l('Turn this option on if you want to display products randomly').'</p>
								</td>
								<td>
									<select name="ppb_random">
										<option value="0" '.($block != null ? ($block->random!=1 ? 'selected="yes"':''):'').'>'.$this->l('No').'</option>
										<option value="1" '.($block != null ? ($block->random==1 ? 'selected="yes"':''):'').'>'.$this->l('Yes').'</option>
									</select>
								</td>
							</tr>

							<tr id="ppb_type_products" '.($block != null ? ($block->type==5 ? '':'style="display:none;"'):'style="display:none;"').' class="separated">
	                            <td class="col-left">
									<label>'.$this->l('Products').'</label>
									<p class="product_description">'.$this->l('enter above ID numbers of products you want to display, or search for products below.').' <a href="http://mypresta.eu/en/art/basic-tutorials/how-to-get-product-id-in-prestashop.html" target="_blank">'.$this->l('How to get product ID number').'</a> '.$this->l('Alternatively you can search for product.').'</p>
								</td>
								<td>
									<textarea name="ppb_products" class="ppb_products"> '.($block != null ? ($block->type==5 ? $block->list_products:''):'').'</textarea>
									<label>'.$this->l('Search for product').'</label>
		                            <div class="margin-form">
		                                <input type="text" name="ppb_search" class="ppb_search" style="max-width:200px;"> '.$this->l('').'</a>
		                                <div id="ppb_search_result" style="margin-top:10px;">
		                                </div>
		                            </div>
								</td>
							</tr>
			                <tr id="ppb_type_nb" '.($block != null ? ($block->type!=5 ? '':'style="display:none;"'):'').'>
	                            <td class="col-left">
									<label>'.$this->l('Number of products you want to display').'</label>
									<p class="product_description">'.$this->l('Define the number of products that module will display in this product list').'</p>
								</td>
								<td>
									<input type="text" name="ppb_nb" style="max-width:200px;" value="'.($block != null ? $block->nb:'').'">
								</td>
							</tr>
							<tr >
	                            <td class="col-left">
									<label>'.$this->l('Active').'</label>
									<p class="product_description">'.$this->l('Check if you want activate block (it will be visible for customers once you will save it)').'</p>
								</td>
								<td>
		                            <select type="text" name="ppb_active" style="max-width:200px;">
		                                <option value="0" '.($block != null ? ($block->active!=1 ? 'selected="yes"':''):'').'>'.$this->l('No').'</option>
		                                <option value="1" '.($block != null ? ($block->active==1 ? 'selected="yes"':''):'').'>'.$this->l('Yes').'</option>
		                            </select>
								</td>
							</tr>
           				    <tr>
	                            <td class="col-left">
									<label>'.$this->l('First custom code element').'</label>
									 <p class="product_description">'.$this->l('Turn this option on if you want to add custom code block as first element of list').'</p>
                                    <p class="product_description">'.$this->l('This option works only for first row of products').'</p>
								</td>
								<td>
									<select type="text" id="ppb_before" name="ppb_before" style="max-width:200px;" onchange="if($(\'#ppb_before\').val()==1){$(\'#customcodebefore\').show();}else{$(\'#customcodebefore\').hide();}">
		                                <option value="0" '.($block != null ? ($block->before!=1 ? 'selected="yes"':''):'').'>'.$this->l('No').'</option>
		                                <option value="1" '.($block != null ? ($block->before==1 ? 'selected="yes"':''):'').'>'.$this->l('Yes').'</option>
		                            </select>
								</td>
							</tr>
							<tr  id="customcodebefore" '.($block != null ? ($block->before==1 ? '':'style="display:none;"'):'style="display:none;"').'>
	                            <td class="col-left">
									<label>'.$this->l('Custom code').'</label>
									<p class="product_description">'.$this->l('Enter custom html code here. It will appear as a first element in row.').'</p>
									<p class="product_description">'.$this->l('Example of code for bootstrap templates.').'</p>
	                                <textarea style="resize:none; box-shadow: 0 1px 2px rgba(0,0,0,0.0) inset; background:none; border:0px; width:200px; height:110px;"><li class="ajax_block_product col-xs-12 col-sm-4 col-md-3">'.$this->l('Example of custom contents').'</li></textarea>
								</td>
								<td>
		                            '.$custom_before.'
								</td>
							</tr>
							<tr>
								<td class="col-left" colspan="2">
									<h2 style="margin-top:20px; margin-bottom:20px;">'.$this->l('Global appearance settings').'</h2>
								</td>
							</tr>
							<tr id="appearance_cat">
	                            <td class="col-left">
									<label>'.$this->l('Categories').'</label>
									<p class="product_description">'.$this->l('Turn this option on if you want to display this list of related products on product pages associated with selected category').'</p>
								</td>
								<td>
									<select name="ppb_global_categories" id="ppb_global_categories" onchange="if($(\'#ppb_global_categories\').val()==1){$(\'#appearance_categories\').show();}else{$(\'#appearance_categories\').hide();}">
										<option value="0" '.($block != null ? ($block->global_cat!=1 ? 'selected="yes"':''):'').'>'.$this->l('No').'</option>
										<option value="1" '.($block != null ? ($block->global_cat==1 ? 'selected="yes"':''):'').'>'.$this->l('Yes').'</option>
									</select>
								</td>
							</tr>
							<tr id="appearance_categories" '.($block != null ? ($block->global_cat==1 ? '':'style="display:none;"'):'style="display:none;"').'>
	                            <td class="col-left">
									<label>'.$this->l('Category ID numbers').'</label>
									<p class="product_description">'.$this->l('Enter ID numbers of categories').'</p>
								</td>
								<td>
									<textarea name="ppb_categories" class="ppb_categories"> '.($block != null ? ($block->global_cat==1 ? $block->categories:''):'').'</textarea>
								</td>
							</tr>
							<tr style="">
								<td class="col-left" colspan="2">
								<br/>
								</td>
							</tr>
							<tr style="border-top:1px solid #c0c0c0;">
								<td class="col-left" colspan="2">
								<br/>
								</td>
							</tr>
							<tr id="appearance_man">
	                            <td class="col-left">
									<label>'.$this->l('Manufacturers').'</label>
									<p class="product_description">'.$this->l('Turn this option on if you want to display this list of related products on product pages associated with selected manufacturers').'</p>
								</td>
								<td>
									<select name="ppb_global_manufacturers" id="ppb_global_manufacturers" onchange="if($(\'#ppb_global_manufacturers\').val()==1){$(\'#appearance_manufacturers\').show();}else{$(\'#appearance_manufacturers\').hide();}">
										<option value="0" '.($block != null ? ($block->global_man!=1 ? 'selected="yes"':''):'').'>'.$this->l('No').'</option>
										<option value="1" '.($block != null ? ($block->global_man==1 ? 'selected="yes"':''):'').'>'.$this->l('Yes').'</option>
									</select>
								</td>
							</tr>
							<tr id="appearance_manufacturers" '.($block != null ? ($block->global_man==1 ? '':'style="display:none;"'):'style="display:none;"').'>
	                            <td class="col-left">
									<label>'.$this->l('Manufacturers ID numbers').'</label>
									<p class="product_description">'.$this->l('Enter ID numbers of manufacturers').'</p>
								</td>
								<td>
									<textarea name="ppb_manufacturers" class="ppb_manufacturers"> '.($block != null ? ($block->global_man==1 ? $block->manufacturers:''):'').'</textarea>
								</td>
							</tr>
	                        </tbody>
							</table>
                            <input type="hidden" name="selecttab" value="1">'.($block != null ? '<input type="submit" class="button bt-icon btn btn-default extra button" name="edit_block" value="'.$this->l('Save changes').' "/>':'<input type="submit" class="button bt-icon btn btn-default extra button" name="add_new_block" value="'.$this->l('Add new list').' "/>').'
                     </form>
                 </fieldset>';
		return $form;
	}

	public function displayForm()
	{
		$output = '';
		$form = '';
		$form .= '
                <fieldset>
                    <legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Global Settings').'</legend>
                    <form name="globalsettings" id="globalsettings" method="post">
                        <div class="bootstrap">
	                        <div class="alert alert-info">
								'.$this->l('Select the way of how tabs on product page will appear. If you are on prestashop 1.6 with default-bootstrap theme and want to use real product tabs like it was in 1.5 please follow this guide: ').' <a href="http://mypresta.eu/en/art/prestashop-16/product-tabs.html" target="_blank"/>'.$this->l('real product tabs in prestashop 1.6').'</a>
							</div>
						</div>
                        <label>'.$this->l('Select tabs type').'</label>
            			<div class="margin-form">
                            <select type="text" name="ppb_internal_tabs" style="max-width:200px;">
                                <option value="1" '.(Configuration::get('ppb_internal_tabs') == 1 ? "selected=\"selected\" " : "").'>'.$this->l('PrestaShop 1.6 type (wide bars)').'</option>
                                <option value="0" '.(Configuration::get('ppb_internal_tabs') == 1 ? "" : "selected=\"selected\" ").'>'.$this->l('PrestaShop 1.5 type (real tabs)').'</option>
                            </select>
           				</div>
                        <input type="submit" class="extra button" name="global_settings" value="'.$this->l('save settings').' "/>
                     </form>
                 </fieldset>';

		return $output.'
        <script type="text/javascript" src="../modules/ppb/js/script.js"/></script>
        <script type="text/javascript">
    			$(function() {
    				var $mySlides = $("#homepageblocks");
    				$mySlides.sortable({
    					opacity: 0.6,
    					cursor: "move",
    					update: function() {
    						var order = $(this).sortable("serialize") + "&action=updateSlidesPosition";
    						$.post("'._PS_BASE_URL_.__PS_BASE_URI__.'modules/'.$this->name.'/ajax_'.$this->name.'.php", order);
    						}
    					});
    				$mySlides.hover(function() {
    					$(this).css("cursor","move");
    					},
    					function() {
    					$(this).css("cursor","auto");
    				});
    			});
    		    </script>
        <style>
            .language_flags {text-align:left;}
            #topmenu-horizontal-module {overflow:hidden; background-color: #F8F8F8; border: 1px solid #CCCCCC; margin-bottom: 10px; padding: 10px 0; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px;}
            #topmenu-horizontal-module .addnew, #topmenu-horizontal-module .btnpro {-webkit-border-radius:4px; -moz-border-radius:4px; -border-radius:4px; padding:5px; margin-right:10px; cursor:pointer; width:52px; height:52px; display:inline-block; float:right; text-align:center; border:1px dotted #c0c0c0; }
            #topmenu-horizontal-module .addnew:hover, #topmenu-horizontal-module .btnpro:hover {border:1px solid #bfbfbf; background:#f3f3f3;}
            #topmenu-horizontal-module span.img {margin:auto; width:32px; height:32px; display:block;}
            #topmenu-horizontal-module span.txt {margin-top:3px; width:52px; display:block; text-align:center;}
            #topmenu-horizontal-module .addnew span.img {background:url(\''._MODULE_DIR_.$this->name.'/img/add.png\') no-repeat center;}
            #topmenu-horizontal-module .save span.img {background:url(\''._MODULE_DIR_.$this->name.'/img/on.png\') no-repeat center;}
            #topmenu-horizontal-module .back span.img {background:url(\''._MODULE_DIR_.$this->name.'/img/back.png\') no-repeat center;}

                .slides {margin:0px; padding:0px;}
                .slides li { font-size:15px!important; list-style: none; margin: 0 0 4px 0; padding: 15px 10px; background-color: #F4E6C9; border: #CCCCCC solid 1px; color:#000;}
                .slides li:hover {border:1px #000 dashed; cursor:move;}
                .slides li .name {font-size:18px!important;}
                .slides li .nb {color:#FFF; background:#000; padding:5px 10px; font-size:18px; font-weight:bold; margin-right:10px; }

                .activate {display:inline-block; float:right; padding-right:5px;  cursor:pointer; position:relative; top:2px;}
                .activate img {max-width:50px; height:auto;}
                .remove {opacity:0.3; position:relative; top:-1px; width:24px; height:24px; display:inline-block; float:right; background:url("../modules/'.$this->name.'/img/trash.png") top no-repeat; cursor:pointer;}
                .edit {margin-right:6px; opacity:0.3; position:relative;  width:24px; height:24px; display:inline-block; float:right; background:url("../modules/'.$this->name.'/img/edit.png") top no-repeat; cursor:pointer;}

                .remove:hover, .edit:hover, .activate:hover { opacity:1.0; }
                .edit,.remove {margin-right:5px;}


        </style>
        <form name="selectform1" id="selectform1" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="1"></form>
        <form name="selectform2" id="selectform2" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="2"></form>
        <form name="selectform3" id="selectform3" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="3"></form>
        <form name="selectform4" id="selectform4" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="4"></form>
        <form name="selectform99" id="selectform99" action="'.$_SERVER['REQUEST_URI'].'" method="post"><input type="hidden" name="selecttab" value="99"></form>
        '."<div id='cssmenu'>
            <ul>
               <li class='bgver'><a><span>v".$this->version."</span></a></li>
               <li style='position:relative; display:inline-block; float:right; '><a style=\"display:block; width:40px;\" href='http://mypresta.eu' target='_blank' title='prestashop modules'><span ><img src='../modules/ppb/img/logo-white.png' alt='prestashop modules' style=\"position:absolute; top:17px; right:16px;\"/></span></a></li>
            </ul>
        </div>".'<link href="../modules/'.$this->name.'/css.css" rel="stylesheet" type="text/css" />'.$form;
	}

	public function inconsistency(){
		$prefix = _DB_PREFIX_;
		$table = array();
		$error = array();
		$form = '';
		$table['ppbp_block']['list_products']['type']='text';
		$table['ppbp_block']['list_products']['length']='X';
		$table['ppbp_block']['list_products']['default']='X';
		$table['ppbp_block']['before']['type']='int';
		$table['ppbp_block']['before']['length']='1';
		$table['ppbp_block']['before']['default']='0';
		$table['ppbp_block_lang']['custom_before']['type']='text';
		$table['ppbp_block_lang']['custom_before']['length']='X';
		$table['ppbp_block_lang']['custom_before']['default']='X';
		$table['ppbp_block']['after']['type']='int';
		$table['ppbp_block']['after']['length']='1';
		$table['ppbp_block']['after']['default']='0';
		$table['ppbp_block']['random']['type']='int';
		$table['ppbp_block']['random']['length']='1';
		$table['ppbp_block']['random']['default']='0';
		$table['ppbp_block']['product']['type']='int';
		$table['ppbp_block']['product']['length']='7';
		$table['ppbp_block']['product']['default']='X';
		$table['ppbp_block_lang']['custom_after']['type']='text';
		$table['ppbp_block_lang']['custom_after']['length']='X';
		$table['ppbp_block_lang']['custom_after']['default']='X';
		$table['ppbp_block']['global_man']['type']='int';
		$table['ppbp_block']['global_man']['length']='1';
		$table['ppbp_block']['global_man']['default']='0';
		$table['ppbp_block']['global_cat']['type']='int';
		$table['ppbp_block']['global_cat']['length']='1';
		$table['ppbp_block']['global_cat']['default']='0';
		$table['ppbp_block']['categories']['type']='text';
		$table['ppbp_block']['categories']['length']='X';
		$table['ppbp_block']['categories']['default']='X';
		$table['ppbp_block']['manufacturers']['type']='text';
		$table['ppbp_block']['manufacturers']['length']='X';
		$table['ppbp_block']['manufacturers']['default']='X';

		$return='';

		//ppbp_block
		foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='"._DB_NAME_."' AND TABLE_NAME='"._DB_PREFIX_."ppbp_block'") AS $key => $column){
			$return[$column['COLUMN_NAME']]="1";
		}
		foreach ($table['ppbp_block'] as $key => $field){
			if (!isset($return[$key])){
				$error[$key]['type']="0";
				$error[$key]['message']=$this->l('Database inconsistency, column does not exist');
				if ($field['default']!="X"){
					if ($this->runStatement("ALTER TABLE `${prefix}ppbp_block` ADD COLUMN `".$key."` ".$field['type']."(".$field['length'].") NULL DEFAULT '".$field['default']."'")){
						$error[$key]['fixed']=$this->l('... FIXED!');
					} else {
						$error[$key]['fixed']=$this->l('... ERROR!');
					}
				} else {
					if ($this->runStatement("ALTER TABLE `${prefix}ppbp_block` ADD COLUMN `".$key."` ".$field['type'])){
						$error[$key]['fixed']=$this->l('... FIXED!');
					} else {
						$error[$key]['fixed']=$this->l('... ERROR!');
					}
				}
				if (isset($field['config'])){
					Configuration::updateValue($field['config'],'1');
				}
			} else {
				$error[$key]['type']="1";
				$error[$key]['message']=$this->l('OK!');
				$error[$key]['fixed']=$this->l('');
				if (isset($field['config'])){
					Configuration::updateValue($field['config'],'1');
				}
			}
		}


		//ppbp_block_lang
		foreach (Db::getInstance()->executeS("SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA ='"._DB_NAME_."' AND TABLE_NAME='"._DB_PREFIX_."ppbp_block_lang'") AS $key => $column){
			$return[$column['COLUMN_NAME']]="1";
		}
		foreach ($table['ppbp_block_lang'] as $key => $field){
			if (!isset($return[$key])){
				$error[$key]['type']="0";
				$error[$key]['message']=$this->l('Database inconsistency, column does not exist');
				if ($field['default']!="X"){
					if ($this->runStatement("ALTER TABLE `${prefix}ppbp_block_lang` ADD COLUMN `".$key."` ".$field['type']."(".$field['length'].") NULL DEFAULT '".$field['default']."'")){
						$error[$key]['fixed']=$this->l('... FIXED!');
					} else {
						$error[$key]['fixed']=$this->l('... ERROR!');
					}
				} else {
					if ($this->runStatement("ALTER TABLE `${prefix}ppbp_block_lang` ADD COLUMN `".$key."` ".$field['type'])){
						$error[$key]['fixed']=$this->l('... FIXED!');
					} else {
						$error[$key]['fixed']=$this->l('... ERROR!');
					}
				}
				if (isset($field['config'])){
					Configuration::updateValue($field['config'],'1');
				}
			} else {
				$error[$key]['type']="1";
				$error[$key]['message']=$this->l('OK!');
				$error[$key]['fixed']=$this->l('');
				if (isset($field['config'])){
					Configuration::updateValue($field['config'],'1');
				}
			}
		}
		$form.='<table class="inconsistency"><tr><td colspan="4" style="text-align:center">'.$this->l('UPGRADE').'</td></tr>';
		foreach ($error as $column => $info){
			$form.="<tr><td class='inconsistency".$info['type']."'></td><td>".$column."</td><td>".$info['message']."</td><td>".$info['fixed']."</td></tr>";
		}
		$form.="</table>";

		//return $form;
		return true;
	}

}
?>