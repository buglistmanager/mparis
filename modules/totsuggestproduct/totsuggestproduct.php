<?php
/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to a commercial license from 202 ecommerce
 * Use, copy, modification or distribution of this source file without written
 * license agreement from 202 ecommerce is strictly forbidden.
 *
 * @author    202 ecommerce <contact@202-ecommerce.com>
 * @copyright Copyright (c) 202 ecommerce 2014
 * @license   Commercial license
 *
 * Support <support@202-ecommerce.com>
 */

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

class TotSuggestProduct extends Module
{

    /**
     * Lien
     * @var string
     */
    private $link = null;

    /**
     * If website use prestashop 1.5 or more
     * @var boolean
     */
    public static $version_15 = false;

    /**
     * Constructor of module
     * @version 1.0.0
     */
    public function __construct()
    {
        $this->name = 'totsuggestproduct'; // Name module
        $this->tab = 'others'; // Tab module
        $this->version = '2.1.5'; // Version of module 1.0.0
        $this->author = '202-ecommerce'; // Author module
        $this->module_key = 'ce891e79df159a4f202bfa2b8d2cd367';
        parent::__construct(); // Parent constructor

        $this->displayName = $this->l('Suggest product with statistics'); // Translation display name
        $this->description = $this->l('Manage the Shopping cart cross selling area'); // Translation description

        //
        // Check version of prestashop
        $this->checkVersion();
        if (Module::isInstalled($this->name)) {
            $this->upgrade();
        }
        // Ling
        if (version_compare(_PS_VERSION_, '1.5', '>')) {
            $this->link = 'index.php?controller=' . Tools::getValue('controller') . '&configure=' . $this->name . '&token=' . Tools::getValue('token') . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        } else {
            $this->link = 'index.php?tab=' . Tools::getValue('tab') . '&configure=' . $this->name . '&token=' . Tools::getValue('token') . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        }

        /* Backward compatibility pour les version 1.4 */
        if (_PS_VERSION_ < '1.5') {
            require(_PS_MODULE_DIR_ . $this->name . '/backward_compatibility/backward.php');
        }
    }


    /**
     * Installing the module
     * @version 1.0.0
     * @return boolean
     */
    public function install()
    {
        if ($this->installSQL() === false
            || parent::install() === false
            || $this->registerHook('shoppingCart') === false
            || $this->registerHook('header') === false
            || $this->registerHook('cart') === false
            || $this->registerHook('newOrder') === false
            || $this->registerHook('updateOrderStatus') === false
            || Configuration::updateValue('TOTSUGGESTPRODUCT_INSTALL', date('Y-m-d h:i:s')) === false
        ) {
            return false;
        }

        if (self::$version_15 === true && $this->registerHook('displayBackOfficeHeader') === false) {
            return false;
        }

        Configuration::updateValue('SUGGEST_PRODUCT_CAMPAIGN_LIMIT', 5);

        return true;
    }

    /**
     * Removing the module
     * @version 1.0.0
     * @return boolean
     */
    public function uninstall()
    {
        if (parent::uninstall() === false) {
            return false;
        }

        if (!$this->uninstallSQL()) {
            return false;
        }

        return true;
    }


    /**
     * Upgrading the module
     */
    public function upgrade()
    {
        if (count(Db::getInstance()->ExecuteS('SHOW TABLES LIKE "' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule"')) == 0) {
            $this->installSQL3();
            $query = 'SELECT * FROM `' . _DB_PREFIX_ . 'totsuggestproduct`';
            $res = Db::getInstance()->ExecuteS($query);
            foreach ($res as $onetot) {
                //Insert Cartu Rule
                $insertCartRule = 'INSERT INTO `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule` VALUES(\'\', \'' . $onetot['campaignName'] . '\')';
                Db::getInstance()->Execute($insertCartRule);
                //Get id of last inserted Cart Rule
                $rqidCartRule = 'SELECT id_totsuggestproduct_cart_rule FROM `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule`
                                            ORDER BY id_totsuggestproduct_cart_rule DESC ';
                $idCartRule = Db::getInstance()->getValue($rqidCartRule);
                //Modify totsuggestproduct entry
                Db::getInstance()->Execute('UPDATE ' . _DB_PREFIX_ . 'totsuggestproduct
                                                                        SET id_cat = \'' . $idCartRule . '\' WHERE id = \'' . (int)$onetot['id'] . '\'');
                //Insert Cart Rule Group
                Db::getInstance()->Execute('INSERT INTO `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_group`
                                                                        VALUES(\'\', \'' . $idCartRule . '\', \'1\')');
                //Get Id of last inserted cart rule group
                $idCartRuleGroup = Db::getInstance()->getValue('SELECT id_totsuggestproduct_cart_rule_group FROM `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_group`
                                                                                                            ORDER BY id_totsuggestproduct_cart_rule_group DESC');
                //Insert Rule value
                Db::getInstance()->Execute('INSERT INTO `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_value`
                                                                    VALUES(\'\',
                                                                        \'' . (int)$idCartRuleGroup . '\',
                                                                        \'category\',
                                                                        \'' . (int)$onetot['id_cat'] . '\'
                                                                        )');
                if ($onetot['checked'] == 'on') {
                    $children = Category::getChildren($onetot['id_cat'], Configuration::get('PS_LANG_DEFAULT'), true);
                    $this->_upgradeChildren($children, $idCartRuleGroup);
                }
            }
            Db::getInstance()->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'totsuggestproduct`
                                                                            CHANGE `id_cat` `id_totsuggestproduct_cart_rule` INT(11) NOT NULL');
            Db::getInstance()->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'totsuggestproduct`
                                                                            CHANGE `id` `id_totsuggestproduct` INT(11) NOT NULL AUTO_INCREMENT');
            Db::getInstance()->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'totsuggestproduct_lang`
                                                                            CHANGE `value`  `justification` VARCHAR(255) NOT NULL ');
            Db::getInstance()->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'totsuggestproduct`
                                                                            DROP COLUMN checked');
        }
    }

    public function _upgradeChildren($children, $idCartRuleGroup)
    {
        foreach ($children as $child) {
            //Insert Rule value
            Db::getInstance()->Execute('INSERT INTO `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_value`
                                                        VALUES(\'\',
                                                            \'' . (int)$idCartRuleGroup . '\',
                                                            \'category\',
                                                            \'' . (int)$child['id_category'] . '\'
                                                            )');
            $newchildren = Category::getChildren($child['id_category'], Configuration::get('PS_LANG_DEFAULT'), true);
            if (count($newchildren) > 0) {
                $this->_upgradeChildren($newchildren, $idCartRuleGroup);
            }
        }
    }
    ################################################################
    ## Install / Uninstall SQL
    ################################################################

    private function installSQL()
    {
        // First insert
        // Head table
        $totsuggestproduct = '
                    CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct` (
                             `id_totsuggestproduct` INT(11) NOT NULL AUTO_INCREMENT,  
                             `id_totsuggestproduct_cart_rule` INT(11) NOT NULL,  
                             `id_product` INT(11) NOT NULL, 
                             `campaignName` VARCHAR(255) DEFAULT NULL, 
                             `enabled` BOOLEAN NOT NULL, 
                             `visible` BOOLEAN NOT NULL, 
                             `date_begin` datetime NOT NULL, 
                             `date_end` datetime NOT NULL,  
                             `id_shop` INT, 
                             `id_shop_group` INT,  
                             PRIMARY KEY (`id_totsuggestproduct`)
                 ) 
                 ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';

        if (Db::getInstance()->Execute($totsuggestproduct) === false) {
            return false;
        }

        // Translate
        // Second insert
        $totsuggestproduct_lang = '
                 CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct_lang` (
                            `id_totsuggestproduct` INT(11) NOT NULL,  
                            `id_lang` INT(11) NOT NULL,
                            `justification` VARCHAR(255) NOT NULL,
                            PRIMARY KEY (`id_totsuggestproduct`, `id_lang`)
                 ) 
                 ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        if (Db::getInstance()->Execute($totsuggestproduct_lang) === false) {
            return false;
        }

        // Stats
        // Third insert
        $totsuggestproduct_stats = '
                 CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct_stats` (
                            `id_totsuggestproduct_stats` INT(11) NOT NULL AUTO_INCREMENT,  
                            `id_product` INT(11) NOT NULL,  
                            `id_product_attribute` INT(11) NOT NULL,  
                            `id_customization` INT(11) NOT NULL,  
                            `id_cart` INT(11) NOT NULL,  
                            `date_add_to_cart` datetime NOT NULL,  
                            `date_delete_from_cart` datetime NOT NULL,  
                            `date_ordered` datetime NOT NULL,  
                            `id_campaign` INT(11) NOT NULL, 
                            `displayed` datetime NOT NULL,  
                            PRIMARY KEY (`id_totsuggestproduct_stats`)
                 )
                 ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';

        if (Db::getInstance()->Execute($totsuggestproduct_stats) === false) {
            return false;
        }

        return $this->installSQL3();
    }

    private function installSQL3()
    {
        // Rules
        // Fourth insert
        $totsuggestproduct_cart_rule = '
                 CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule` (
                            `id_totsuggestproduct_cart_rule` INT(11) NOT NULL AUTO_INCREMENT,
                            `name` VARCHAR(255) NOT NULL,
                            PRIMARY KEY (`id_totsuggestproduct_cart_rule`)
                 )
                 ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';

        if (Db::getInstance()->Execute($totsuggestproduct_cart_rule) === false) {
            return false;
        }

        // Country
        // Fourth insert
        $totsuggestproduct_cart_country = '
                 CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct_cart_country` (
                            `totsuggestproduct_cart_country` INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                            `id_country` INT(11) NOT NULL,
                            `id_totsuggestproduct_cart_rule` INT(11) NOT NULL,
                            UNIQUE KEY (`id_country`, `id_totsuggestproduct_cart_rule`)
                 )
                 ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        if (Db::getInstance()->Execute($totsuggestproduct_cart_country) === false) {
            return false;
        }

        // Group rules
        // Fifth
        $totsuggestproduct_cart_rule_product_rule_group = '
                 CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_group` (
                            `id_totsuggestproduct_cart_rule_group` INT(11) NOT NULL AUTO_INCREMENT,
                            `id_totsuggestproduct_cart_rule` INT(11) NOT NULL,
                            `rules_validated` INT(2) NOT NULL,
                            PRIMARY KEY (`id_totsuggestproduct_cart_rule_group`)
                 )
                 ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';

        if (Db::getInstance()->Execute($totsuggestproduct_cart_rule_product_rule_group) === false) {
            return false;
        }

        // product rules
        // Sixth
        $totsuggestproduct_cart_rule_product_rule = '
                 CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_value` (
                            `id_totsuggestproduct_cart_rule_value` INT(11) NOT NULL AUTO_INCREMENT,
                            `id_totsuggestproduct_cart_rule_group` INT(11) NOT NULL,
                            `type` ENUM("product", "product_attribute", "category", "supplier", "manufacturer"),
                            `value` INT(11) NOT NULL,
                            PRIMARY KEY (`id_totsuggestproduct_cart_rule_value`)
                 )
                 ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';

        if (Db::getInstance()->Execute($totsuggestproduct_cart_rule_product_rule) === false) {
            return false;
        }

        $totsuggestproduct_has_been_seen = '
                CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'totsuggestproduct_has_been_seen` (
                    `id_cart` INT(11) NOT NULL, 
                    `id_product` INT(11) NOT NULL,
                    `id_campaign` INT(11) NOT NULL,
                    PRIMARY KEY (`id_cart`,`id_product`)
                )
                ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

        if (Db::getInstance()->Execute($totsuggestproduct_has_been_seen) === false) {
            return false;
        }

        // If SQL install has no problem
        return true;
    }

    private function uninstallSQL()
    {
        $sql = 'DROP TABLE IF EXISTS
                            `' . _DB_PREFIX_ . 'totsuggestproduct`,
                            `' . _DB_PREFIX_ . 'totsuggestproduct_lang`,
                            `' . _DB_PREFIX_ . 'totsuggestproduct_stats`,
                            `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule`,
                            `' . _DB_PREFIX_ . 'totsuggestproduct_cart_country`,
                            `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_group`,
                            `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_value`,
                            `' . _DB_PREFIX_ . 'totsuggestproduct_has_been_seen`
                            ';
        if (Db::getInstance()->Execute($sql) === false) {
            return false;
        }

        return true;
    }

    ################################################################
    ## hook
    ################################################################

    /**
     * Add Style
     * @global Object $smarty
     */
    public function hookHeader()
    {
        $cookie = $this->context->cookie;
        $cart = $this->context->cart;
        $smarty = $this->context->smarty;

        // On order page
        if ($smarty->getTemplateVars('page_name') == 'order' || $smarty->getTemplateVars('page_name') == 'order-opc') {
            $CSS = $this->_path . '/views/css/totsuggestproduct.css';
            $js = $this->_path . '/views/js/totsuggestproduct.js';
            // Before 1.5
            if (self::$version_15 === false) {
                Tools::addCSS($CSS, 'all');
                Tools::addJS($js);
                $tpldir = $smarty->getTemplateVars('tpl_dir');
                $tpldirArray = explode('/', $tpldir);
                $themeName = $tpldirArray[count($tpldirArray) - 2];
                Tools::addCSS('/themes/' . $themeName . '/css/product.css', 'all');
            } else {
                $this->context->controller->addCSS('/themes/' . $this->context->shop->theme_name . '/css/product.css', 'all');
                $this->context->controller->addCSS($CSS, 'all');
                $this->context->controller->addJS($js);
            }
        } elseif ($smarty->getTemplateVars('page_name') == 'product') {
            if (Tools::getValue('totsuggestproduct') == 1) {
                //Product has been suggested and clicked on, but not directly added to cart
                $query = 'REPLACE INTO ' . _DB_PREFIX_ . 'totsuggestproduct_has_been_seen
                                    VALUES (\'' . (int)$cart->id . '\',
                                                    \'' . (int)Tools::getValue('id_product') . '\',
                                                    \'' . (int)Tools::getValue('id_campaign') . '\')';
                Db::getInstance()->Execute($query);
            }
        }
    }

    /**
     * Display suggest
     * @return string
     */
    public function hookShoppingCart()
    {
        $this->includeClass();

        $cookie = $this->context->cookie;
        $smarty = $this->context->smarty;
        $cart = $this->context->cart;

        // Version < 1.5
        if (self::$version_15 === false) {
            $version = '14';

        } else {
            $version = '15';
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                $version = '16';
            }
        }

        $suggestedProducts = TotRules::checkValidity();

        foreach ($suggestedProducts as $productsuggest) {
            $product = $productsuggest['product'];
            $campaign = $productsuggest['suggest'];
            $query = 'REPLACE INTO ' . _DB_PREFIX_ . 'totsuggestproduct_has_been_seen
                        VALUES (\'' . (int)$cart->id . '\',
                        \'' . $product->id . '\',
                        \'' . $campaign['id'] . '\')';
            Db::getInstance()->Execute($query);
        }

        $smarty->assign(array(
            'suggestedProducts' => $suggestedProducts,
            'productEndUrl'     => ((int)Configuration::get('PS_REWRITING_SETTINGS') ? '?totsuggestproduct=1&id_campaign=' : '&totsuggestproduct=1&id_campaign='),
            'campaign_limit'    => Configuration::get('SUGGEST_PRODUCT_CAMPAIGN_LIMIT'),
            'version'           => $version
        ));

        return $this->display(__FILE__, 'views/templates/hook/totSuggestProduct.tpl');
    }

    /**
     * Update order status
     * @param array $params
     */
    public function hookUpdateOrderStatus($params)
    {
        $orderState = $params['newOrderStatus'];
        $order = new Order($params['id_order']);
        if ($orderState->id == Configuration::get('PS_OS_CANCELED')) {
            DB::getInstance()->Execute('UPDATE ' . _DB_PREFIX_ . 'totsuggestproduct_stats
                                            SET date_ordered = \'0000-00-00 00:00:00\' 
                                            WHERE id_cart = \'' . (int)$order->id_cart . '\'');

            return;
        }
        $this->updateOrderStats($params['cart']);
    }

    /**
     * Only in version 1.5+
     */
    public function hookdisplayBackOfficeHeader()
    {
        /*$this->context->controller->addCSS($this->_path . 'css/admin.css', 'all');
        $this->context->controller->addCSS('/js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.csss');
        $this->context->controller->addJquery();
        $this->context->controller->addJqueryPlugin('autocomplete');*/
    }

    /**
     * Update the value for stats
     */
    public function hookNewOrder()
    {
        $cart = $this->context->cart;
        $this->updateOrderStats($cart);
    }

    /**
     * Get information about the add to Cart
     * @global cart
     */
    public function hookCart()
    {
        $cart = $this->context->cart;

        $tableStats = _DB_PREFIX_ . 'totsuggestproduct_stats';
        if ($cart instanceof Cart) {
            if ((Tools::getValue('totstats') == 'totsuggestProduct' || $this->getCampaignOfSuggestedProduct()) && !Tools::getValue('delete')) {//If adding a product via totsuggestproduct
                $product = $cart->getLastProduct();
                $productAttributeID = $product['id_product_attribute'];
                $productID = $product['id_product'];
                $totstatsCampaign = $this->getCampaign();
                $rq_ProductAlreadyExists = 'SELECT COUNT(*) FROM `' . $tableStats . '` WHERE `id_product` = \'' . (int)$productID . '\' AND `id_cart` = \'' . $cart->id . '\'';
                $ProductAlreadyExists = DB::getInstance()->getValue($rq_ProductAlreadyExists);
                if ($ProductAlreadyExists > 0) {
                    DB::getInstance()->Execute('
                    UPDATE ' . $tableStats . '
                         SET 
                            `date_add_to_cart` = \'' . pSQL(date('Y-m-d H:i:s')) . '\',
                            `id_campaign` = \'' . pSQL($totstatsCampaign) . '\',
                            `id_product_attribute` = \'' . (int)$productAttributeID . '\'
                         WHERE 
                            `id_product` = \'' . (int)$productID . '\'
                            AND `id_cart` = \'' . (int)$cart->id . '\'');
                } else {
                    DB::getInstance()->Execute('
                    INSERT INTO ' . $tableStats . ' (`id_product`, `id_product_attribute`, `id_cart`, `date_add_to_cart`, `id_campaign`)
                         VALUES (
                                    \'' . (int)$productID . '\',
                                    \'' . (int)$productAttributeID . '\',
                                    \'' . (int)$cart->id . '\',
                                    \'' . pSQL(date('Y-m-d H:i:s')) . '\',
                                    \'' . pSQL($totstatsCampaign) . '\'
                         )');
                }
            } else {//Retrieve products that were added via totsuggestproduct and check if they were delete
                if (is_object($cart)) {
                    $rq_ProductInCartFromSuggest = 'SELECT * FROM `' . $tableStats . '` WHERE `id_cart` = \'' . $cart->id . '\' AND date_add_to_cart != \'0000-00-00 00:00:00\'';
                    $ProductInCartFromSuggest = DB::getInstance()->ExecuteS($rq_ProductInCartFromSuggest);
                    foreach ($ProductInCartFromSuggest as $product) {
                        $rq_IsProductInCart = 'SELECT COUNT(*) FROM `' . _DB_PREFIX_ . 'cart_product`
                                                        WHERE `id_cart` = \'' . (int)$cart->id . '\'
                                                        AND `id_product` = \'' . (int)$product['id_product'] . '\'
                                                        AND `id_product_attribute` = \'' . (int)$product['id_product_attribute'] . '\'
                                                    ';
                        if (DB::getInstance()->getValue($rq_IsProductInCart) == 0) {
                            DB::getInstance()->Execute('
                            UPDATE `' . $tableStats . '`
                                 SET 
                                        `date_delete_from_cart` = \'' . pSQL(date('Y-m-d H:i:s')) . '\'
                                 WHERE 
                                        `id_product` = \'' . (int)$product['id_product'] . '\'
                                        AND `id_product_attribute` = \'' . (int)$product['id_product_attribute'] . '\'
                                        AND `id_cart` = \'' . (int)$cart->id . '\'
                            ');
                        }
                    }
                }
            }
        }
    }


    /**
     * Display Admin
     */
    public function getContent()
    {
        if (Tools::getValue('campaign_limit')) {
            Configuration::updateValue('SUGGEST_PRODUCT_CAMPAIGN_LIMIT', Tools::getValue('campaign_limit'));
        }
        $this->_html = '';
        if (version_compare(_PS_VERSION_, '1.6', '>')) {
            $this->_html .= '
                <style>
                    .nobootstrap {
                        min-width: 1px;
                        width: auto;
                    }
                </style>
            ';
        }
        // If before 1.5
        if (self::$version_15 === false) {
            $this->_html .= '
                            <link rel="stylesheet" type="text/css" href="../css/jquery.autocomplete.css" />
                            <script type="text/javascript" src="../js/jquery/jquery.autocomplete.js"></script>';
        }

        $this->_html .= '<link rel="stylesheet" type="text/css" href="' . $this->_path . '/views/css/admin.css" />';
        $this->_html .= $this->displayBann();

        $this->_html .= '<h2>' . $this->l('Suggest product with statistics') . '</h2>';
        $this->_html .= $this->tabs(true);
        // Disable
        if (Tools::getValue('enabled') && Tools::getValue('btnSubmit') === false) {
            $this->includeClass();
            $totSuggest = new TotSuggest(Tools::getValue('enabled'));
            $totSuggest->enabled = true;
            if ($totSuggest->update()) {
                Tools::redirectAdmin($this->link . '&conf=4');
            }

        }
        // Enable
        if (Tools::getValue('disabled') && Tools::getValue('btnSubmit') === false) {
            $this->includeClass();
            $totSuggest = new TotSuggest(Tools::getValue('disabled'));
            $totSuggest->enabled = false;
            if ($totSuggest->update()) {
                Tools::redirectAdmin($this->link . '&conf=4');
            }

        }
        // Delete
        if (Tools::getValue('deleteCampain') && Tools::getValue('btnSubmit') === false) {
            $this->includeClass();
            $totSuggest = new TotSuggest(Tools::getValue('deleteCampain'));
            $totSuggest->enabled = false;
            $totSuggest->visible = false;
            $totSuggest->date_end = strftime('%Y-%m-%d %H:%M:%S');
            $totSuggest->save();
        }
        // Delete
        if (Tools::getValue('deleteRule') && Tools::getValue('btnSubmit') === false) {
            $this->includeClass();
            $totRules = new TotRules(Tools::getValue('deleteRule'));
            $totRules->enabled = false;
            if ($totRules->delete()) {
                Tools::redirectAdmin($this->link . '&conf=2' . '&tottab=rules');
            }

        }
        // Add
        if (Tools::getValue('add') === false && Tools::getValue('edit') === false) {
            // Switch to tab
            if (Tools::getValue('tottab') == 'stats') {
                $this->_html .= $this->stats();
            } elseif (Tools::getValue('tottab') == 'rules') {
                $this->_html .= $this->rules();
            } elseif (Tools::getValue('tottab') == 'campaigns' || Tools::getValue('tottab') === false) {
                $this->_html .= $this->campaigns();
            }

        } elseif (Tools::getValue('add') == 'Campaign') {
            $this->_html .= $this->displayFormCampaign();
        } elseif (Tools::getValue('add') == 'Rule') {
            $this->_html .= $this->displayFormRule();
        } elseif (Tools::getvalue('edit') == 'Campaign') {
            $this->_html .= $this->displayFormCampaign();
        } elseif (Tools::getvalue('edit') == 'Rule') {
            $this->_html .= $this->displayFormRule();
        }

        $this->_html .= $this->tabs(false);

        return $this->_html;
    }


    ################################################################
    ## Method
    ################################################################

    /**
     * Get Campaigns depending on the product
     */

    private function getCampaign()
    {
        if (Tools::getValue('totstatsCampaign')) {
            return Tools::getValue('totstatsCampaign');
        } else {
            return (int)$this->getCampaignOfSuggestedProduct();
        }
    }

    /**
     * Check if last product added by customer has been clicked on as a suggest product and return campaign id
     */
    private function getCampaignOfSuggestedProduct()
    {
        $cart = $this->context->cart;

        $hasbeenclicked = false;

        $product = $cart->getLastProduct();
        $query = 'SELECT * FROM ' . _DB_PREFIX_ . 'totsuggestproduct_has_been_seen
                            WHERE id_cart = \'' . (int)$cart->id . '\'
                            AND id_product=\'' . (int)$product['id_product'] . '\'';
        $res = Db::getInstance()->ExecuteS($query);
        if (count($res) != 0) {
            $hasbeenclicked = $res[0]['id_campaign'];
        }

        return $hasbeenclicked;
    }

    private function displayBann()
    {
        $_html = '
            <div style="border: 1px solid #dfd5c3;margin-bottom:10px;">
            <table style="width:100%;line-height:16px;min-height: 86px;">
            <tr>
            <td style="padding: 11px 0px;max-width: 40%;">
            <div style="height:32px;float: left;padding:0 11px;">
            <img src="' . $this->_path . 'logo.png" alt="" />
            </div>
            <div style="padding-left: 78px;">
            <span style="font-family: Arial;font-weight: bold;color: #669900;font-size: 14px;width: 413px;height: 28px;text-align: left;vertical-align: bottom;text-decoration: none;">' . $this->displayName . '</span>
            <br />
            <span style="font-size: 12px;text-align: left;vertical-align: bottom;">' . $this->description . '
            </span>
            </div>
            </td>
            <td style="max-width: 40%;">
            ' . $this->l('By') . ' <a href="http://www.202-ecommerce.com/" target="_blank" style="font-family: Arial;font-weight: bold;color: #669900;font-size: 14px;width: 413px;height: 28px;text-align: left;vertical-align: bottom;text-decoration: none;">202-ecommerce</a>
            <br /> ' . $this->l('Web agency specialized in ecommerce web sites') . '
            </td>
 
            </tr>
            </table>
            </div>
        ';

        return $_html;
    }

    /**
     * Check Prestashop version
     */
    private function checkVersion()
    {
        if (version_compare(_PS_VERSION_, '1.5', '>')) {
            self::$version_15 = true;
        }

    }

    /**
     * Tabs
     * @param boolean
     * @return string
     */
    private function tabs($begin)
    {
        if ($begin) {
            $html = '
                <ul id="tabs">
                    <li>
                       <a ' . ((Tools::getValue('tottab') == 'campaigns' && Tools::getValue('add') === false) || (Tools::getValue('tottab') === false && Tools::getValue('add') === false && Tools::getValue('edit') === false) ? 'class="sousmenuactif"' : '') . ' href="' . $this->link . '&tottab=campaigns">' . $this->l('Campaigns') . '</a>
                    </li>
                    <li>
                       <a ' . ((Tools::getValue('tottab') == 'rules' && Tools::getValue('add') === false) ? 'class="sousmenuactif"' : '') . ' href="' . $this->link . '&tottab=rules">' . $this->l('Rules') . '</a>
                    </li>
                    <li>
                       <a ' . ((Tools::getValue('tottab') == 'stats' && Tools::getValue('add') === false) ? 'class="sousmenuactif"' : '') . ' href="' . $this->link . '&tottab=stats">' . $this->l('Statistics') . '</a>
                    </li>';
            if (Tools::getValue('add') !== false || Tools::getValue('edit') !== false) {
                $html .= '
                    <li>
                        <a href="#" class="sousmenuactif">
                            ' . $this->l('Form') . '
                        </a>
                </li>';
            }

            $html .= '
                </ul>
            <div id="contentTabs">';
        } else {
            $html = '</div>'; //End of contentTabs;
        }

        return $html;
    }

    /**
     * Tabs Stats
     * @global Object $cookie
     * @global Object $smarty
     * @return Smarty template
     */
    private function stats()
    {
        include_once dirname(__FILE__) . '/inc/totstatisticssuggestproduct.php';

        $cookie = $this->context->cookie;
        $smarty = $this->context->smarty;

        $campaignName = ((Tools::getValue('campaignName') && Tools::getValue('campaignName') != '') ? Tools::getValue('campaignName') : null);
        $date_begin = ((Tools::getValue('date_begin') && Tools::getValue('date_begin') != '') ? Tools::getValue('date_begin') : Configuration::get('TOTSUGGESTPRODUCT_INSTALL'));
        $date_end = ((Tools::getValue('date_end') && Tools::getValue('date_end') != '') ? Tools::getValue('date_end') : date('Y-m-d', strtotime('1 day')));
        $totStats = new TotStatisticSuggestProduct('totsuggestproduct', Configuration::get('TOTSUGGESTPRODUCT_INSTALL'));
        if ($campaignName != null) {
            $dates = $totStats->getDatePromo($campaignName);
            $date_begin = $dates[0]['date_begin'];
            $date_end = ($dates[0]['date_end'] == '0000-00-00 00:00:00' ? $date_end : $dates[0]['date_end']);
        }
        $percentage = $totStats->getPercentage($date_begin, $date_end, $campaignName);
        $increaseSales = $totStats->getIncreaseSalesRate($date_begin, $date_end, $campaignName);
        $specificInformations = $totStats->getInfoSpecific($date_begin, $date_end);
        if (self::$version_15 === false) {
            $ids = array('date_begin_totsuggest', 'date_end_totsuggest');
            $smarty->assign('datepicker', $ids);
        } else {
            $this->context->controller->addJqueryUI('ui.datepicker');
            $smarty->assign('datepicker', 'show_version15');
        }

        $data = array(
            'v15'           => self::$version_15,
            'multishop'     => Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE'),
            'campaigns'     => $specificInformations,
            'path'          => $this->_path,
            'increaseSales' => $increaseSales,
            'percentage'    => $percentage,
            'cookie'        => $cookie,
            'name'          => $this->name,
            'date_end'      => $date_end,
            'date_begin'    => $date_begin
        );

        $smarty->assign($data);

        return $this->display(dirname(__FILE__), '/views/templates/hook/Stats.tpl');
    }

    /**
     * Form
     * @version 1.0
     * @global String current index
     * @global object Cookie
     * @return string
     */
    private function campaigns()
    {
        $cookie = $this->context->cookie;
        $smarty = $this->context->smarty;

        if (self::$version_15 === true && Shop::getContext() == Shop::CONTEXT_SHOP) {
            $shop = Shop::getShop(Shop::getContextShopID());
            $nameShop = $shop['name'];
        } elseif (self::$version_15 === true && Shop::getContext() == Shop::CONTEXT_GROUP) {
            $nameShop = $this->l('Group');
        } else {
            $nameShop = $this->l('All Shops');
        }

        $data = array(
            'v15'             => self::$version_15,
            'defaultLanguage' => (int)(Configuration::get('PS_LANG_DEFAULT')),
            'multishop'       => Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE'),
            'path'            => $this->_path,
            'cookie'          => $cookie,
            'linkHome'        => $this->link,
            'name'            => $this->name,
            'campaigns'       => $this->displayCampaign(),
            'noCampaigns'     => (count($smarty->getTemplateVars('campaigns')) == 0 ? $this->l('No campaign for the moment') : ''),
            'campaign_limit'  => Configuration::get('SUGGEST_PRODUCT_CAMPAIGN_LIMIT')
        );

        $smarty->assign($data);

        return $this->display(dirname(__FILE__), '/views/templates/hook/Campaigns.tpl');
    }

    private function updateOrderStats($cart)
    {
        $rq_CartSuggest = 'SELECT * FROM `' . _DB_PREFIX_ . 'totsuggestproduct_stats` WHERE `id_cart` = \'' . (int)$cart->id . '\' AND `date_add_to_cart` > `date_delete_from_cart` AND date_ordered = \'0000-00-00 00:00:00\'';
        $CartSuggest = Db::getInstance()->ExecuteS($rq_CartSuggest);
        foreach ($CartSuggest as $ProductSuggest) {
            $rq_update = '
                UPDATE `' . _DB_PREFIX_ . 'totsuggestproduct_stats` SET `date_ordered` = \'' . pSQL(date('Y-m-d H:i:s')) . '\'
                WHERE `id_cart` = \'' . (int)$cart->id . '\'
                         AND `id_product` = \'' . (int)$ProductSuggest['id_product'] . '\'
                         AND `id_product_attribute` = \'' . (int)$ProductSuggest['id_product_attribute'] . '\'';
            Db::getInstance()->Execute($rq_update);
        }
    }

    /**
     * Display Rules
     * @global String Current Index
     * @global Object Cookie
     * @global Object Smarty
     * @return Smarty template
     */
    private function rules()
    {
        $this->includeClass();

        $cookie = $this->context->cookie;
        $smarty = $this->context->smarty;

        $data = array(
            'v15'      => self::$version_15,
            'path'     => $this->_path,
            'linkHome' => $this->link,
            'name'     => $this->name,
            'cookie'   => $cookie,
            'rules'    => TotRules::getRules(),
            'noRules'  => (count($smarty->getTemplateVars('rules')) == 0 ? $this->l('No rule for the moment') : '')
        );

        $smarty->assign($data);

        return $this->display(dirname(__FILE__), '/views/templates/hook/Rules.tpl');
    }

    /**
     * Display campaign
     * @global Object Smarty
     * @return Array
     */
    private function displayCampaign()
    {
        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;

        // Query
        $sql = '
                 SELECT ts.*, tsl.`justification`, tcr.`name` FROM `' . _DB_PREFIX_ . 'totsuggestproduct` AS ts
                            LEFT OUTER JOIN `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule` AS tcr
                                     ON tcr.`id_totsuggestproduct_cart_rule` = ts.`id_totsuggestproduct_cart_rule`
                            LEFT OUTER JOIN `' . _DB_PREFIX_ . 'totsuggestproduct_lang` AS tsl
                                     ON tsl.`id_totsuggestproduct` = ts.`id_totsuggestproduct` AND tsl.`id_lang` = \'' . (int)$cookie->id_lang . '\'
                 WHERE ts.`visible` = 1 ';
        $values = Db::getInstance()->ExecuteS($sql);

        if (is_array($values) && count($values)) {
            // Test version
            if (self::$version_15 === true) {
                if (Shop::getContext() == Shop::CONTEXT_SHOP) {
                    $shop = Shop::getShop(Shop::getContextShopID());
                    $id_cat_default = $shop['id_category'];
                } else {
                    $id_cat_default = 1;
                }

                if (Shop::getContext() == Shop::CONTEXT_SHOP) {
                    // Query
                    $sql .= ' ' . Shop::addSqlRestriction() . ' OR (`id_shop` = 0 AND `id_shop_group` = 0)';
                }
            } else {
                $id_cat_default = 1;
            }

            $var = null;
            foreach ($values as $key => $value) {
                $values[$key]['product'] = new Product($value['id_product'], false, Configuration::get('PS_LANG_DEFAULT'));
            }

            $smarty->assign('languages', $var);
        }

        return $values;
    }

    /**
     * Form Campaign
     * @global Object $smarty
     * @return Smarty Template
     */
    private function displayFormCampaign()
    {
        $this->includeClass();

        $html = null;

        if (Tools::getValue('btnSubmit')) {
            if ($this->processCampaign() === true) {
                Tools::redirectAdmin($this->link . '&conf=' . (Tools::getValue('edit') === false ? '3' : '4'));
            } else {
                $html .= $this->form;
            }
        }

        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;

        $languages = Language::getLanguages(false); // On rÃ©cupÃ¨re les langues
        $defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT')); // Langue par dÃ©faut

        if (Tools::getValue('edit') && Tools::getValue('id')) {
            $totSuggest = new TotSuggest((int)Tools::getValue('id'));
            $product = new Product($totSuggest->id_product, false, $cookie->id_lang);
            $totSuggest->productName = $product->id . ' - ' . $product->name;

            $smarty->assign('campaign', $totSuggest);
        }

        $data = array(
            'path'            => $this->_path,
            'linkHome'        => $this->link,
            'langs'           => $languages,
            'defaultLanguage' => $defaultLanguage,
            'flags'           => $this->displayFlags($languages, $defaultLanguage, 'justification', 'justification' . '', true),
            'rules'           => TotRules::getRules()
        );

        $smarty->assign($data);

        return $html . $this->display(dirname(__FILE__), '/views/templates/hook/FormCampaign.tpl');
    }

    /**
     * Form Campaign
     * @global Object $smarty
     * @return Smarty Template
     */
    private function displayFormRule()
    {
        $this->includeClass();
        $html = null;

        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;

        if (Tools::getValue('btnSubmit') !== false) {
            if ($this->processFormRule()) {
                Tools::redirectAdmin($this->link . '&conf=' . (Tools::getValue('edit') === false ? '3&add=Campaign' : '4&tottab=rules'));
            } else {
                $html .= $this->form;
            }
        }

        $id_cat = (self::$version_15 === true ? (Shop::isFeatureActive() ? Configuration::get('PS_HOME_CATEGORY') : Configuration::get('PS_ROOT_CATEGORY')) : 1);
        $cat = new Category($id_cat); // On commence l'arborescense au niveau de la div accueil
        $arborescence = $cat->recurseLiteCategTree(1000); // Arborescence

        $groups = array();

        $totRules = new TotRules(Tools::getValue('id'));
        if (Tools::getValue('edit') && Tools::getValue('id')) {
            $groups = $totRules->getGroups();
        }

        $SQL = 'SELECT MAX(`id_totsuggestproduct_cart_rule_group`) FROM `' . _DB_PREFIX_ . 'totsuggestproduct_cart_rule_group` ';
        $newGroup = DB::getInstance()->getValue($SQL);
        $data = array(
            'groups'        => Tools::getValue('group') !== false ? Tools::getValue('group') : $groups,
            'rule'          => $totRules,
            'path'          => $this->_path,
            'linkHome'      => $this->link,
            'newGroup'      => $newGroup,
            'manufacturers' => Manufacturer::getManufacturers(),
            'attributes'    => Attribute::getAttributes($cookie->id_lang),
            'suppliers'     => Supplier::getSuppliers(),
            'categories'    => $this->displayCategorySelect($arborescence['children'], '')
        );

        $smarty->assign($data);

        return $html . $this->display(dirname(__FILE__), '/views/templates/hook/FormRule.tpl');
    }

    /**
     * Display category select
     * @param type $child
     * @param type $prefix
     * @return type
     */
    private function displayCategorySelect($child, $prefix)
    {
        $cats = array();
        $lang = (int)Configuration::get('PS_LANG_DEFAULT');
        foreach ($child as $category) {
            if (is_array($category['name'])) {
                $catname = $category['name'][$lang];
            } else {
                $catname = $category['name'];
            }

            $cats[] = array(
                'id'   => $category['id'],
                'name' => $prefix . ' ' . $catname
            );

            if (count($category['children']) >= 1) {
                $cats = array_merge($cats, $this->displayCategorySelect($category['children'], $prefix . '---'));
            }
        }

        return $cats;
    }

    /**
     * Inclue les différentes class
     */
    private function includeClass()
    {
        $dir = self::$version_15 === true ? '1.5/' : '1.4/';

        include_once dirname(__FILE__) . '/inc/' . $dir . 'totSuggest.php';
        include_once dirname(__FILE__) . '/inc/' . $dir . 'totRules.php';
        include_once dirname(__FILE__) . '/inc/' . $dir . 'totRulesGroup.php';
        include_once dirname(__FILE__) . '/inc/' . $dir . 'totRulesValue.php';
        include_once dirname(__FILE__) . '/inc/' . $dir . 'totRulesCountry.php';
    }

    /**
     * Process rule
     * @return boolean
     */
    private function processFormRule()
    {
        if (Tools::getValue('name')) {
            $totRules = new TotRules(Tools::getValue('id'));
            $totRules->name = Tools::getValue('name');
            // Save rule
            if (Tools::getValue('name') && $totRules->save() === true) {
                $del = false;

                // Groups
                $groups = Tools::getValue('group');

                if ($groups && count($groups)) {
                    foreach ($groups as $key => $group) {
                        // Delete all at the first time of while
                        if ($del === false) {
                            if ($totRules->deleteGroups() === false) {
                                $this->form = parent::displayError($this->l('Error since delete older group'));

                                return false;
                            }
                            $del = true;
                        }

                        // Create new group
                        $groupRules = new TotRulesGroup();

                        $groupRules->id_totsuggestproduct_cart_rule = $totRules->id;
                        $groupRules->rules_validated = isset($group['rules_validated']) ? $group['rules_validated'] : 0;
                        // Save new group

                        if ($groupRules->save() === false) {
                            $totRules->delete();
                            $this->form = parent::displayError($this->l('Check your form'));

                            return false;
                        }

                        // If rule product
                        if (isset($group['product']) && !empty($group['product'])) {
                            $products = explode(',', $group['product']);
                            // If some product
                            foreach ($products as $product) {
                                if (is_numeric($product)) {
                                    $totRulesValue = new TotRulesValue();
                                    $totRulesValue->id_totsuggestproduct_cart_rule_group = $groupRules->id;
                                    $totRulesValue->type = 'product';
                                    $totRulesValue->value = $product;
                                    $totRulesValue->save();
                                }
                            }
                        }
                        // If rule product_attribute
                        if (isset($group['product_attribute']) && !empty($group['product_attribute']) && count($group['product_attribute'])) {
                            foreach ($group['product_attribute'] as $attribute) {
                                if (is_numeric($attribute)) {
                                    $totRulesValue = new TotRulesValue();
                                    $totRulesValue->id_totsuggestproduct_cart_rule_group = $groupRules->id;
                                    $totRulesValue->type = 'product_attribute';
                                    $totRulesValue->value = $attribute;
                                    $totRulesValue->save();
                                }
                            }
                        }
                        // If rule category
                        if (isset($group['category']) && !empty($group['category']) && count($group['category'])) {
                            // If some category
                            foreach ($group['category'] as $category) {
                                if (is_numeric($category)) {
                                    $totRulesValue = new TotRulesValue();
                                    $totRulesValue->id_totsuggestproduct_cart_rule_group = $groupRules->id;
                                    $totRulesValue->type = 'category';
                                    $totRulesValue->value = $category;
                                    $totRulesValue->save();
                                }
                            }
                        }
                        // If rule supplier
                        if (isset($group['supplier']) && !empty($group['supplier']) && count($group['supplier'])) {
                            // If some supplier
                            foreach ($group['supplier'] as $supplier) {
                                if (is_numeric($supplier)) {
                                    $totRulesValue = new TotRulesValue();
                                    $totRulesValue->id_totsuggestproduct_cart_rule_group = $groupRules->id;
                                    $totRulesValue->type = 'supplier';
                                    $totRulesValue->value = $supplier;
                                    $totRulesValue->save();
                                }
                            }
                        }
                        // If rule manufacturer
                        if (isset($group['manufacturer']) && !empty($group['manufacturer']) && count($group['manufacturer'])) {
                            // If some manufacturer
                            foreach ($group['manufacturer'] as $manufacturer) {
                                if (is_numeric($manufacturer)) {
                                    $totRulesValue = new TotRulesValue();
                                    $totRulesValue->id_totsuggestproduct_cart_rule_group = $groupRules->id;
                                    $totRulesValue->type = 'manufacturer';
                                    $totRulesValue->value = $manufacturer;
                                    $totRulesValue->save();
                                }
                            }
                        }
                    }
                } else {
                    $totRules->deleteGroups();
                }

                return true;

            } else {
                $this->form = parent::displayError($this->l('Check your form'));
            }
        } else {
            $this->form = parent::displayError($this->l('Check your form'));
        }

        return false;
    }

    /**
     * Process campagin
     * @return boolean
     */
    private function processCampaign()
    {
        $this->form = null;
        $totSuggest = new TotSuggest(Tools::getValue('id'));
        $totSuggest->id_totsuggestproduct_cart_rule = Tools::getValue('rule');
        $totSuggest->id_product = Tools::getValue('product');
        $totSuggest->date_begin = date('Y-m-d H:i:s');
        $totSuggest->campaignName = Tools::getValue('campaign');
        $totSuggest->justification = Tools::getValue('justification');
        $totSuggest->enabled = (boolean)Tools::getValue('enabled') == 'on' ? true : false;
        if (self::$version_15 === true) {
            $totSuggest->id_shop = Shop::getContextShopID();
            $totSuggest->id_shop_group = Shop::getContextShopGroupID();
        }

        if (Tools::getValue('rule') && Tools::getValue('product') && Tools::getValue('campaign') && $totSuggest->save()) {
            return true;
        } else {
            $this->form = parent::displayError($this->l('Please check your form'));

            return false;
        }
    }
}
