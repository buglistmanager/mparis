<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (file_exists('../../../../config/config.inc.php')) {
    include('../../../../config/config.inc.php');
}


class TotStatisticCore
{

    public $moduleName;
    public $installModuleDate;

    public function __construct($pModuleName, $pInstallModuleDate)
    {
        $this->moduleName = $pModuleName;
        $this->installModuleDate = $pInstallModuleDate;
        $this->promotionalCodes = $this->getPromotionalCodes();
    }



    public function getGlobalStatistics()
    {
        $globalInformation = $this->getTotalProductsModule();
        $data = array();
        $data['totalRevenue'] = $this->calculateTotalRevenue($globalInformation);
        $data['count'] = $this->countProducts($globalInformation);
        return $data;
    }

    /***************************
    ******** SALES RATE ********
    ***************************/

    public function getIncreaseSalesRate($date_begin, $date_end = null, $campaignName = null)
    {
        $data = array();
        $totalSales = $this->getTotalSales($date_begin, $date_end, $campaignName);
        $totalSales202Products = $this->getTotalSales202Products($date_begin, $date_end, $campaignName);
        $data['totSales'] = $totalSales;
        $data['totSales202'] = $totalSales202Products;
        if ($data['totSales'] - $data['totSales202'] != 0) {
            $data['increase'] = $data['totSales202'] / ($data['totSales'] - $data['totSales202']) * 100;
        } else {
            $data['increase'] = 0;
        }

        return $data;
    }

    private function getTotalSales($date_begin, $date_end, $campaignName, $product = null)
    {
        $sqlOrdersSince = 'SELECT SUM(total_paid) as sum 
                            FROM '._DB_PREFIX_.'orders 
                            WHERE date_add > \''.$date_begin.'\'
                                AND date_add < \''.$date_end.'\'';

        if ($date_end != null) {
            $sqlOrdersSince .= 'AND invoice_date < \''.$date_end.'\'';
        }
        $val = Db::getInstance()->ExecuteS($sqlOrdersSince);
        return $val[0]['sum'];
    }

    public function getTotalSalesProduct($date_begin, $date_end, $product)
    {
        $sqlProducts = 'SELECT * FROM '._DB_PREFIX_.'orders AS o
                            INNER JOIN '._DB_PREFIX_.'order_detail AS od ON o.id_order = od.id_order
                            WHERE o.date_add >= \''.$date_begin.'\'
                                AND o.date_add <= \''.$date_end.'\'
                                AND od.product_id = \''.(int)$product.'\'';

        $products = Db::getInstance()->ExecuteS($sqlProducts);
        $salesProducts = 0;
        foreach ($products as $product) {
            $salesProducts += $product['product_price'] + ($product['product_price'] * ($product['tax_rate'] / 100));
            if ($product['reduction_percent'] > 0) {
                $salesProducts -= $product['reduction_percent'] / 100 * $product['product_price'];
            }
            if ($product['reduction_amount'] > 0) {
                $salesProducts -= $product['reduction_amount'];
            }
        }
        return $salesProducts;
    }

    public function getTotalSales202Products($date_begin, $date_end, $id_campaign = null, $product = null)
    {
        $sql202Products = 'SELECT * FROM '._DB_PREFIX_.$this->moduleName.'_stats AS stats
                            INNER JOIN '._DB_PREFIX_.'orders AS o ON stats.id_cart = o.id_cart
                            INNER JOIN '._DB_PREFIX_.'order_detail AS od ON o.id_order = od.id_order
                                AND stats.id_product = od.product_id
                                AND stats.id_product_attribute = od.product_attribute_id
                            WHERE stats.date_ordered >= \''.$date_begin.'\'';
        if ($id_campaign != null) {
            $sql202Products .= ' AND stats.id_campaign = \''.$id_campaign.'\'';
        }
        if ($product != null) {
            $sql202Products .= ' AND stats.id_product = \''.(int)$product.'\'';
        }
        $products = Db::getInstance()->ExecuteS($sql202Products);
        $sales202Products = 0;
        foreach ($products as $product) {
            $sales202Products += $product['product_price'] + ($product['product_price'] * ($product['tax_rate'] / 100));
            if ($product['reduction_percent'] > 0) {
                $sales202Products -= $product['reduction_percent'] / 100 * $product['product_price'];
            }
            if ($product['reduction_amount'] > 0) {
                $sales202Products -= $product['reduction_amount'];
            }
        }
        return $sales202Products;
    }

    /************************************************************************
    * Return all the products sold thanks to the module without date period *
    *************************************************************************/

    public function getInfoProducts($date_begin = null, $date_end = null, $campaignName = null)
    {
        $cookie = $this->context->cookie;
        $products = $this->getTotalProductsModuleForPeriod($date_begin, $date_end, $campaignName);
        $return = '<table class="table tableDnD"><tr><th>Product</th><th>Quantity</th><th>Promotional code</th>';

        foreach ($products as $product) {
            $productObj = new Product($product['id_product'], true, $cookie->id_lang);
            $return .= '<tr>';
            $return .= '<td>'.$productObj->name.'</td>';
            $return .= '<td>'.$product['somme'].'</td>';
            $return .= '<td>'.$product['campaignName'].'</td>';
            $return .= '</tr>';
        }
        $return .= '</table>';
        return $return;
    }

    /*
    * Return the number of products sold thanks to the module
    */
    private function getTotalProductsModuleForPeriod($date_begin = null, $date_end = null, $campaignName = null)
    {
        $sqlGlobalProductSell = 'SELECT *, COUNT(*) AS somme FROM '._DB_PREFIX_.$this->moduleName.'_stats AS stats
                                WHERE 1 ';

        if ($date_begin != null) {
            $sqlGlobalProductSell .= 'AND stats.date_ordered >= \''.pSQL($date_begin).'\' ';
        }

        if ($date_end != null) {
            $sqlGlobalProductSell .= 'AND stats.date_add_to_cart <= \''.pSQL($date_end).'\' ';
        }

        if ($campaignName != null) {
            $sqlGlobalProductSell .= 'AND stats.campaignName = \''.$campaignName.'\' ';
        }

        $sqlGlobalProductSell .= 'GROUP BY stats.id_product, id_product_attribute, campaignName';
        return Db::getInstance()->ExecuteS($sqlGlobalProductSell);
    }



    public function getPercentage($date_begin = null, $date_end = null, $promo = null)
    {
        $displayed = $this->getDisplayedOffers($date_begin, $date_end, $promo);
        $bought = $this->getBoughtOffers($date_begin, $date_end, $promo);
        $percentage = ($displayed > 0) ? $bought * 100 / $displayed : 0;

        return array(
            'displayed' => $displayed,
            'bought' => $bought,
            'percentage' => $percentage
        );
    }

    /*
    * $array must have product_quantity, id_product
    */
    private function calculateTotalRevenue($array)
    {
        $totalRevenue = 0;
        foreach ($array as $oneProduct) {
            $totalRevenue += Product::getPriceStatic(
                $oneProduct['id_product'],
                true,
                $oneProduct['id_product_attribute']
            ) * $oneProduct['product_quantity'];
        }

        return $totalRevenue;
    }

    private function countProducts($array)
    {
        return count($array);
    }



    /*
        FUNCTION TO OVERRIDE
    */

    public function getDisplayedOffers($date_begin = null, $date_end = null, $campaignName = null)
    {
    }

    public function getBoughtOffers($date_begin = null, $date_end = null, $campaignName = null)
    {
    }

    public function getPromotionalCodes()
    {
    }

    public function getDatePromo($campaignName)
    {
    }

    public function getInfoSpecific($date_begin, $date_end)
    {
    }
}
