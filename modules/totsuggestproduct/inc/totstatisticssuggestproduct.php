<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (file_exists('../../../../config/config.inc.php')) {
    include('../../../../config/config.inc.php');
}

if (file_exists(dirname(__FILE__).'/totstatisticsclass.php')) {
    require_once(dirname(__FILE__).'/totstatisticsclass.php');
}


class TotStatisticSuggestProduct extends TotStatisticCore
{

    public function __construct($pModuleName, $pInstallModuleDate)
    {
        parent::__construct($pModuleName, $pInstallModuleDate);
    }

    public function getPromotionalCodes()
    {
        $sqlCodes = 'SELECT * FROM '._DB_PREFIX_.$this->moduleName.'';
        return Db::getInstance()->ExecuteS($sqlCodes);
    }

    public function getDisplayedOffers($date_begin = null, $date_end = null, $campaignName = null)
    {
        if ($campaignName != null) {
            $sql = 'SELECT COUNT(*) AS count FROM '._DB_PREFIX_.'totsuggestproduct_stats WHERE id_campaign = \''.$campaignName.'\'';
        } else {
            $dateBeginSQL = ($date_begin == null) ? pSQL(Configuration::get('TOTSUGGESTPRODUCT_INSTALL')) : pSQL($date_begin);
            $dateEndSQL = ($date_end == null) ? pSQL(date('Y-m-d H:i:s', strtotime('1 day'))) : pSQL($date_end);
            $sql = 'SELECT COUNT(*) AS count FROM '._DB_PREFIX_.'totsuggestproduct_stats WHERE displayed > \''.$dateBeginSQL.'\' AND displayed < \''.$dateEndSQL.'\'';
        }

        $displayedOffers = DB::getInstance()->ExecuteS($sql);
        return $displayedOffers[0]['count'];
    }

    public function getBoughtOffers($date_begin = null, $date_end = null, $campaignName = null)
    {
        $dateBeginSQL = ($date_begin == null) ? pSQL(Configuration::get('TOTSUGGESTPRODUCT_INSTALL')) : pSQL($date_begin);
        $dateEndSQL = ($date_end == null) ? ' AND 1 ' : " AND stats.date_ordered <= '".pSQL($date_end)."' ";
        $campaignNameSQL = ($campaignName == null) ? '' : "AND stats.campaignName = '".$campaignName."'";

        $sqlBoughtOffers = 'SELECT COUNT(*) AS count FROM '._DB_PREFIX_.'totsuggestproduct_stats AS stats
                                WHERE 1 
                                AND stats.date_ordered >= \''.$dateBeginSQL.'\' 
                                '.$dateEndSQL.'
                                '.$campaignNameSQL.'';

        $boughtOffers = DB::getInstance()->ExecuteS($sqlBoughtOffers);

        return $boughtOffers[0]['count'];
    }

    public function getDatePromo($campaignName)
    {
        $sqlDates = 'SELECT * FROM '._DB_PREFIX_.$this->moduleName." WHERE campaignName = '".$campaignName."'";
        return Db::getInstance()->ExecuteS($sqlDates);
    }

    public function getInfoSpecific($date_begin, $date_end)
    {
        $allCampaignsSQL = '
                              SELECT * 
                              FROM `'._DB_PREFIX_.$this->moduleName.'` AS ts
                                   INNER JOIN `'._DB_PREFIX_.$this->moduleName.'_cart_rule` AS tsr
                                        ON tsr.`id_totsuggestproduct_cart_rule` = ts.`id_totsuggestproduct_cart_rule`
                              WHERE (`date_begin` < \''.$date_end.'\' AND `date_end` > \''.$date_begin.'\') OR (`date_begin` < \''.$date_end.'\' AND `date_end` = \'0000-00-00 00:00:00\')
                              ORDER BY `id_totsuggestproduct` ';
        $allCampaign = Db::getInstance()->ExecuteS($allCampaignsSQL);
        $campaignsInfo = array();

        foreach ($allCampaign as $campaign) {

            $displayedSQL = 'SELECT COUNT(*) AS displayed FROM '._DB_PREFIX_.$this->moduleName.'_stats WHERE id_campaign = \''.$campaign['id_totsuggestproduct'].'\' and displayed BETWEEN \''.$date_begin.'\' AND \''.$date_end.'\'';
            $displayed = Db::getInstance()->getValue($displayedSQL);

            $boughtSQL = 'SELECT COUNT(*) AS bought FROM '._DB_PREFIX_.$this->moduleName.'_stats WHERE id_campaign = \''.$campaign['id_totsuggestproduct'].'\' and date_ordered BETWEEN \''.$date_begin.'\' AND \''.$date_end.'\'';
            $bought = Db::getInstance()->getValue($boughtSQL);

            $addedToCartSQL = 'SELECT COUNT(*) AS added FROM '._DB_PREFIX_.$this->moduleName.'_stats WHERE id_campaign = \''.$campaign['id_totsuggestproduct'].'\' AND date_add_to_cart != \'0000-00-00 00:00:00\'';
            $addedToCart = Db::getInstance()->getValue($addedToCartSQL);

            $conversion = ($displayed != 0) ? ($bought / $displayed * 100) : 0;
            $conversion = number_format($conversion, 2).'%';
                    ////////////////////////
                    $categoryName = null;
/*
            $category = new Category($campaign['id_cat']);
            $categoryName = $category->getName(Configuration::get('PS_LANG_DEFAULT'));
*/
            $product = new Product($campaign['id_product'], false, Configuration::get('PS_LANG_DEFAULT'));

            $sales = $this->getTotalSales202Products($campaign['date_begin'], '', $campaign['id_totsuggestproduct'], $campaign['id_product']);

            $dateEnd = ($campaign['date_end'] == '0000-00-00 00:00:00') ? date('Y-m-d', strtotime('1 day')) : $campaign['date_end'];
            $salesProduct = $this->getTotalSalesProduct($campaign['date_begin'], $dateEnd, $campaign['id_product']);

            $salesPercentage = number_format((($salesProduct == 0) ? 0 : $sales / $salesProduct) * 100, 2).'%';

            $campaignsInfo[] = array_merge(
                $campaign,
                array(
                    'displayed' => $displayed,
                    'bought' => $bought,
                    'added' => $addedToCart,
                    'conversion' => $conversion,
                    'sales' => number_format($sales, 2),
                    'salesPercentage' => $salesPercentage,
                    'category' => $categoryName,
                    'product' => $product->name
                )
            );
        }

        return $campaignsInfo;
    }
}
