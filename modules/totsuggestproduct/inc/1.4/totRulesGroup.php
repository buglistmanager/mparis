<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

class TotRulesGroup extends ObjectModel
{

    public $id_totsuggestproduct_cart_rule_group;
    public $id_totsuggestproduct_cart_rule;
    public $rules_validated;
    protected $table = 'totsuggestproduct_cart_rule_group';
    protected $identifier = 'id_totsuggestproduct_cart_rule_group';
    protected $fieldsRequired = array(
        'id_totsuggestproduct_cart_rule',
        'rules_validated'
    );
    protected $fieldsValidate = array(
        'id_totsuggestproduct_cart_rule' => 'isUnsignedId',
        'rules_validated' => 'isInt'
    );

    public function __construct($id = false)
    {
        parent::__construct($id);
    }

    public function getFields()
    {
        parent::validateFields();
        $fields = array();
        if ($this->id > 0) {
            $fields['id_totsuggestproduct_cart_rule_group'] = (int)($this->id);
        }
        $fields['rules_validated'] = pSQL($this->rules_validated);
        $fields['id_totsuggestproduct_cart_rule'] = (int)$this->id_totsuggestproduct_cart_rule;
        return $fields;
    }

    public function deleteGroups()
    {
        $SQL = '
               DELETE 
               FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group`
                    WHERE `id_totsuggestproduct_cart_rule` = \''.(int)$this->id.'\'';
        return DB::getInstance()->execute($SQL);
    }
}
