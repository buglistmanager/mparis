<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */
class TotRulesValue extends ObjectModel
{

    public $id_totsuggestproduct_cart_rule_value;
    public $id_totsuggestproduct_cart_rule_group;
    public $type;
    public $value;
    protected $table = 'totsuggestproduct_cart_rule_value';
    protected $identifier = 'id_totsuggestproduct_cart_rule_value';
    protected $fieldsRequired = array(
        'id_totsuggestproduct_cart_rule_group',
        'type',
        'value'
    );
    protected $fieldsValidate = array(
        'id_totsuggestproduct_cart_rule_group' => 'isUnsignedId',
        'type' => 'isString',
        'value' => 'isInt'
    );

    public function getFields()
    {
        parent::validateFields();
        $fields = array();
        if ($this->id > 0) {
            $fields['id_totsuggestproduct_cart_rule_value'] = (int)($this->id);
        }
        $fields['type'] = pSQL($this->type);
        $fields['value'] = (int)$this->value;
        $fields['id_totsuggestproduct_cart_rule_group'] = (int)$this->id_totsuggestproduct_cart_rule_group;
        return $fields;
    }
    public function __construct($id = false, $id_lang = false)
    {
        parent::__construct($id, $id_lang);
    }
}
