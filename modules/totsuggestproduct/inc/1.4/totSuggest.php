<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */
class TotSuggest extends ObjectModel
{

    public $id_totsuggestproduct;
    public $id_totsuggestproduct_cart_rule;
    public $id_product;
    public $checked;
    public $campaignName;
    public $enabled = true;
    public $visible = true;
    public $justification;
    public $date_begin;
    public $date_end;
    public $id_shop;
    public $id_shop_group;
    protected $table = 'totsuggestproduct';
    protected $identifier = 'id_totsuggestproduct';
    protected $fieldsRequired = array(
        'id_totsuggestproduct_cart_rule',
        'id_product',
        'campaignName'
    );
    protected $fieldsValidateLang = array(
        'justification' => 'isString'
    );
    protected $fieldsValidate = array(
        'id_totsuggestproduct_cart_rule' => 'isUnsignedId',
        'id_product' => 'isUnsignedId',
        'campaignName' => 'isString',
        'enabled' => 'isBool',
        'visible' => 'isBool',
        'id_shop' => 'isNullOrUnsignedId',
        'id_shop_group' => 'isNullOrUnsignedId'
    );

    public function getFields()
    {
        parent::validateFields();
        $fields = array();
        if ($this->id > 0) {
            $fields['id_totsuggestproduct'] = (int)($this->id);
        }
        $fields['campaignName'] = pSQL($this->campaignName);
        $fields['id_totsuggestproduct_cart_rule'] = (int)$this->id_totsuggestproduct_cart_rule;
        $fields['id_product'] = (int)$this->id_product;
        $fields['enabled'] = (bool)$this->enabled;
        $fields['visible'] = (int)$this->visible;
        $fields['id_shop'] = (int)$this->id_shop;
        $fields['id_shop_group'] = (int)$this->id_shop_group;
        $fields['date_begin'] = $this->date_begin;
        $fields['date_end'] = $this->date_end;
        return $fields;
    }

    public function getTranslationsFieldsChild()
    {
        parent::validateFieldsLang();

        $fieldsArray = array('justification');
        $fields = array();
        $languages = Language::getLanguages(false);
        $defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
        foreach ($languages as $language) {
            $fields[$language['id_lang']]['id_lang'] = (int)($language['id_lang']);
            $fields[$language['id_lang']][$this->identifier] = (int)($this->id);
            foreach ($fieldsArray as $field) {
                if (!Validate::isTableOrIdentifier($field)) {
                    die(Tools::displayError());
                }
                if (isset($this->{$field}[$language['id_lang']]) && !empty($this->{$field}[$language['id_lang']])) {
                    $fields[$language['id_lang']][$field] = pSQL($this->{$field}[$language['id_lang']], true);
                } elseif (in_array($field, $this->fieldsRequiredLang)) {
                    $fields[$language['id_lang']][$field] = pSQL($this->{$field}[$defaultLanguage], true);
                } else {
                    $fields[$language['id_lang']][$field] = '';
                }
            }
        }
        return $fields;

    }


    public function __construct($id = null, $id_lang = null)
    {
        parent::__construct($id, $id_lang);
    }
}
