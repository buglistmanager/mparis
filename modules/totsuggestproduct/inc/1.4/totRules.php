<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */
class TotRules extends ObjectModel
{

    public $id_totsuggestproduct_cart_rule;
    public $name;

    static public $version_15 = false;
    protected $table = 'totsuggestproduct_cart_rule';
    protected $identifier = 'id_totsuggestproduct_cart_rule';
    protected $fieldsRequired = array(
        'name'
    );
    protected $fieldsValidate = array(
        'name' => 'isString'
    );

    public function __construct($id = false)
    {
        parent::__construct($id);
    }


    public function getFields()
    {
        parent::validateFields();
        $fields = array();
        if ($this->id > 0) {
            $fields['id_totsuggestproduct_cart_rule'] = (int)($this->id);
        }
        $fields['name'] = pSQL($this->name);
        return $fields;
    }

    /**
     * Return all rules
     * @return type
     */
    public static function getRules()
    {
        $SQL = '
            SELECT * 
            FROM  `'._DB_PREFIX_.'totsuggestproduct_cart_rule` 
            ORDER BY `name` ASC ';

        return DB::getInstance()->executeS($SQL);
    }

    /**
     * Delete Rule, Groups and Value
     * @return type
     */
    public function delete()
    {
        return $this->deleteGroups() && parent::delete();
    }

    /**
     * Delete Rule, Groups and Value
     * @return boolean
     */
    public function deleteGroups()
    {
        $SQLValue = '
            DELETE 
            FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_value`
                WHERE `id_totsuggestproduct_cart_rule_group` IN (
                    SELECT `id_totsuggestproduct_cart_rule_group`
                    FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group`
                        WHERE `id_totsuggestproduct_cart_rule` = \''.(int)$this->id.'\'
                ) ';
        if (DB::getInstance()->execute($SQLValue) === false) {
            return false;
        }

        $SQLGroup = '
            DELETE 
            FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group`
                WHERE `id_totsuggestproduct_cart_rule` = \''.(int)$this->id.'\'
            ';
        if (DB::getInstance()->execute($SQLGroup) === false) {
            return false;
        }

        return true;
    }

    public function getGroups()
    {
        $cookie = $this->context->cookie;

        $SQL = '
            SELECT `id_totsuggestproduct_cart_rule_group`, `rules_validated`
            FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group`
                WHERE `id_totsuggestproduct_cart_rule` = \''.(int)$this->id_totsuggestproduct_cart_rule.'\'';

        $grps = DB::getInstance()->executeS($SQL);
        // Instance of group
        $groups = array();
        // If group exists
        if ($grps && count($grps)) {
            // While group
            foreach ($grps as $grp) {
                $groups[$grp['id_totsuggestproduct_cart_rule_group']] = array(
                    'rules_validated' => $grp['rules_validated']
                );
            }

            // Get Value
            $SQLValues = '
                SELECT tcv.`value`, tcv.`type`, tcg.`id_totsuggestproduct_cart_rule_group`
                FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_value` AS tcv
                    INNER JOIN `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group` AS tcg
                        ON tcv.`id_totsuggestproduct_cart_rule_group` = tcg.`id_totsuggestproduct_cart_rule_group`
                        AND `id_totsuggestproduct_cart_rule` = \''.(int)$this->id_totsuggestproduct_cart_rule.'\'
                ';
            $vals = DB::getInstance()->executeS($SQLValues);
            foreach ($vals as $val) {
                $groups[$val['id_totsuggestproduct_cart_rule_group']][$val['type']][] = $val['value'];
            }

            foreach ($groups as $key => $group) {
                if (isset($groups[$key]['product'])) {
                    $groups[$key]['product'] = implode(',', $group['product']);
                    foreach ($group['product'] as $product) {
                        if (!isset($groups[$key]['product_name'])) {
                            $groups[$key]['product_name'] = null;
                        }

                        $groups[$key]['product_name'] .= ' <div class="product'.$product.'">'.$product.' - '.
                            $this->getProductName($product, false, $cookie->id_lang).
                            '<span onclick="delGift_new($(this), '.$product.');" style="cursor: pointer;">
                            <img src="../img/admin/delete.gif" /></span></div>';
                    }
                    $groups[$key]['product_name'] = urlencode($groups[$key]['product_name']);
                } else {
                    $groups[$key]['product'] = null;
                    $groups[$key]['product_name'] = null;
                }
            }
                return $groups;
        } else {
            return array();
        }

    }

    public static function checkValidity()
    {
        $context = Context::getContext();
        $cookie = $context->cookie;
        $cart = $context->cart;

        $rules = array();
        $suggestedProducts = array();

        //$this->includeClass();
        $products = $cart->getProducts();
        $prods = array();
        foreach ($products as $prod) {
            $prods[] = $prod['id_product'];
        }

        foreach ($products as $product) {
            list($id_product, $id_product_attribute, $id_category, $id_supplier, $id_manufacturer) = array(
                    $product['id_product'],
                    $product['id_product_attribute'],
                    $product['id_category_default'],
                    $product['id_supplier'],
                    $product['id_manufacturer']
                );

            $SQL = '
                SELECT 
                    COUNT(v.`value`) AS total,
                    g.`id_totsuggestproduct_cart_rule_group`,
                    s.*,
                    g.`rules_validated`, 
                    (
                        SELECT COUNT(`id_totsuggestproduct_cart_rule_group`) 
                        FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group` AS cg
                        WHERE cg.`id_totsuggestproduct_cart_rule` = r.`id_totsuggestproduct_cart_rule`
                    ) AS `nbGroup`
                FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_value` AS v
                INNER JOIN `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group` AS g
                    ON g.`id_totsuggestproduct_cart_rule_group` = v.`id_totsuggestproduct_cart_rule_group`
                INNER JOIN `'._DB_PREFIX_.'totsuggestproduct_cart_rule` AS r
                    ON r.`id_totsuggestproduct_cart_rule` = g.`id_totsuggestproduct_cart_rule`
                INNER JOIN `'._DB_PREFIX_.'totsuggestproduct` AS s
                    ON s.`id_totsuggestproduct_cart_rule` = r.`id_totsuggestproduct_cart_rule` 
                    AND s.`enabled` = 1 AND s.`id_product` NOT IN ('.implode(',', $prods).')
                WHERE 
                    (v.`value` = \''.(int)$id_product.'\' AND v.`type` = \'product\')
                    OR (v.`value` IN (
                        SELECT c.`id_attribute` 
                        FROM `'._DB_PREFIX_.'product_attribute_combination` AS c
                        WHERE c.`id_product_attribute` = \''.$id_product_attribute.'\' AND c.`id_attribute` = v.`value`
                            AND v.`type` = \'product_attribute\'
                        )
                    )     
                    OR (v.`value` IN (
                        SELECT id_category 
                        FROM `'._DB_PREFIX_.'category_product` 
                        WHERE id_product = \''.(int)$id_product.'\'
                    ) AND v.`type` = \'category\')
                    OR (v.`value` = \''.(int)$id_supplier.'\' AND v.`type` = \'supplier\')
                    OR (v.`value` = \''.(int)$id_manufacturer.'\' AND v.`type` = \'manufacturer\')
                GROUP BY g.`id_totsuggestproduct_cart_rule_group`, s.`id_totsuggestproduct` ';

            // Get all groups with validated rules
            $groups = DB::getInstance()->executeS($SQL);

            foreach ($groups as $group) {
                if ($group['total'] >= $group['rules_validated']) {
                    if (!isset($rules[$group['id_totsuggestproduct']])) {
                        $rules[$group['id_totsuggestproduct']] = array();
                    }

                    $rules[$group['id_totsuggestproduct']]['id_totsuggestproduct'] = $group['id_totsuggestproduct'];
                    $rules[$group['id_totsuggestproduct']]['id_product'] = $group['id_product'];
                    $rules[$group['id_totsuggestproduct']]['nbGroup'] = $group['nbGroup'];
                    if (!isset($rules[$group['id_totsuggestproduct']]['groups'][$group['id_totsuggestproduct_cart_rule_group']])) {
                        $rules[$group['id_totsuggestproduct']]['groups'][$group['id_totsuggestproduct_cart_rule_group']] = 0;
                    }

                    $rules[$group['id_totsuggestproduct']]['groups'][$group['id_totsuggestproduct_cart_rule_group']]++;
                }
            }
        }

        foreach ($rules as $k => $rule) {
            if ($rule['nbGroup'] != count($rule['groups'])) {
                unset($rules[$k]);
            }

        }

        // Default value
        $sizeImage = 'home';

        // If is in under 1.5
        if (version_compare(_PS_VERSION_, '1.5', '>=')) {
            // If 1.6
            $prefix_img_type = (version_compare(_PS_VERSION_, '1.6', '<') ? 'home' : 'medium');

            if (ImageType::getByNameNType($prefix_img_type.'_default')) {
                $sizeImage = $prefix_img_type.'_default';
            } elseif (ImageType::getByNameNType($prefix_img_type)) {
                $sizeImage = $prefix_img_type;
            }
        }

        foreach ($rules as $k => $rule) {
            $MySQLQuery = '
                SELECT i.`id_image`
                FROM '._DB_PREFIX_.'image AS i 
                WHERE i.`id_product` = "'.(int)$rule['id_product'].'" AND i.`cover` = 1 ';
            $id_image = DB::getInstance()->getValue($MySQLQuery);
            $product = new Product($rule['id_product'], true, $cookie->id_lang);
            $totSuggest = new TotSuggest($k, $cookie->id_lang);
            $suggestedProducts[] = array(
                'product'      => $product,
                'allow_oosp'   => $product->out_of_stock,
                'image'        => $product->id.'-'.$id_image,
                'suggest'      => array(
                                    'id'   => $totSuggest->id,
                                    'text' => $totSuggest->justification
                                ),
                'static_token' => Tools::getToken(false),
                'tax_enabled'  => Configuration::get('PS_TAX'),
                'sizeImage'    => $sizeImage
            );
        }

        foreach ($suggestedProducts as $product) {
            $sql = 'SELECT COUNT(*) FROM '._DB_PREFIX_."totsuggestproduct_stats 
            WHERE id_product = '".$product['product']->id."' AND id_cart = '".$cart->id."'";
            $count = DB::getInstance()->getValue($sql);
            if ($count > 0) {
                $sql = 'UPDATE '._DB_PREFIX_."totsuggestproduct_stats 
                SET displayed = '".pSQL(date('Y-m-d H:i:s'))."', id_campaign = '".pSQL($product['suggest']['id'])."' 
                WHERE id_product = '".$product['product']->id."' AND id_cart = '".$cart->id."'";
            } else {
                $sql = 'INSERT INTO '._DB_PREFIX_."totsuggestproduct_stats (id_product, id_cart, displayed, id_campaign) 
                VALUES('".$product['product']->id."',
                    '".$cart->id."',
                    '".pSQL(date('Y-m-d H:i:s'))."',
                    '".$product['suggest']['id']."')";
            }
            Db::getInstance()->Execute($sql);
        }

        return $suggestedProducts;
    }

    private function getProductName($id_product, $id_product_attribute, $id_lang)
    {
        $product = new Product($id_product, true, $id_lang);
        return $product->name;
    }
}
