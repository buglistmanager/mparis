<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
*
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
*
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */

class TotRulesCountry extends ObjectModel
{

    public $id_totsuggestproduct_cart_rule;
    public $id_country;

    protected $table = 'totsuggestproduct_cart_country';
    protected $identifier = 'id_totsuggestproduct_cart_country';
    protected $fieldsRequired = array(
        'id_totsuggestproduct_cart_rule',
        'id_country'
    );
    protected $fieldsValidate = array(
        'id_totsuggestproduct_cart_rule' => 'isUnsignedId',
        'id_country' => 'isInt'
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function getFields()
    {
        parent::validateFields();
        $fields = array();
        if ($this->id > 0) {
            $fields['id_totsuggestproduct_cart_country'] = (int)($this->id);
        }
        $fields['id_country'] = pSQL($this->id_country);
        $fields['id_totsuggestproduct_cart_rule'] = (int)$this->id_totsuggestproduct_cart_rule;
        return $fields;
    }
}
