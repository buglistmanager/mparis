<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */
class TotRulesValue extends ObjectModel
{

    public $id_totsuggestproduct_cart_rule_value;
    public $id_totsuggestproduct_cart_rule_group;
    public $type;
    public $value;

    public static $definition = array(
            'table' => 'totsuggestproduct_cart_rule_value',
            'primary' => 'id_totsuggestproduct_cart_rule_value',
            'fields' => array(
                'id_totsuggestproduct_cart_rule_group' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
                'type' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
                'value' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true)
        )
    );

    public function __construct($id = false, $id_lang = false)
    {
        parent::__construct($id, $id_lang);
    }
}
