<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */
class TotRulesGroup extends ObjectModel
{

    public $id_totsuggestproduct_cart_rule_group;
    public $id_totsuggestproduct_cart_rule;
    public $rules_validated;
    public static $definition = array(
            'table' => 'totsuggestproduct_cart_rule_group',
            'primary' => 'id_totsuggestproduct_cart_rule_group',
            'fields' => array(
                'id_totsuggestproduct_cart_rule' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
                'rules_validated' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true)
        )
    );

    public function __construct($id = false)
    {
        parent::__construct($id);
    }

    public function deleteGroups()
    {
        $SQL = '
               DELETE 
               FROM `'._DB_PREFIX_.'totsuggestproduct_cart_rule_group`
                    WHERE `id_totsuggestproduct_cart_rule` = \''.(int)$this->id.'\' ';
        return DB::getInstance()->execute($SQL);
    }
}
