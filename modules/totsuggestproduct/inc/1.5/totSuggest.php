<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */
class TotSuggest extends ObjectModel
{

    public $id_totsuggestproduct;
    public $id_totsuggestproduct_cart_rule;
    public $id_product;
    public $checked;
    public $campaignName;
    public $enabled = true;
    public $visible = true;
    public $justification;
    public $date_begin;
    public $date_end = '0000-00-00 00:00:00';
    public $id_shop;
    public $id_shop_group;
    public static $definition = array(
            'table' => 'totsuggestproduct',
            'primary' => 'id_totsuggestproduct',
            'multilang' => true,
            'fields' => array(
                'id_totsuggestproduct_cart_rule' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
                'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
                'campaignName' => array('type' => self::TYPE_STRING, 'validate' => 'isString','required' => true),
                'justification' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString'),
                'enabled' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                'visible' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                'date_begin' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
                'date_end' => array('type' => self::TYPE_DATE),
                'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
                'id_shop_group' => array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId')
        )
    );

    public function __construct($id = null, $id_lang = null)
    {
        parent::__construct($id, $id_lang);
    }
}
