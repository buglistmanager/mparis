<?php
/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/

if (!defined('_PS_VERSION_')) {
    die(header('HTTP/1.0 404 Not Found'));
}

/**
 * Description of totRules
 *
 * @author Guillaume Deloince
 */
class TotRulesCountry extends ObjectModel
{

    public $id_totsuggestproduct_cart_rule;
    public $id_country;

    public static $definition = array(
            'table' => 'totsuggestproduct_cart_country',
            'primary' => 'id_totsuggestproduct_cart_country',
            'fields' => array(
                'id_totsuggestproduct_cart_rule' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
                'id_country' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true)
        )
    );

    public function __construct()
    {
        parent::__construct();
    }
}
