{*
* The tree for the module totSuggestProduct
* By 202-ecommerce
* Created : 02-06-2012
*}
<tr>
     <td style="text-align:left;width:280px;padding:0 10px;"><input type="checkbox" name="suggest[{$node.id}][checked]" {if !empty($suggest[$node.id]['checked'])}checked{/if} />{$prefix}{$node.name}</td>
     <td style="height:30px;width:280px;padding:0 10px;"">
          <div id="choix{$node.id}" {if !empty($suggest[$node.id]['id_product']) && !empty($suggest[$node.id]['name_product'])}style="display:block;"{/if}>{if !empty($suggest[$node.id]['id_product']) && !empty($suggest[$node.id]['name_product'])}{$suggest[$node.id]['id_product']} - {$suggest[$node.id]['name_product']}<span onclick="delGift{$node.id}({$suggest[$node.id]['id_product']})"><img src="../img/admin/delete.gif" /></span>{/if}</div>
          <div id="choose{$node.id}"{if !empty($suggest[$node.id]['id_product']) && !empty($suggest[$node.id]['name_product'])}style="display:none;"{/if}>
               <input type="hidden" name="suggest[{$node.id}][id_product]" id="produit{$node.id}" value="{if !empty($suggest[$node.id]['id_product']) && !empty($suggest[$node.id]['name_product'])}{$suggest[$node.id]['id_product']}{/if}" />
               <input type="text" name="suggest[{$node.id}][name]" id="id{$node.id}" value="{if !empty($suggest[$node.id]['id_product']) && !empty($suggest[$node.id]['name_product'])}{$suggest[$node.id]['name_product']}{/if}" />
               <img onclick="javascript:addGift{$node.id}();" style="cursor: pointer;" src="../img/admin/add.gif" alt="produit{$node.id}" title="" />
          </div>
          <script type="text/javascript">
               $(function(){
                    $("#id{$node.id}").autocomplete("ajax_products_list.php", {
                    delay: 100,
                    minChars: 1,
                    autoFill: true,
                    max:20,
                    matchContains: true,
                    mustMatch:true,
                    scroll:false,
                    cacheLength:0,
                    multipleSeparator:"||",
                    formatItem: function(item) {
                         return item[0];
                    }
                    }).result(function(event, item){
                         $("#produit{$node.id}").val(item[1]);
                    });
               });

                function addGift{$node.id}(){
                    if( $("#produit{$node.id}").val() != "" ){
                        var lineDisplay = $("#produit{$node.id}").val() + " - " + $("#id{$node.id}").val();
                        $("#choix{$node.id}").html(lineDisplay + '<span onclick="delGift{$node.id}(' + $('#produit{$node.id}').val() + ');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span>');
                        $("#choix{$node.id}").slideDown();
                        $("#choose{$node.id}").slideUp();
                    }
                }
                
                function delGift{$node.id}(){
                    $("#produit{$node.id}").val("");
                    $("#id{$node.id}").val("");
                    $("#choix{$node.id}").html("");
                    $("#choix{$node.id}").slideUp();
                    $("#choose{$node.id}").slideDown();
               }
          </script>
     </td>
     <td style="width:300px;"><input type="text" name="suggest[{$node.id}][texte]" value="{if !empty($suggest[$node.id]['text'])}{$suggest[$node.id]['text']}{else}" placeholder="On rentre le texte qui s'affichera pour : {$node.name}{/if}" style="width:250px;" /></td>
</tr>
{assign var=prefix value="&nbsp;&nbsp;&nbsp;&nbsp;"|cat:$prefix}
{foreach from=$node.children item=child}
     {include file=$arbre node=$child prefix=$prefix}
{/foreach}