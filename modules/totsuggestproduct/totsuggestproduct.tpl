{** The hookCart of module totSuggesProduct* By 202-ecommerce* Created : 02-06-2012*}
{if $suggestedProducts}
<p class="cart_navigation">
	{if !$opc}<a href="{$link->getPageLink('order.php', true)}?step=1{if $back}&amp;back={$back}{/if}" class="exclusive" title="{l s='Next' mod='totsuggestproduct'}">{l s='Next' mod='totsuggestproduct'} &raquo;</a>{/if}
	<a href="{if (isset($smarty.server.HTTP_REFERER) && strstr($smarty.server.HTTP_REFERER, $link->getPageLink('order.php'))) || !isset($smarty.server.HTTP_REFERER)}{$link->getPageLink('index.php')}{else}{$smarty.server.HTTP_REFERER|escape:'htmlall':'UTF-8'|secureReferrer}{/if}" class="button_large" title="{l s='Continue shopping' mod='totsuggestproduct'}">&laquo; {l s='Continue shopping' mod='totsuggestproduct'}</a>
</p>
{/if}
<div class="clear" style="clear:both;height: 20px;"></div>
{assign var="i" value=0}
{foreach from=$suggestedProducts key=test item=Product}
     <div id="primary_block" {if $i%2 != 0}style="margin-right:0;"{/if}>
          <form id="buy_block" action="{$link->getPageLink('cart.php')}" method="post">
               <input type="hidden" name="token" value="{$Product.static_token}">
               <input type="hidden" name="id_product" value="{$Product.product->id}" id="product_page_product_id">
               <input type="hidden" name="add" value="1">
               <input type="hidden" name="id_product_attribute" id="idCombination" value="{$Product.product->getDefaultAttribute($Product.product->id)}">
               <input type="hidden" name="quantity_wanted" id="quantity_wanted" value="1">

               <input type="hidden" name="totstats" value="totsuggestProduct"/>
               <input type="hidden" name="totstatsCampaign" value="{$Product.suggest.id}"/>

               <!-- Module totsuggestproduct-->
               <table class="totsuggestproductcart">
                    <tr>
                         <td colspan="3" class="argu">{if !empty($Product.suggest.text)}{$Product.suggest.text}{/if}</td>
                    </tr>
                    <tr>
                         <td><a href="{$Product.product->getLink()}"><img src="{$link->getImageLink($Product.product->link_rewrite, $Product.image, $Product.sizeImage)}" alt="" /></a></td>
                         <td><b><a href="{$Product.product->getLink()}">{$Product.product->name}</a></b><br /><a href="{$Product.product->getLink()}">{$Product.product->description_short}</a>
                              <div class="infosCart">
                                   {if !$priceDisplay || $priceDisplay == 2} 
                                        {assign var='productPrice' value=$Product.product->getPrice(true, $smarty.const.NULL, 2)} 
                                        {assign var='productPriceWithoutRedution' value=$Product.product->getPriceWithoutReduct(false, $smarty.const.NULL)}   
                                   {elseif $priceDisplay == 1}   
                                        {assign var='productPrice' value=$Product.product->getPrice(false, $smarty.const.NULL, 2)}  
                                        {assign var='productPriceWithoutRedution' value=$Product.product->getPriceWithoutReduct(true, $smarty.const.NULL)}  
                                   {/if}   
                                   {if $Product.product->specificPrice AND $Product.product->specificPrice.reduction AND $productPriceWithoutRedution > $productPrice}                  
                                        <p>
                                             <span class="discount">      
                                                  {l s='Reduced price!' mod='totsuggestproduct'}<br />    
                                             </span>
                                        </p>
                                   {/if}
                                   <p class="price">
                                        {convertPrice price=$productPrice}
                                        {if $Product.product->specificPrice AND $Product.product->specificPrice.reduction AND $productPriceWithoutRedution > $productPrice}
                                             {if $Product.tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) OR !isset($display_tax_label))} 
                                                  {if $priceDisplay == 1}       
                                                       {l s='tax excl.' mod='totsuggestproduct'} 
                                                  {else}          
                                                       {l s='tax incl.' mod='totsuggestproduct'}     
                                                  {/if}                      
                                             {/if}        
                                        {/if}        
                                   </p>        
                                   {if $Product.product->specificPrice AND $Product.product->specificPrice.reduction}     
                                        <p id="old_price">      
                                             <span class="bold">   
                                                  {if $priceDisplay >= 0 && $priceDisplay <= 2}       
                                                       {if $productPriceWithoutRedution > $productPrice}
                                                            <span id="old_price_display">       
                                                                 {convertPrice price=$productPriceWithoutRedution}   
                                                            </span>                                     
                                                            {if $Product.tax_enabled && $display_tax_label == 1}    
                                                                 {if $priceDisplay == 1}        
                                                                      {l s='tax excl.' mod='totsuggestproduct'}   
                                                                 {else}                            
                                                                      {l s='tax incl.' mod='totsuggestproduct'}     
                                                                 {/if}                   
                                                            {/if}            
                                                       {/if}                         
                                                  {/if}               
                                             </span>  
                                        </p>               
                                   {/if}     
                              </div>
                              {*
                              <div class="buttonCart">
                                   <p{if (!$Product.allow_oosp && $Product.product->quantity <= 0) OR !$Product.product->available_for_order OR (isset($restricted_country_mode) AND $restricted_country_mode) OR $PS_CATALOG_MODE} style="display: none;"{/if} id="add_to_cart" class="buttons_bottom_block"><input type="submit" name="Submit" value="&nbsp;" class="exclusive" /></p>
                              </div>*}
                         </td>    
                    </tr>  

               </table>
          </form><!-- /Module totsuggestproduct-->
     </div>
     {math equation="$i + 1" assign="i"}
{/foreach}
<div class="clear"></div>