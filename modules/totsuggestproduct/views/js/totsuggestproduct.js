/**
* NOTICE OF LICENSE
*
* This source file is subject to a commercial license from 202 ecommerce
* Use, copy, modification or distribution of this source file without written
* license agreement from 202 ecommerce is strictly forbidden.
* 
* @author    202 ecommerce <contact@202-ecommerce.com>
* @copyright Copyright (c) 202 ecommerce 2014
* @license   Commercial license
* 
* Support <support@202-ecommerce.com>
*/
if(typeof(ajaxCart) != "undefined")
{
     ajaxCart.add = function(idProduct, idCombination, addedFromProductPage, callerElement, quantity, whishlist){
          if (addedFromProductPage && !checkCustomizations())
          {
               alert(fieldRequired);
               return ;
          }
          emptyCustomizations();
          //disabled the button when adding to not double add if user double click
          if (addedFromProductPage)
          {
               $('#add_to_cart input').attr('disabled', true).removeClass('exclusive').addClass('exclusive_disabled');
               $('.filled').removeClass('filled');
          }
          else
               $(callerElement).attr('disabled', true);

          if ($('#cart_block_list').hasClass('collapsed'))
               this.expand();

          if ($('.totsuggestproductcart_14').length) {
               var baseUri = baseDir + 'cart.php';
          }

          //send the ajax request to the server
          $.ajax({
               type: 'POST',
               headers: { "cache-control": "no-cache" },
               url: baseUri + '?rand=' + new Date().getTime(),
               async: true,
               cache: false,
               dataType : "json",
               data: 'controller=cart&add=1&ajax=true&qty=' + ((quantity && quantity != null) ? quantity : '1') + '&id_product=' + idProduct + '&token=' + static_token + ( (parseInt(idCombination) && idCombination != null) ? '&ipa=' + parseInt(idCombination): ''),
               success: function(jsonData,textStatus,jqXHR)
               {
                    // add appliance to whishlist module
                    if (whishlist && !jsonData.errors)
                         WishlistAddProductCart(whishlist[0], idProduct, idCombination, whishlist[1]);

                    // add the picture to the cart
                    var $element = $(callerElement).parent().parent().find('a.product_image img,a.product_img_link img');
                    if (!$element.length)
                         $element = $('#bigpic');
                    var $picture = $element.clone();
                    var pictureOffsetOriginal = $element.offset();

                    if ($picture.size())
                         $picture.css({'position': 'absolute', 'top': pictureOffsetOriginal.top, 'left': pictureOffsetOriginal.left});

                    var pictureOffset = $picture.offset();
                    if ($('#cart_block')[0] && $('#cart_block').offset().top && $('#cart_block').offset().left)
                         var cartBlockOffset = $('#cart_block').offset();
                    else
                         var cartBlockOffset = $('#shopping_cart').offset();

                    // Check if the block cart is activated for the animation
                    if (cartBlockOffset != undefined && $picture.size())
                    {
                         $picture.appendTo('body');
                         $picture.css({ 'position': 'absolute', 'top': $picture.css('top'), 'left': $picture.css('left'), 'z-index': 4242 })
                         .animate({ 'width': $element.attr('width')*0.66, 'height': $element.attr('height')*0.66, 'opacity': 0.2, 'top': cartBlockOffset.top + 30, 'left': cartBlockOffset.left + 15 }, 1000)
                         .fadeOut(100, function() {
                              ajaxCart.updateCartInformation(jsonData, addedFromProductPage);
                         });
                    }
                    else
                         ajaxCart.updateCartInformation(jsonData, addedFromProductPage);

                    window.location.reload();
               },
               error: function(XMLHttpRequest, textStatus, errorThrown)
               {
                    alert("Impossible to add the product to the cart.\n\ntextStatus: '" + textStatus + "'\nerrorThrown: '" + errorThrown + "'\nresponseText:\n" + XMLHttpRequest.responseText);
                    //reactive the button when adding has finished
                    if (addedFromProductPage)
                         $('#add_to_cart input').removeAttr('disabled').addClass('exclusive').removeClass('exclusive_disabled');
                    else
                         $(callerElement).removeAttr('disabled');
               }
          });
     }


     ajaxCart.overrideButtonsInThePage = function(){
          //for every 'add' buttons...
          $('.ajax_add_to_cart_button').unbind('click').click(function(){
               var idProduct =  $(this).attr('rel').replace('nofollow', '').replace('ajax_id_product_', '');
               if ($(this).attr('disabled') != 'disabled')
                    ajaxCart.add(idProduct, null, false, this);
               return false;
          });
          //for product page 'add' button...
          $('#add_to_cart input').unbind('click').click(function(){
               ajaxCart.add( $('#product_page_product_id').val(), $('#idCombination').val(), true, null, $('#quantity_wanted').val(), null);
               return false;
          });

          //for product page 'add' button...
          $('.totsuggestproductcart_14 #add_to_cart input, .totsuggestproductcart_15 #add_to_cart input').unbind('click').click(function(){
               ajaxCart.add( $(this).parents('#buy_block').find('input[name=id_product]').val(), $(this).parents('#buy_block').find('input[name=id_product_attribute]').val(), true, null,  $(this).parents('#buy_block').find('input[name=quantity_wanted]').val(), null);
               return false;
          });

          //for 'delete' buttons in the cart block...
          $('#cart_block_list .ajax_cart_block_remove_link').unbind('click').click(function(){
               // Customized product management
               var customizationId = 0;
               var productId = 0;
               var productAttributeId = 0;
               var customizableProductDiv = $($(this).parent().parent()).find("div[id^=deleteCustomizableProduct_]");

               if (customizableProductDiv && $(customizableProductDiv).length)
               {
                    $(customizableProductDiv).each(function(){
                         var ids = $(this).attr('id').split('_');
                         if (typeof(ids[1]) != 'undefined')
                         {
                              customizationId = parseInt(ids[1]);
                              productId = parseInt(ids[2]);
                              if (typeof(ids[3]) != 'undefined')
                                   productAttributeId = parseInt(ids[3]);
                              return false;
                         }
                    });
               }

               // Common product management
               if (!customizationId)
               {
                    //retrieve idProduct and idCombination from the displayed product in the block cart
                    var firstCut = $(this).parent().parent().attr('id').replace('cart_block_product_', '');
                    firstCut = firstCut.replace('deleteCustomizableProduct_', '');
                    ids = firstCut.split('_');
                    productId = parseInt(ids[0]);
                    if (typeof(ids[1]) != 'undefined')
                         productAttributeId = parseInt(ids[1]);
               }

               var idAddressDelivery = $(this).parent().parent().attr('id').match(/.*_\d+_\d+_(\d+)/)[1];

               // Removing product from the cart
               ajaxCart.remove(productId, productAttributeId, customizationId, idAddressDelivery);
               return false;
          });
     }

}
