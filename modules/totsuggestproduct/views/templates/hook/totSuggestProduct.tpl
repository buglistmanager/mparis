{** The hookCart of module totSuggesProduct* By 202-ecommerce* Created : 02-06-2012*}
<!-- Module totsuggestproduct-->
<ul class="clear totsuggestproductcart_{$version}"  id="product_list">
	
{foreach from=$suggestedProducts item=Product name="suggestcount"}
	{if $smarty.foreach.suggestcount.iteration <= $campaign_limit}
	<li class="clearfix">
		<form id="buy_block" action="{$link->getPageLink('cart.php')}" method="post">
			<input type="hidden" name="token" value="{$Product.static_token}">
			<input type="hidden" name="id_product" value="{$Product.product->id}" id="product_page_product_id">
			<input type="hidden" name="add" value="1">
			<input type="hidden" name="id_product_attribute" id="idCombination" value="{$Product.product->getDefaultAttribute($Product.product->id)}">
			<input type="hidden" name="quantity_wanted" id="quantity_wanted" value="1">

			<input type="hidden" name="totstats" value="totsuggestProduct"/>
			<input type="hidden" name="totstatsCampaign" value="{$Product.suggest.id}"/>
			
			<div class="center_block">
				{assign var='productlinkreal' value={$Product.product->getLink()}}
				{assign var='totproductlink' value={$productlinkreal}|cat:{$productEndUrl}|cat:{$Product.suggest.id}}
				<a href="{$totproductlink}" class="product_img_link">
					<img src="{$link->getImageLink($Product.product->link_rewrite, $Product.image, $Product.sizeImage)}" alt="" />
				</a>
				<div>{if !empty($Product.suggest.text)}{$Product.suggest.text}{/if}</div>
				<h3>
					<a href="{$totproductlink}">{$Product.product->name}</a>
				</h3>
				<p class="product_desc" style="display: none;">
					<a href="{$totproductlink}">{$Product.product->description_short}</a>{* |truncate:150:"...":true *}
				</p>
			</div>
			<div class="right_block">
				{if !$priceDisplay || $priceDisplay == 2} 
					{assign var='productPrice' value=$Product.product->getPrice(true, $smarty.const.NULL, 2)} 
					{assign var='productPriceWithoutRedution' value=$Product.product->getPriceWithoutReduct(false, $smarty.const.NULL)}   
				{elseif $priceDisplay == 1}   
					{assign var='productPrice' value=$Product.product->getPrice(false, $smarty.const.NULL, 2)}  
					{assign var='productPriceWithoutRedution' value=$Product.product->getPriceWithoutReduct(true, $smarty.const.NULL)}  
				{/if}   
				{if $Product.product->specificPrice AND $Product.product->specificPrice.reduction AND $productPriceWithoutRedution > $productPrice}                  
					<p>
						<span class="discount">      
							{l s='Reduced price!' mod='totsuggestproduct'}<br />    
						</span>
					</p>
				{/if}
				<p class="price">
					{convertPrice price=$productPrice}
					{if $Product.product->specificPrice AND $Product.product->specificPrice.reduction AND $productPriceWithoutRedution > $productPrice}
						{if $Product.tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) OR !isset($display_tax_label))} 
							{if $priceDisplay == 1}       
								{l s='tax excl.' mod='totsuggestproduct'} 
							{else}          
								{l s='tax incl.' mod='totsuggestproduct'}     
							{/if}                      
						{/if}        
					{/if}        
				</p>        
				{if $Product.product->specificPrice AND $Product.product->specificPrice.reduction}     
					<p id="old_price">      
						<span class="bold">   
							{if $priceDisplay >= 0 && $priceDisplay <= 2}       
								{if $productPriceWithoutRedution > $productPrice}
									<span id="old_price_display">       
										{convertPrice price=$productPriceWithoutRedution}   
									</span>                                     
									{if $Product.tax_enabled && $display_tax_label == 1}    
										{if $priceDisplay == 1}        
											{l s='tax excl.' mod='totsuggestproduct'}   
										{else}                            
											{l s='tax incl.' mod='totsuggestproduct'}     
										{/if}                   
									{/if}            
								{/if}                         
							{/if}               
						</span>  
					</p>               
				{/if}     
				{if ($Product.product->quantity >= 1 AND $Product.product->available_for_order AND !$PS_CATALOG_MODE AND !isset($restricted_country_mode)) OR (isset($Product.allow_oosp) AND $Product.allow_oosp)}   
					<p id="add_to_cart" class="buttons_bottom_block">       
						<input type="submit" name="Submit" value="{l s='Add to cart' mod='totsuggestproduct'}" class="exclusive">  
					</p>      
				{/if}   	
			</div>
		</form>
	</li>
	{/if}
{/foreach} 
</ul>
<!-- /Module totsuggestproduct-->