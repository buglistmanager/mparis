{*
* @author 202 ecommerce <contact@202-ecommerce.com>
* @copyright  Copyright (c) 202 ecommerce 2014
* @license    Commercial license
*}
{foreach from=$group.product_associated item=product_associated}
    <div class="product{$product_associated.product_id|escape:'htmlall':'UTF-8'}">
        {$product_associated.product_id|escape:'htmlall':'UTF-8'}
        - {$product_associated.product|escape:'htmlall':'UTF-8'}
        <span onclick="delGift_new($(this), {$product_associated.product_id|escape:'htmlall':'UTF-8'});"
              style="cursor: pointer;">
            <img src="../img/admin/delete.gif"/>
        </span>
    </div>
{/foreach}
