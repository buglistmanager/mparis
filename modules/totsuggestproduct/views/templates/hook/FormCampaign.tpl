{*
* @author 202 ecommerce <contact@202-ecommerce.com>
* @copyright  Copyright (c) 202 ecommerce 2014
* @license    Commercial license
*}
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='Add Campaign' mod='totsuggestproduct'}
    </legend>
    <form method="post" action="" class="form">
        <table class="table tableDnD">
            <thead>
            <tr>
                <th>{l s='Name' mod='totsuggestproduct'}</th>
                <th>{l s='Value' mod='totsuggestproduct'}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <label for="rule">{l s='Choose your rule' mod='totsuggestproduct'}</label>
                </td>
                <td>
                    <select name="rule" id="rule">
                        <option value="">==== {l s='CHOOSE' mod='totsuggestproduct'} ====</option>
                        {foreach $rules as $rule}
                            <option value="{$rule.id_totsuggestproduct_cart_rule|escape:'htmlall':'UTF-8'}"{if isset($campaign) && isset($campaign->id_totsuggestproduct_cart_rule) && $campaign->id_totsuggestproduct_cart_rule == $rule.id_totsuggestproduct_cart_rule} selected{/if}>{$rule.name|escape:'htmlall':'UTF-8'}</option>
                        {/foreach}
                    </select>
                    <a href="{$linkHome|escape:'htmlall':'UTF-8'}&add=Rule"><img style="cursor: pointer;"
                                                                                 src="../img/admin/add.gif"
                                                                                 alt="produit" title=""/></a>
                </td>
            </tr>
            <tr id="prod">
                <td>
                    <label for="product">{l s='Product' mod='totsuggestproduct'}</label>
                </td>
                <td>
                    <div id="choose"{if isset($campaign) && isset($campaign->productName)} style="display:block;"{/if}>
                        {if isset($campaign) && isset($campaign->productName)}
                            <div class="product-line-{$campaign->id_product|escape:'htmlall':'UTF-8'}">
                                {$campaign->productName|escape:'htmlall':'UTF-8'} <span
                                        onclick="delGift_new({$campaign->id_product|escape:'htmlall':'UTF-8'});"
                                        style="cursor: pointer;"><img src="../img/admin/delete.gif"/></span>
                            </div>
                        {/if}
                    </div>
                    <div id="choose_new"{if isset($campaign) && isset($campaign->productName)} style="display:none;"{/if}>
                        <input type="hidden" name="product" id="product_new"
                               value="{if isset($campaign) && isset($campaign->id_product)}{$campaign->id_product|escape:'htmlall':'UTF-8'}{/if}"/>
                        <input type="text" name="suggest_name" id="product"
                               placeholder="{l s='Name of your product' mod='totsuggestproduct'}" value="" size="50"/>
                        <img onclick="addGift_new();" style="cursor: pointer;" src="../img/admin/add.gif" alt="produit"
                             title=""/>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="justification">{l s='Justification' mod='totsuggestproduct'}</label>
                </td>
                <td>
                    {foreach $langs as $lang}
                        <div id="justification_{$lang.id_lang|escape:'htmlall':'UTF-8'}"
                             style="display:{if $lang.id_lang == $defaultLanguage}block{else}none{/if};float: left;">
                            <input type="text" name="justification[{$lang.id_lang|escape:'htmlall':'UTF-8'}]"
                                   id="justification" placeholder="{l s='Your justification' mod='totsuggestproduct'}"
                                   value="{if isset($campaign) && isset($campaign->justification) && isset($campaign->justification.{$lang.id_lang})}{$campaign->justification.{$lang.id_lang|escape:'htmlall':'UTF-8'}}{/if}"
                                   size="50"/>
                        </div>
                    {/foreach}
                    {$flags|escape:false}{* Validation: ModuleCore::displayFlags generate html code, so no html escape. *}
                </td>
            </tr>
            <tr>
                <td>
                    <label for="campaign">{l s='Campaign' mod='totsuggestproduct'}</label>
                </td>
                <td>
                    <input type="text" name="campaign" id="campaign"
                           placeholder="{l s='Name of your campaign' mod='totsuggestproduct'}"
                           value="{if isset($campaign) && isset($campaign->campaignName)}{$campaign->campaignName|escape:'htmlall':'UTF-8'}{/if}"
                           size="50"/>
                </td>
            </tr>
            <tr>
                <td>
                    <label id="enable">{l s='Enable' mod='totsuggestproduct'}</label>
                </td>
                <td>
                    <input type="checkbox" name="enabled"
                           id="enable"{if (isset($campaign) && isset($campaign->enabled) && $campaign->enabled == 1) || !isset($campaign)} checked{/if}/>
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2">
                    <input type="submit" name="btnSubmit" class="button"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</fieldset>
{if version_compare($smart.const_PS_VERSION_, '1.6', '<')}
    <link rel="stylesheet" type="text/css" href="../js/jquery/plugins/autocomplete/jquery.autocomplete.css"/>
    <script type="text/javascript" src="../js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>
{/if}
{literal}
    <script type="text/javascript">
        $(function () {
            $("#product").unbind('autocomplete').autocomplete("ajax_products_list.php", {
                delay: 100,
                minChars: 1,
                autoFill: true,
                max: 20,
                matchContains: true,
                mustMatch: true,
                scroll: false,
                cacheLength: 0,
                multipleSeparator: ",",
                formatItem: function (item) {
                    return item[1] + ' - ' + item[0];
                }
            }).result(function (event, item) {
                addGift_new(item);
                $(this).val('');
            });

            $("#product").setOptions({
                extraParams: {excludeIds: getGiftIds()}
            });
        });

        function getGiftIds() {
            var products_val = $('#product_new').val();
            if (products_val != "")
                return products_val;
            else
                return '-1';
        }

        function addGift_new(item) {
            var products_val = $('#product_new').val();
            var products = products_val.split(',');

            if (getGiftIds() != "-1")
                products_val += ',';

            products_val += item[1];

            $('#product_new').val(products_val);

            var line = $('<div class="product-line-' + item[1] + '"></div>');
            line.append(item[1] + ' - ' + item[0]);
            line.append('<span onclick="delGift_new(' + item[1] + ');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span>');

            $('#choose').append(line);
            if ($('#choose > div').length == 1)
                $('#choose').slideDown();

            $("#product").setOptions({
                extraParams: {excludeIds: getGiftIds()}
            });

            $("#choose").slideDown();
            $("#choose_new").slideUp();
        }

        function delGift_new(id_product) {
            var product_temp = [];
            var products_val = $('#product_new').val();
            var products = products_val.split(',');
            for (k in products) {
                if (products[k] != id_product)
                    product_temp.push(products[k]);
            }
            $('#product_new').val(product_temp.join(','));
            $('#choose > div.product-line-' + id_product + '').remove();

            $("#product").setOptions({
                extraParams: {excludeIds: getGiftIds()}
            });

            $("#choose").slideUp();
            $("#choose_new").slideDown();
        }
    </script>
{/literal}
