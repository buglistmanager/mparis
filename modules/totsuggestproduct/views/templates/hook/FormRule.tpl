{*
* @author 202 ecommerce <contact@202-ecommerce.com>
* @copyright  Copyright (c) 202 ecommerce 2014
* @license    Commercial license
*}
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='Add Rule' mod='totsuggestproduct'}</legend>
    <form method="post" action="" class="form">
        <table class="table tableDnD duplicated">
            <thead>
            <tr>
                <th>{l s='Name' mod='totsuggestproduct'}</th>
                <th>{l s='Value' mod='totsuggestproduct'}</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    <label for="name">{l s='Name' mod='totsuggestproduct'}</label>
                </td>
                <td>
                    <input type="text" name="name" id="name" placeholder="{l s='Name' mod='totsuggestproduct'}"
                           value="{if Tools::getValue('name')}Tools::getValue('name'){else}{$rule->name|escape:'htmlall':'UTF-8'}{/if}"
                           size="50"/>
                </td>
            </tr>
            <tr class="duplicate">
                <td>
                    <label>{l s='Value' mod='totsuggestproduct'}</label>
                </td>
                <td>
                    <table class="table tableDnD">
                        <tr>
                            <th>{l s='Product' mod='totsuggestproduct'}</th>
                            <th>{l s='Attribute' mod='totsuggestproduct'}</th>
                            <th>{l s='Category' mod='totsuggestproduct'}</th>
                            <th>{l s='Supplier' mod='totsuggestproduct'}</th>
                            <th>{l s='Manufacturer' mod='totsuggestproduct'}</th>
                        </tr>
                        <tr>
                            <td>
                                <div class="product">
                                    <div class="choose"></div>
                                    <div class="choose_new">
                                        <input type="hidden" name="" class="product_new"/>
                                        <input type="hidden" name="" class="product_tmp"/>
                                        <input type="hidden" name="" class="product_name"/>
                                        <input type="text" name="suggest_name" class="productComplete"
                                               placeholder="{l s='Name of your product' mod='totsuggestproduct'}"
                                               value="" size="30"/>
                                        <img onclick="addGift_new($(this));" style="cursor: pointer;"
                                             src="../img/admin/add.gif" alt="produit" title=""/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="product_attribute">
                                    <select name="" multiple size="6">
                                        {foreach $attributes as $attribute}
                                            <option value="{$attribute.id_attribute|escape:'htmlall':'UTF-8'}">{$attribute.attribute_group|escape:'htmlall':'UTF-8'}
                                                - {$attribute.name|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="category">
                                    <select name="" multiple size="6">
                                        {foreach $categories as $category}
                                            <option value="{$category.id|escape:'htmlall':'UTF-8'}">{$category.name|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="supplier">
                                    <select name="" multiple size="6">
                                        {foreach $suppliers as $supplier}
                                            <option value="{$supplier.id_supplier|escape:'htmlall':'UTF-8'}">{$supplier.name|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="manufacturer">
                                    <select name="" multiple size="6">
                                        {foreach $manufacturers as $manufacturer}
                                            <option value="{$manufacturer.id_manufacturer|escape:'htmlall':'UTF-8'}">{$manufacturer.name|escape:'htmlall':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div class="quantityDiv">
                        <label>{l s='Number of rules to validate' mod='totsuggestproduct'}</label><input type="text"
                                                                                                         name=""
                                                                                                         value="1"
                                                                                                         class="quantityRules"/>
                    </div>
                    <a class="button deleteGroup" onClick="deleteGroup($(this))">
                        <img src="../img/admin/forbbiden.gif" alt=""/>{l s='Delete group' mod='totsuggestproduct'}
                    </a>
                </td>
            </tr>
            {if isset($groups) && $groups == true && sizeof($groups)}
                {foreach $groups as $key => $group}
                    <tr class="new">
                        <td>
                            <label>{l s='Value' mod='totsuggestproduct'}</label>
                        </td>
                        <td>
                            <table class="table tableDnD">
                                <tr>
                                    <th>{l s='Product' mod='totsuggestproduct'}</th>
                                    <th>{l s='Attribute' mod='totsuggestproduct'}</th>
                                    <th>{l s='Category' mod='totsuggestproduct'}</th>
                                    <th>{l s='Supplier' mod='totsuggestproduct'}</th>
                                    <th>{l s='Manufacturer' mod='totsuggestproduct'}</th>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="product">
                                            <div class="choose">{include file="./form_rule_product.tpl"}</div>
                                            <div class="choose_new">
                                                <input type="hidden"
                                                       name="group[{$key|escape:'htmlall':'UTF-8'}][product]"
                                                       class="product_new"
                                                       value="{$group.product|escape:'htmlall':'UTF-8'}"/>
                                                <input type="hidden"
                                                       name="group[{$key|escape:'htmlall':'UTF-8'}][product_name]"
                                                       class="product_name"
                                                       value="{$group.product_name|escape:'htmlall':'UTF-8'}"/>
                                                <input type="hidden" name="" class="product_tmp"/>
                                                <input type="text" name="suggest_name" class="productComplete"
                                                       placeholder="{l s='Name of your product' mod='totsuggestproduct'}"
                                                       value="" size="30"/>
                                                <img onclick="addGift_new($(this));" style="cursor: pointer;"
                                                     src="../img/admin/add.gif" alt="produit" title=""/>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="product_attribute">
                                            <select name="group[{$key|escape:'htmlall':'UTF-8'}][product_attribute][]"
                                                    multiple size="6">
                                                {foreach $attributes as $attribute}
                                                    <option value="{$attribute.id_attribute|escape:'htmlall':'UTF-8'}"{if isset($group.product_attribute) && in_array($attribute.id_attribute, $group.product_attribute)} selected{/if}>{$attribute.attribute_group|escape:'htmlall':'UTF-8'}
                                                        - {$attribute.name|escape:'htmlall':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="category">
                                            <select name="group[{$key|escape:'htmlall':'UTF-8'}][category][]" multiple
                                                    size="6">
                                                {foreach $categories as $category}
                                                    <option value="{$category.id|escape:'htmlall':'UTF-8'}"{if isset($group.category) && in_array($category.id, $group.category)} selected{/if}>{$category.name|escape:'htmlall':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="supplier">
                                            <select name="group[{$key|escape:'htmlall':'UTF-8'}][supplier][]" multiple
                                                    size="6">
                                                {foreach $suppliers as $supplier}
                                                    <option value="{$supplier.id_supplier|escape:'htmlall':'UTF-8'}"{if isset($group.supplier) && in_array($supplier.id_supplier, $group.supplier)} selected{/if}>{$supplier.name|escape:'htmlall':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="manufacturer">
                                            <select name="group[{$key|escape:'htmlall':'UTF-8'}][manufacturer][]"
                                                    multiple size="6">
                                                {foreach $manufacturers as $manufacturer}
                                                    <option value="{$manufacturer.id_manufacturer|escape:'htmlall':'UTF-8'}"{if isset($group.manufacturer) && in_array($manufacturer.id_manufacturer, $group.manufacturer)} selected{/if}>{$manufacturer.name|escape:'htmlall':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <div class="quantityDiv">
                                <label>{l s='Number of rules to validate' mod='totsuggestproduct'}</label><input
                                        type="text" name="group[{$key|escape:'htmlall':'UTF-8'}][rules_validated]"
                                        value="{if isset($group.rules_validated)}{$group.rules_validated|escape:'htmlall':'UTF-8'}{/if}"
                                        class="quantityRules"/>
                            </div>
                            <a class="button deleteGroup" onClick="deleteGroup($(this))">
                                <img src="../img/admin/forbbiden.gif"
                                     alt=""/>{l s='Delete group' mod='totsuggestproduct'}
                            </a>
                        </td>
                    </tr>
                {/foreach}
            {/if}
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2">
                    <a class="button onClick">
                        <img src="../img/admin/add.gif"/>
                        {l s='Add group' mod='totsuggestproduct'}
                    </a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="btnSubmit" class="button"/>
                </td>
            </tr>
            </tfoot>
        </table>
    </form>
</fieldset>
{if version_compare($smart.const_PS_VERSION_, '1.6', '<')}
    <link rel="stylesheet" type="text/css" href="../js/jquery/plugins/autocomplete/jquery.autocomplete.css"/>
    <script type="text/javascript" src="../js/jquery/plugins/autocomplete/jquery.autocomplete.js"></script>
{/if}
{literal}
<script type="text/javascript">
    $(function () {

        var size = {/literal}{$newGroup|escape:'htmlall':'UTF-8'}{literal} +1;

        var options = {
            delay: 100,
            minChars: 1,
            autoFill: true,
            max: 20,
            matchContains: true,
            mustMatch: true,
            scroll: false,
            cacheLength: 0,
            multipleSeparator: ",",
            formatItem: function (item) {
                return item[1] + ' - ' + item[0];
            }
        };

        $(".new .choose > div").each(function () {
            var id = $(this).attr("class").replace("product", "");
            $(this).children("span").attr("onClick", "delGift_new($(this), " + id + ");");
        });

        $(".onClick").click(function () {
            var new_duplicate = $(".duplicate").clone();
            new_duplicate.appendTo("form table.table.tableDnD.duplicated > tbody").removeClass("duplicate").addClass('new');

            $(".new:last-child .product_new").attr("name", "group[" + size + "][product]");
            $(".new:last-child .product_name").attr("name", "group[" + size + "][product_name]");
            $(".new:last-child .product_attribute select").attr("name", "group[" + size + "][product_attribute][]");
            $(".new:last-child .category select").attr("name", "group[" + size + "][category][]");
            $(".new:last-child .supplier select").attr("name", "group[" + size + "][supplier][]");
            $(".new:last-child .manufacturer select").attr("name", "group[" + size + "][manufacturer][]");
            $(".new:last-child .quantityRules").attr("name", "group[" + size + "][rules_validated]");

            size++;
            var pC = new_duplicate.children("td:last-child").children("table.table.tableDnD").children("tbody").children("tr:last-child").children("td").children(".product").children(".choose_new").children(".productComplete");

            pC.autocomplete("ajax_products_list.php", options)
                    .result(function (event, item) {
                        totCheck($(this), item);
                    });

            pC.setOptions({
                extraParams: {excludeIds: getGiftIds(pC)}
            });
        });

        $(".productComplete").autocomplete("ajax_products_list.php", options)
                .result(function (event, item) {
                    totCheck($(this), item);
                });

        $(".productComplete").each(function () {
            var element = $(this);
            $(this).setOptions({
                extraParams: {excludeIds: getGiftIds(element)}
            });
        });

    });

    function totCheck(element, item) {

        var products_val = element.parent(".choose_new").children(".product_new").val();
        var products = products_val.split(',');
        var gift = getGiftIds(element.parent(".choose_new").children(".product_new"));
        if (gift && gift != "-1")
            products_val += ',';

        products_val += item[1];

        element.parent(".choose_new").children(".product_new").val(products_val);
        element.parent(".choose_new").children(".product_tmp").val(item[1]);

        element.parent(".choose_new").children(".product_name").val(item[0]);

        $(element).setOptions({
            extraParams: {excludeIds: getGiftIds(element)}
        });
    }

    function getGiftIds(element) {

        var products_val = $(element).parent().find('.product_new').val();

        if (products_val != "")
            return products_val;
        else
            return '-1';
    }

    function deleteGroup(element) {
        element.parents('.new').remove();

        $(element).setOptions({
            extraParams: {excludeIds: getGiftIds(element)}
        });
    }

    function addGift_new(element) {
        var par = element.parents(".choose_new");
        if (par.children(".product_new").val() != "") {
            var lineDisplay = par.children(".product_tmp").val() + " - " + par.children(".productComplete").val();
            par.parent().children(".choose").html(par.parent().children(".choose").html() + ' <div class="product' + par.children(".product_tmp").val() + '">' + lineDisplay + '<span onclick="delGift_new($(this), ' + par.children(".product_tmp").val() + ');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span></div>');
            par.children(".product_name").val(encodeURI(par.parent().children(".choose").html()));
            par.children(".productComplete").val("");
            par.children(".product_tmp").val("");

            $(element).setOptions({
                extraParams: {excludeIds: getGiftIds(element)}
            });
        }
    }

    function delGift_new(element, id) {
        var par = element.parents(".choose");
        par.children(".product" + id).remove();
        par.parent().children(".choose_new").children(".product_name").val(par.html());
        var val = par.parent().children(".choose_new").children(".product_new").val();
        var reg1 = new RegExp('(,' + id + ',)'); // Au coeur
        var reg2 = new RegExp('^(' + id + ')$'); // Tout seul
        var reg3 = new RegExp('(,' + id + ')$'); // A la fin
        var reg4 = new RegExp('^(' + id + ',)'); // Au début
        var replace = val.replace(reg1, ',').replace(reg2, '').replace(reg3, '').replace(reg4, '');
        par.parent().children(".choose_new").children(".product_new").val(replace);
        par.parent().children(".choose_new").children(".product_name").val(encodeURI(par.html()));

        var values = par.parent().find('.choose_new > .productComplete');
        $(values).setOptions({
            extraParams: {excludeIds: getGiftIds(values)}
        });
    }

</script>
</script>
{/literal}
