{*
* @author 202 ecommerce <contact@202-ecommerce.com>
* @copyright  Copyright (c) 202 ecommerce 2014
* @license    Commercial license
*}
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif">{l s='Global configurations' mod='totsuggestproduct'}
    </legend>
    <form action="" method="post">
        <label id="campaign_limit2" for="campaign_limit">
            {l s='Limit max of suggestions to display in front' mod='totsuggestproduct'}
        </label>
        <div class="margin">
            <input type="number" name="campaign_limit" id="campaign_limit"
                   value="{$campaign_limit|escape:'htmlall':'UTF-8'}">
        </div>
        <div class="clear"></div>
        <label for="">&nbsp;</label>
        <div class="margin">
            <input type="submit" class="button"/>
        </div>
    </form>
</fieldset>
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='My campaigns' mod='totsuggestproduct'}
    </legend>
    <div class="newCampaign">
        <a href="{$linkHome|escape:'htmlall':'UTF-8'}&add=Campaign">
            <input type="button" class="button" value="{l s='Create a campaign' mod='totsuggestproduct'}"/>
        </a>
    </div>
    <table class="table tableDnD">
        <thead>
        <tr>
            <th>{l s='Rule name' mod='totsuggestproduct'}</th>
            <th>{l s='Product' mod='totsuggestproduct'}</th>
            <th>{l s='Justification' mod='totsuggestproduct'}</th>
            <th>{l s='Campaign' mod='totsuggestproduct'}</th>
            <th>{l s='Action' mod='totsuggestproduct'}</th>
            {if $v15 === true && $multishop === 1}
                <th>{l s='Shop' mod='totsuggestproduct'}</th>
            {/if}
        </tr>
        </thead>
        <tbody>
        {if isset($campaigns) && is_array($campaigns) && sizeof($campaigns)}
            {foreach $campaigns as $campaign}
                <tr>
                    <td class="catName">
                        {$campaign.name|escape:'htmlall':'UTF-8'}
                    </td>
                    <td class="productName">
                        {$campaign.product->id|escape:'htmlall':'UTF-8'}
                        - {$campaign.product->name|escape:'htmlall':'UTF-8'}
                    </td>
                    <td class="translate">
                        {$campaign.justification|escape:'htmlall':'UTF-8'}
                    </td>
                    <td style="width:100px">
                        {$campaign.campaignName|escape:'htmlall':'UTF-8'}
                    </td>
                    <td style="width:100px">
                        <a href="{$linkHome|escape:'htmlall':'UTF-8'}&{if $campaign.enabled == 1}disabled{else}enabled{/if}={$campaign.id_totsuggestproduct|escape:'htmlall':'UTF-8'}">
                            <img src="../img/admin/{if $campaign.enabled != 1}disabled{else}enabled{/if}.gif"/>
                        </a>
                        <a href="{$linkHome|escape:'htmlall':'UTF-8'}&edit=Campaign&id={$campaign.id_totsuggestproduct|escape:'htmlall':'UTF-8'}">
                            <img src="../img/admin/edit.gif"
                                 alt="produit{$campaign.id_totsuggestproduct|escape:'htmlall':'UTF-8'}" title=""/>
                        </a>
                        <a href="{$linkHome|escape:'htmlall':'UTF-8'}&deleteCampain={$campaign.id_totsuggestproduct|escape:'htmlall':'UTF-8'}">
                            <img src="../img/admin/delete.gif"
                                 alt="produit{$campaign.id_totsuggestproduct|escape:'htmlall':'UTF-8'}" title=""/>
                        </a>
                    </td>
                    {if $v15 === true && $multishop === 1}
                        <td>
                            {if $campaign.id_shop == 0 AND $campaign.id_shop_group == 0}
                                {l s='All shops' mod='totsuggestproduct'}
                            {else if $campaign.id_shop != 0}
                                {Shop::getShop($campaign.id_shop)|escape:'htmlall':'UTF-8'}
                            {else}
                                {l s='Group' mod='totsuggestproduct'}
                            {/if}
                        </td>
                    {/if}
                </tr>
            {/foreach}
        {else}
            {if isset($noCampaigns)}
                <tr>
                    <td colspan="{if $v15 === true && $multishop === 1}6{else}5{/if}">
                        <div class="warn">
                            {$noCampaigns|escape:'htmlall':'UTF-8'}
                        </div>
                    </td>
                </tr>
            {/if}
        {/if}
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='Help' mod='totsuggestproduct'}</legend>
    <p>
        - {l s='You can create different campaigns and follow the stats in the Statistics tab.' mod='totsuggestproduct'}
        <br/>
        - {l s='Before creating a campaign, you should create a rule who can be used by different campaigns.' mod='totsuggestproduct'}
        <br/>
        - {l s='Use the Justification field to explain to your customer why he/she should buy your product.' mod='totsuggestproduct'}
        <br/>
        - {l s='When you delete a campaign, it stay in the statistic. The column end set the date of the day ended' mod='totsuggestproduct'}
        <br/>
    </p>
</fieldset>
