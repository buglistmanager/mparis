{*
* @author 202 ecommerce <contact@202-ecommerce.com>
* @copyright  Copyright (c) 202 ecommerce 2014
* @license    Commercial license
*}
{if isset($datepicker)}
    {if is_array($datepicker)}
        {includeDatepicker($datepicker)}
    {else}
        {if $datepicker = "show_version15"}
            <script type="text/javascript">
                $(function () {
                    $("#date_begin_totsuggest").datepicker({
                        prevText: "",
                        nextText: "",
                        dateFormat: "yy-mm-dd"
                    });
                    $("#date_end_totsuggest").datepicker({
                        prevText: "",
                        nextText: "",
                        dateFormat: "yy-mm-dd"
                    });
                });
            </script>
        {/if}
    {/if}
{/if}

<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='Statistics' mod='totsuggestproduct'}
    </legend>
    <form method="post" action="">
        {l s='Date begin' mod='totsuggestproduct'} : <input type="text" name="date_begin" id="date_begin_totsuggest"
                                                            value="{$date_begin|escape:'htmlall':'UTF-8'}"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        {l s='Date end' mod='totsuggestproduct'} : <input type="text" name="date_end"
                                                          value="{$date_end|escape:'htmlall':'UTF-8'}"
                                                          id="date_end_totsuggest"/> &nbsp;&nbsp;&nbsp;
        <input type="submit" class="button"/>
    </form>
    <br/>
    <br/>
    <br/>
    <table class="table tableDnD">
        <thead>
        <tr>
            <th>{l s='Campaign' mod='totsuggestproduct'}</th>
            <th>{l s='Rule name' mod='totsuggestproduct'}</th>
            <th>{l s='Product' mod='totsuggestproduct'}</th>
            <th>{l s='Begin' mod='totsuggestproduct'}</th>
            <th>{l s='End' mod='totsuggestproduct'}</th>
            <th>{l s='Displayed' mod='totsuggestproduct'}</th>
            <th>{l s='Bought' mod='totsuggestproduct'}</th>
            <th>{l s='Conversion' mod='totsuggestproduct'}</th>
            <th>{l s='Sales' mod='totsuggestproduct'}</th>
            <th>{l s='Sales Ratio' mod='totsuggestproduct'}</th>
            {if $v15 === true && $multishop === 1}
                <th>{l s='Shop' mod='totsuggestproduct'}</th>
            {/if}
        </tr>
        </thead>
        <tbody>
        {if isset($campaigns) && is_array($campaigns) && sizeof($campaigns)}
            {foreach $campaigns as $campaign}
                <tr>
                    <td>{$campaign.campaignName|escape:'htmlall':'UTF-8'}</td>
                    <td>{$campaign.name|escape:'htmlall':'UTF-8'}</td>
                    <td>{$campaign.product|escape:'htmlall':'UTF-8'}</td>
                    <td>{substr($campaign.date_begin, 0, 10)|escape:'htmlall':'UTF-8'}</td>
                    <td>{(($campaign.date_end != '0000-00-00 00:00:00') ? substr($campaign.date_end, 0, 10) : {l s='On going' mod='totsuggestproduct'})|escape:'htmlall':'UTF-8'}</td>
                    <td>{$campaign.displayed|escape:'htmlall':'UTF-8'}</td>
                    <td>{$campaign.bought|escape:'htmlall':'UTF-8'}</td>
                    <td>{$campaign.conversion|escape:'htmlall':'UTF-8'}</td>
                    <td>{$campaign.sales|escape:'htmlall':'UTF-8'}</td>
                    <td>{$campaign.salesPercentage|escape:'htmlall':'UTF-8'}</td>
                    {if $v15 === true && $multishop === 1}
                        <td>
                            {if $campaign.id_shop == 0 AND $campaign.id_shop_group == 0}
                                {l s='All shops' mod='totsuggestproduct'}
                            {else if $campaign.id_shop != 0}
                                {Shop::getShop($campaign.id_shop)|escape:'htmlall':'UTF-8'}
                            {else}
                                {l s='Group' mod='totsuggestproduct'}
                            {/if}
                        </td>
                    {/if}
                </tr>
            {/foreach}
        {/if}
        <tr class="bold">
            <td colspan="5" class="right">{l s='Total' mod='totsuggestproduct'}</td>
            <td>{$percentage.displayed|escape:'htmlall':'UTF-8'}</td>
            <td>{$percentage.bought|escape:'htmlall':'UTF-8'}</td>
            <td></td>
            <td colspan="2">+ {number_format($increaseSales.increase, 2)|escape:'htmlall':'UTF-8'}
                % {l s='of sales' mod='totsuggestproduct'}</td>
        </tr>
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='Help' mod='totsuggestproduct'}</legend>
    <p>
        - {l s='You can change the begin date and the end date for have a look by months or else the stats.' mod='totsuggestproduct'}
        <br/><br/>
        {l s='The column :' mod='totsuggestproduct'}<br/>
        - {l s='"Displayed" means how many times a suggested product has been displayed in the cart.' mod='totsuggestproduct'}
        <br/>
        - {l s='"Bought" means a customer who would have seen a suggested product has bought the product suggested.' mod='totsuggestproduct'}
        <br/>
        - {l s='"Conversion" calculs the pourcentage of the columns Display and Bought. For example, if the product is displayed ten times and bought five times, the total will be 50%.' mod='totsuggestproduct'}
        <br/>
        - {l s='"Sales" shows you how much you have sell more thanks to our module. The price displayed is tax free.' mod='totsuggestproduct'}
        <br/>
        - {l s='"Sales Ratio" calculs the pourcentage of sells between the sell with and without our module. For exemple, if a customer buy an iPod Nano without our module and two others customers buy an iPod Nano, the pourcentage will be 66,33%. It start from the installation date of the module.' mod='totsuggestproduct'}
        <br/>
    </p>
</fieldset>
