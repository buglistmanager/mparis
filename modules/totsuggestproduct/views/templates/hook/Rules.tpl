{*
* @author 202 ecommerce <contact@202-ecommerce.com>
* @copyright  Copyright (c) 202 ecommerce 2014
* @license    Commercial license
*}
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='Rules' mod='totsuggestproduct'}</legend>
    <a href="{$linkHome|escape:'htmlall':'UTF-8'}&add=Rule">
        <input type="button" class="button" value="{l s='Create a rule' mod='totsuggestproduct'}"/>
    </a>
    <table class="table tableDnD">
        <thead>
        <tr>
            <th>{l s='Rule name' mod='totsuggestproduct'}</th>
            <th>{l s='Action' mod='totsuggestproduct'}</th>
        </tr>
        </thead>
        <tbody>
        {if isset($rules) && is_array($rules) && sizeof($rules)}
            {foreach $rules as $rule}
                <tr>
                    <td>{$rule.name|escape:'htmlall':'UTF-8'}</td>
                    <td class="center" style="width:100px;">
                        <a href="{$linkHome|escape:'htmlall':'UTF-8'}&edit=Rule&id={$rule.id_totsuggestproduct_cart_rule|escape:'htmlall':'UTF-8'}">
                            <img src="../img/admin/edit.gif"
                                 alt="produit{$rule.id_totsuggestproduct_cart_rule|escape:'htmlall':'UTF-8'}" title=""/>
                        </a>
                        <a href="{$linkHome|escape:'htmlall':'UTF-8'}&deleteRule={$rule.id_totsuggestproduct_cart_rule|escape:'htmlall':'UTF-8'}">
                            <img src="../img/admin/delete.gif"
                                 alt="produit{$rule.id_totsuggestproduct_cart_rule|escape:'htmlall':'UTF-8'}" title=""/>
                        </a>
                    </td>
                </tr>
            {/foreach}
        {else}
            {if isset($noRules)}
                <tr>
                    <td colspan="2">
                        <div class="warn">
                            {$noRules|escape:'htmlall':'UTF-8'}
                        </div>
                    </td>
                </tr>
            {/if}
        {/if}
        </tbody>
    </table>
</fieldset>
<fieldset>
    <legend><img src="{$path|escape:'htmlall':'UTF-8'}logo.gif"
                 alt="{l s='202-ecommerce' mod='totsuggestproduct'}"/>{l s='Help' mod='totsuggestproduct'}</legend>
    <p>
        - {l s='A rule can be selected in different campaigns.' mod='totsuggestproduct'}<br/>
        - {l s='You can define as many conditions you want in a rule.' mod='totsuggestproduct'}<br/>
        - {l s='To suggest a product, the order has to respect all the conditions except if you add a minimum of conditions to be respected.' mod='totsuggestproduct'}
    </p>
</fieldset>
