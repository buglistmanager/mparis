<?php
if (!defined('_PS_VERSION_'))
	exit;

class homenewproducts extends Module
{
	private $_html = '';
	private $_postErrors = array();

	function __construct()
	{
		$this->name = 'homenewproducts';
		$this->tab = 'front_office_features';
		$this->version = '1.1';
		$this->author = 'MyPresta.eu';

		parent::__construct();

		$this->displayName = $this->l('New products on the homepage');
		$this->description = $this->l('Displays new products in the middle of your homepage.');
	}

	function install(){
		$this->_clearCache('homenewproducts.tpl');
		Configuration::updateValue('HNP_NBR', 8);

		if (!parent::install()
			|| !$this->registerHook('displayHome')
			|| !$this->registerHook('header')
			|| !$this->registerHook('addproduct')
			|| !$this->registerHook('updateproduct')
			|| !$this->registerHook('deleteproduct')
		)
			return false;
		return true;
	}
	
	public function uninstall(){
		$this->_clearCache('homenewproducts.tpl');
		return parent::uninstall();
	}

	public function getContent(){
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitHomeFeatured'))
		{
			$nbr = (int)Tools::getValue('nbr');
			if (!$nbr OR $nbr <= 0 OR !Validate::isInt($nbr))
				$errors[] = $this->l('An invalid number of products has been specified.');
			else
				Configuration::updateValue('HNP_NBR', (int)($nbr));
			if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
				$output .= $this->displayConfirmation($this->l('Your settings have been updated.'));
		}
		return $output.$this->displayForm();
	}

	public function displayForm(){
		$output = '<iframe src="http://mypresta.eu/content/uploads/2012/09/htmlbox_advertise.html" width="100%" height="130" border="0" style="border:none;"></iframe>
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('Define the number of products to be displayed.').'</label>
				<div class="margin-form">
					<input type="text" size="5" name="nbr" value="'.Tools::safeOutput(Tools::getValue('nbr', (int)(Configuration::get('HNP_NBR')))).'" />
					<p class="clear">'.$this->l('Define the number of products that you would like to display on homepage (default: 8).').'</p>

				</div>
				<center><input type="submit" name="submitHomeFeatured" value="'.$this->l('Save').'" class="button" /></center>
			</fieldset>
		</form>'.$this->l('like us on Facebook').'</br><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Ffacebook.com%2Fmypresta&amp;send=false&amp;layout=button_count&amp;width=120&amp;show_faces=true&amp;font=verdana&amp;colorscheme=light&amp;action=like&amp;height=21&amp;appId=276212249177933" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:120px; height:21px; margin-top:10px;" allowtransparency="true"></iframe>
        '.'<div style="float:right; text-align:right; display:inline-block; margin-top:10px; font-size:10px;">
        '.$this->l('Proudly developed by').' <a href="http://mypresta.eu" style="font-weight:bold; color:#B73737">MyPresta<font style="color:black;">.eu</font>.</a>
        ';
		return $output;
	}

	public function hookDisplayHeader($params){
		$this->hookHeader($params);
	}

	public function hookHeader($params){
		$this->context->controller->addCSS(($this->_path).'homenewproducts.css', 'all');
	}

	public function hookDisplayHome($params){
		if (!$this->isCached('homenewproducts.tpl', $this->getCacheId('homenewproducts'))){
			$nb = (int)Configuration::get('HNP_NBR');  
            $productClass = new Product(); 
            $products = $productClass -> getNewProducts((int)Context::getContext()->language->id,0,($nb ? $nb : 8));
			$this->smarty->assign(array(
				'products' => $products,
				'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
				'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
			));
		}
		return $this->display(__FILE__, 'homenewproducts.tpl', $this->getCacheId('homenewproducts'));
	}

	public function hookAddProduct($params){
		$this->_clearCache('homenewproducts.tpl');
	}

	public function hookUpdateProduct($params){
		$this->_clearCache('homenewproducts.tpl');
	}

	public function hookDeleteProduct($params){
		$this->_clearCache('homenewproducts.tpl');
	}
}