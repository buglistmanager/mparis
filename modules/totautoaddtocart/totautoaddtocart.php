<?php
/**
 * Description of totUnknown
 *
 * @version 1.4.2
 * @author    202-ecommerce
 * @copyright 2007-2014 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

if (!defined('_PS_VERSION_'))
	exit;

/**
* Ajout automatique au panier
* 
* @version 1.4
* @author Guillaume DELOINCE 
* @name totAutoAddToCart
* @package totAutoAddToCart
*/
class TotAutoAddToCart extends Module
{

	/**
	* Product lists
	* @version 1.0
	* @var array 
	*/
	private $list = array();

	/**
	* Constructor      
	*/
	public function __construct()
	{
		$this->name = 'totautoaddtocart';

		$this->tab = 'others';
		$this->version = "1.5.2";
		$this->author = '202-ecommerce';
		$this->module_key = 'e8cd86f07323c07706ea91fccfd1edcb';

		parent::__construct();

		$this->displayName = $this->l('Auto add to cart');
		$this->description = $this->l('Add product to cart');

		if (version_compare(_PS_VERSION_, '1.5', '>'))
			$this->link = 'index.php?controller='.Tools::getValue('controller').'&amp;configure='.$this->name.'&amp;token='.Tools::getValue('token').'&amp;tab_module='.$this->tab.'&amp;module_name='.$this->name;
		else
			$this->link = 'index.php?tab='.Tools::getValue('tab').'&amp;configure='.$this->name.'&amp;token='.Tools::getValue('token').'&amp;tab_module='.$this->tab.'&amp;module_name='.$this->name;

		if (Module::isInstalled($this->name))
			$this->upgrade();
	}

	/**
	* BO: Adding a table in the database and module installation
	* @version 1.3
	* @return boolean 
	*/
	public function install()
	{
			// Install is Ok
			$MySQLQuery = '
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.Tools::strtolower($this->name).'` (
					`id_totautoaddtocart` int(11) NOT NULL AUTO_INCREMENT,
					`id_product` int(11) NOT NULL,
					`type` int(11) NOT NULL,
					`price` int(11) NOT NULL,
					`onproduct_id` varchar(255) NOT NULL,
					`date_created` varchar(20) NOT NULL,
					`status` INT( 11 ) NOT NULL DEFAULT  "0",
					`add_quantity` BOOLEAN NOT NULL DEFAULT "0",
					`id_shop` INT NOT NULL DEFAULT "0",
					`id_shop_group` INT NOT NULL DEFAULT "0",
					PRIMARY KEY (`id_totautoaddtocart`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;';

			$MySQLQuery2 = '
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` (
				  `id_action` int(11) NOT NULL AUTO_INCREMENT,
				  `id_cart` int(11) NOT NULL,
				  `id_totautoaddtocart` int(11) NOT NULL,
				  `id_trigger_product` int(11) NOT NULL,
				  `quantity` int(11) NOT NULL,
				  `date` datetime NOT NULL,
				  `id_product_attribute` int(11) NOT NULL,
				  `product_deleted` int(1) NOT NULL,
				  PRIMARY KEY (`id_action`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

			if (parent::install() === false || !$this->registerHook('cart') || !DB::getInstance()->Execute($MySQLQuery) || !DB::getInstance()->Execute($MySQLQuery2) || !Configuration::updateValue('TOTAUTOADDTOCART_VERSION', $this->version))
				return false;
			
			return true;
	}

	private function upgrade()
	{
		$version = Configuration::get('TOTAUTOADDTOCART_VERSION');
		if ($version === false || $this->version > $version)
		{
			if ($version === false)
			{
				$SQL = 'ALTER TABLE  `'._DB_PREFIX_.Tools::strtolower($this->name).'` CHANGE  `onproduct_id`  `onproduct_id` TEXT NOT NULL';
				DB::getInstance()->execute($SQL);
			}

			if (version_compare($version, '1.5.0'))
			{
				$MySQLQuery = '
					CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` (
					  `id_action` int(11) NOT NULL AUTO_INCREMENT,
					  `id_cart` int(11) NOT NULL,
					  `id_totautoaddtocart` int(11) NOT NULL,
					  `id_trigger_product` int(11) NOT NULL,
					  `quantity` int(11) NOT NULL,
					  `date` datetime NOT NULL,
					  `id_product_attribute` int(11) NOT NULL,
					  `product_deleted` int(1) NOT NULL,
					  PRIMARY KEY (`id_action`)
					) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';
				DB::getInstance()->execute($MySQLQuery);
			}

			Configuration::updateValue('TOTAUTOADDTOCART_VERSION', $this->version);
		}
	}

	/**
	* BO : Remove the table and uninstall the module
	* @version 1.2
	* @return boolean 
	*/
	public function uninstall()
	{
			$MySQLQuery = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.Tools::strtolower($this->name).'`';
			$MySQLQuery2 = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.Tools::strtolower($this->name).'_action`';
			if (Db::getInstance()->Execute($MySQLQuery) === true && Db::getInstance()->Execute($MySQLQuery2) === true && parent::uninstall() === true)
				return true;
			else
				return false;
	}

	/**
	* BO : Display admin panel
	* @version 1.4.2
	* @return string
	*/
	public function getContent()
	{
		if (version_compare(_PS_VERSION_, '1.6', '>'))
		{
			$this->_html = '
				<style>
					.nobootstrap
					{
						min-width: 1px;
						width: auto;
					}
				</style>
			';
		}

		$this->warning = array();
			// If module is inactive
		if ($this->active == 0)
			$this->warning[] = $this->l('The module is not active');
			// if warning exists
		if (count($this->warning))
			$this->_html .= $this->displayWarning();
			// If know action
		$this->_html .= $this->preProcess();
		$action = Tools::getValue('act');
		if ($action)
		{
					// Change statut gift
			if ($action == 'status')
				$this->_html .= $this->changeStatus();
					// Edit gift
			else if ($action == 'edit')
				$this->_html .= $this->Add(Tools::getValue('id'));
					// Add gift
			else if ($action == 'add')
				$this->_html .= $this->Add();
		}
		else
			$this->_html .= $this->viewList();

		return $this->displayBanner().$this->_html;
	}

	private function displayBanner()
	{
		$translations = array(
			'by' => $this->l('By'),
			'web' => $this->l('Web agency specialized in ecommerce web sites'),
			'addons' => $this->l('Our modules on addons'),
			'blog' => $this->l('News & advice on our blog')
		);

		$module = array(
			'description' => $this->description,
			'name' => $this->name,
			'displayName' => $this->displayName,
			'_path' => $this->_path
		);

		if (version_compare(_PS_VERSION_, '1.5', '>'))
		{
			$smarty = $this->context->smarty;
			$lang = $this->context->language;
		} 
		else
		{
			global $smarty, $cookie;
			$lang = new Language($cookie->id_lang);
		}

		$datas = array(
			'module' => $module,
			'translations' => $translations,
			'lang' => $lang
		);

		$smarty->assign($datas);

		return $this->display(__FILE__, '/views/templates/hook/banner.tpl');
	}

	/**
	* Display a warning message
	* @version 1.0
	* @param string $warn Warning message to display
	*/
	public function displayWarning()
	{
			$str_output = '';
			if (!empty($this->warning))
			{
				$str_output .= '<script type="text/javascript">
					$(document).ready(function()
					{
						$(\'#linkSeeMore\').unbind(\'click\').click(function(){
							$(\'#seeMore\').show(\'slow\');
							$(this).hide();
							$(\'#linkHide\').show();
							return false;
						});
						$(\'#linkHide\').unbind(\'click\').click(function(){
							$(\'#seeMore\').hide(\'slow\');
							$(this).hide();
							$(\'#linkSeeMore\').show();
							return false;ra
						});
						$(\'#hideWarn\').unbind(\'click\').click(function(){
							$(\'.warn\').hide(\'slow\', function (){
								$(\'.warn\').remove();
							});
							return false;
						});
					});
				</script>
			<div class="warn">';
					$str_output .= '<span style="float:right"><a id="hideWarn" href=""><img alt="X" src="../img/admin/close.png" /></a></span>'.
						(count($this->warning) > 1 ? $this->l('There are') : $this->l('There is')).' '.count($this->warning).' '.(count($this->warning) > 1 ? $this->l('warnings') : $this->l('warning'))
						.'<span style="margin-left:20px;" id="labelSeeMore">
						<a id="linkSeeMore" href="#" style="text-decoration:underline">'.$this->l('Click here to see more').'</a>
				<a id="linkHide" href="#" style="text-decoration:underline;display:none">'.$this->l('Hide warning').'</a></span><ul style="display:none;" id="seeMore">';
						foreach ($this->warning as $val)
							$str_output .= '<li>'.$val.'</li>';
				$str_output .= '</ul>';
				$str_output .= '</div>';
			}
			return $str_output;
	}

	/**
	* FO : Attachment point: at the time of adding a product
	* @version 1.4.2
	* @global obj Cart Cart infos
	* @global obj Cookie Users infos
	* @return boolean 
	*/
	public function hookCart($params)
	{
		// If version is 1.4 or less
		if (version_compare(_PS_VERSION_, '1.5', '<'))
			global $cart, $cookie;
		else
		{
			$cart = $this->context->cart;
			$cookie = $this->context->cookie;
		}

		if (!Validate::isLoadedObject($cart))
			$cart = new Cart();

		// Recovery of products from cart
		$products = $cart->getProducts();
		$price = 0;
		$priceByManuf = array();
		foreach ($products as $product)
		{
			$this->list[] = $product['id_product'];
			$price += $product['total_wt'];
			if (!isset($priceByManuf[$product['id_manufacturer']]))
				$priceByManuf[$product['id_manufacturer']] = 0;
			$priceByManuf[$product['id_manufacturer']] += $product['total_wt'];
		}
			// If the shopping cart you keep it empty
		if (count($products) >= 1)
		{
			$this->productWithPrice($price);
			$this->productWithManuf($priceByManuf, $products[0]['cart_quantity']);
			$this->firstProduct($products[0]['cart_quantity']);
					// If it requires a product from another
			foreach ($products as $product)
				$this->productFromAnOther($product['id_product'], $product['cart_quantity']);

			//VERIFIE SI IL FAUT SUPPRIMER DES PRODUITS DECLENCHé

			$actions = $this->addAllActions($cart->id); //Toutes les actions liées au panier id_cart 
			foreach ($actions as $action)
			{
				if ($this->verifProductActionHasBeenRemoved($action, $products))
					$this->updateActionProductDeleted($action);
				switch ($this->whatRuleOfAction($action))
				{ // Retourne 'PRODUIT_AJOUTE', 'MONTANT_TOTAL', 'MONTANT_MARQUE'
					case 'PRODUCT_ADDED':
						if ($this->verifProductHasBeenRemoved($action, $products)) // Retourne ou false
							$this->removeProduct($action, $cart); // Supprime la ligne de ta table action. Puis supprimer le produit du panier. // $cart->updateQty(1, )
						break;
					case 'TOTAL_AMOUNT':
						if ($this->verifCartLowerThanAmount($action, $cart))
							$this->removeProduct($action, $cart);
						break;
					case 'BRAND_AMOUNT':
						if ($this->verifCartLowerThanAmountBrand($action, $priceByManuf))
							$this->removeProduct($action, $cart);
						break;
				}
			}

		}
		else if (count($products) == 0)
		{
			$id_cart = (int)$cart->id;
			
			// $resTmp = DB::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` WHERE `id_cart` = '.$id_cart.' ');
			$cookie->__unset('gift');
			$res = DB::getInstance()->ExecuteS('SELECT `id_totautoaddtocart` FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'`');
			foreach ($res as $del)
			{
				$cookie->__unset('id'.$del['id_totautoaddtocart']);
				$cookie->__unset('add'.$del['id_totautoaddtocart']);
			}
		}
		return true;
	}

	public function verifProductActionHasBeenRemoved($action, $products)
	{
		$rule = $this->getRuleFromAction($action);

		foreach ($products as $product)
		{
			if ($product['id_product'] == $rule['id_product'])
				return false;
		}
		return true;
	}

	public function updateActionProductDeleted($action)
	{
		Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` SET product_deleted = 1 WHERE id_action = '.$action['id_action'].' ');
	}


	/**
	* Verify if people have a request for an other product
	* @version 1.4.2
	* @global object Cart 
	* @global object Cookie User informations
	* @param int ID Product
	* @param int Quantity 
	*/
	private function productFromAnOther($id_product, $cart_quantity)
	{
		global $cookie;
		$SQL = '
		SELECT au.`id_product`, au.`id_totautoaddtocart`, au.`add_quantity`, a.`product_deleted`
		FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` AS au
		LEFT OUTER JOIN `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` AS a
			ON 
					au.`id_totautoaddtocart` = a.`id_totautoaddtocart` 
				AND (a.`product_deleted` = 0 OR a.`product_deleted` IS NULL) 
				AND a.`id_cart` = '.(int)$this->getCartId().'
		WHERE 
			au.`status` = 1 
			AND au.`type` = 2
			AND au.`onproduct_id` LIKE \'%-'.$id_product.'-%\' ';

		if (version_compare(_PS_VERSION_, '1.5', '>'))
			$SQL .= ' '.Shop::addSqlRestriction().' ';

			// If it requires a product from another
		$res = DB::getInstance()->ExecuteS($SQL);
		if (count($res))
		{
			foreach ($res as $product)
			{
				if (!in_array($product['id_product'], $this->list) && $product['product_deleted'] == 0)
					$this->gestionStock($product, $cart_quantity);
			}
		}
	}

	/**
	* Add product with the first product
	* @version 1.4.2
	* @global object Cookie User informations
	*/
	private function firstProduct($cart_quantity)
	{
		global $cookie;
		$SQL = '
		SELECT au.`id_product`, au.`id_totautoaddtocart`, au.`add_quantity`, a.`product_deleted`
		FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` AS au
		LEFT OUTER JOIN `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` AS a
			ON 
					au.`id_totautoaddtocart` = a.`id_totautoaddtocart` 
				AND (a.`product_deleted` = 0 OR a.`product_deleted` IS NULL) 
				AND a.`id_cart` = '.(int)$this->getCartId().'
		WHERE 
			au.`status` = 1 
			AND au.`type` = 1';
			
		if (version_compare(_PS_VERSION_, '1.5', '>'))
			$SQL .= ' '.Shop::addSqlRestriction().' ';

			// If you require a product
		$res = DB::getInstance()->ExecuteS($SQL);
		if (count($res))
		{
			foreach ($res as $product)
			{
				if (!in_array($product['id_product'], $this->list) && $product['product_deleted'] == 0)
					$this->gestionStock($product, $cart_quantity);
			}
		}
	}

	public function addAllActions($id_cart)
	{
		$sql = ('SELECT * FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` WHERE id_cart = '.$id_cart.' ');
		$res = DB::getInstance()->ExecuteS($sql);
		return $res;
	}

	public function whatRuleOfAction($action)
	{
		//Récupération de la règle
		$rule = $this->getRuleFromAction($action);
		if ($rule['type'] == 1 || $rule['type'] == 2)
			return 'PRODUCT_ADDED';
		elseif ($rule['type'] == 3)
			return 'TOTAL_AMOUNT';
		elseif ($rule['type'] == 4)
			return 'BRAND_AMOUNT';

	}

	public function getRuleFromAction($action)
	{
		$sql = 'SELECT * FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` WHERE id_totautoaddtocart = '.$action['id_totautoaddtocart'].'  '; 
		$rule = DB::getInstance()->getRow($sql);
		return $rule;
	}

	public function verifProductHasBeenRemoved($action, $products)
	{
		foreach ($products as $product)
		{
			if ($product['id_product_attribute'] == $action['id_product_attribute'] && $product['id_product'] == $action['id_trigger_product'])
				return false;
		}
		return true;

	}

	public function removeProduct($action, $cart)
	{
		$res = DB::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` WHERE `id_action` = '.$action['id_action'].' ');
		// $resTmp = DB::getInstance()->Execute('UPDATE `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` 
		// 	SET product_deleted = 1
		// 	WHERE id_action = '.$action['id_action'].' ');

		$sql = 'SELECT * FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` 
				WHERE id_totautoaddtocart = "'.$action['id_totautoaddtocart'].'"'; 
		$product = DB::getInstance()->getRow($sql);
		$quantity = $action['quantity'];
		$id_product = $product['id_product'];
		$id_product_attribute = Product::getDefaultAttribute($id_product);
		$operator = 'down';
		$del_trigger_item = $cart->updateQty($quantity, $id_product, $id_product_attribute, false, $operator);
	}

	public function verifCartLowerThanAmount($action, $cart)
	{
		$sql = 'SELECT * FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` WHERE id_totautoaddtocart = "'.$action['id_totautoaddtocart'].'"'; 
		$price_rule = DB::getInstance()->getRow($sql);
		$price = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
		if ($price <= $price_rule['price'])
			return true;
		return false;
	}

	public function verifCartLowerThanAmountBrand($action, $priceByManuf)
	{	
		$sql = 'SELECT * FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` WHERE id_totautoaddtocart = "'.$action['id_totautoaddtocart'].'"'; 
		$price_rule = DB::getInstance()->getRow($sql);
		$manufacturer_id = Tools::substr($price_rule['onproduct_id'], 1);
		if (!array_key_exists($manufacturer_id, $priceByManuf) || $priceByManuf[$manufacturer_id] < $price_rule['price'])
			return true;
		return false;
	}

	/**
	* Add product with price
	* @version 1.4.2
	* @global object Cookie User informations
	* @param int Price price in cart
	*/
	private function productWithPrice($price)
	{
		global $cookie;

		$SQL = '
			SELECT au.`id_product`, au.`id_totautoaddtocart`, a.`product_deleted`
			FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` AS au
			LEFT OUTER JOIN `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` AS a
				ON 
						au.`id_totautoaddtocart` = a.`id_totautoaddtocart` 
					AND (a.`product_deleted` = 0 OR a.`product_deleted` IS NULL) 
					AND a.`id_cart` = '.(int)$this->getCartId().'
			WHERE 
				au.`status` = 1 
				AND au.`type` = 3 ';

		if (version_compare(_PS_VERSION_, '1.5', '>'))
			$SQL .= ' '.Shop::addSqlRestriction().' ';

			// If you require a product for the price
		$res = DB::getInstance()->ExecuteS($SQL);

		if (count($res))
		{
			foreach ($res as $product)
			{
				// && !is_array($product['product_deleted'] == 0)
				if (!in_array($product['id_product'], $this->list) && $product['product_deleted'] == 0)
					$this->gestionStock($product);
			}
		}
	}

	private function getCartId()
	{
		if (version_compare(_PS_VERSION_, '1.5', '<'))
			global $cart;
		else
			$cart = $this->context->cart;

		return $cart->id;
	}

	/**
	* Add product with Manuf
	* @version 1.4.2
	* @global object Cookie User informations
	* @param int Price price in cart
	*/
	private function productWithManuf($price, $cart_quantity)
	{
		global $cookie;
			// If you require a product for the price
		$SQL = '
			SELECT au.`id_product`, au.`id_totautoaddtocart`, au.`add_quantity`, au.`onproduct_id`, au.`price`, a.`product_deleted`
			FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` AS au
			LEFT OUTER JOIN `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` AS a
				ON 
						au.`id_totautoaddtocart` = a.`id_totautoaddtocart` 
					AND (a.`product_deleted` = 0 OR a.`product_deleted` IS NULL) 
					AND a.`id_cart` = '.(int)$this->getCartId().'
			WHERE 
				au.`status` = 1 
				AND au.`type` = 4 ';

		if (version_compare(_PS_VERSION_, '1.5', '>'))
			$SQL .= ' '.Shop::addSqlRestriction().' ';

		$res = DB::getInstance()->ExecuteS($SQL);

		if (count($res))
		{
			foreach ($res as $product)
			{
				// && !is_array($product['product_deleted'] == 0)
				if (isset($price[Tools::substr($product['onproduct_id'], 1)]) 
					&& $price[Tools::substr($product['onproduct_id'], 1)] >= $product['price'] 
					&& !in_array($product['id_product'], $this->list)
					&& $product['product_deleted'] == 0)
					$this->gestionStock($product, $cart_quantity);
			}
		}
	}

	/**
	* Managing change in stock
	* @version 1.4.2.1
	* @param array SQL Query result
	* @param obj Cookie User infos
	* @param obj Cart cart infos
	* @param int Quantity required
	*/
	private function gestionStock($product, $qty = 1)
	{
		// IF 1.5
		if (version_compare(_PS_VERSION_, '1.5', '>'))
		{
			$cart = $this->context->cart;
			$cookie = $this->context->cookie;
		} 
		else
			global $cart, $cookie;

		$last_product = $cart->getLastProduct();
		$attribute = Product::getDefaultAttribute($product['id_product']);

		$session = isset($cookie->gift) ? $cookie->gift : '';
			
			// We check whether the query you want to add the amount
		$qty = isset($product['add_quantity']) && $product['add_quantity'] == 1 ? $qty : 1;

			// It updates the quantity
		$cart->updateQty($qty, $product['id_product'], $attribute);
			// It places the product in the list
		$this->list[] = $product['id_product'];

		DB::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.Tools::strtolower($this->name).'_action` (id_cart,id_totautoaddtocart,id_trigger_product,quantity,date,id_product_attribute, product_deleted) VALUES ('.(int)$cart->id.', '.(int)$product['id_totautoaddtocart'].', '.(int)$last_product['id_product'].', '.(int)$qty.',
        NOW(), '.(int)$last_product['id_product_attribute'].', '.'0'.')');
	}

	/**
	* BO : Display list
	* @version 1.4.2
	* @global obj cookie User infos
	* @global string Admin url
	* @return string 
	*/
	private function viewList()
	{
			global $cookie, $currentIndex;
			$var = '
				<h2 style="text-align:center;">'.$this->displayName.'</h2>
				<fieldset class="width6">
					<legend><img src="'.$this->_path.'logo.gif">'.$this->l('Auto add product list').'</legend>
					<a style="padding:3px;float:right;margin-bottom:10px;" class="button btn btn-default" href="'.$currentIndex.'	&configure=totautoaddtocart&token='.Tools::getValue('token').'&tab_module=totautoaddtocart&module_name=totautoaddtocart&act=add" >'.$this->l('+ Add').'</a>
					<table style="width:100%;" class="table tableDnD" border="1">
						<tr class="nodrag nodrop">
							<th>'.$this->l('Auto add product').'</th>
							<th>'.$this->l('Trigger').'</th>
							<th style="width:50px;">'.$this->l('Enable/Disable').' </th>
							<th style="width:40px;"></th>
						</tr>';
			$SQL = 'SELECT * FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` WHERE 1 ';

			if (version_compare(_PS_VERSION_, '1.5', '>'))
				$SQL .= ' '.Shop::addSqlRestriction().' ';

			$res = DB::getInstance()->ExecuteS($SQL);
			if ($res !== false && count($res))
			{
				foreach ($res as $gifts)
				{
					$gift = new Product($gifts['id_product']); // Gift product
					$explode = explode('-', Tools::substr($gifts['onproduct_id'], 1));
					unset($explode[count($explode) - 1]);
					$more = $this->l('Nothing');
					// If you want to add a product
					if ($gifts['type'] == 1)
						$more = $this->l('With the first product added');

					else if ($gifts['type'] == 2)
					{
						$more = $this->l('Product(s)').' : ';
						$i = 1;
						foreach ($explode as $value)
						{
							$products = new Product($value); // Product in which appear the gift
							$more .= $products->name[$cookie->id_lang];
							if ($i < count($explode))
								$more .= ', ';
							$i++;
						}
					}
					else if ($gifts['type'] == 3)
					{ 
					// If you want the added with a price
						$more = $this->l('Price').' : '.$gifts['price'];
					}
					else if ($gifts['type'] == 4)
					{ 
					// If you want the added with a price
						$manufacturer = new Manufacturer(Tools::substr($gifts['onproduct_id'], 1));
						$more = $this->l('Manufacturer').' : '.$manufacturer->name;
					}

					$status = $gifts['status'] == 1 ? 'enable' : 'disable'; // Display icon statut

					$var .= '
						<tr>
							<td>'.$gift->name[$cookie->id_lang].'</td>
							<td>'.$more.'</td>
							<td style="text-align:center;"><a href="'.$currentIndex.'&configure=totautoaddtocart&token='.Tools::getValue('token').'		&tab_module=totautoaddtocart&module_name=totautoaddtocart&action_status=status&id='.$gifts['id_totautoaddtocart'].'&status='.$gifts['status'].'"><img src="..	/img/admin/'.$status.'d.gif" alt="" title="'.$this->l(Tools::ucfirst($status)).'"/></a></td>
							<td style="width:40px;text-align:center;"><a href="'.$currentIndex.'&configure=totautoaddtocart&token='.Tools::getValue('token').'	&tab_module=totautoaddtocart&module_name=totautoaddtocart&act=edit&id='.$gifts['id_totautoaddtocart'].'"><img src="../img/admin/edit.gif" alt="" title="'.$this->l('Edit').'" /></a><a href="'.$currentIndex.'&configure=totautoaddtocart&token='.Tools::getValue('token').'	&tab_module=totautoaddtocart&module_name=totautoaddtocart&act=delete&id='.$gifts['id_totautoaddtocart'].'"><img src="../img/admin/delete.gif" alt="" title="'.$this->l('Delete').'"/></a></td>
						</tr>
					';
				}
			}
			else
				$var .= '<tr><td colspan="4" style="text-align:center;">'.$this->l('Nothing').'</td></tr>';
			
			$var .= '
					</table>
				</fieldset>
				<br />
				<fieldset class="width6">
					<legend><img src="'.$this->_path.'logo.gif">'.$this->l('Help').'</legend>
						- '.$this->l('Clic "+ Add" to configure a new triggered addition to basket : choose product to be added (called "Auto add product"), then define trigger.').'<br />
						- '.$this->l('This module does not add vaucher code to basket.').'<br /><br />
						'.$this->displayName.' '.$this->l('by').' <a style="text-decoration:underline;" href="http://www.202-ecommerce.com/'.$this->name.'?sourceid=mod&lang='.(int)$cookie->id_lang.'" target="_blank">202-ecommerce</a><br />
						'.$this->l('Thinking about entering French market ? Contact us for local SEO.').'
				</fieldset>
							';

			return $var;
	}

	/**
	* Add / Edit / Delete / Change statut
	* @version 1.4.2
	* @return string 
	*/
	private function preProcess()
	{
		if (Tools::getValue('act') && (Tools::getValue('act') == 'add' || Tools::getValue('act') == 'edit') && Tools::getValue('btnAdd'))
		{
			// If form is ok
			if (Tools::getValue('gift') && Tools::getValue('type'))
			{
				if (Tools::getValue('type') == 2)
				{// Si on choisit un produit
					if (!Tools::getValue('inputAccessories'))
						return parent::displayError($this->l('Thank you for filling out the form correctly.'));

					else
						$_POST['price'] = 0;
				}
				elseif (Tools::getValue('type') == 3)
				{// Si on choisit le prix
					if (!Tools::getValue('price'))
						return parent::displayError($this->l('Thank you for filling out the form correctly.'));

					$_POST['inputAccessories'] = 0;
				}
				else if (Tools::getValue('type') == '4')
				{
					$_POST['price'] = (int)Tools::getValue('price_manufacturer');
					$_POST['inputAccessories'] = (int)Tools::getValue('manufacturer');
				}
				$SQL = 'INSERT INTO `'._DB_PREFIX_.Tools::strtolower($this->name).'` SET 
                  `id_product` = '.(int)Tools::getValue('gift').', 
                  `type` = '.(int)Tools::getValue('type').', 
                  `price` = '.(int)Tools::getValue('price').', 
                  `onproduct_id` =  \'-'.(int)Tools::getValue('inputAccessories').'-\', 
                  `date_created` = '.(int)time().', 
                  `add_quantity` = '.(int)(Tools::getValue('addQuantity') && Tools::getValue('addQuantity') == 'on' ? true : false).',
                  `status` = '.(int)(boolean)Tools::getValue('status').'  ';
				if (version_compare(_PS_VERSION_, '1.5', '>'))
					$SQL .= ', `id_shop` = '.(int)$this->context->shop->id.', `id_shop_group` =  '.(int)$this->context->shop->id_shop_group.' ';

				if (!Tools::getValue('id') && DB::getInstance()->Execute($SQL))
				{
					unset($_GET['act']);
					Tools::redirectAdmin(html_entity_decode($this->link).'&conf=3');
				}
				else if (Tools::getValue('id') && DB::getInstance()->Execute('UPDATE `'._DB_PREFIX_.Tools::strtolower($this->name).'` SET 
					`id_product` = '.(int)Tools::getValue('gift').', 
					`type` = '.(int)Tools::getValue('type').', 
					`price` = '.(float)Tools::getValue('price').', 
					`onproduct_id` = \'-'.Tools::getValue('inputAccessories').'\', 
					`date_created` = '.(int)time().', 
					`add_quantity` = '.(int)(Tools::getValue('addQuantity') && Tools::getValue('addQuantity') == 'on' ? true : false).', 
					`status` = '.(Tools::getValue('status') == 'on' ? 1 : 0).
					' WHERE `id_totautoaddtocart` = '.(int)Tools::getValue('id').'  '))
				{
					unset($_GET['act']);
					Tools::redirectAdmin(html_entity_decode($this->link).'&conf=4');
				}
				else
					return parent::displayError($this->l('Impossible to add or edit your configuration product.'));
			}
			else
				return parent::displayError($this->l('Thank you for filling out the form correctly.'));
		}
		else if (Tools::getValue('act') && Tools::getValue('act') == 'delete')
		{
			$MySQLQuery = 'DELETE FROM `'._DB_PREFIX_.'totautoaddtocart` WHERE 
			`id_totautoaddtocart` = '.(int)Tools::getValue('id').' ';
			if (DB::getInstance()->Execute($MySQLQuery))
			{
				unset($_GET['act']);
				return parent::displayConfirmation($this->l('This auto add product has been deleted'));
			}
			else
				return parent::displayError($this->l('Impossible to delete this auto add product'));
		}
		else if (Tools::getValue('action_status') == 'status' && Tools::getValue('id'))
		{
			$status = (Tools::getValue('status') == 1) ? 0 : 1;
			$MySQLQuery = 'UPDATE `'._DB_PREFIX_.Tools::strtolower($this->name).'` SET 
			`status` = "'.(int)$status.'"  WHERE `id_totautoaddtocart` = "'.(int)Tools::getValue('id').'" ';
			if (DB::getInstance()->Execute($MySQLQuery))
			{
				unset($_GET['act']);
				Tools::redirectAdmin(html_entity_decode($this->link).'&conf=4');
			}
			else
				return parent::displayError($this->l('Impossible to update the auto add product'));
		}
	}

	/**
	* BO : Add / Edit a new automatic addition
	* @version 1.2
	* @global string URI admin
	* @param int $id ID gift
	* @return string 
	*/
	public function Add($id = null)
	{
		global $currentIndex, $cookie;

		$var = '
			<h2 style="text-align:center;">'.$this->l('Auto add product').'</h2>
			<fieldset class="width6">
				<legend><img src="'.$this->_path.'logo.gif">'.$this->l('Add auto add product').'</legend>';
		$id = $id_product = $productid = $type = $price = $onproduct_id = '';
		$show = ' style="display:none;" ';
		$hide = ' style="display:block;" ';
		$selected1 = $selected2 = $selected3 = $selected4 = '';
		$addQuantity = $status = false;
		$show1 = $show2 = $show3 = $show4 = ' style="display:none;" ';
		$more = $name = '';

		if (Tools::getValue('id'))
		{
			// SQL Query for get gifts
			$MySQLQuery = '
				SELECT * 
				FROM `'._DB_PREFIX_.Tools::strtolower($this->name).'` 
				WHERE `id_totautoaddtocart` = '.(int)Tools::getValue('id').' ';
						// Exec
			$res = DB::getInstance()->ExecuteS($MySQLQuery);
			extract($res[0]);
						// Create product
			$gift = new Product($id_product, false, $cookie->id_lang);
			$productid = $gift->name.' (ref: '.$gift->reference.') <span onclick="delGift('.$id_product.');" style="cursor: pointer;"><img src="../img/admin/delete.gif"></span><br />';
			$selected1 = ($type == 1) ? ' selected ' : '';
			$selected2 = ($type == 2) ? ' selected ' : '';
			$selected3 = ($type == 3) ? ' selected ' : '';
			$selected4 = ($type == 4) ? ' selected ' : '';
			$addQuantity = ($add_quantity == 0 ? false : true);
			$show1 = ($type == 1) ? '' : ' style="display:none;" ';
			$show2 = ($type == 2) ? '' : ' style="display:none;" ';
			$show3 = ($type == 3) ? '' : ' style="display:none;" ';
			$show4 = ($type == 4) ? '' : ' style="display:none;" ';
			$show = ' style="display:block;" ';
			$hide = ' style="display:none;" ';
			$explode = explode('-', Tools::substr($onproduct_id, 1));
			if (count($explode))
			{
				unset($explode[count($explode) - 1]);
				foreach ($explode as $value)
				{
					$products = new Product($value, false, $cookie->id_lang); // Produit dans lequel apparaîtra le cadeau
					$more .= $products->name.' (ref: '.$products->reference.') <span onclick="delAccessory('.$value.');" style="cursor: pointer;"><img src="../img/admin/delete.gif"></span><br />';
					$name .= $products->name.' (ref: '.$products->reference.') ¤';
				}
			}
		}

		if (version_compare(_PS_VERSION_, '1.5', '<'))
			$var .= $this->JS14();
		else
			$var .= $this->JS15orMore($id_product > 0 ? $id_product : -1);

		$var .= '
				<form method="post">
					<table style="width:100%;"  valign="middle">
						<tr>
							<td style="width:120px;">
								<label class="t" for="giftId">'.$this->l('Auto add product').'</label>
							</td>
							<td>
								<div id="choix"'.$show.'>'.$productid.'</div>
								<div id="choose"'.$hide.'>
									<input type="hidden" name="gift" id="gift" value="'.$id_product.'" />
									<input type="text" name="giftId" id="giftId" placeholder="'.$this->l('Product').'"/>
									<script type="text/javascript">
										$(function()
										{
											$("#giftId")
											.autocomplete("ajax_products_list.php?excludeIds=-1&",
											{
												delay: 100,
												minChars: 1,
												autoFill: true,
												max:20,
												matchContains: true,
												mustMatch:true,
												scroll:false,
												cacheLength:0,
												multipleSeparator:"||",
												formatItem: function(item)
												{
													return item[0];
												}
											}).result(function(event, item){
												$("#gift").val(item[1]);
											});
										});
										'.$this->addPackItem().'
										'.$this->delPackItem().'
									</script>
									<span onclick="addGift();" style="cursor: pointer;"><img src="../img/admin/add.gif" alt="'.$this->l('Add an item to the pack').'" title="'.$this->l('Add an item to the pack').'" /></span>
								</div>
							</td>
						</tr>

						<tr>
							<td><label class="t" for="typeGift">'.$this->l('Trigger').'</label></td>
							<td>
								<select name="type" id="typeGift" onChange="changeType(this.value);">
									<option value="1"'.$selected1.'>'.$this->l('First product added to cart').'</value>
									<option value="2"'.$selected2.'>'.$this->l('Specific product added to cart').'</value>
									<option value="3"'.$selected3.'>'.$this->l('Basket amount over threshold').'</value>
									<option value="4"'.$selected4.'>'.$this->l('Choose manufacturer and price').'</value>
								</select>
								<script type="text/javascript">
									function changeType(value){
										$(".trtype2").slideUp();
										$(".trtype3").slideUp();
										$(".trtype4").slideUp();
										$(".trtype"+value+"").slideDown().addClass("displayed");
									}
								</script>
							</td>
						</tr>
						<tr>
							<td>
								<div class="trtype2" '.$show2.'><label class="t">'.$this->l('Added product').'</label></div>
								<div class="trtype3" '.$show3.'><label class="t">'.$this->l('Basket threshold').'</label></div>
								<div class="trtype4" '.$show4.'><label class="t">'.$this->l('Added product').'</label></div>
							</td>
							<td id="viewmore">
								<div class="trtype2" '.$show2.'>
									<div id="divAccessories">'.$more.'</div>
                                             <input type="hidden" name="inputAccessories" id="inputAccessories" value="'.($type == 2 ? str_replace('--', '-', Tools::substr($onproduct_id, 1).'-') : '').'" />
									<input type="hidden" name="nameAccessories" id="nameAccessories" value="'.$name.'" />
									<script type="text/javascript">
										var formProduct;
										var accessories = new Array();
									</script>
									
									<div id="ajax_choose_product" style="padding:6px; padding-top:2px; width:600px;">
										<p class="clear">'.$this->l('Write the first letters of the name of the product, then choose the product from the dropdown').'</p>
										<input type="text" value="" id="product_autocomplete_input"/>';
										if (Tools::substr(_PS_VERSION_, 0, 3) == '1.4')
											$var .= '<img onclick="$(this).prev().search();" style="cursor: pointer;" src="../img/admin/add.gif" alt="Ajouter un accessoire" title="Ajouter un 		accessoire" />';
										$var .= '
									</div>
								</div>
								<div class="trtype3" '.$show3.'>
									<input type="text" name="price" value="'.$price.'" placeholder="'.$this->l('Price').'" />
								</div>
								<div class="trtype4" '.$show4.'>
									<select name="manufacturer">';
									$manufacturers = Manufacturer::getManufacturers();
									foreach ($manufacturers as $manufacturer)
										$var .= '<option value="'.$manufacturer['id_manufacturer'].'"'.($type == 4 && Tools::substr($onproduct_id, 1) == $manufacturer['	id_manufacturer'] ? ' selected' : '' ).'>'.$manufacturer['name'].'</option>';

						$var .= '                          </select>
									<br />
									<input type="text" name="price_manufacturer" value="'.$price.'" placeholder="'.$this->l('Price').'" />
								</div>
							</td>
						</tr>
						<tr class="trtype2" '.$show2.'>
							<td>
								<label class="t" for="addQuantity">'.$this->l('Add product quantity').'</label>
							</td>
							<td>
								<input type="checkbox" id="addQuantity" name="addQuantity" '.($addQuantity ? 'checked ' : '').'/>
							</td>
						</tr>
						<tr>
							<td>
								<label class="t" for="status">'.$this->l('Active').'</label>
							</td>
							<td>
								<input type="checkbox" id="status" name="status" '.($status ? 'checked ' : '').'/>
							</td>
						</tr>
						<tr>
							<td colspan=2><br />
								<input type="button" class="button btn btn-default" value="'.$this->l('Cancel').'" onClick="document.location.href=\''.$currentIndex.'&configure=totautoaddtocart&token='.Tools::getValue('token').'&tab_module=totautoaddtocart&module_name=totautoaddtocart\'"> 
								<input type="submit" class="button btn btn-default" name="btnAdd" value="'.$this->l('Save').'" />
							</td>
						</tr>
					</table>
                </form>
			</fieldset>';
		return $var;
	}

	/**
	* BO : Automatic addition of the product desired
	* @version 1.0
	* @return string 
	*/
	private function addPackItem()
	{
		return '
		function addGift(){
			if( $("#gift").val() != "" ){
				var lineDisplay = $("#gift").val() + " - " + $("#giftId").val();
				$("#choix").html(lineDisplay + \'<span onclick="delGift(\' + $(\'#gift\').val() + \');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />\');
				$("#choix").slideDown();
				$("#choose").slideUp();
			}
		}';
	}

	/**
	* BO : Automatic removal of the product
	* @version 1.0
	* @return string 
	*/
	private function delPackItem()
	{
			return '
			function delGift(){
				$("#gift").val("");
				$("#giftId").val("");
				$("#choix").html("");
				$("#choix").slideUp();
				$("#choose").slideDown();
			}';
	}

	/**
	* Javascript for 1.5
	* @version 1.1
	* @return string 
	*/
	private function JS15orMore($id_product)
	{
		$this->context->controller->addJqueryPlugin('autocomplete');
			$var = '
				<script type="text/javascript">
					urlToCall = null;
					/* function autocomplete */
					$(function()
					{
						$("#product_autocomplete_input")
						.autocomplete("ajax_products_list.php",
						{
							minChars: 1,
							autoFill: true,
							max:20,
							matchContains: true,
							mustMatch:true,
							scroll:false,
							cacheLength:0,
							formatItem: function(item)
							{
								return item[1]+" ¤ "+item[0];
							}
						}).result(addAccessory);

									$("#product_autocomplete_input").setOptions({
										extraParams:
										{excludeIds : getAccessorieIds()}
									});
					});
	
					function addAccessory(event, data, formatted)
					{
						if (data == null)
							return false;
						var productId = data[1];
						var productName = data[0];
					
						var $divAccessories = $("#divAccessories");
						var $inputAccessories = $("#inputAccessories");
						var $nameAccessories = $("#nameAccessories");
					
						/* delete product from select + add product line to the div, input_name, input_ids elements */
						$divAccessories.html($divAccessories.html() + productName + " <span onclick=\"delAccessory(" + productId + ");\" style=\"cursor: pointer;\"><img src=\"../img/admin/delete.gif\" /></span><br />");
						$nameAccessories.val($nameAccessories.val() + productName + "¤");
						$inputAccessories.val($inputAccessories.val() + productId + "-");
						$("#product_autocomplete_input").val("");
						$("#product_autocomplete_input").setOptions({
							extraParams:
							{excludeIds : getAccessorieIds()}
						});
					}

					function getSelectedIds()
					{
						// input lines QTY x ID-
						var ids = '.$id_product.'+\',\';
						ids += $(\'#inputPackItems\').val().replace(/\\d+x/g, \'\').replace(/\-/g,\',\');
						ids = ids.replace(/\,$/,\'\');

						return ids;

					}

					function delAccessory(id)
					{
						var div = getE(\'divAccessories\');
						var input = getE(\'inputAccessories\');
						var name = getE(\'nameAccessories\');

						// Cut hidden fields in array
						var inputCut = input.value.split(\'-\');
						var nameCut = name.value.split(\'¤\');

						if (inputCut.length != nameCut.length)
							return alert(\'Bad size\');

						// Reset all hidden fields
						input.value = \'\';
						name.value = \'\';
						div.innerHTML = \'\';
						for (i in inputCut)
						{
							// If empty, error, next
							if (!inputCut[i] || !nameCut[i])
								continue ;

							// Add to hidden fields no selected products OR add to select field selected product
							if (inputCut[i] != id)
							{
								input.value += inputCut[i] + \'-\';
								name.value += nameCut[i] + \'¤\';
								div.innerHTML += nameCut[i] + \' <span onclick="delAccessory(\' + inputCut[i] + \');" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />\';
							}
							else
								$(\'#selectAccessories\').append(\'<option selected="selected" value="\' + inputCut[i] + \'-\' + nameCut[i] + \'">\' + inputCut[i] + \' - \' + nameCut[i] + \'</option>\');
						}

						$(\'#product_autocomplete_input\').setOptions({
							extraParams:
							{excludeIds : getAccessorieIds()}
						});
					}

					function getAccessorieIds()
					{
						var ids = '.$id_product.'+\',\';
						ids += $(\'#inputAccessories\').val().replace(/\\-/g,\',\').replace(/\\,$/,\'\');
						ids = ids.replace(/\,$/,\'\');
						console.log(ids);
						return ids;
					}
				</script>';
			return $var;
	}

	/**
	* Javascript for 1.4
	* @version 1.4.2
	* @return string 
	*/
	private function JS14()
	{
		$var = '
		<link rel="stylesheet" type="text/css" href="../css/jquery.autocomplete.css" />
		<script type="text/javascript" src="../js/jquery/jquery.autocomplete.js"></script>
		<script type="text/javascript">
			urlToCall = null;
			/* function autocomplete */
			$(function()
			{
				$("#product_autocomplete_input")
				.autocomplete("ajax_products_list.php?excludeIds=-1",
				{
					minChars: 1,
					autoFill: true,
					max:20,
					matchContains: true,
					mustMatch:true,
					scroll:false,
					cacheLength:0,
					formatItem: function(item)
					{
						return item[1]+" ¤ "+item[0];
					}
				}).result(addAccessory);
			});

                  function getAccessorieIds()
                 
                 {
                    var ids;
                    ids += $("#inputAccessories").val().replace(/\-/g,",").replace(/\,$/,"");
                    ids = ids.replace(/\,$/,"");

                    return ids;
                  }
		</script>';
		return $var;
	}

}

?>