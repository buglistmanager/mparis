<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_b47d4038bc9e897796571917fc9a99c4'] = 'Ajout de produit au panier';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_99a04da048d840f06605e0759630a882'] = 'Ajout automatique de produits au panier';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_0f16a4733b92853bfcf8b1aa58d2f1cc'] = 'Le module n\'est pas activer';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_53e5aa2c97fef1555d2511de8218c544'] = 'Par';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_fadb4bfb725434ec7543ec47ab00e52e'] = 'Agence web spécialisée dans la création de site d\'e-commerce';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_f2cacc583ed117c4484f476fdacbc0ad'] = 'Nos modules sur addons ';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_0a9d06a1d889174d9b26b343b2f23e5e'] = 'Nouveautés & conseils sur notre blog';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_6357d3551190ec7e79371a8570121d3a'] = 'Il y a';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_4ce81305b7edb043d0a7a5c75cab17d0'] = 'Il y a';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_3879149292f9af4469cec013785d6dfd'] = 'avertissements';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_7b83d3f08fa392b79e3f553b585971cd'] = 'avertissement';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_8a3cfd894d57e33c55400fc9d76aa08a'] = 'Click ici pour en savoir plus';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_a92269f5f14ac147a821728c23204c0b'] = 'Cacher les avertissements';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_08ecb494f6ad9ea52749162fd66e1c06'] = 'Liste des ajouts automatiques';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_111e7bf6f4dbfcf3ce8e5e8348e1f5f4'] = '+ Ajouter';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_30244696ffaf6b43b1090b773d131114'] = 'Produit à ajouter';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_f698f67f5666aff10729d8a1cb1c14d2'] = 'Déclencheur';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_e566fe9aef1502d69ccdbe28e1957535'] = 'Actif ?';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_f80a4ad87fee7c9fdc19b7769495fdb5'] = 'Aucun ajout automatique';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_29e17815968f9d1ed6faf42f65b0d651'] = 'Premier produit ajouté';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_0b2b18f550826ea6412e8ab61a719afd'] = 'Produit(s)';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_3601146c4e948c32b6424d2c0a7f0118'] = 'Montant';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Marque';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_7dce122004969d56ae2e0245cb754d35'] = 'Modifier';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_f2a6c498fb90ee345d997f888fce3b18'] = 'Supprimer';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_6a26f548831e6a8c26bfbbd9f6ec61e0'] = 'Aide';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_e14c53f2b8319b137ab6f6461cb099b4'] = 'Cliquez sur \"+ Ajouter\" pour créer un nouvel ajout automatique : choisissez le produit à ajouter, puis le déclencheur.';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_81474b8f6798f917774c40cf51367943'] = 'Ce module n\'ajoute pas automatiquement de code de réduction';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_df3f079de6961496f0460dcfdbf9bca3'] = 'par';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_da0928eae10c0fd35d0b0d746ce0c058'] = ' ';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_62e54d5c70ec83b89b7f5732bfdcc26b'] = 'Merci de corriger le formulaire';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_b142671344cffd3f879f3313cd357d02'] = 'Impossible d\'ajouter ou modifier la configuration';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_85c31c707bbbfa42d38ca7d8b619d7cb'] = 'Cette configuration a été supprimée';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_ed9e19338a8f72d8fb5025180c30cb4b'] = 'Impossible de supprimer cette configuration';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_82c92be43816048df83ee4b2c736c9f6'] = 'Impossible de mettre à jour';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_2f72dece1e18a6869c2e2449a68d40fb'] = 'Configuration d\'un ajout automatique au panier';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_deb10517653c255364175796ace3553f'] = 'Produit';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_c7a915a59b56dc898c0ae52b64bed0c5'] = 'Ajouter un produit';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_7925250b598c5bc0c648cc2b8d47c623'] = 'Premier produit ajouté au panier';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_d9e21c0aa19898b81749415520febfbb'] = 'Produit spécifique ajouté au panier';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_218e887f231a1070166ee127be446222'] = 'Montant du panier au dessus du seuil';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_c9a9f85a007759a017d1a6ead4a49434'] = 'Choisir une marque et un prix';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_8c2e40e794c226a228e0c7f69e8e917a'] = 'Produit déclencheur';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_858f0a157c49c290de0734d795717e63'] = 'Montant du panier';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_5c4b2402fe8fcee1efb5ae6bd5bbc701'] = 'Tapez les premières lettres du nom du produit, puis choisissez le produit dans la liste déroulante';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_2c6437f2c7e2a8b53c3ab391237561da'] = 'Ajout une quantité produit';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Actif';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_ea4788705e6873b424c65e91c2846b19'] = 'Annuler';
$_MODULE['<{totautoaddtocart}prestashop>totautoaddtocart_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
