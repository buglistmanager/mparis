{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div style="border: 1px solid #dfd5c3;margin-bottom:10px;">
	<table style="width:100%;line-height:16px;">
		<tr>
			<td style="padding: 11px 0px;max-width: 40%;">
				<div style="height:32px;float: left;padding:0 11px;">
					<img src="{$module._path|escape:'UTF-8'}logo.gif" alt="" />
				</div>
				<div>
					<span style="font-family: Arial;font-weight: bold;color: #669900;font-size: 14px;width: 413px;height: 28px;text-align: left;vertical-align: bottom;text-decoration: none;">{$module.displayName|escape:'UTF-8'}</span>
					<br />{$module.description|escape:'UTF-8'}
				</div>
			</td>
			<td style="max-width: 40%;">
				{$translations.by|escape:'UTF-8'} <a href="http://www.202-ecommerce.com/" target="_blank" style="font-family: Arial;font-weight: bold;color: #669900;font-size: 14px;width: 413px;height: 28px;text-align: left;vertical-align: bottom;text-decoration: none;">202-ecommerce</a>
				<br /> {$translations.web|escape:'UTF-8'}
			</td>
			<td style="max-width: 40%;">
				<a href="http://addons.prestashop.com/{$lang->iso_code|escape:'UTF-8'}/27_202-ecommerce" target="_blank">{$translations.addons|escape:'UTF-8'}</a>
				<br /><a href="http://www.202-ecommerce.com/" target="_blank">{$translations.blog|escape:'UTF-8'}</a>
			</td>
		</tr>
	</table>
</div>