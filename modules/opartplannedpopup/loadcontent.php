<?php
/**
 * Module opartplannedpopup
 *
 * @category Prestashop
 * @category Module
 * @author    Olivier CLEMENCE <manit4c@gmail.com>
 * @copyright Op'art
 * @license   Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
$context = Context::getContext();
$return = array();
if (Tools::getIsset('optoken') && Tools::getValue('optoken') != '') {
    $token = Tools::getToken(_PS_MODULE_DIR_."opartplannedpopup/loadcontent.php");
    if (Tools::getValue('optoken') == $token) {

        if (Tools::getIsset('type') && Tools::getValue('type') == 'cms')
        {
            $cms = new CMS(Tools::getValue('id'), (int)$context->language->id);
            echo $cms->content;
        }
        else if (Tools::isSubmit('submitNewsletter'))
        {
            include(dirname(__FILE__).'/../blocknewsletter/blocknewsletter.php');
            $block_newsletter = new Blocknewsletter();
            $block_newsletter->hookDisplayLeftColumn(null);
            $return['erreur'] = $block_newsletter->error;
            $return['valid'] = $block_newsletter->valid;
            echo Tools::jsonEncode($return);
        }
        else if (Tools::getValue('type') == 'newsletter')
        {
            $context = Context::getContext();

            if (_PS_VERSION_ >= '1.7') {
                $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
                $context->smarty->assign(array('base_dir_ssl' => $protocol_link.Tools::getShopDomainSsl().__PS_BASE_URI__));
            }

            $tpl = dirname(__FILE__).'/views/templates/front/blocknewsletter.tpl';
            $context->smarty->display($tpl);
        }
        else if (Tools::getIsset('type') && Tools::getValue('type') == 'classic')
        {
            $context = Context::getContext();
            $sql = '
	SELECT o.id_opartplannedpopup,ol.code
	FROM '._DB_PREFIX_.'opartplannedpopup o
	LEFT JOIN '._DB_PREFIX_.'opartplannedpopup_lang ol ON (o.id_opartplannedpopup=ol.id_opartplannedpopup)
	WHERE ol.id_lang = '.(int)$context->language->id.' AND o.id_opartplannedpopup = '.(int)Tools::getValue('id');

            if (!$result = Db::getInstance()->getRow($sql)) $erreur = 'Nothing';
            else
            {
                /*print_r($result);*/
                echo $result['code'];
            }
        }
        else
            echo 'got problem2';
    }
} else {
    echo "got problem1";
}
?>