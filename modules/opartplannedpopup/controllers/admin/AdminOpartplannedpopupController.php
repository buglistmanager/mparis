<?php
/**
 * Module opartplannedpopup
 *
 * @category Prestashop
 * @category Module
 * @author    Olivier CLEMENCE <manit4c@gmail.com>
 * @copyright Op'art
 * @license   Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
 */

require_once(_PS_MODULE_DIR_.'opartplannedpopup/models/PlannedPopup.php');

class AdminOpartplannedpopupController extends ModuleAdminController
{

	public function __construct()
	{
        if (_PS_VERSION_ >= '1.7') {
            $this->module = Module::getInstanceByName('opartplannedpopup');
        }
		$this->bootstrap = true;
		$this->table = 'opartplannedpopup';
		$this->className = 'PlannedPopup';
		$this->lang = true;
		$this->deleted = false;
		$this->colorOnBackground = false;
		$this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
		$this->context = Context::getContext();
		$this->override_folder = _PS_MODULE_DIR_.'opartplannedpopup/views2/';
		parent::__construct();
	}

    /**
     * Translate function fix on prestashop 1.7 controller
     * @param $string
     * @param null $class
     * @param bool $addslashes
     * @param bool $htmlentities
     * @return mixed
     */
    protected function l($string, $class = null, $addslashes = false, $htmlentities = true)
    {
        if (_PS_VERSION_ >= '1.7') {
            return Context::getContext()->getTranslator()->trans($string);
        } else {
            return parent::l($string, $class, $addslashes, $htmlentities);
        }
    }

	/**
	 * Function used to render the list to display for this controller
	 */
	public function renderList()
	{
		$this->addRowAction('edit');
		$this->addRowAction('delete');

		$this->bulk_actions = array(
			'delete' => array(
				'text' => $this->l('Delete selected'),
				'confirm' => $this->l('Delete selected items?')
			)
		);
		$this->fields_list = array(
			'id_opartplannedpopup' => array(
				'title' => $this->l('ID'),
				'align' => 'center',
				'width' => 25
			),
			'title' => array(
				'title' => $this->l('Title'),
				'width' => 'auto',
			),
			'width' => array(
				'title' => $this->l('Width'),
				'width' => 'auto',
			),
			'height' => array(
				'title' => $this->l('Height'),
				'width' => 'auto',
			)
		);
		$this->context->smarty->assign('moduledir', _MODULE_DIR_.'opartplannedpopup/');
		$lists = parent::renderList();
		$html = '';
		$html .= $this->context->smarty->fetch(parent::getTemplatePath().'header.tpl');
		$html .= $lists;
		$html .= $this->context->smarty->fetch(parent::getTemplatePath().'help.tpl');
		return $html;
	}

	public function renderForm()
	{
		$this->fields_form = array(
			'tinymce' => true,
			'legend' => array(
				'title' => $this->l('Popup'),
				//'image' => '../img/admin/cog.gif',
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Title:'),
                                        'required' => true,
					'name' => 'title',
					'size' => 100
				),
				array(
					'type' => 'checkbox',
					'name' => 'use_newsletter',
					'desc' => $this->l('If you want use the newsletter module, click here.'),
					'class' => '',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('Show newsletter form'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfCms'
				),
				array(
					'type' => 'checkbox',
					'name' => 'use_cmsCheckbox',
					'desc' => $this->l('If you want use the content of a cms page, click here'),
					'class' => 'showTextField',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('Use cms page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfNewsletter'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Cms Page id:'),
					'name' => 'use_cms',
					'id' => 'use_cms_text',
					'desc' => $this->l('Add the id of your cms page'),
					'form_group_class' => 'use_cms_text',
					'field_assoc' => 'use_cms',
				),
				array(
					'type' => 'textarea',
					'lang' => true,
					'autoload_rte' => true,
					'label' => $this->l('Code:'),
					'name' => 'code',
					'rows' => 10,
					'cols' => 102,
					'form_group_class' => 'hideIfNewsletter hideIfCms'
				),
				array(
					'type' => 'select',
					'label' => $this->l('Responsive:'),
					'name' => 'responsive',
					'options' => array(
						'query' => array(
							array('key' => 0, 'name' => $this->l('no')),
							array('key' => 1, 'name' => $this->l('yes')),
						),
						'name' => 'name',
						'id' => 'key'
					),
					'desc' => $this->l('If responsive is set to "yes", height and width will be in percent')
				),
				array(
					'type' => 'text',
					'label' => $this->l('Width:'),
                                        'required' => true,
					'name' => 'width',
					'size' => 10,
					'desc' => $this->l('If responsive is set to "yes", this field must be lower or equal to 100')
				),
				array(
					'type' => 'text',
					'label' => $this->l('Height:'),
                                        'required' => true,
					'name' => 'height',
					'size' => 10,
					'desc' => $this->l('If responsive is set to "yes", this field must be lower or equal to 100. If you want that the popup height is adapted automatically set this field to 0')
				),
				array(
					'type' => 'date',
					'label' => $this->l('Start date:'),
                                        'required' => true,
					'name' => 'start_date',
				),
				array(
					'type' => 'date',
					'label' => $this->l('End date:'),
                                        'required' => true,
					'name' => 'end_date',
				),
				array(
					'type' => 'text',
					'label' => $this->l('Display time:'),
                                        'required' => true,
					'name' => 'display_time',
					'desc' => $this->l('Number of days the popup will not be displayed after the user has closed it. Put 0 for ignore this option')
				),
				array(
					'type' => 'text',
					'label' => $this->l('Time before start:'),
                                        'required' => true,
					'name' => 'time_before_start',
					'desc' => $this->l('Time before the popup opens in milisecondes.').' '.
					$this->l('Put 6000 if you want the popup is displayed after 6 secondes. Put 0 for display the popup instantly')
				),
				array(
					'type' => 'text',
					'label' => $this->l('Close time:'),
                                        'required' => true,
					'name' => 'close_time',
					'desc' => $this->l('Time during the popup will be displayed in milisecondes.').' '.
					$this->l('Put 6000 if you want the popup is closed after 6 secondes. Put 0 for popup stay displayed')
				),
				array(
					'descBefore' => $this->l('choose where the popup will be displayed'),
					'type' => 'checkbox',
					'name' => 'display_all',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on all page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					)
				),
				array(
					'type' => 'checkbox',
					'name' => 'display_home',
					'class' => 'uncheckIfAll',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on home'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfAllPage'
				),
				//cat
				array(
					'type' => 'checkbox',
					'name' => 'display_catCheckbox',
					'class' => 'showTextField uncheckIfAll',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on category page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfAllPage'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Category id:'),
					'name' => 'display_cat',
					'value' => '',
					'id' => 'display_cat_text',
					'desc' => $this->l('Add the id of each category where you want to display the popup.').' '.
						$this->l('Leave this field blank if you want to display the popup on all categories pages'),
					'form_group_class' => 'display_cat_text',
					'field_assoc' => 'display_cat',
				),
				//product
				array(
					'type' => 'checkbox',
					'name' => 'display_prodCheckbox',
					'class' => 'showTextField uncheckIfAll',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on product page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfAllPage'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Product id:'),
					'name' => 'display_prod',
					'id' => 'display_prod_text',
					'desc' => $this->l('Add the id of each product where you want to display the popup.').' '.
						$this->l('Leave this field blank if you want to display the popup on all products pages'),
					'form_group_class' => 'display_prod_text',
					'field_assoc' => 'display_prod',
				),
				//cms
				array(
					'type' => 'checkbox',
					'name' => 'display_cmsCheckbox',
					'class' => 'showTextField uncheckIfAll',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on cms page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfAllPage'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Cms page id:'),
					'name' => 'display_cms',
					'id' => 'display_cms_text',
					'desc' => $this->l('Add the id of each cms where you want to display the popup.').' '.
						$this->l('Leave this field blank if you want to display the popup on all cms pages'),
					'form_group_class' => 'display_cms_text',
					'field_assoc' => 'display_cms',
				),
				//supplier
				array(
					'type' => 'checkbox',
					'name' => 'display_supplierCheckbox',
					'class' => 'showTextField uncheckIfAll',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on supplier page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfAllPage'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Supplier id:'),
					'name' => 'display_supplier',
					'id' => 'display_supplier_text',
					'desc' => $this->l('Add the id of each supplier where you want to display the popup.').' '.
					$this->l('Leave this field blank if you want to display the popup on all supplier pages'),
					'form_group_class' => 'display_supplier_text',
					'field_assoc' => 'display_supplier',
				),
				//manufacturer
				array(
					'type' => 'checkbox',
					'name' => 'display_manufacturerCheckbox',
					'class' => 'showTextField uncheckIfAll',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on manufacturer page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfAllPage'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Manufacturer id:'),
					'name' => 'display_manufacturer',
					'id' => 'display_manufacturer_text',
					'desc' => $this->l('Add the id of each manufacturer where you want to display the popup.').' '.
						$this->l('Leave this field blank if you want to display the popup on all manufacturer pages'),
					'form_group_class' => 'display_manufacturer_text',
					'field_assoc' => 'display_manufacturer',
				),
				//order
				array(
					'type' => 'checkbox',
					'name' => 'display_orderCheckbox',
					'class' => 'showTextField uncheckIfAll',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('display on order page'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'hideIfAllPage'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Order page:'),
					'name' => 'display_order',
					'id' => 'display_order_text',
					'desc' => (_PS_VERSION_ >= '1.7') ? $this->l('Add step number of each order process where you want to display the popup. 0=cart, 1=order') : $this->l('Add step number of each order process where you want to display the popup. 0=cart 1=address 2=shipping, 3=payment'),
					'form_group_class' => 'display_order_text',
					'field_assoc' => 'display_order',
				),
				array(
					'type' => 'select',
					'label' => $this->l('Display for:'),
					'name' => 'display_for',
					'options' => array(
						'query' => array(
							array('key' => 0, 'name' => $this->l('all user')),
							array('key' => 1, 'name' => $this->l('loged user')),
							array('key' => 2, 'name' => $this->l('unloged user')),
						),
						'name' => 'name',
						'id' => 'key'
					),
				),
				//custom url
				array(
					'type' => 'text',
					'label' => $this->l('Custom url:'),
					'name' => 'custom_url',
					'size' => 125,
					'lang' => true,
					'desc' => $this->l('add here your custom url without domainName.').' '.
						$this->l('You can add multiple url separated by a comma. Leave it empty for ignore this option'),
					'form_group_class' => 'hideIfAllPage'
				),
				//advanced settings
				array(
					'descBefore' => $this->l('Advanced settings'),
					'type' => 'checkbox',
					'name' => 'show_close_ico',
					'id' => 'show_close_ico',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('Show close icone'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'advancedSettings'
				),
				array(
					'type' => 'checkbox',
					'name' => 'close_out',
					'values' => array(
						'query' => array(
							array(
								'id' => 'on',
								'name' => $this->l('Close on click out'),
								'value' => '1'
							),
						),
						'id' => 'id',
						'name' => 'name'
					),
					'form_group_class' => 'advancedSettings'
				),
				//exclude url
				array(
					'type' => 'text',
					'label' => $this->l('url to exclude:'),
					'name' => 'exclude_url',
					'size' => 125,
					'lang' => true,
					'desc' => $this->l('add here url (without domain name) which must be excluded . You can add multiple url separated by a comma.').' '.
						$this->l('Leave it empty for ignore this option<br />For example to exclude login page add "/authentification".').
						$this->l('This feature can be used only if url rewriting is activated on your shop'),
					'form_group_class' => 'advancedSettings'
				),
				array(
					'type' => 'text',
					'label' => $this->l('Css class name:'),
					'name' => 'css_class_name',
					'size' => 10,
					'desc' => $this->l('You can add a class name to personalize your popup'),
					'form_group_class' => 'advancedSettings'
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				//'class' => 'button'
			)
		);

		//get languages
		$languages = Language::getLanguages(false);
		//custom_url init
		foreach ($languages as $language) $this->fields_value['custom_url'][$language['id_lang']] = '';

		if (!($obj = $this->loadObject(true))) return;

		//attribution des valeurs
		if ($obj->id != null)
		{
			$fields_list = PlannedPopup::$definition['fields'];
			foreach (array_keys($fields_list) as $key) $this->fields_value[$key] = $obj->$key;

			//custom_url assign value
			foreach ($languages as $language)
					$this->fields_value['custom_url'][$language['id_lang']] = $obj->custom_url[$language['id_lang']];
		}
		else
		{
			//set init value for empty form
			$this->fields_value['use_cms'] = '-1';
			$this->fields_value['display_cat'] = '-1';
			$this->fields_value['display_prod'] = '-1';
			$this->fields_value['display_cms'] = '-1';
			$this->fields_value['display_supplier'] = '-1';
			$this->fields_value['display_manufacturer'] = '-1';
			$this->fields_value['display_order'] = '-1';
			$this->fields_value['display_all'] = '1';
			$this->fields_value['close_out_on'] = '1';
		}

		$html = '';
		if (version_compare(_PS_VERSION_, '1.6.0', '<')) $this->context->smarty->assign('isPs15', 'ok');

		$html .= $this->context->smarty->fetch(parent::getTemplatePath().'header.tpl');
		$html .= parent::renderForm();

		//parent::initToolbar();
		//$html .= parent::renderForm();
		$this->context->smarty->assign('moduledir', _MODULE_DIR_.'opartplannedpopup/');
		$html .= $this->context->smarty->fetch(parent::getTemplatePath().'footer-form.tpl');
		$html .= $this->context->smarty->fetch(parent::getTemplatePath().'help.tpl');
		return $html;
	}

	public function postProcess()
	{
		if (Tools::isSubmit('submitAdd'.$this->table))
		{
			$error_txt = array();
			$obj = new PlannedPopup((int)Tools::getValue('id_opartplannedpopup'));

			//title
			if (Tools::getValue('title') == '') $error_txt[] = $this->l('title field can not be empty');

			if (Tools::getValue('responsive') == 1)
			{
				if (Tools::getValue('width') > 100 || Tools::getValue('height') > 100)
						$error_txt[] = $this->l('Width and Height can not exceed 100 if you set responsive to yes');
			}
			//display cms and newsletter can not be checked at same time
			if (Tools::getValue('display_cms') == 1 && Tools::getValue('use_newsletter') == 1)
					$error_txt[] = $this->l('display cms and newsletter can not be checked at same time');

			if (Tools::getValue('start_date') == '') $error_txt[] = $this->l('start date field can not be empty');

			if (Tools::getValue('end_date') == '') $error_txt[] = $this->l('end date field can not be empty');

			if (Tools::getValue('display_time') == '') $error_txt[] = $this->l('display time field can not be empty');

			if (Tools::getValue('time_before_start') == '') $error_txt[] = $this->l('display time before start can not be empty');

			//cat,prod,cms,supplier,manufacturer
			if (Tools::getValue('display_catCheckbox_on') == 'on' &&
				Tools::getValue('display_cat') != '' && !$obj->isValideListId(Tools::getValue('display_cat')))
					$error_txt[] = $this->l('Add id category page like this 1,3,25 or leave this field empty');

			if (Tools::getValue('display_prodCheckbox_on') == 1 &&
				Tools::getValue('display_prod') != '' && !$obj->isValideListId(Tools::getValue('display_prod')))
					$error_txt[] = $this->l('Add id product page like this 1,3,25 or leave this field empty');

			if (Tools::getValue('display_cmsCheckbox_on') == 1 &&
				Tools::getValue('display_cms') != '' && !$obj->isValideListId(Tools::getValue('display_cms')))
					$error_txt[] = $this->l('Add id cms page like this 1,3,25 or leave this field empty');

			if (Tools::getValue('display_supplierCheckbox_on') == 1 &&
				Tools::getValue('display_supplier') != '' && !$obj->isValideListId(Tools::getValue('display_supplier')))
					$error_txt[] = $this->l('Add id supplier page like this 1,3,25 or leave this field empty');

			if (Tools::getValue('display_manufacturerCheckbox_on') == 1 &&
				Tools::getValue('display_manufacturer') != '' && !$obj->isValideListId(Tools::getValue('display_manufacturer')))
					$error_txt[] = $this->l('Add id supplier page like this 1,3,25 or leave this field empty');

			if (Tools::getValue('display_orderCheckbox_on') == 1 &&
				Tools::getValue('display_order') != '' && !$obj->isValideListId(Tools::getValue('display_order')))
					$error_txt[] = $this->l('Add step number of order page like this 1,2,3 or leave this field empty');


			if (count($error_txt)>0)
                            $this->errors = $error_txt;                        
                        else 
                            $this->object = $obj;
                        

			return parent::postProcess();
		}
		return parent::postProcess();
	}

	public function copyFromPost(&$object, $table)
	{
		parent::copyFromPost($object, $table);
		$object->copyFromPost();
	}

}
