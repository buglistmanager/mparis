/*
 * @category Prestashop
 * @category Module
 * @author Olivier CLEMENCE <manit4c@gmail.com>
 * @copyright  Op'art
 * @license Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
 **/
function opartPlannedPopupLoadContent(popId,contentId,contentType,rePosPopup) {
    $('#'+popId).append('<div id="opartplannedpopuploading_'+popId+'"><img src="' + baseDir + 'modules/opartplannedpopup/views/img/planned-loader.gif" alt="" /></div>').show('slow');
    $.ajax({
        type : 'POST',
        url : baseDir+'modules/opartplannedpopup/loadcontent.php',
        data: 'id='+contentId+'&type='+contentType+'&optoken='+optoken,
        success : function(data){
            $('#opartplannedpopuploading_'+popId).hide();
            $('#'+popId).append(data);
            if(rePosPopup) {
                parentId = $('#'+popId).parent().attr('id');
                //setOpartPlannedPopupPos(parentId);
                window.setTimeout(function() {setOpartPlannedPopupPos(parentId)}, 250);
            }
        }, error : function(XMLHttpRequest, textStatus, errorThrown) {
            $('#opartplannedpopuploading_'+popId).hide();
            $('#'+popId).append('Une erreur est survenue !');
        }
    });

}



function setOpartPlannedPopupPos(popId) {
    if($('#'+popId).hasClass('opartplannedpopup_is_percent')) {
        popWidth=Number($(window).width()*$('#'+popId).attr('initWidth')/100);
        popHeight=Number($(window).height()*$('#'+popId).attr('initHeight')/100);
        $('#' + popId).css({
            'width': popWidth+'px',
            'height': popHeight+'px'
        })
    }

    var popTop = ($(window).height()-$('#' + popId).outerHeight(true))/2+$(document).scrollTop();
    var popLeft = ($(window).width()-$('#' + popId).outerWidth(true))/2+$(document).scrollLeft();
    console.log('t='+popTop+' '+$('#' + popId).outerHeight(true)+' '+$(window).height()+' '+$(document).scrollTop());
    $('#' + popId).css({
        'top' : popTop+"px",
        'left' : popLeft+"px"
    });
}

function OpartPlannedPopupInsertParam(targetUrl,param){
    targetUrl += (targetUrl.split('?')[1] ? '&':'?') + param;
    return targetUrl;
}

function closeOpartPlannedPopup(id,redirectUrl,addCookie) {
    expireTime=$('#opartplannedpopup_'+id).attr('expiretime');
    opartPlannedPopupAddCookie(expireTime,id);
    $('#opartplannedpopup_fade_'+id+', #opartplannedpopup_'+id).fadeOut(function() {
        $('#opartplannedpopup_fade_'+id+', a.opartplannedpopup_close_'+id+', #opartplannedpopup_'+id).remove();
        delete opartPlannedPopupOpen[opartPlannedPopupOpen.indexOf('opartplannedpopup_'+id)];
    });
    $('#opartplannedpopup_'+id).html('');
    if(typeof redirectUrl !== 'undefined') {
        if(addCookie==1) {
            redirectUrl=OpartPlannedPopupInsertParam(redirectUrl,'addCookieTo='+id+'&addCookieToExpire='+expireTime);
            window.location.href = redirectUrl;
        }
        return false;
    }
}

function opartPlannedPopupAddCookie(expireTime,id,newsletter) {
    if($.cookie('opartplannedpopup_'+id) && $.cookie('opartplannedpopup_'+id)=='newsletter')
        return false;

    if(expireTime!=null) {
        cookieValue=(newsletter==true)?'newsletter':'hide';
        $.cookie('opartplannedpopup_'+id,cookieValue,{ expires: parseInt(expireTime), path: '/' });
    }
}

function opartPlannedPopupPostAjaxForm(event,idForm) {
    event.preventDefault();
    $( "#opartPlannedPopupFormResultErreur" ).hide("fast");
    $( "#opartPlannedPopupFormResultValid" ).hide("fast");
    url = $('#'+idForm).attr( "action" )+'?optoken='+optoken;
    postVal={};
    $('#'+idForm+' input').each(function() {
        postVal[this.name]=this.value;
    })
    $.post(url,postVal,function(data) {
        if(data['erreur']) {
            $( "#opartPlannedPopupFormResultErreur" ).empty().append(data['erreur']);
            $( "#opartPlannedPopupFormResultErreur" ).show("slow");
        }
        if(data['valid']) {
            $( "#opartPlannedPopupFormResultValid" ).empty().append(data['valid']);
            $( "#opartPlannedPopupFormResultValid" ).show("slow");
            var parentId=$('#'+idForm).closest('.opartplannedpopup_block').attr('id');
            id=parentId.substring(parentId.lastIndexOf("_")+1);
            expireTime=365//1 an
            opartPlannedPopupAddCookie(expireTime,id,true);
        }
    },'json');
}

opartPlannedPopupOpen=new Array;



$(window).resize(function() {
    for (var i = 0; i < opartPlannedPopupOpen.length; i++) {
        setOpartPlannedPopupPos(opartPlannedPopupOpen[i]);
    }
});

$(window).scroll(function() {
    for (var i = 0; i < opartPlannedPopupOpen.length; i++) {
        setOpartPlannedPopupPos(opartPlannedPopupOpen[i]);
    }
});

$(document).ready(function(){
    if(typeof addCookieTo!=='undefined')
        opartPlannedPopupAddCookie(addCookieToExpire,addCookieTo);

    var interval=new Array();
    for (var key in opartPlannedPopupArray) {
        //check cookie
        var cookieName=opartPlannedPopupArray[key].cookieName;
        var cookieValue=$.cookie(cookieName);
        if(!$.cookie(cookieName)) {
            interval[opartPlannedPopupArray[key].id] = setTimeout(
                (function(id) {
                    return function() {
                        launchPopup(id)
                    }
                })(opartPlannedPopupArray[key].id) ,opartPlannedPopupArray[key].time_before_start);
        }
    }
})

function launchPopup(popId) {
    var opartPlannedPopup=opartPlannedPopupArray[popId];
    if(opartPlannedPopup.use_newsletter_on!=null && opartPlannedPopup.use_newsletter_on==1) {
        var popupType="newsletter";
        var idContent=null;
    }
    else if(opartPlannedPopup.use_cms!='-1') {
        var popupType="cms";
        var idContent=opartPlannedPopup.use_cms;
    }
    else {
        var popupType="classic";
        var idContent=opartPlannedPopup.id;
    }
    var closeInterval=new Array();
    showOpartPlannedPopup(opartPlannedPopup.id,opartPlannedPopup.width,opartPlannedPopup.height,opartPlannedPopup.responsive,popupType,idContent,opartPlannedPopup.display_time,opartPlannedPopup.show_close_ico,opartPlannedPopup.close_out,opartPlannedPopup.css_class_name);
    if(opartPlannedPopup.close_time > 0) {
        closeInterval[opartPlannedPopup.id] = setTimeout(
            (function(id) {
                return function() {
                    closeOpartPlannedPopup(id)
                }
            })(opartPlannedPopup.id) ,opartPlannedPopup.close_time);
    }
}



function showOpartPlannedPopup(id,initWidth,initHeight,percent,contentType,contentId,expireTime,show_close_ico,close_out,css_class_name) {
    var popId="opartplannedpopup_"+id;
    var popContentId='opartplannedpopupcontent_'+id;
    var popFadeId='opartplannedpopup_fade_'+id;
    var popCloseId='opartplannedpopup_close_'+id;
    alert('test');
    $('body').append('<div id="'+popId+'" expiretime="'+expireTime+'"></div>');

    opartPlannedPopupOpen.push(popId);

    if(percent==1) {
        popWidth=window.innerWidth*initWidth/100;
        popHeight=window.innerHeight*initHeight/100;
        $('#' + popId).addClass("opartplannedpopup_is_percent");
    }
    else {
        popWidth=initWidth;
        popHeight=initHeight;
    }
    //if height is 0
    if(initHeight == 0)
        popHeight = 'auto';
    //show close ico
    closeIco=(show_close_ico==1)?'<a href="#" class="opartplannedpopup_close" id="'+popCloseId+'"><img src="' + baseDir + 'modules/opartplannedpopup/views/img/close_pop.png" class="btn_close" /></a>':'';

    //close on click out
    if(close_out==1) {
        $(document).on('click', '#'+popFadeId, function() {
            closeOpartPlannedPopup(id);
            return false;
        })
    }

    contentType=(contentType=="undefined")?null:contentType;
    $('#' + popId).addClass("opartplannedpopup_block");
    if(css_class_name!="")
        $('#' + popId).addClass(css_class_name);

    $('#' + popId).fadeIn().css({
        'width': Number(popWidth)+'px',
        //'height': Number(popHeight)+'px'
        'height': (initHeight == 0)?'auto':Number(popHeight)+'px'
    })
        .html(closeIco+'<div class="opartPlannedPopupContent" id="'+popContentId+'"></div>');

    $('#' + popId).attr({
        'initHeight':(initHeight == 0)?'auto':initHeight,
        'initWidth':initWidth
    })

    var popTop = ($(window).height()-$('#' + popId).outerHeight(true))/2+$(document).scrollTop();
    var popLeft = ($(window).width()-$('#' + popId).outerWidth(true))/2+$(document).scrollLeft();
    //console.log(popTop+' '+$('#' + popId).outerHeight(true));
    $('#' + popId).css({
        'top' : popTop+"px",
        'left' : popLeft+"px"
    });

    $('body').append('<div id="'+popFadeId+'" class="opartplannedpopup_fade"></div>');
    $('#'+popFadeId).css({'filter' : 'alpha(opacity=50)'}).fadeIn();

    $('body').append('');
    $('#'+popFadeId).css({'filter' : 'alpha(opacity=50)'}).fadeIn();

    var rePosPopup = (initHeight == 0);
    opartPlannedPopupLoadContent(popContentId,contentId,contentType,rePosPopup);

    return false;
}

$(document).on('click', 'img.btn_close', function(e) {
    var id=$(this).parent().parent().attr('id');
    id=id.substr(id.lastIndexOf('_')+1,id.length);
    closeOpartPlannedPopup(id);
    return false;
});