{**
* @category Prestashop
* @category Module
* @author Olivier CLEMENCE <manit4c@gmail.com>
* @copyright  Op'art
* @license Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
**}
<div id="newsletter_block_left" class="block">
	<p class="title_block">{l s='Newsletter' mod='opartplannedpopup'}</p>
	<div class="block_content">
	{if isset($msg) && $msg}
		<p class="{if $nw_error}warning_inline{else}success_inline{/if}">{$msg|escape:'html'}</p>
	{/if}
		<form action="{$base_dir_ssl|escape:'html'}modules/opartplannedpopup/loadcontent.php" method="post" id="opartPlannedPopupFormNewsletter">
			<p>
				<input class="inputNew" id="opartplannedpopup-newsletter-input" type="text" name="email" size="18" value="{if isset($value) && $value}{$value|escape:'html'}{else}{l s='your e-mail' mod='opartplannedpopup'}{/if}" />
				<input type="submit" value="ok" class="button_mini" name="submitNewsletter" />
				<input type="hidden" name="action" value="0" />
			</p>
		</form>
	</div>
	<div id="opartPlannedPopupFormResultValid" class="success" style="display:none;"></div>
	<div id="opartPlannedPopupFormResultErreur" class="warning" style="display:none;"></div>
</div>
<!-- /Block Newsletter module-->

<script type="text/javascript">
    var placeholder = "{l s='your e-mail' mod='opartplannedpopup' js=1}";
        $(document).ready(function() {ldelim}
            $('#opartplannedpopup-newsletter-input').on({ldelim}
                focus: function() {ldelim}
                    if ($(this).val() == placeholder) {ldelim}
                        $(this).val('');
                    {rdelim}
                {rdelim},
                blur: function() {ldelim}
                    if ($(this).val() == '') {ldelim}
                        $(this).val(placeholder);
                    {rdelim}
                {rdelim}
            {rdelim});

            {if isset($msg)}
                $('#columns').before('<div class="clearfix"></div><p class="{if $nw_error}warning{else}success{/if}">{l s='Newsletter:' js=1 mod='opartplannedpopup'} {$msg|escape:'html'}</p>');
            {/if}
        });

        $("#opartPlannedPopupFormNewsletter").submit(function(event) {
        	opartPlannedPopupPostAjaxForm(event,this.id);
        });
</script>
