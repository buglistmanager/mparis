{**
* @category Prestashop
* @category Module
* @author Olivier CLEMENCE <manit4c@gmail.com>
* @copyright  Op'art
* @license Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
**}
<!-- opartplannedpopup START -->
<script type="text/javascript">
opartPlannedPopupArray=[];
{foreach from=$popupList item=popup name=foo}	
	tempObject={
		'id':{$popup.id_opartplannedpopup|escape:"html"},
		'cookieName':'opartplannedpopup_{$popup.id_opartplannedpopup|escape:"html"}',
		'use_newsletter_on':{$popup.use_newsletter_on|escape:"html"},
		'use_cms':{$popup.use_cms|escape:"html"},
		'width':{$popup.width|escape:"html"},
		'height':{$popup.height|escape:"html"},
		/*'height':-1,*/
		'responsive':{$popup.responsive|escape:"html"},
		'display_time':{$popup.display_time|escape:"html"},
		'show_close_ico':{$popup.show_close_ico_on|escape:"html"},
		'close_out':{$popup.close_out_on|escape:"html"},
		'css_class_name':'{$popup.css_class_name|escape:"html"}',
		'time_before_start':{$popup.time_before_start|escape:"html"},
		'close_time':{$popup.close_time|escape:"html"}
	};
	opartPlannedPopupArray[{$popup.id_opartplannedpopup|escape:"html"}]=tempObject;
	{if isset($smarty.get.addCookieTo) && isset($smarty.get.addCookieToExpire)}
		addCookieTo={$smarty.get.addCookieTo|escape:"html"};
		addCookieToExpire={$smarty.get.addCookieToExpire|escape:"html"};
	{/if}
{/foreach}	
{if isset($base_dir)}
	var baseDir = '{$base_dir|escape:"html"}';
{/if}
</script>
<!-- opartplannedpopup END -->