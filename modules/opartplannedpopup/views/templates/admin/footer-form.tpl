{**
* @category Prestashop
* @category Module
* @author Olivier CLEMENCE <manit4c@gmail.com>
* @copyright  Op'art
* @license Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
**}
<script type="text/javascript">
//use_cms
$(document).ready(function(){
	//newsletter
	if($('#use_newsletter_on').attr('checked')) {
		$('.hideIfNewsletter').slideToggle('fast');
	}
	$('#use_newsletter_on').click(function() {
		if($(this).attr('checked')) {
			if($('#use_cmsCheckbox_on').attr('checked'))
				$('#use_cmsCheckbox_on').trigger( "click" );
			$('.hideIfNewsletter:visible').hide('fast');
		}
		else 
			$('.hideIfNewsletter').show('fast');		
	})
	
	//cms
	if($('#use_cmsCheckbox_on').attr('checked')) {
		$('.hideIfCms').slideToggle('fast');
	}
	$('#use_cmsCheckbox_on').click(function() {
		if($(this).attr('checked')) {
			$('.hideIfCms:visible').hide('fast');
			if($('#use_newsletter_on').attr('checked'))
				$('#use_newsletter_on').trigger( "click" );
		}
		else 
			$('.hideIfCms').show('fast');
	})
	
	
	$('.showTextField').click(function() {
		var targetClass=$(this).attr('id');
		targetClass=targetClass.replace("Checkbox_on","_text");
		$('.'+targetClass).toggle('fast');
		if(!$(this).attr('checked')) 
			$('#'+targetClass).val('-1');//set -1 for text field
		
	})
	
	//init display element
	$('.showTextField').each(function() {
		var targetId=$(this).attr('id');
		targetId=targetId.replace("Checkbox_on","_text");
		//console.log($('#'+targetId).val()+' '+targetId);
		if($('#'+targetId).val()!='-1') {
			//console.log($('#'+targetId).val());
			$(this).attr('checked',true);
		}
		else
			$('.'+targetId).css('display','none');
	})
	

	//display_all
	if($('#display_all_on').attr('checked')=='checked') {
		$('.hideIfAllPage').hide('fast');
	}
	$('#display_all_on').click(function() {
		$('.hideIfAllPage').toggle('fast');
		//console.log($('.hideIfAllPage > input:checkbox').attr('id'));
		$('.uncheckIfAll').each(function() {
			if($(this).attr('checked'))
				$(this).trigger( "click" );
		})
	})
})
</script>