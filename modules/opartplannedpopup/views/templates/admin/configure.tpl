{**
* @category Prestashop
* @category Module
* @author Olivier CLEMENCE <manit4c@gmail.com>
* @copyright  Op'art
* @license Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
**}
<fieldset>
<p>
{l s='no configure is needed for this module but you should read the documentation' mod='opartplannedpopup'} <a href="../modules/opartplannedpopup/readme_fr.pdf" target="blank">{l s='In french' mod='opartplannedpopup'}</a> {l s='or' mod='opartplannedpopup'} <a href="../modules/opartplannedpopup/readme_en.pdf" target="blank">{l s='in english' mod='opartplannedpopup'}</a> {l s='before use it' mod='opartplannedpopup'}.
</p>
</fieldset>