{**
* @category Prestashop
* @category Module
* @author Olivier CLEMENCE <manit4c@gmail.com>
* @copyright  Op'art
* @license Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
**}
{extends file="helpers/form/form.tpl"}
{if isset($isPs15)}
	{block name="label"}	
		{if isset($input.form_group_class)}
			<div class="{$input.form_group_class|escape:'html'}">
		{/if}
		{$smarty.block.parent}
	{/block}
	{block name="field"}
		{$smarty.block.parent}
		{if isset($input.form_group_class)}
	 		</div>
	 	{/if}
	{/block}
{/if}
{block name="input"}
	{if isset($input.descBefore)}
		<h3>{$input.descBefore|escape:'html'}</h3>
	{/if}
	{$smarty.block.parent}
{/block}