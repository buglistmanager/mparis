{**
* @category Prestashop
* @category Module
* @author Olivier CLEMENCE <manit4c@gmail.com>
* @copyright  Op'art
* @license Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
**}
<fieldset>
	<legend>{l s='Help' mod='opartplannedpopup'}</legend>
	<strong>{l s='If you require assistance, you can' mod='opartplannedpopup'}:</strong><br />
        <ul>
            <li>{l s='Read the documentation' mod='opartplannedpopup'}<a href="{$moduledir|escape:'html'}readme_fr.pdf" target="blank">{l s='In french' mod='opartplannedpopup'}</a> {l s='or' mod='opartplannedpopup'} <a href="{$moduledir|escape:'html'}readme_en.pdf" target="blank">{l s='in english' mod='opartplannedpopup'}</a></li>
            <li>
                {l s='Ask your question on the forum' mod='opartplannedpopup'}:
                <a href="http://www.prestashop.com/forums/topic/272165-module-planifiez-laffichage-de-popup/" target="blank">{l s='in french here' mod='opartplannedpopup'}</a>
                {l s='or' mod='opartplannedpopup'}
                <a href="http://www.prestashop.com/forums/topic/367548-module-schedule-the-popup-display/" target="blank">{l s='in english here' mod='opartplannedpopup'}</a>
            </li>
        </ul>
</fieldset>
{*** START HIDE FOR PS ***}
<fieldset>   
    <legend>{l s='Tutorial videos (in French)' mod='opartplannedpopup'}</legend>
    <div style="float:left; margin:0 30px 15px 0;">
        <strong>{l s='General demonstration' mod='opartplannedpopup'}</strong><br />
        <iframe width="560" height="315" src="//www.youtube.com/embed/yufGABkmU-g?list=UUEs2hXwx2jIb9j9PwBoI-lw" frameborder="0" allowfullscreen></iframe>
    </div>
    <div style="float:left; margin:0 30px 15px 0;">
        <strong>{l s='Newsletter integration' mod='opartplannedpopup'}</strong><br />
        <iframe width="560" height="315" src="//www.youtube.com/embed/n0y1TbH5Qio?list=UUEs2hXwx2jIb9j9PwBoI-lw" frameborder="0" allowfullscreen></iframe>
	</div>
    <div style="float:left; margin:0 30px 15px 0;">
        <strong>{l s='Display popup for unlogged or loged user only' mod='opartplannedpopup'}</strong><br />
        <iframe width="560" height="315" src="//www.youtube.com/embed/gwB46LRrvx8?list=UUEs2hXwx2jIb9j9PwBoI-lw" frameborder="0" allowfullscreen></iframe>
	</div>
</fieldset>

<fieldset>
	<legend>{l s='Stay in touch' mod='opartplannedpopup'}</legend>
	<table style="width:100%">
	<tr>
		<td style="width:25%; text-align:center;">
			<a style="text-decoration: none; display: inline-block; color: #333; text-align: center; font: 13px/16px arial,sans-serif; white-space: nowrap;" href="//plus.google.com/u/0/110689055013102717522?prsrc=3" target="_top" rel="publisher"> <img style="border: 0; width: 64px; height: 64px;" src="//ssl.gstatic.com/images/icons/gplus-64.png" alt="" /><br /> <span style="font-weight: bold;">Olivier Cl&eacute;mence <br /> D&eacute;veloppeur Prestashop</span><br />sur Google+ </a>
		</td>
		<td style="width:25%; text-align:center;">
			<div class="fb-like-box" data-href="https://www.facebook.com/opart.agence.web" data-width="202" data-height="102" data-colorscheme="light" data-show-faces="false" data-header="true" data-stream="false" data-show-border="true">&nbsp;</div>
			<div id="fb-root" style="display:inline-block">&nbsp;</div>
			{literal}
			<script>// <![CDATA[
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&appId=458318920918141&version=v2.0";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			// ]]></script>
		</td>
		<td style="width:25%; text-align:center;">
			<!--  badge twitter -->
			<a class="twitter-follow-button" href="https://twitter.com/maniT4c" data-show-count="false" data-lang="fr">Suivre @maniT4c</a>
		</td>
		<td style="width:25%; text-align:center;">
			{/literal}<a href="http://www.youtube.com/user/OlivierClemence" title="{l s='youtube channel' mod='opartplannedpopup'}" target="blank"><img src="{$moduledir|escape:'html'}views/img/youtube.png" alt="{l s='youtube channel' mod='opartplannedpopup'}" width="120" /></a>{literal}
		</td>
	</tr>
	</table>
	<script>// <![CDATA[
	!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
	// ]]></script>
	{/literal}<br />
</fieldset>
<br />
<fieldset>
	<legend>{l s='wan\'t more ?' mod='opartplannedpopup'}</legend>
	<p>{l s='You can also read my blog (in french)' mod='opartplannedpopup'}: <a href="http://www.blog.manit4c.com">blog.manit4c.com</a></p>
</fieldset>
{*** END HIDE FOR PS ***}