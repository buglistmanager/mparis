<?php
/**
 * Module opartplannedpopup
 *
 * @category Prestashop
 * @category Module
 * @author    Olivier CLEMENCE <manit4c@gmail.com>
 * @copyright Op'art
 * @license   Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
 */

/* Security */
if (!defined('_PS_VERSION_')) exit;

/* Loading Models */
require_once(_PS_MODULE_DIR_.'opartplannedpopup/models/PlannedPopup.php');

class Opartplannedpopup extends Module
{

	public function __construct()
	{
		$this->name = 'opartplannedpopup';
		$this->tab = 'front_office_features';
		$this->version = '1.3.8';

		$this->author = 'Op\'art - Olivier CLEMENCE';
		$this->need_instance = 0;
		$this->module_key="3ba43cf553901f39fc03bd467a170099";

		parent::__construct();

		$this->displayName = $this->l('Op\'art planned popup');
		$this->description = $this->l('Schedule when displayed alert popup');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall this module ?');

		if ($this->active)
		{
			if (Configuration::get('OPART_PLANNEDPOPUP_CONF') == '') $this->warning = $this->l('You have to configure your module');
			else
			{
				//check if popup exists
				$query = 'SELECT p.id_opartplannedpopup FROM '._DB_PREFIX_.'opartplannedpopup p';
				$popup_list = Db::getInstance()->ExecuteS($query);
				if (count($popup_list) <= 0)
				{
					$warning_text = 'You have not yet added any popup.';
					$warning_text .= ' If you do not use the OpartPlannedPopup module for the moment, it is best to disable it';
					$this->warning = $this->l($warning_text);
				}
			}
		}
	}

	public function install()
	{
		if (version_compare(_PS_VERSION_, '1.5.0', '<')) return false;

		// Install SQL
		$sql = $this->getInstallQuery();
		foreach ($sql as $s) if (!Db::getInstance()->execute($s)) return false;

		// Install Tabs
		$parent_tab = new Tab();
		$parent_tab->name = array();
		foreach (Language::getLanguages() as $language) $parent_tab->name[$language['id_lang']] = 'Op\'art planned popup';
		$parent_tab->class_name = 'AdminMainOpartplannedpopup';
		$parent_tab->id_parent = 0;
		$parent_tab->module = $this->name;
		$parent_tab->add();

		$tab1 = new Tab();
		$tab1->name = array();
		foreach (Language::getLanguages() as $language) $tab1->name[$language['id_lang']] = 'Popup';
		//$tab1->name = 'Popup';
		$tab1->class_name = 'AdminOpartplannedpopup';
		$tab1->id_parent = $parent_tab->id;
		$tab1->module = $this->name;
		$tab1->add();

		//Init
		Configuration::updateValue('OPART_PLANNEDPOPUP_CONF', 'ok');

		// Install Module
        if (_PS_VERSION_ >= '1.7') {
            /* Install Module version > 1.7 */
            if (parent::install() == false || ! $this->registerHook('displayHeader')) {
                return false;
            }
        } else {
            /* Install Module version < 1.7 */
            if (parent::install() == false || ! $this->registerHook('displayFooter')) {
                return false;
            }
        }
        return true;
	}

	public function uninstall()
	{
		// Uninstall SQL
		$sql = $this->getUninstallQuery();
		foreach ($sql as $s) if (!Db::getInstance()->execute($s)) return false;

		Configuration::deleteByName('OPART_PLANNEDPOPUP_CONF');

		// Uninstall Tabs
		$tab = new Tab((int)Tab::getIdFromClassName('AdminMainOpartplannedpopup'));
		$tab->delete();

		$tab = new Tab((int)Tab::getIdFromClassName('AdminOpartplannedpopup'));
		$tab->delete();

		// Uninstall Module
		if (!parent::uninstall()) return false;
		return true;
	}

	private function prepareHook()
	{
		$where = '';
		//display_home
		if (get_class($this->context->controller) == 'IndexController') $where = 'p.display_home_on=1';

		//display_cat
		if (get_class($this->context->controller) == 'CategoryController')
		{
			$current_id = (int)Tools::getValue('id_category');
			$where = 'p.display_cat="" OR (p.display_cat LIKE "'.$current_id.'" OR ';
			$where .= 'p.display_cat LIKE "'.$current_id.',%" OR p.display_cat LIKE "%,'.$current_id.',%" ';
			$where .= 'OR p.display_cat LIKE "%,'.$current_id.'")';
		}

		//display_prod
		if (get_class($this->context->controller) == 'ProductController')
		{
			$current_id = (int)Tools::getValue('id_product');
			$where = 'p.display_prod="" OR (p.display_prod LIKE "'.$current_id.'" OR ';
			$where .= 'p.display_prod LIKE "'.$current_id.',%" OR p.display_prod LIKE "%,'.$current_id.',%" ';
			$where .= 'OR p.display_prod LIKE "%,'.$current_id.'")';
		}

		//display_cms
		if (get_class($this->context->controller) == 'CmsController')
		{
			$current_id = (int)Tools::getValue('id_cms');
			$where = 'p.display_cms="" OR (p.display_cms LIKE "'.$current_id.'" ';
			$where .= 'OR p.display_cms LIKE "'.$current_id.',%" OR p.display_cms LIKE "%,'.$current_id.',%" OR ';
			$where .= 'p.display_cms LIKE "%,'.$current_id.'")';
		}

		//display_supplier
		if (get_class($this->context->controller) == 'SupplierController')
		{
			$current_id = (int)Tools::getValue('id_supplier');
			$where = 'p.display_supplier="" OR (p.display_supplier LIKE "'.$current_id.'" OR ';
			$where .= 'p.display_supplier LIKE "'.$current_id.',%" OR p.display_supplier LIKE "%,'.$current_id.',%" OR ';
			$where .= 'p.display_supplier LIKE "%,'.$current_id.'")';
		}

		//display_manufacturer
		if (get_class($this->context->controller) == 'ManufacturerController')
		{
			$current_id = (int)Tools::getValue('id_manufacturer');
			$where = 'p.display_manufacturer="" OR (p.display_manufacturer LIKE "'.$current_id.'" OR ';
			$where .= 'p.display_manufacturer LIKE "'.$current_id.',%" OR p.display_manufacturer LIKE "%,'.$current_id.',%" OR ';
			$where .= 'p.display_manufacturer LIKE "%,'.$current_id.'")';
		}

        if (get_class($this->context->controller) == 'CartController')
        {
            $current_step = (int)'0';
            $where = 'p.display_order="" OR (p.display_order LIKE "'.$current_step.'" OR ';
            $where .= 'p.display_order LIKE "'.$current_step.',%" OR p.display_order LIKE "%,'.$current_step.',%" OR ';
            $where .= 'p.display_order LIKE "%,'.$current_step.'")';
        }

		//order
		if (get_class($this->context->controller) == 'OrderController')
		{
            if (Tools::getIsset('step')) {
                $current_step = Tools::getValue('step');
            } else {
                $current_step = (int)'1';
            }
			$where = 'p.display_order="" OR (p.display_order LIKE "'.$current_step.'" OR ';
			$where .= 'p.display_order LIKE "'.$current_step.',%" OR p.display_order LIKE "%,'.$current_step.',%" OR ';
			$where .= 'p.display_order LIKE "%,'.$current_step.'")';
		}

		//custom_url
		$current_url = Tools::strtolower($_SERVER['REQUEST_URI']);
		$join_custom_url = ' OR (pl.custom_url LIKE "'.pSQL($current_url).'" OR ';
		$join_custom_url .= 'pl.custom_url LIKE "'.pSQL($current_url).',%" OR pl.custom_url LIKE "%,'.pSQL($current_url).',%" OR ';
		$join_custom_url .= 'pl.custom_url LIKE "%,'.pSQL($current_url).'")';

		//display_all
		$where .= ($where != '') ? ' OR p.display_all_on=1' : 'p.display_all_on=1';

		//close where
		$where = ' AND ('.$where.$join_custom_url.')';

		//get date
		$date_now = date('Y-m-d h:i:s');

		//end query
		$query = 'SELECT p.id_opartplannedpopup,p.use_newsletter_on,p.use_cms,p.width,p.height,p.responsive,p.start_date,p.end_date,';
		$query .= 'p.display_time,p.time_before_start,p.close_time,p.display_all_on,p.show_close_ico_on,p.close_out_on,p.css_class_name,p.display_for,pl.exclude_url ';
		$query .= 'FROM '._DB_PREFIX_.'opartplannedpopup p ';
		$query .= 'LEFT JOIN '._DB_PREFIX_.'opartplannedpopup_lang pl ON (p.id_opartplannedpopup=pl.id_opartplannedpopup) WHERE ';
		$query .= 'pl.id_lang = '.(int)$this->context->language->id.' AND p.start_date<="'.pSQL($date_now).'" AND ';
		$query .= 'p.end_date>="'.pSQL($date_now).'" '.$where.' GROUP BY p.id_opartplannedpopup ORDER BY p.id_opartplannedpopup';

		$popup_list = Db::getInstance()->ExecuteS($query);
		//on verifie si le popup est de type newsletter et si oui on verifie si l'user est deja newsletterRegistered
		$current_user = $this->context->customer;
		$popup_list_count = count($popup_list);
		if ($current_user->isLogged() && $current_user->newsletter == 1)
                    for ($i = 0; $i < $popup_list_count; $i++)
			if ($popup_list[$i]['use_newsletter_on'] == 1) unset($popup_list[$i]);

		for ($i = 0; $i < $popup_list_count; $i++)
		{
			if ($popup_list[$i]['display_for'] == 1 && !$current_user->isLogged()) unset($popup_list[$i]);
			if (isset($popup_list[$i]) && $popup_list[$i]['display_for'] == 2 && $current_user->isLogged()) unset($popup_list[$i]);
		}
		//url a exclure
		$parsed_url = parse_url($current_url);
		foreach ($popup_list as $key => $popup)
		{
			$exclude_url = explode(',', $popup['exclude_url']);
			if (count($exclude_url) > 0 && in_array($parsed_url['path'], $exclude_url)) unset($popup_list[$key]);
		}
		return $popup_list;
	}

        
    public function getBaseDir() {
	$useSSL = ((isset($this->ssl) && $this->ssl && Configuration::get('PS_SSL_ENABLED')) || Tools::usingSecureMode()) ? true : false;
        $protocol_content = ($useSSL) ? 'https://' : 'http://';
	return $baseDir=$protocol_content.Tools::getHttpHost().__PS_BASE_URI__;
    }
    
    /**
     * Register module scripts and styles for PS_VERSION > 1.7
     */
    public function hookdisplayHeader()
    {

        $token = Tools::getToken(_PS_MODULE_DIR_."opartplannedpopup/loadcontent.php");
        Media::addJsDef(array("optoken" => $token));
        $popup_list = $this->prepareHook();
        if ($popup_list == false || !count($popup_list) > 0) return false;
        $this->smarty->assign(array('popupList' => $popup_list));
        $this->smarty->assign(array('base_dir' => $this->getBaseDir()));
        //Register Module script
        $this->context->controller->registerJavascript(
            'modules-opartplannedpopup-js',
            'modules/'.$this->name.'/views/js/script17.js'
        );

        $this->context->controller->registerJavascript(
            'modules-opartplannedpopup-cooki',
            'js/jquery/plugins/jquery.cooki-plugin.js'
        );

        //Register Module style
        $this->context->controller->registerStylesheet(
            'modules-opartplannedpopup-css',
            'modules/'.$this->name.'/views/css/style.css'
        ); 

        return $this->display(__FILE__, '/views/templates/front/popup.tpl');
    }

	public function hookDisplayFooter()
	{
        $token = Tools::getToken(_PS_MODULE_DIR_."opartplannedpopup/loadcontent.php");
        Media::addJsDef(array("optoken" => $token));
		$popup_list = $this->prepareHook();
                $this->smarty->assign(array('base_dir' => $this->getBaseDir()));
		if ($popup_list == false || !count($popup_list) > 0) return false;
		$this->smarty->assign(array('popupList' => $popup_list));
		$this->context->controller->addJS(_PS_JS_DIR_.'jquery/jquery-1.7.2.min.js');
		$this->context->controller->addJS($this->_path.'views/js/script.js');
		if (version_compare(_PS_VERSION_, '1.5.6.1', '>='))
				$this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/jquery.cooki-plugin.js');
		else $this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/jquery.cookie-plugin.js');
		$this->context->controller->addCSS($this->_path.'views/css/style.css');
		return $this->display(__FILE__, '/views/templates/front/popup.tpl');
	}

	public function getContent()
	{
		$this->_html = $this->display(__FILE__, 'views/templates/admin/configure.tpl');
		return $this->_html;
	}

	private function getInstallQuery()
	{
		$sql = array();

		// Create Table slideshow
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'opartplannedpopup` (
		  `id_opartplannedpopup` int(10) NOT NULL AUTO_INCREMENT,
		  `title` varchar(128) NOT NULL,
		  `use_newsletter_on` int(1),
		  `use_cms` int(4),	  
		  `width` int(4),
		  `height` int(4),
		  `responsive` int(1),
		  `start_date` datetime,
		  `end_date` datetime,
		  `display_time` int(4),
		  `time_before_start` int(7),
		  `close_time` int(7),
		  `display_all_on` int(1),
		  `display_home_on` int(1),
		  `display_cat` longtext,
		  `display_prod` longtext,
		  `display_cms` longtext,
		  `display_supplier` longtext,
		  `display_manufacturer` longtext,
		  `display_order` longtext,
		  `display_for` int(1),
		  `show_close_ico_on` int(4),	
		  `close_out_on` int(4),
		  `css_class_name` varchar(128),	
  		PRIMARY KEY (`id_opartplannedpopup`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

		// Create Table slideshow_lang
		$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'opartplannedpopup_lang` (
		  `id_opartplannedpopup` int(10) NOT NULL AUTO_INCREMENT,
		  `id_lang` int(10) NOT NULL,
		  `code` longtext,
		  `custom_url` longtext,
		  `exclude_url` longtext,
  		UNIQUE KEY `opartplannedpopup_lang_index` (`id_opartplannedpopup`,`id_lang`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';

		return $sql;
	}

	private function getUninstallQuery()
	{
		$sql = array();
		$sql[] = 'SET foreign_key_checks = 0;';
		$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'opartplannedpopup`;';
		$sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'opartplannedpopup_lang`;';
		$sql[] = 'SET foreign_key_checks = 1;';
		return $sql;
	}

}
