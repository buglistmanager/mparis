<?php
/**
 * Module opartplannedpopup
 *
 * @category Prestashop
 * @category Module
 * @author    Olivier CLEMENCE <manit4c@gmail.com>
 * @copyright Op'art
 * @license   Tous droits réservés / Le droit d'auteur s'applique (All rights reserved / French copyright law applies)
 */

class PlannedPopup extends ObjectModel
{

	/** @var string Name */
	public $title;
	public $use_newsletter_on;
	public $use_cms;
	public $width;
	public $height;
	public $responsive;
	public $start_date;
	public $end_date;
	public $display_time;   
	public $time_before_start;
	public $close_time;
	public $display_all_on;
	public $display_home_on;
	public $display_cat;
	public $display_prod;
	public $display_cms;
	public $display_supplier;
	public $display_manufacturer;
	public $display_order;
	public $display_for;
	public $show_close_ico_on;
	public $close_out_on;
	public $css_class_name;
	public $code;
	public $custom_url;
	public $exclude_url;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'opartplannedpopup',
		'primary' => 'id_opartplannedpopup',
		'multilang' => true,
		'fields' => array(
			'title' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
			'use_newsletter_on' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => false),
			'use_cms' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => false),
			'width' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
			'height' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
			'responsive' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => true),
			'start_date' => array('type' => self::TYPE_DATE, 'valide' => 'isDate', 'required' => true),
			'end_date' => array('type' => self::TYPE_DATE, 'valide' => 'isDate', 'required' => true),
			'display_time' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => true),
			'time_before_start' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => true),
			'close_time' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => true),
			'display_all_on' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => false),
			'display_home_on' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => false),
			'display_cat' => array('type' => self::TYPE_STRING, 'valide' => 'isString', 'required' => false),
			'display_prod' => array('type' => self::TYPE_STRING, 'valide' => 'isString', 'required' => false),
			'display_cms' => array('type' => self::TYPE_STRING, 'valide' => 'isString', 'required' => false),
			'display_supplier' => array('type' => self::TYPE_STRING, 'valide' => 'isString', 'required' => false),
			'display_manufacturer' => array('type' => self::TYPE_STRING, 'valide' => 'isString', 'required' => false),
			'display_order' => array('type' => self::TYPE_STRING, 'valide' => 'isString', 'required' => false),
			'display_for' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => true),
			'show_close_ico_on' => array('type' => self::TYPE_BOOL, 'valide' => 'isInt', 'required' => false),
			'close_out_on' => array('type' => self::TYPE_INT, 'valide' => 'isInt', 'required' => false),
			'css_class_name' => array('type' => self::TYPE_STRING, 'valide' => 'isString', 'required' => false),
			/* Lang fields */
			'code' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999, 'required' => false),
			'custom_url' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999, 'required' => false),
			'exclude_url' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999, 'required' => false)
		),
	);

	/* custom validator */
	public function isValideListId($value)
	{
		if ($value == '') return true;
		$pattern = '/^[0-9]+(,[0-9]+)*$/';
		if (preg_match($pattern, $value) == 1) return true;

		return false;
	}

	public function copyFromPost()
	{
		//p($_POST);
		foreach ($_POST as $key => $value)
		{
			if (key_exists($key, $this) && $key != 'id_'.$this->table)
				$this->{$key} = $value;
		}

		//use_newsletter
		$this->use_newsletter_on = (key_exists('use_newsletter_on', $_POST)) ? 1 : 0;
		//display_all
		$this->display_all_on = (key_exists('display_all_on', $_POST)) ? 1 : 0;
		//display_home
		$this->display_home_on = (key_exists('display_home_on', $_POST)) ? 1 : 0;
		//display_cat
		if (!key_exists('display_cat', $_POST)) $this->display_cat = '-1';
		//display_prod
		if (!key_exists('display_prod', $_POST)) $this->display_prod = '-1';
		//display_cms
		if (!key_exists('display_cms', $_POST)) $this->display_cms = '-1';
		//display_supplier
		if (!key_exists('display_supplier', $_POST)) $this->display_supplier = '-1';
		//display_manufacturer
		if (!key_exists('display_manufacturer', $_POST)) $this->display_manufacturer = '-1';
		//display_manufacturer
		if (!key_exists('display_order', $_POST)) $this->display_order = '-1';
		//use_cms
		if (!key_exists('use_cms', $_POST)) $this->use_cms = '-1';
		//show_close_ico
		$this->show_close_ico_on = (key_exists('show_close_ico_on', $_POST)) ? 1 : 0;
		//close_out
		$this->close_out_on = (key_exists('close_out_on', $_POST)) ? 1 : 0;

		/* Multilingual fields */
		if (count($this->fieldsValidateLang))
		{
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
					foreach (array_keys($this->fieldsValidateLang) as $field)
						if (Tools::getIsset($field.'_'.(int)$language['id_lang']))
							$this->{$field}[(int)$language['id_lang']] = Tools::getValue($field.'_'.(int)$language['id_lang']);
		}
	}

}
