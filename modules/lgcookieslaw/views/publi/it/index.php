		<table id="banner-lg" class="panel">
			<tr>
				<td id="guia">
					<a href="../modules/lgcookieslaw/readme/readme_it.pdf" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/help.png"/>
				</td>
				<td id="soporte">
					<a href="https://addons.prestashop.com/it/write-to-developper?id_product=8734" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/support.png"/>
				</td>
				<td id="video">
					<a href="https://addons.prestashop.com/it/product.php?id_product=8734" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/video.png"/>
				</td>
				<td id="opinion">
					<a href="http://addons.prestashop.com/it/ratings.php" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/rateus.png"/>
				</td>
				<td id="titulo">MODULI PRESTASHOP</BR>CHE TI POSSONO INTERESSARE...</td>
				<td class="logos">
					<a href="http://addons.prestashop.com/en/seo-prestashop-modules/11399-301-302-303-url-redirects-seo.html" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/redirect.jpg"/></td>
				<td class="modulos">
					<a href="http://addons.prestashop.com/en/seo-prestashop-modules/11399-301-302-303-url-redirects-seo.html" target="_blank"><span class="enlaceverde">Modulo</span></BR><span class="enlacenegro">REINDIRIZZAMENTI 301, 302, 303</span><a/>
				</td>
				<td class="logos">
					<a href="http://addons.prestashop.com/en/seo-prestashop-modules/7507-multilingual-and-multistore-sitemap-generator-seo.html" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/sitemap.jpg"/></td>
				<td class="modulos">
					<a href="http://addons.prestashop.com/en/seo-prestashop-modules/7507-multilingual-and-multistore-sitemap-generator-seo.html" target="_blank"><span class="enlaceverde">Modulo</span></BR><span class="enlacenegro">SITEMAP MULTILINGUE</span><a/>
				</td>
				<td class="logos">
					<a href="http://addons.prestashop.com/en/bulk-update-prestashop-modules/18065-fast-access-to-order-details-quick-view-overview.html" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/order.jpg"/>
				<td class="modulos">
					<a href="http://addons.prestashop.com/en/bulk-update-prestashop-modules/18065-fast-access-to-order-details-quick-view-overview.html" target="_blank"><span class="enlaceverde">Modulo</span></BR><span class="enlacenegro">ACCESSO RAPIDO ORDINI</span><a/>
				</td>
				<td id="boton">
					<a href="http://addons.prestashop.com/it/22_linea-grafica" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/all.png"/>
				</td>
				<td id="logo">
					<a href="http://addons.prestashop.com/it/22_linea-grafica" target="_blank"/>
					<img src="/modules/lgcookieslaw/views/img/publi/it/img/logo.jpg"/>
				</td>
			</tr>
		</table>