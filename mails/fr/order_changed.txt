
[{shop_url}] 

Bonjour {firstname} {lastname}, 

Votre commande sur {shop_name} ayant pour référence {order_name}
a été modifiée par le marchand. 

Vous pouvez accéder à tout moment au suivi de votre commande dans la rubrique "Mon compte"
[{my_account_url}] sur notre site. 


 

