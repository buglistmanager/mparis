
[{shop_url}] 

Bonjour {firstname} {lastname}, 

Nous avons procédé au remboursement de votre commande sur
{shop_name} portant la référence {order_name}. 

Vous pouvez accéder à tout moment au suivi de votre commande dans la rubrique "Mon compte"
[{my_account_url}] sur notre site. 


